# # ex-erebus
# # Refresh map_cached_results table every 15 minutes
#   */15 * * * * 	php /srv/www/jobungouk/current/bin/cache_map_searches.php

# # Update empty domain and partner in bounce and complaint every 15 min NOT MOVED To AWS (PS)
# #  */15 * * * * 	php /srv/jobungouk/bin/update_bacon.php

# # Scrub user data against DNM lists every 15 min
#   */15 * * * * 	php /srv/www/jobungouk/current/bin/scrub_dnm_list.php

# # Remove older than 90 days records in clickers90 table for JUK (Daily 1AM UK, 2AM NL)
#   0 2 * * *  	php /srv/www/jobungouk/current/bin/clean_clickers90.php

#  # Coreg Injection (07:00AM CET 2012-12-14 | 10,000) NOT MOVED TO AWS (PS)
# #0       7       14 12 *  php /srv/jobungou/bin/coreg_injection.php


# ##################################################################

# # ex-wilson
# # NOTE: Any specific hour needs to be adjusted to UK timezone
# # Add 1 hour to UK time. e.g. 1AM UK = 2AM NL

# # Daily aggregation of versionator_stats unique visitors stats
# # (12:30AM UK / 1:30AM NL)
# 30 1 * * * /srv/www/jobungouk/current/bin/aggregate_search_unique_ip.php

# # Daily truncating of previous day's partner source stats unique views
# # and clicks data in partner_source_search and partner_source_click
# # (12:40AM UK / 1:40AM NL)
# 40 1 * * * /srv/www/jobungouk/current/bin/truncate_partner_source_uniques.php

# # Daily aggregation of daily coreg welcome partner performance (12:05 UK | 1:05 NL)
# 05 1 * * * /srv/www/jobungouk/current/bin/agg_coreg_welperf_partner.php

# # Daily aggregation of daily coreg welcome partner/source performance (12:10 UK | 1:10 NL)
# 10 1 * * * /srv/www/jobungouk/current/bin/agg_coreg_welperf_source.php

# # Daily aggregation of daily coreg welcome domain (12:15 UK | 1:15 NL)
# 15 1 * * * /srv/www/jobungouk/current/bin/agg_coreg_weldomain.php

# # Replicate global removal list to bacon UK. NOT NEEDED. NOT MOVED To AWS (PS)
# # Dashboard global removal upload posts to UK bacon.
# #*/15 * * * * /home/suy/bin/replicate_global_removal.php

# # Backup JUK DB to restdev. NOT MOVED TO AWS (PS)
# # -suy, 20130703
# 5 0 * * * /home/suy/bin/backup_db.sh

# ############## Campaign Prep ############

# # Daily digest - daily prep starts at (12:30AM UK / 1:30AM NL),
# # Sends at (6:30AM UK / 7:30AM NL).
# 30 1 * * * /srv/www/jobungouk/current/bin/daily_digest_v25.php

# # Welcome reminder 1 - daily prep starts at (1AM UK / 2AM NL),
# # Sends at (5AM UK / 6AM NL).
# #0 2 * * * /srv/www/jobungouk/current/bin/welcome_reminder_1_v25.php
# 0 2 * * * /srv/www/jobungouk/current/bin/welcome_reminder_v25.php -d 1

# # Welcome reminder 2 - daily prep starts at (1:15AM UK / 2:15AM NL),
# # Sends at (5:20AM UK / 6:20AM NL).
# #15 2 * * * /srv/www/jobungouk/current/bin/welcome_reminder_2_v25.php
# 15 2 * * * /srv/www/jobungouk/current/bin/welcome_reminder_v25.php -d 2

# # Welcome reminder 3 - daily prep starts at (1:30AM UK / 2:30AM NL),
# # Sends at (5:40AM UK / 6:40AM NL).
# #30 2 * * * /srv/www/jobungouk/current/bin/welcome_reminder_3_v25.php
# 30 2 * * * /srv/www/jobungouk/current/bin/welcome_reminder_v25.php -d 3

# # Welcome reminder 4 - daily prep starts at (1:45AM UK / 2:45AM NL),
# # Sends at (6AM UK / 7AM NL).
# #45 2 * * * /srv/www/jobungouk/current/bin/welcome_reminder_4_v25.php
# 45 2 * * * /srv/www/jobungouk/current/bin/welcome_reminder_v25.php -d 4

# # Welcome reminder 5 - daily prep starts at (2AM UK / 3AM NL),
# # Sends at (6:20AM UK / 7:20AM NL).
# #0 3 * * * /srv/www/jobungouk/current/bin/welcome_reminder_5_v25.php
# 0 3 * * * /srv/www/jobungouk/current/bin/welcome_reminder_v25.php -d 5

# # No location welcome reminder 1 - daily prep starts at (2:15AM UK / 3:15AM NL),
# # Sends at (5:10AM UK / 6:10AM NL).
# 15 3 * * * /srv/www/jobungouk/current/bin/welcome_noloc_reminder_v25.php -d 1

# # No location welcome reminder 2 - daily prep starts at (2:30AM UK / 3:30AM NL),
# # Sends at (5:30AM UK / 6:30AM NL).
# 30 3 * * * /srv/www/jobungouk/current/bin/welcome_noloc_reminder_v25.php -d 2

# # No location welcome reminder 3 - daily prep starts at (2:45AM UK / 3:45AM NL),
# # Sends at (5:50AM UK / 6:50AM NL).
# 45 3 * * * /srv/www/jobungouk/current/bin/welcome_noloc_reminder_v25.php -d 3

# # No location welcome reminder 4 - daily prep starts at (3AM UK / 4AM NL),
# # Sends at (6:10AM UK / 7:10AM NL).
# 0 4 * * * /srv/www/jobungouk/current/bin/welcome_noloc_reminder_v25.php -d 4

# # No location welcome reminder 5 - daily prep starts at (3:15AM UK / 4:15AM NL),
# # Sends at (6:30AM UK / 7:30AM NL).
# 15 4 * * * /srv/www/jobungouk/current/bin/welcome_noloc_reminder_v25.php -d 5

# # Make weekly recommended jobs csv lists (Every Mon 5AM UK / 6AM NL)
# 0 6 * * 1 /srv/www/jobungouk/current/bin/recmd_jobs_makelist.php

# # Recommended jobs CL60 Full (Every Mon, Prep 1PM UK / 2PM NL, Send 5PM UK / 6PM NL)
# 0 14 * * 1 /srv/www/jobungouk/current/bin/recmd_jobs_send.php -f

# # Recommended jobs CL60 partial (Every Tue - Fri, Prep 1PM UK / 2PM NL, Send 5PM UK / 6PM NL)
# 0 14 * * 2-5 /srv/www/jobungouk/current/bin/recmd_jobs_send.php

# # Make weekly recommended jobs >60 csv list (Every Mon 6AM UK / 7AM NL)
# 0 7 * * 1 /srv/www/jobungouk/current/bin/recmd_jobs_61_makelist.php

# # Recommended jobs >60 (Every Wed, Prep 2PM UK / 3PM NL, Send 5:30PM UK / 6:30PM NL)
# 0 15 * * 3 /srv/www/jobungouk/current/bin/recmd_jobs_61_send.php