-- Read-write user
CREATE USER 'juk_rw'@'%'
IDENTIFIED BY 'truecrimeisneverpunished';
GRANT USAGE
ON *.*
TO 'juk_rw'@'%'
WITH MAX_QUERIES_PER_HOUR 0
    MAX_CONNECTIONS_PER_HOUR 0
    MAX_UPDATES_PER_HOUR 0
    MAX_USER_CONNECTIONS 0;
GRANT ALL PRIVILEGES
ON `jobungouk`.*
TO 'juk_rw'@'%';
GRANT FILE
ON *.*
TO 'juk_rw'@'%';

-- Read-only user
CREATE USER 'juk_ro'@'%'
IDENTIFIED BY 'onlytherighteousmayjudge';
GRANT USAGE
ON *.*
TO 'juk_ro'@'%'
WITH MAX_QUERIES_PER_HOUR 0
    MAX_CONNECTIONS_PER_HOUR 0
    MAX_UPDATES_PER_HOUR 0
    MAX_USER_CONNECTIONS 0;
GRANT SELECT
ON `juk`.*
TO 'juk_ro'@'%';

-- We're done!
FLUSH PRIVILEGES;

