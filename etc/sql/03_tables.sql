-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 27, 2013 at 08:35 PM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `jobungouk`
--

-- --------------------------------------------------------

--
-- Table structure for table `accept_cookie`
--

CREATE TABLE IF NOT EXISTS `accept_cookie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `ip` varchar(16) NOT NULL,
  `device` varchar(32) NOT NULL,
  `browser` varchar(32) NOT NULL,
  `browser_version` varchar(32) NOT NULL,
  `user_agent` text NOT NULL,
  `is_mobile` tinyint(1) NOT NULL,
  `date_accepted` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `agg_injection_performance`
--

CREATE TABLE IF NOT EXISTS `agg_injection_performance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_injected` date NOT NULL,
  `injected` int(11) NOT NULL,
  `accepted` int(11) NOT NULL,
  `rejected` int(11) NOT NULL,
  `mailable` int(11) NOT NULL,
  `unsent` int(11) NOT NULL,
  `sent` int(11) NOT NULL,
  `bounced` int(11) NOT NULL,
  `opened` int(11) NOT NULL,
  `clicked` int(11) NOT NULL,
  `complained` int(11) NOT NULL,
  `unsubscribed` int(11) NOT NULL,
  `job_searched` int(11) NOT NULL,
  `job_clicked` int(11) NOT NULL,
  `valid_kw` int(11) NOT NULL,
  `date_updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cached_map_results`
--

CREATE TABLE IF NOT EXISTS `cached_map_results` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(3) NOT NULL,
  `zip` varchar(20) NOT NULL,
  `longitude` float NOT NULL,
  `latitude` float NOT NULL,
  `location` varchar(255) NOT NULL,
  `extra` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `latitude` (`latitude`),
  KEY `longitude` (`longitude`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `clickers90`
--

CREATE TABLE IF NOT EXISTS `clickers90` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `first_name` varchar(64) NOT NULL,
  `city` varchar(64) NOT NULL,
  `postal_code` varchar(16) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `is_keyword_valid` tinyint(1) NOT NULL,
  `partner` varchar(64) NOT NULL,
  `source` varchar(255) NOT NULL,
  `date_opt_in` datetime NOT NULL,
  `opt_in_ip` varchar(16) NOT NULL,
  `date_updated` date NOT NULL,
  `datetime_updated` datetime NOT NULL,
  `date_added` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `date_updated` (`date_updated`),
  KEY `user_id` (`user_id`),
  KEY `partner_source` (`partner`,`source`),
  KEY `datetime_updated` (`datetime_updated`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `coreg`
--

CREATE TABLE IF NOT EXISTS `coreg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `email_domain` varchar(255) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `city` varchar(64) NOT NULL,
  `postal_code` varchar(16) NOT NULL,
  `partner` varchar(64) NOT NULL,
  `source` varchar(128) NOT NULL,
  `subcode` varchar(32) NOT NULL,
  `sub_id` varchar(255) DEFAULT NULL,
  `opt_in_ip` varchar(16) NOT NULL,
  `date_opt_in` datetime NOT NULL,
  `accepted` tinyint(1) NOT NULL DEFAULT '1',
  `reject_reason_id` int(11) NOT NULL,
  `rejection_reason` varchar(255) DEFAULT NULL,
  `is_mailable` tinyint(1) NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `email` (`email`),
  KEY `partner_source` (`partner`,`source`),
  KEY `date_created` (`date_created`),
  KEY `email_domain` (`email_domain`),
  KEY `reject_reason_id` (`reject_reason_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `coreg_partner`
--

CREATE TABLE IF NOT EXISTS `coreg_partner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `partner` varchar(16) NOT NULL,
  `source` varchar(128) NOT NULL,
  `subcode` varchar(32) NOT NULL,
  `status` varchar(16) NOT NULL DEFAULT 'active',
  `forward_to_leads` tinyint(1) NOT NULL DEFAULT '0',
  `is_paid` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `partner_source_subcode` (`partner`,`source`,`subcode`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `coregInjection`
--

CREATE TABLE IF NOT EXISTS `coregInjection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `listName` varchar(128) NOT NULL,
  `userId` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `partner` varchar(64) NOT NULL,
  `source` varchar(128) NOT NULL,
  `accepted` tinyint(1) NOT NULL DEFAULT '0',
  `mailable` tinyint(1) NOT NULL DEFAULT '0',
  `rejectId` tinyint(1) NOT NULL DEFAULT '0',
  `dateInjected` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `email` (`email`),
  KEY `dateInjected` (`dateInjected`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `coregDaily`
--

CREATE TABLE IF NOT EXISTS `coregPivotDaily` (
  `Received` int(11) NOT NULL DEFAULT '0',
  `Accepted` int(11) NOT NULL DEFAULT '0',
  `Rejected` int(11) NOT NULL DEFAULT '0',
  `NoLoc` int(11) NOT NULL DEFAULT '0',
  `NoLocRecovered` int(11) NOT NULL DEFAULT '0',
  `Mailable` int(11) NOT NULL DEFAULT '0',
  `Invalid` int(11) NOT NULL DEFAULT '0',
  `Dnm` int(11) NOT NULL DEFAULT '0',
  `Within7` int(11) NOT NULL DEFAULT '0',
  `Within60` int(11) NOT NULL DEFAULT '0',
  `Date` date NOT NULL,
  `Updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`Date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `coregPartner`
--

CREATE TABLE `coregPivotPartner` (
  `Partner` varchar(64) NOT NULL,
  `Source` varchar(255) NOT NULL,
  `Received` int(11) NOT NULL DEFAULT '0',
  `Accepted` int(11) NOT NULL DEFAULT '0',
  `Rejected` int(11) NOT NULL DEFAULT '0',
  `NoLoc` int(11) NOT NULL DEFAULT '0',
  `NoLocRecovered` int(11) NOT NULL DEFAULT '0',
  `Mailable` int(11) NOT NULL DEFAULT '0',
  `Invalid` int(11) NOT NULL DEFAULT '0',
  `Dnm` int(11) NOT NULL DEFAULT '0',
  `Within7` int(11) NOT NULL DEFAULT '0',
  `Within60` int(11) NOT NULL DEFAULT '0',
  `Date` date NOT NULL,
  `Updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`Date`,`Partner`,`Source`),
  KEY `dt` (`Date`),
  KEY `p` (`Partner`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

-- --------------------------------------------------------

--
-- Table structure for table `coregPivotWelDomain`
--

CREATE TABLE `coregPivotWelDomain` (
  `WelcomeType` tinyint(4) NOT NULL,
  `Date` date NOT NULL,
  `Domain` varchar(255) NOT NULL,
  `Sent` int(11) NOT NULL DEFAULT '0',
  `Clicks` int(11) NOT NULL DEFAULT '0',
  `Opens` int(11) NOT NULL DEFAULT '0',
  `Bounced` int(11) NOT NULL DEFAULT '0',
  `Complaints` int(11) NOT NULL DEFAULT '0',
  `Unsubscribed` int(11) NOT NULL DEFAULT '0',
  `Updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`Date`,`Domain`,`WelcomeType`),
  KEY `dt` (`Date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

-- --------------------------------------------------------

--
-- Table structure for table `coregPivotWelPerf`
--

CREATE TABLE `coregPivotWelPerf` (
  `WelcomeType` tinyint(4) NOT NULL,
  `Date` date NOT NULL,
  `Partner` varchar(64) NOT NULL,
  `Sent` int(11) NOT NULL DEFAULT '0',
  `Unsent` int(11) NOT NULL DEFAULT '0',
  `Clicks` int(11) NOT NULL DEFAULT '0',
  `Opens` int(11) NOT NULL DEFAULT '0',
  `Bounced` int(11) NOT NULL DEFAULT '0',
  `Complaints` int(11) NOT NULL DEFAULT '0',
  `Unsubscribed` int(11) NOT NULL DEFAULT '0',
  `El1` int(11) NOT NULL DEFAULT '0',
  `El2` int(11) NOT NULL DEFAULT '0',
  `El3` int(11) NOT NULL DEFAULT '0',
  `El4` int(11) NOT NULL DEFAULT '0',
  `JobSearch` int(11) NOT NULL DEFAULT '0',
  `JobClicks` int(11) NOT NULL DEFAULT '0',
  `Updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`WelcomeType`,`Date`,`Partner`),
  KEY `dt` (`Date`),
  KEY `p` (`Partner`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

-- --------------------------------------------------------

--
-- Table structure for table `coregPivotWelPerf`
--

CREATE TABLE `coregPivotWelPerfSource` (
  `WelcomeType` tinyint(4) NOT NULL,
  `Date` date NOT NULL,
  `Partner` varchar(64) NOT NULL,
  `Source` varchar(255) NOT NULL,
  `Sent` int(11) NOT NULL DEFAULT '0',
  `Unsent` int(11) NOT NULL DEFAULT '0',
  `Clicks` int(11) NOT NULL DEFAULT '0',
  `Opens` int(11) NOT NULL DEFAULT '0',
  `Bounced` int(11) NOT NULL DEFAULT '0',
  `Complaints` int(11) NOT NULL DEFAULT '0',
  `Unsubscribed` int(11) NOT NULL DEFAULT '0',
  `El1` int(11) NOT NULL DEFAULT '0',
  `El2` int(11) NOT NULL DEFAULT '0',
  `El3` int(11) NOT NULL DEFAULT '0',
  `El4` int(11) NOT NULL DEFAULT '0',
  `JobSearch` int(11) NOT NULL DEFAULT '0',
  `JobClicks` int(11) NOT NULL DEFAULT '0',
  `Updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`WelcomeType`,`Date`,`Partner`,`Source`),
  KEY `dt` (`Date`),
  KEY `p` (`Partner`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

-- --------------------------------------------------------

--
-- Table structure for table `corrections`
--

CREATE TABLE IF NOT EXISTS `corrections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bad` varchar(255) NOT NULL,
  `good` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `bad` (`bad`),
  KEY `good` (`good`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `email_click_tracking`
--

CREATE TABLE IF NOT EXISTS `email_click_tracking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email_version_id` int(11) NOT NULL,
  `result_version_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `partner` varchar(64) NOT NULL,
  `source` varchar(255) NOT NULL,
  `action` varchar(25) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `email_id` (`email_version_id`),
  KEY `result_id` (`result_version_id`),
  KEY `date_created` (`date_created`),
  KEY `user_id` (`user_id`),
  KEY `partner_source` (`partner`,`source`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `email_version`
--

CREATE TABLE IF NOT EXISTS `email_version` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(32) NOT NULL DEFAULT 'unknown',
  `name` varchar(255) NOT NULL,
  `campaign_name` varchar(32) NOT NULL,
  `description` text NOT NULL,
  `weight` smallint(6) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `date_expire` datetime NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `campaign_name` (`campaign_name`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `geocode_block`
--

CREATE TABLE IF NOT EXISTS `geocode_block` (
  `start_ip_num` int(10) unsigned NOT NULL,
  `end_ip_num` int(10) unsigned NOT NULL,
  `loc_id` bigint(20) unsigned NOT NULL,
  UNIQUE KEY `start_ndx` (`start_ip_num`),
  KEY `loc_id` (`loc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `geocode_location`
--

CREATE TABLE IF NOT EXISTS `geocode_location` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `country` varchar(2) NOT NULL,
  `region` varchar(2) NOT NULL,
  `city` varchar(255) NOT NULL,
  `postal_code` varchar(6) NOT NULL,
  `latitude` decimal(8,4) NOT NULL,
  `longitude` decimal(8,4) NOT NULL,
  `metro_code` int(10) unsigned NOT NULL,
  `area_code` char(3) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `crp` (`country`,`region`,`postal_code`),
  KEY `cc` (`country`,`city`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `geocode_location_expansion`
--

CREATE TABLE IF NOT EXISTS `geocode_location_expansion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country` varchar(8) NOT NULL DEFAULT 'GB',
  `region` varchar(8) NOT NULL,
  `city` varchar(64) NOT NULL,
  `postal_code` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `city_postal` (`city`,`postal_code`),
  KEY `postal_code` (`postal_code`),
  KEY `city` (`city`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `job_click`
--

CREATE TABLE IF NOT EXISTS `job_click` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `query` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `radius` smallint(6) NOT NULL,
  `daysback` smallint(6) NOT NULL,
  `page_number` tinyint(4) NOT NULL,
  `ip` varchar(16) NOT NULL,
  `device` varchar(32) NOT NULL,
  `browser` varchar(32) NOT NULL,
  `browser_version` varchar(32) NOT NULL,
  `is_mobile` tinyint(1) NOT NULL,
  `version_id` int(11) NOT NULL,
  `engine` varchar(16) NOT NULL DEFAULT 'indeed',
  `job_key` varchar(64) NOT NULL,
  `job_title` varchar(255) NOT NULL,
  `job_company` varchar(255) NOT NULL,
  `job_location` varchar(255) NOT NULL,
  `position` int(11) NOT NULL,
  `age` varchar(32) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `sub_id` varchar(64) NOT NULL,
  `partner` varchar(64) NOT NULL,
  `source` varchar(255) NOT NULL,
  `sponsored` tinyint(1) NOT NULL,
  `date_clicked` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `email` (`email`),
  KEY `partner_source` (`partner`,`source`),
  KEY `date_clicked` (`date_clicked`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `no_result_details`
--

CREATE TABLE IF NOT EXISTS `no_result_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `query` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `channel` varchar(32) NOT NULL,
  `ip` varchar(16) NOT NULL,
  `page_number` int(11) NOT NULL,
  `provider_calls` int(11) NOT NULL,
  `search_engine` varchar(16) NOT NULL,
  `version_id` int(11) NOT NULL,
  `daysback` smallint(6) NOT NULL,
  `radius` smallint(6) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `sub_id` varchar(64) NOT NULL,
  `partner` varchar(64) NOT NULL,
  `source` varchar(255) NOT NULL,
  `date_searched` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `email` (`email`),
  KEY `search_date` (`date_searched`),
  KEY `user_id` (`user_id`),
  KEY `partner_source` (`partner`,`source`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `partner_source_click`
--

CREATE TABLE IF NOT EXISTS `partner_source_click` (
  `search_date` date NOT NULL,
  `partner` varchar(64) NOT NULL,
  `source` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  UNIQUE KEY `unique_key` (`search_date`,`partner`,`source`,`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

--
-- Triggers `partner_source_click` - NOT USED
--
-- DROP TRIGGER IF EXISTS `update_unique_clicks`;
-- DELIMITER //
-- CREATE TRIGGER `update_unique_clicks` AFTER INSERT ON `partner_source_click`
-- FOR EACH ROW BEGIN
--    UPDATE partner_source_stats SET unique_clicks=unique_clicks+1 WHERE
--    search_date=NEW.search_date AND partner=NEW.partner AND
--    source=NEW.source;
--  END
--//
--DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `partner_source_search`
--

 CREATE TABLE `partner_source_search` (
  `search_date` date NOT NULL,
  `partner` varchar(64) NOT NULL,
  `source` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  UNIQUE KEY `unique_key` (`search_date`,`partner`,`source`,`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

--
-- Triggers `partner_source_search` - NOT USED
--
--DROP TRIGGER IF EXISTS `update_unique_views`;
--DELIMITER //
--CREATE TRIGGER `update_unique_views` AFTER INSERT ON `partner_source_search`
-- FOR EACH ROW BEGIN
--    UPDATE partner_source_stats SET unique_views=unique_views+1 WHERE
--    search_date=NEW.search_date AND partner=NEW.partner AND
--    source=NEW.source;
--  END
--//
--DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `partner_source_stats`
--

CREATE TABLE `partner_source_stats` (
  `search_date` date NOT NULL,
  `partner` varchar(64) NOT NULL,
  `source` varchar(255) NOT NULL,
  `page_views` int(11) NOT NULL,
  `unique_views` int(11) NOT NULL,
  `job_clicks` int(11) NOT NULL,
  `unique_clicks` int(11) NOT NULL,
  UNIQUE KEY `unique_key` (`search_date`,`partner`,`source`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

-- --------------------------------------------------------

--
-- Table structure for table `reject_reason`
--

CREATE TABLE IF NOT EXISTS `reject_reason` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `search_key` varchar(25) NOT NULL,
  `description` varchar(35) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `result_version`
--

CREATE TABLE IF NOT EXISTS `result_version` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alt_version_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `weight` smallint(6) NOT NULL,
  `header_file` varchar(255) NOT NULL,
  `css_file` varchar(255) NOT NULL,
  `body_file` varchar(255) NOT NULL,
  `footer_file` varchar(255) NOT NULL,
  `traffic_type` varchar(16) NOT NULL DEFAULT 'email' COMMENT 'Valid values: "email", "organic", "mobile","other"',
  `page_type` varchar(16) NOT NULL DEFAULT 'all' COMMENT 'Valid values: "odd", "even", "all"',
  `items_per_page` int(11) NOT NULL,
  `organic_per_page` int(11) NOT NULL,
  `sponsored_per_page` int(11) NOT NULL,
  `ad_per_page` int(11) NOT NULL,
  `result_pattern` varchar(255) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `date_expire` datetime NOT NULL,
  `modal_enabled` tinyint(1) NOT NULL DEFAULT '1',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `return_path_seeds`
--

CREATE TABLE IF NOT EXISTS `return_path_seeds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `city` varchar(64) NOT NULL,
  `postal_code` varchar(16) NOT NULL,
  `keyword_1` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `search_unique_ip`
--

CREATE TABLE IF NOT EXISTS `search_unique_ip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `search_date` date NOT NULL,
  `version_id` int(11) NOT NULL,
  `page_number` smallint(6) NOT NULL,
  `ip` varchar(16) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_key` (`search_date`,`version_id`,`page_number`,`ip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `search_with_results`
--

CREATE TABLE IF NOT EXISTS `search_with_results` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Post-migration after row 3850000',
  `user_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `query` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `channel` varchar(32) NOT NULL,
  `ip` varchar(16) NOT NULL,
  `page_number` int(11) NOT NULL,
  `provider_calls` int(11) NOT NULL,
  `search_engine` varchar(16) NOT NULL,
  `version_id` int(11) NOT NULL,
  `daysback` smallint(6) NOT NULL,
  `radius` smallint(6) NOT NULL,
  `user_agent` text NOT NULL,
  `total_results` mediumint(9) NOT NULL,
  `results_on_page` smallint(6) NOT NULL,
  `sub_id` varchar(64) NOT NULL,
  `partner` varchar(64) NOT NULL,
  `source` varchar(255) NOT NULL,
  `date_searched` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `email` (`email`),
  KEY `search_date` (`date_searched`),
  KEY `user_id` (`user_id`),
  KEY `partner_source` (`partner`,`source`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `email_domain` varchar(255) NOT NULL,
  `first_name` varchar(64) NOT NULL,
  `last_name` varchar(64) NOT NULL,
  `city` varchar(64) NOT NULL,
  `postal_code` varchar(16) NOT NULL,
  `partner` varchar(64) NOT NULL,
  `source` varchar(255) NOT NULL,
  `reg_key` varchar(64) NOT NULL,
  `subcode` varchar(32) NOT NULL,
  `sub_id` varchar(255) DEFAULT NULL,
  `opt_in_ip` varchar(32) NOT NULL,
  `date_opt_in` datetime NOT NULL,
  `is_mailable` tinyint(1) NOT NULL,
  `has_engaged` tinyint(1) NOT NULL,
  `coreg_id` bigint(20) NOT NULL,
  `nowrm` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_coreg_sent` datetime NOT NULL,
  `date_last_mailed` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `partner_source` (`partner`,`source`),
  KEY `date_added` (`date_added`),
  KEY `date_coreg_sent` (`date_coreg_sent`),
  KEY `date_last_mailed` (`date_last_mailed`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `valid_keyword`
--

CREATE TABLE IF NOT EXISTS `valid_keyword` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keyword` varchar(255) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `keyword` (`keyword`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `versionator_stats`
--

DROP TABLE IF EXISTS `versionator_stats`;
CREATE TABLE IF NOT EXISTS `versionator_stats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `search_date` date NOT NULL,
  `version_id` int(11) NOT NULL,
  `page_number` smallint(6) NOT NULL,
  `unique_visitors` int(11) NOT NULL,
  `page_views` int(11) NOT NULL,
  `job_results` int(11) NOT NULL,
  `organic_clicks` int(11) NOT NULL,
  `sponsored_clicks` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_key` (`search_date`,`version_id`,`page_number`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
