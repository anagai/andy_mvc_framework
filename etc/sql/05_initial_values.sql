-- Initial result versions, so we can load the site immediately.
-- suy, 20121120

INSERT INTO result_version
VALUES
(1, 0, 'Default Version', 'Default Version (Keep inactive and do not delete)', 0, 'h1.inc', 'c1.css', 'b1.inc', 'f1.inc', 'organic', 'all', 11, 10, 0, 1, 'A-G,O-I,O-I,O-I,O-I,O-I,O-I,O-I,O-I,O-I,O-I', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 0),
(2,0, 'Email Traffic Job Result', 'Email Traffic Job Result', 100, 'h1.inc', 'c2.css', 'b1.inc', 'f1.inc', 'email', 'all', 10, 8, 0, 2, 'O-I,O-I,O-I,O-I,A-G,O-I,O-I,O-I,O-I,A-G', '2012-11-27 14:01:28', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1);

INSERT INTO `reject_reason` (`id`, `search_key`, `description`) VALUES
(1, 'inv_email', 'Invalid email'), (2, 'inv_ip', 'Invalid IP'), (3, 'inv_dt', 'Invalid date'), (4, 'no_partner', 'Empty partner'),
(5, 'partner_off', 'Partner inactive'), (6, 'no_loc', 'No location'), (7, 'is_black', 'Blacklisted domain'), (8, 'is_global', 'On global removal'),
(9, 'is_complaint', 'On complaint'), (10, 'is_unsub', 'On unsubscribe'), (11, 'is_bounce', 'On bounce'), (12, 'is_btld', 'Bad top level domain'),
(13, 'more_seven', 'Sent > 7 days ago'), (14, 'sent_seven', 'Sent within 7 days'), (15, 'no_rec', 'Failed to store user');