# README #

Code for a job search site. Input fields are keyword and location.

Entry point is in public/index.php. All page requests come through here.

The lib/core/initialize.inc script sets global settings and loads needed class files.

The lib/application.inc handles page url route parsing, render of the views and other important tasks.

Page architecture is MVC.

Page urls are in friendly format e.g. /jobs/q-sales/l-los+angeles. The route parser also is able to retrieve get parameters

The location and keyword fields on job search has look ahead autocomplete drop downs.

Site also processed api feeds from job partners.

The job search page records job click and page view metrics.





