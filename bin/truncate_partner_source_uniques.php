#!/usr/bin/php -q
<?php

/*
 * Created on 7/16/13
 * Written By Sung Hwan Ahn / Andy Nagai
 *
 */

require_once( __DIR__ . "/../lib/core/initialize.inc");

if(Util::isRunningPID()) {
    echo "Already running.\n";
    exit;
}

echo "==========================================================" . "\n";
echo " daily truncate search and click unique tracking tables " . "\n";
echo " for partner_source_stats aggregation " . "\n";
echo "==========================================================" . "\n\n";
echo "Started " . date('m/d/Y H:i:s') . "\n";

$todayDt = date('Y-m-d');

// Only clear yesterdays search_unique_ip data
$sql = "DELETE FROM partner_source_search WHERE search_date < '{$todayDt}'";
$db->query($sql);

$sql = "DELETE FROM partner_source_click WHERE search_date < '{$todayDt}'";
$db->query($sql);

echo "Ended " . date('m/d/Y H:i:s') . "\n";


