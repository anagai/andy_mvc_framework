#!/usr/bin/php -q
<?php

    // vim: ai ts=4 sts=4 et sw=4 tw=79

    /*
     * Created on Dec 6, 2012
     * Written By Sung Hwan Ahn
     * Observed By Andy Nagai
     *
     */

    require_once( __DIR__ . "/../lib/core/initialize.inc");

    if(Util::isRunningPID()) {
        echo "Already running.\n";
        exit;
    }

	function update_bounce_table($user_model, $limit) {

		$bounce_model = Util::load_model('bounce');

		$bounce_model->set_pdo_dbh();

		$sth = $bounce_model->get_empty_domain_partner_records();

		echo ' empty bounced rows: ' . $sth->rowCount() . "\n";

		while ($bounce_row = $sth->fetch(PDO::FETCH_OBJ)) {

			$domain = '';
			$partner = '';
			$source = '';
			//Util::printr($row);

			// No domain. Get domain part from email address
			if(empty($bounce_row->domain)) {
				$email_exp = explode('@', $bounce_row->email_address);
				$domain = array_pop($email_exp);
			}

			// No partner get partner and source from user's record
			if(empty($bounce_row->partner)) {
				$user = $user_model->find_by_email($bounce_row->email_address);
				if(!empty($user)) {
					$partner = $user->partner;
					//if(empty($bounce_row->source)) {
					$source = $user->source;
					//}
				}
			}

			if(!empty($domain) || !empty($partner)) {
				$bounce_model->update_domain_partner($bounce_row->id, $domain, $partner, $source);
			}

		}

	}


	function update_complaint_table($user_model, $limit) {

		$complaint_model = Util::load_model('complaint');

		$complaint_model->set_pdo_dbh();

		$sth = $complaint_model->get_empty_domain_partner_records();

		echo 'empty complaint rows: ' . $sth->rowCount() . "\n";

		while ($complaint_row = $sth->fetch(PDO::FETCH_OBJ)) {

			$domain = '';
			$partner = '';
			$source = '';
			//Util::printr($row);

			// No domain. Get domain part from email address
			if(empty($complaint_row->domain)) {
				$email_exp = explode('@', $complaint_row->email_address);
				$domain = array_pop($email_exp);
			}

			// No partner get partner and source from user's record
			if(empty($complaint_row->partner)) {
				$user = $user_model->find_by_email($complaint_row->email_address);
				if(!empty($user)) {
					$partner = $user->partner;
					//if(empty($bounce_row->source)) {
					$source = $user->source;
					//}
				}
			}

			if(!empty($domain) || !empty($partner)) {
				$complaint_model->update_domain_partner($complaint_row->id, $domain, $partner, $source);
			}

		}

	}

$user_model = Util::load_model('user');

$limit = 1000;

echo "starting update\n";

update_bounce_table($user_model, $limit);

update_complaint_table($user_model, $limit);

echo "update completed\n";

exit;
?>
