#!/usr/bin/php -q
<?php

    // vim: ai ts=4 sts=4 et sw=4 tw=79

    /*
     * Created on Dec 6, 2012
     * Written By Sung Hwan Ahn
     * Observed By Andy Nagai
     *
     */

    require_once( __DIR__ . "/../lib/core/initialize.inc");

    //if(Util::isRunningPID()) {
    //    echo "Already running.\n";
    //    exit;
    //}

    echo "==============================" . "\n";
    echo "  Jobungo UK Coreg Injection  " . "\n";
    echo "==============================" . "\n\n";

    $startTime = microtime(true);

    $fileHandle = basename($_SERVER['PHP_SELF'],'.php');
    $logFile = $fileHandle.'.log';
    $errFile = $fileHandle.'_error.log';

    date_default_timezone_set("America/Los_Angeles");

    $args = getopt("w:");

    $conn = Database::get_pdo_conn(DEFAULT_DB_SETTING);

    //$injectTable = $args['f'];
    $injectTable = '';

    $nowrm = !empty($args['w'])
        ? true
        : false;

    echo 'inject: ' . $injectTable . " nowrm: {$nowrm}\n";

    Util::setLogs($logFile, $errFile);

    Util::log_to_file($logFile, 'Started', 'inject: ' . $injectTable);
    $start = date('Y-m-d H:i:s');
    echo "Started: " . $start . "\n";

    $tmpCsv = '/tmp/juk_inject_csv/';

    function loadFile($file) {
        $ret = shell_exec("mysqlimport -h jobungouk.c6ydzurw8x8j.eu-west-1.rds.amazonaws.com " .
            "-u juk_rw -ptruecrimeis --local --lines-terminated-by='\\n' --ignore-lines=1 " .
            "--fields-terminated-by=',' --columns='first_name, last_name, email, source, " .
            "opt_in_ip, date_opt_in, city, @dummy, postal_code, @dummy, partner, @dummy' " .
            "jobungouk {$file}");

        echo "uploaded: {$ret}\n";
    }

    // Delete already injected records
    $sql = "
        DELETE FROM injection_temp WHERE injected=1;
    ";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    $removed = $stmt->rowCount();

    Util::log_to_file($logFile, 'Rows deleted', "cnt: {$removed}");

    $sql = "
        SELECT count(1) as cnt FROM injection_temp WHERE injected=0;
    ";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    $cnt = $stmt->fetchColumn();

    if($cnt<1) {
        // truncate to reset id column
        $sql = "TRUNCATE injection_temp;";
        $conn->query($sql);
    }

    $csvFiles = array();
    $csvTotal = 0;
    $ymd = date('Ymd');
    $destFile = "{$tmpCsv}injection_temp.csv";
    $globPatt = "{$tmpCsv}*{$ymd}*";
    $files = glob($globPatt);
    if($files!==false && count($files)>0) {
        foreach($files as $file) {
            //unlink($file);
            shell_exec("cp {$file} {$destFile}");
            $csvCount = Util::getCsvCount($destFile);
            $csvTotal += $csvCount;
            loadFile($destFile);
            $csvFiles[] = $file;
            unlink($file);
            unlink($destFile);
        }
    } else {
        Util::log_to_file($logFile, 'No inject files found', "{$globPatt}");
    }

    //$injections = $injections_model->find_all_limit($start, $limit);

    // truncate coregInjection. Only hold 100 days of injections
    //$dt = date("Y-m-d", strtotime("-100 day"));
    //$sql = "DELETE FROM coregInjection WHERE dateInjected<?";

    //$stmt = $conn->prepare($sql);
    //$stmt->execute(array($dt));
    //$affected = $stmt->rowCount();

    //echo "Deleted > 100 day old corgInjection records: " . $affected ."\n";
    //echo "Processing batch: " . $batch . "\n";

    // Load injection lists for the day

    unset($stmt);

    $sql = "
        SELECT * FROM injection_temp WHERE injected=0
    ";

    $stmt = $conn->prepare($sql);
    $stmt->execute();

    $totalUsers = $stmt->rowCount();

    if($totalUsers>0) {
        $msg = "Started Injection<br>" .
            "To Inject: {$totalUsers}<br>" .
            "CSV Count: {$csvTotal}<br>" .
            "files imported: " . implode(",",$csvFiles);
        echo $msg . "\n";
        Util::systemAlert($fileHandle, $msg);
    } else {
        //Util::systemAlert($fileHandle, "No records to inject. Please check injection_temp table.", '', false);
        Util::log_to_file($logFile, 'Nothing to inject', '');
    }

    echo "To Inject: {$totalUsers}\n";

    //while ($end != $process) {
    echo $sql . "\n";
     $stats=array(
        'now'=>0,
        'ps'=>0,
        'start'=>(time()-1),
        'users'=>0
        );
    $cnt = 1;

    //$ch = curl_init();

    function removeUnicode($inp) {
        $out = trim(preg_replace('/[^\x20-\x7e\t]/','',$inp));
        return $out;
    }

    function logOut(&$out, $logFile, $errFile) {
        foreach($out as $resp) {
            if($resp['status']===200) {
                Util::log_to_file($logFile, 'Sent', "resp: {$resp['response']} " .
                    "status: {$resp['status']} url: {$resp['url']}");
            } else {
                Util::log_to_file($errFile, 'Error', "resp: {$resp['response']} " .
                    "status: {$resp['status']} url: {$resp['url']}");
            }
        }
    }

    $multiRequests = 8;

    $url = "http://www.jobungo.co.uk/partnersfeed?";

    $requests = array();

    while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        ++$stats['users'];

        unset($data);

        if(count($requests)==$multiRequests) {
            $out = Util::execCurlMulti('', $requests);
            //print_r($out);
            $stats['now']=time();
            $stats['ps']=($stats['users']/($stats['now']-$stats['start']));
            printf("\rInject Rate: %.2f/s Emails: %d",
                    $stats['ps'],
                    $stats['users']
            );
            logOut($out, $logFile, $errFile);
            unset($requests);
            $requests=array();
            unset($out);
        }

        $data['email'] = $row['email'];
        $data['fname'] = $row['first_name'];
        $data['lname'] = $row['last_name'];
        $data['city'] = $row['city'];
        $data['zip'] = $row['postal_code'];
        $data['pid'] = $row['partner'];
        $data['source'] = $row['source'];
        $data['ip'] = $row['opt_in_ip'];
        $data['date'] = $row['date_opt_in'];
        $data['nowrm'] = $nowrm;
        //$data['inj'] = urlencode($injectTable);

        $requests[] = $url.http_build_query($data, NULL, '&');

/*
            $url = "http://www.jobungo.co.uk/partnersfeed?" .
            //$url = "http://andy-jobungouk.restdev.com/partnersfeed?" .
                   "email=" . urlencode(removeUnicode($row['email'])) . "&" .
                   "fname=" . urlencode(removeUnicode($row['first_name'])) . "&" .
                   "lname=" . urlencode(removeUnicode($row['last_name'])) . "&" .
                   "city=" . urlencode(removeUnicode($row['city'])) . "&" .
                   "zip=" . urlencode(removeUnicode($row['postal_code'])) . "&" .
                   "pid=" . urlencode($row['partner']) . "&" .
                   "source=" . urlencode($row['source']) . "&" .
                   "ip=" . urlencode($row['opt_in_ip']) . "&" .
                   "date=" . urlencode($row['date_opt_in']) . "&" .
                   "nowrm=" . $nowrm . "&" .
                   "inj=" . urlencode($injectTable);

                $curl_options = array(
                    CURLOPT_URL => $url,
                    CURLOPT_HEADER => 0,
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_ENCODING => 'gzip,deflate',
            );

            curl_setopt_array( $ch, $curl_options );
            $output = curl_exec( $ch );
*/
            //$output = Util::curl_request($url);
            //Util::log_to_file($logFile, 'Processed', 'inject: ' . $injectTable  . ' | ' .
            //    $url . ' | ' . $row['email'] . " | " . $output );
            //$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            //if($httpCode!==200) {
            //    Util::log_to_file($errFile, 'Error', 'inject: ' . $injectTable  . ' | ' .
            //    $url . ' | ' . $row['email'] . " | httpcode: {$httpCode} out: " . $output );
            //}
        //}

        //$start += $limit;
        //$injections = $injections_model->find_all_limit($start, $limit);
        $injectId =  $row['id']
            ? $row['id']
            : 0;

        $sql = "UPDATE injection_temp SET injected=1 WHERE id=" . $injectId;
        $updStmt = $conn->prepare($sql);
        $updStmt->execute();

        //echo $row['email'] . ' - ' . $row['id'] . ' - ' . $cnt . "\n";

        //if($stats['now']!=time()){

        //}


        $cnt++;
    }

    if(count($requests)>0) {
        $out = Util::execCurlMulti('', $requests);
        $stats['now']=time();
            $stats['ps']=($stats['users']/($stats['now']-$stats['start']));
            printf("\rInject Rate: %.2f/s Emails: %d",
                    $stats['ps'],
                    $stats['users']
            );
        logOut($out, $logFile, $errFile);
        unset($requests);
    }



    print ("\nStarted: " . $start . "\n");
    $ended = date('Y-m-d H:i:s');
    $dur = Util::get_microtime_duration(strtotime($start),strtotime($ended));
    print ("Ended: " . $ended . "\n");
    echo "Dur: " . $dur . "\n\n";

    Util::log_to_file($logFile, 'Ended',
        'injected: ' . $injectTable . ' rate: ' . $stats['ps'] . ' total: ' . $stats['users'] . ' dur: ' . $dur);

    echo "select partner, sum(accepted) as accepted, sum(mailable) as mailable, " .
        "count(if(rejectId=6,1,Null)) as 'no loc reject mailable', " .
        "count(if(rejectId>0,1,Null)) as 'total rejected' from coregInjection WHERE listName='{$injectTable}' group by partner;\n\n";

    echo "select count(a.id) as rejected, b.description  from coregInjection as a " .
         "inner join reject_reason as b on a.rejectId=b.id where listName='{$injectTable}' group by rejectId;\n";

    $msg = "Injected feed<br>Started: {$start}<br> Ended: {$ended}<br>" .
        "Dur: {$dur}<br>Rate: {$stats['ps']}<br>Users: {$stats['users']}";

    Util::systemAlert($fileHandle, $msg);

    if(isset($database)) { $database->close_connection(); }
?>
