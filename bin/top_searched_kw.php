<?php
require_once( __DIR__ . "/../lib/core/initialize.inc");

if(Util::isRunningPID()) {
    echo "Already running.\n";
    exit;
}

$fileHandle = basename($_SERVER['PHP_SELF'],'.php');
$logFile = $fileHandle.'.log';

$conn = Database::get_pdo_conn(DEFAULT_DB_SETTING);

// Get valid keywords
$stmt['keywords'] = $conn->prepare('SELECT keyword from valid_keyword');
$stmt['keywords']->execute();

while($kw=$stmt['keywords']->fetch(PDO::FETCH_ASSOC)) {
    $validKw[strtolower($kw['keyword'])]=$kw['keyword'];
}

 function cleanVal($val) {
        # remove any unicode
        $val = preg_replace('/[^\x20-\x7e\n\t]/','',trim($val));
        # remove specific url breaking characters
        //$val = str_replace(array("'",'"','/',"\\",','), '', $val);
        # convert multiple spaces to one
        $val = preg_replace('/\s+/', ' ', $val);
        return $val;
    }

    unset($stmt);

    Util::log_to_file($logFile,'Retrieved valid keywords', "cnt: " . count($validKw));

    /******************************************/
    /*** Get top 20 searched valid keywords ***/
    /******************************************/

    $daysBack = 30;
    $fromDt = date('Y-m-d',strtotime("-{$daysBack} day"));

    $sql = "SELECT query from search_with_results where date_searched>=?";

    echo 'days back:' . $daysBack . "\n";
    echo 'getting searches: ' . date('H:i:s') . "\n";

    Util::log_to_file($logFile,"Getting {$daysBack} days of searches", '');
    $taskStart = microtime(true);
    $stmt['searches'] = $conn->prepare($sql);
    $stmt['searches']->execute(array($fromDt));
    $searchCnt = $stmt['searches']->rowCount();
    $dur = microtime(true) - $taskStart;
    echo 'done searches: ' . date('H:i:s') . " dur: {$dur} count: {$searchCnt}\n";
    Util::log_to_file($logFile,'Retrieved searches', "dur: {$dur} count: {$searchCnt}");

    Util::log_to_file($logFile,'Counting keywords', "....");
    $taskStart = microtime(true);
    $validSearch = array();
    $cnt=1;
    while($search=$stmt['searches']->fetch(PDO::FETCH_ASSOC)) {

        $srchKw = strtolower(cleanVal($search['query']));

        // Get list of not valid searches
        //if(!isset($validKw[$srchKw])) {
        //    $searchedKw[$srchKw]++;
        //}

        // Get list of valid keyword searches
        //if(isset($validKw[$srchKw])) {
        //    $searchedKw[$srchKw]++;
        //}

        $searchedKw[$srchKw]++;

        $mem = number_format(memory_get_usage()/1024,1).' kb';
        echo "\rRec: {$cnt} of {$searchCnt} srchsize: " . count($searchedKw) . " memory: {$mem}";
        $cnt++;
    }
    $dur = microtime(true) - $taskStart;
    echo "done counting: dur: {$dur}\n";
    Util::log_to_file($logFile,'Finished counting keywords', "dur: {$dur}");
//    if(count($validSearch)==0) {
//        $msg = "No valid search keywords. Cannot initialize campaign.";
//        Utility::log_to_file($logFile,'Error', $msg);
//        Utility::systemAlert(__FILE__,$msg,'',false);
//        exit($msg."\n");
//    }

    // Reverse sort count desc
    arsort($searchedKw, SORT_NUMERIC);
    $getTop = 50;
    // Get top 30 most searched
    $topSearched = array_slice($searchedKw,0, $getTop);
    print_r($topSearched);
    $fh = fopen('/tmp/juk_top_searched_kw.csv','w');
    foreach($topSearched as $kw=>$cnt) {
        fputcsv($fh,array($kw,$cnt));
    }
    fclose($fh);
    Util::log_to_file($logFile, "Top {$getTop} searches", print_r($topSearched,true));

    // Switch indexes with values
    //$topSearched = array_flip($topSearched);

    // Randomize the order
    //shuffle($topSearched);

    // Get to top 20 random keywords
    //$topSearched = array_slice($topSearched,0,20);
    //print_r($topSearched);

    // Make ready for adding to Config
    /*
    $kwCnt=1;
    foreach($topSearched as $val) {
        if($kwCnt==6 || $kwCnt==8) {
            $kwCnt++;
        }
        $confData['keyword_' . $kwCnt]=$val;
        $kwCnt++;
    }

    Utility::log_to_file($logFile,'Top 20 random searches', print_r($confData,true));


    $lineData = json_encode($confData);
*/
    Util::log_to_file($logFile,'Done', '');
    echo 'Memory: '.number_format(memory_get_usage()/1024,1).'kb  Peak: ('.number_format(memory_get_peak_usage()/1024,1).'kb)';
    unset($searchedKw);
    echo "\n";
exit;

        $msg = "Initialized {$sub_id} campaign starting at {$startTime}<br>";
        $msg .= "Server: {$sendServer}<br>";
        $msg .= "Remote File: {$remote_path}<br>";

        Util::systemAlert(__FILE__,$msg);
    echo 'Done: ' . date("H:i:s") . "\n";
