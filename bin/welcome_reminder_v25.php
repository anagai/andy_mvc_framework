#!/usr/bin/php -q
<?php

    require_once( __DIR__ . "/../lib/core/initialize.inc");

    //if(Util::isRunningPID()) {
    //    echo "Already running.\n";
    //    exit;
    //}

    # Get cli argument
    $args = getopt("d:");

    # Must pass numeric day interval argument and in range 1-5
    if(!empty($args['d']) && is_numeric($args['d']) && preg_match('/[1-5]/',$args['d'])) {
        $dayInterval = $args['d'];
    } else {
       exit( "Usage: " . $argv[ 0 ] . " -d [Day Interval]\n" );
    }

    echo "day interval: " . $dayInterval . "\n";

    echo "===============================================" . "\n";
    echo "  Welcome Reminder {$dayInterval} for ses v2.5 " . "\n";
    echo "===============================================" . "\n\n";

    $conn = Database::get_pdo_conn(DEFAULT_DB_SETTING);

    $ses_model = Util::load_model('ses');

    /*************************/
    /*** CREATING CSV FILE ***/
    /*************************/

    $today = strftime("%Y%m%d", time());

    $filename = "welcome_reminder_{$dayInterval}_{$today}.csv";
    $logFile = "welcome_reminder_{$dayInterval}.log";
    $numOfCamps = 5;

    # Set seed ranges for each campaign
    if($stmt = $conn->prepare('SELECT count(1) FROM return_path_seeds;')) {
        $stmt->execute();
        $seedCount = $stmt->fetchColumn();
        if(empty($seedCount)) {
            Util::log_to_file($logFile, 'No Seeds', "No seeds found");
        } else {
            # number of seeds per campaign
            $limit = floor($seedCount / $numOfCamps);
            # seeds that cannot be evenly divided added to last campaign
            $rem = $seedCount % $numOfCamps;
            $start = 0;

            for($i=1;$i<=$numOfCamps;$i++) {
                if($i==$numOfCamps) {
                    $limit += $rem;
                }
                //echo 'st: ' . $start . ' lim: ' . $limit . "\n";
                $seedLimit[$i] = array('st'=>$start, 'lim'=>$limit);
                $start += $limit;
            }
        }
    } else {
        Util::log_to_file($logFile, 'No Seeds', "Error in retrieving seeds");
    }

    //print_r($seedLimit);

    switch($dayInterval) {
        case 1:
            $startTime = date('Y-m-d 05:00:00');
            $addRpSeeds = TRUE;
            $useRpSched = FALSE;
            $rpSched = array();
            $description = "Jobungo UK first welcome reminder for " . date("Y-m-d");
            $subject = "\$FNAME_COMMA_NONE\$25+ new \$CITY\$ jobs";
        break;
        case 2:
            $startTime = date('Y-m-d 05:20:00');
            $addRpSeeds = TRUE;
            $useRpSched = FALSE;
            $rpSched = array();
            $description = "Jobungo UK second welcome reminder for " . date("Y-m-d");
            $subject = "New \$CITY\$ Jobs for \$USERNAME\$";
        break;
        case 3:
            $startTime = date('Y-m-d 05:40:00');
            $addRpSeeds = TRUE;
            $useRpSched = FALSE;
            $rpSched = array();
            $description = "Jobungo UK third welcome reminder for " . date("Y-m-d");
            $subject = "\$CITY\$ Job Alert\$SPACE_FOR_FNAME\$";
        break;
        case 4:
            $startTime = date('Y-m-d 06:00:00');
            $addRpSeeds = TRUE;
            $useRpSched = FALSE;
            $rpSched = array();
            $description = "Jobungo UK fourth welcome reminder for " . strftime("%Y-%m-%d", time());
            $subject = "\$FNAME_COMMA_NONE\$View new jobs near \$CITY\$";
        break;
        case 5:
            $startTime = date('Y-m-d 06:20:00');
            $addRpSeeds = TRUE;
            $useRpSched = FALSE;
            $rpSched = array();
            $description = "Jobungo UK fifth welcome reminder for " . strftime("%Y-%m-%d", time());
            $subject = "New \$CITY\$ job openings\$SPACE_FOR_FNAME\$";
        break;
    }

    echo "Creating CSV file {$filename} ..." . "\n";

    $total_count = $ses_model->create_welrem_v25_csv_file($conn, Util::getUtcDt($startTime), CSV_PATH . $filename, $dayInterval,
        $addRpSeeds, $useRpSched, $rpSched, $seedLimit[$dayInterval]);

    if ($total_count < 1) {
        Util::log_to_file($logFile, 'No Users', "Nothing to send");
        exit("No need to send the digest email: 0 users\n");
    }

    echo "Created CSV File!\n\n";

    //***********************
    //*** UPLOAD CSV FILE ***
    //***********************

    echo "Connecting to Snowdon server...\n";
    //$filename='juk_test.csv';
    //$filename = 'juk_test_noloc.csv';
    //$filename ='test_users_v2_nokw_50000.csv';
    $sub_id = "jukwrm{$dayInterval}{$today}";
    $rp_id = "jobungoukjukwrm{$today}";
    //$sub_id = "test{$today}b";

    $remote_path = "/srv/www/ses25/current/db/incoming/jobungo.co.uk/{$sub_id}.csv";

    $sesConn = Util::get_ssh_connection('SNOWDON');
    $sesConn->put($remote_path, CSV_PATH . $filename, NET_SFTP_LOCAL_FILE);

    echo "File transferred to {$remote_path}\n\n";
    Util::log_to_file($logFile,'Uploaded CSV', "System uploaded {$filename} to Snowdon server.");
/*
    if($conn=ssh2_connect('176.58.120.39',22,array(
        'client_to_server'=>array(
                'comp'=>'zlib,none'
        ),
        'server_to_client'=>array(
                'comp'=>'zlib,none'
        )
))){
        ssh2_auth_password($conn,'sftp','r3stm3d1a');
        //$sftp=ssh2_sftp($conn);
        ssh2_scp_send($conn, CSV_PATH . $filename, $remote_path);
        ssh2_exec($conn, 'exit');
        //if($fh=fopen("ssh2.sftp://{$sftp}/srv/www/ses25/current/db/incoming/wullo.com/testing.csv",'w')){
        //        fputs($fh,'this is a test: '.microtime(TRUE));
        //        fclose($fh);
        //}
}
*/

    //***************************
    //*** SETUP MAIL CAMPAIGN ***
    //***************************

    echo "Setup campaign...\n";

    $key = sha1('hcAdI2k4HZ0XFIb_UifZmGCKkXGqs5LV:54.75.255.72');

    $url = "http://snowdon.restdev.com/api/campaign/?_[key]={$key}&" .
        "sub-id={$sub_id}&" .
        "description=" . urlencode($description) . "&" .
        "start-time=" . urlencode(Util::getUtcDt($startTime)) . "&" .
        "template-id=juk_welcome_reminder_{$dayInterval}&" .
        "campaign-subject=" . urlencode($subject) . "&" .
        "rp-id={$rp_id}&" .
        "total-min-results=0&" .
        "ses-account=dedicated_certified&site=jobungouk&" .
        "send-profile=jobungo_uk_digests";

    $output = Util::curl_request($url);

    echo "sub_id: {$sub_id}\n";
    echo "description: " . $description . "\n";
    echo "csv: {$filename}\n";
    echo "start-time: " . $startTime . "\n";
    echo "url: " . $url . "\n\n";

    if (empty($output)) {
        //$ses_model->update_digest_user_last_emailed($start_time, $clickerDays);
        echo "Successfully schedueled welcome reminder {$dayInterval} emails";
        Util::log_to_file($logFile,'Success', "System scheduled welcome reminder {$dayInterval} emails sending at ". $startTime);

    } else {
        echo "Failed to schedule emails: {$output}";
        Util::log_to_file($logFile,'Failed', "Failed to schedule welcome reminder {$dayInterval} emails: {$output}");
    };

    if(isset($database)) { $database->close_connection(); }

?>
