#!/usr/bin/php -q
<?php

require_once( __DIR__ . "/../lib/core/initialize.inc");

if(Util::isRunningPID()) {
    echo "Already running.\n";
    exit;
}

//Util::startTimer('script');

$startTime = microtime(true);

$fileHandle = basename($_SERVER['PHP_SELF'],'.php');
$logFile = $fileHandle.'.log';

//echo "==========================================================" . "\n";
//echo "  Aggregate coreg welcome domain " . "\n";
//echo "==========================================================" . "\n\n";
//echo "Started: " . date('Y-m-d H:i:s') . "\n";
include_once(HELPER_PATH . 'agg_coreg_weldomain_helper.inc');

$args = getopt("p:");

$prevDay = false;

# Must pass numeric day interval argument and in range 1-5
if(!empty($args['p'])) {
    if(is_numeric($args['p']) && $args['p']>0) {
        $prevDay = $args['p'];
    } else {
        exit( "Usage: " . $argv[ 0 ] . " -p [Prev Day Interval]\n" );
    }

}

Util::log_to_file($logFile, 'Started', '');

$dbConn = Database::get_pdo_conn(DEFAULT_DB_SETTING);

// Ensure no lock on table read on mckinley
$dbConn->exec('SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;');

//Util::startTimer();

// Get welcome campaign names
/*
$sql = "SELECT type, campaign_name as camp FROM email_version WHERE
        (type='welcome_email' AND name!='welcome_email_test') OR
        type='welcome_email_noloc'
    ";

$stmt = $dbConn->prepare($sql);
$stmt->execute();
$welCamps = $stmt->fetchAll(PDO::FETCH_ASSOC);
*/

if($prevDay) {
    // Aggregate previous day if param passed
    $dt = date('Y-m-d', strtotime("-{$prevDay} day"));
} else {
    // Aggregate current day
    $dt = date('Y-m-d');
}

//$dt = date('Y-m-d', strtotime('-1 day'));

$aggMnths = array(
    date('Ym', strtotime($dt)),
    date('Ym', strtotime("{$dt} first day of previous month")),
    date('Ym', strtotime("{$dt} first day of 2 months ago"))
    );

//$curMnth = date('Ym', strtotime($dt));
//$prevMnth = date('Ym', strtotime("{$dt} first day of previous month"));
//$prevTwoMnth = date('Ym', strtotime("{$dt} first day of 2 months ago"));

// For recent months prior to activation of monthly rotation use
// non-rotated campaigns for 8/14 - 10/14

$welCamps = array();

foreach($aggMnths as $aggMnth) {
    if($aggMnth == '201410' ||
       $aggMnth == '201409' ||
       $aggMnth == '201408') {
        if(!in_array('juk_welcome', $welCamps)) {
            $welCamps[] = 'juk_welcome';
        }
        if(!in_array('juk_welcome_noloc', $welCamps)) {
            $welCamps[] = 'juk_welcome_noloc';
        }
    } else {
        $welCamps[] = "juk_welcome_{$aggMnth}";
        $welCamps[] = "juk_welcome_noloc_{$aggMnth}";
    }
}

//Util::log_to_file('agg_coreg_welperf.log', 'campaigns', Util::elapsedTime());

// Get connection to k2



//$dt = '2013-09-01';
//$toDt = date('Y-m-d');

//while($dt < $toDt) {

    $aggRows = array();

    echo "Aggregated: $dt \n";
    Util::log_to_file($logFile, 'Date Processing', $dt);
    //Get welcome stats

    //Util::startTimer();
    $aggRows = getWelcomeStats($welCamps, $dt);
    //Util::log_to_file('agg_coreg_welperf.log', 'welcomeStats', Util::elapsedTime());

//Util::printr($aggRows);
//exit;

    //Util::startTimer();
    $dnmData = getDnmStats($welCamps, $dt);

    //echo 'dnmData: ';
    //print_r($dnmData);

    //Util::log_to_file('agg_coreg_welperf.log', 'dnmStats', Util::elapsedTime());

    if(!empty($dnmData)) {
        foreach($dnmData as $type=>$d) {
            foreach($dnmData[$type] as $domain=>$dnm) {
                $aggRows[$type][$domain]['bnce'] = (int)$dnm['bnce'];
                $aggRows[$type][$domain]['cmpt'] = (int)$dnm['cmpt'];
                $aggRows[$type][$domain]['unsub'] = (int)$dnm['unsub'];
            }
        }
    }

//Util::printr($aggRows);
//exit;

    //$now = time();

    //$dur = $start - $now;

    if(count($aggRows)>0) {

        // Remove unknown in case agg script rerun to correct unknown partner
        //$sql = "DELETE FROM coregPivotWelPerf WHERE Date=? AND Partner='unknown'";
        //$delStmt = $dbConn->prepare($sql);
        //$delStmt->execute(array($dt));

        foreach($aggRows as $type=>$r) {
            foreach($aggRows[$type] as $domain=>$row) {
                    $aSent = !empty($row['sent'])
                        ? $row['sent']
                        : 0;

                    $aClik = !empty($row['clik'])
                        ? $row['clik']
                        : 0;

                    $aOpen = !empty($row['open'])
                        ? $row['open']
                        : 0;

                    $aBnce = !empty($row['bnce'])
                        ? $row['bnce']
                        : 0;

                    $aCmpt = !empty($row['cmpt'])
                        ? $row['cmpt']
                        : 0;

                    $aUnsub = !empty($row['unsub'])
                        ? $row['unsub']
                        : 0;

                $sql = "
                    SELECT 1 FROM coregPivotWelDomain WHERE Date=?
                    AND Domain=? AND WelcomeType=? LIMIT 1
                    ";

                $chkStmt = $dbConn->prepare($sql);

                if($chkStmt->execute(array($dt, $domain, $type))) {
                    $recCnt = $chkStmt->fetchColumn(0);
                    // If return something then record exists
                    if(!empty($recCnt)) {
                        // Update agg row
                        $sql = "
                            UPDATE coregPivotWelDomain SET Sent={$aSent},
                            Clicks={$aClik}, Opens={$aOpen}, Bounced={$aBnce},
                            Complaints={$aCmpt}, Unsubscribed={$aUnsub}
                            WHERE Date=? AND Domain=? AND WelcomeType=?
                        ";

                    } else {
                        // New agg row
                        $sql = "
                            INSERT INTO coregPivotWelDomain (Date, Domain, WelcomeType,
                            Sent, Clicks, Opens, Bounced, Complaints, Unsubscribed)
                            VALUES(?, ?, ?, {$aSent}, {$aClik}, {$aOpen}, {$aBnce},
                            {$aCmpt}, {$aUnsub})
                        ";

                    }

                    //echo $sql . "\n";

                    $saveStmt = $dbConn->prepare($sql);
                    $saveStmt->execute(array($dt, $domain, $type));

                } else {
                    $err = $chkStmt->errorInfo();
                    echo $err[2];
                }
            }
        }

    }

    //$dt = date('Y-m-d', strtotime($dt . ' +1 day'));

//}
$aggRecs = count($aggRows);
$dur = Util::get_microtime_duration($startTime, microtime(true));
$doneDt = date('Y-m-d');
Util::log_to_file($logFile, 'Aggregated: ', "dt: {$dt} recs: {$aggRecs}");
Util::log_to_file($logFile, 'Done: ', $dur);
echo 'Done: ' . date('Y-m-d H:i:s') . "\n";
echo 'Duration: ' . $dur . ' sec' . "\n";
$msg="Duration: {$dur}<br>
    Aggregate dt: {$dt}<Br>
    Records updated: {$aggRecs}<br>
";
Util::systemAlert($fileHandle." {$doneDt}", $msg);

