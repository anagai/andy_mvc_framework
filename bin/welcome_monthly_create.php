#!/usr/bin/php -q
<?php

require_once( __DIR__ . "/../lib/core/initialize.inc");

if(Util::isRunningPID()) {
    echo "Already running.\n";
    exit;
}

$startTime = microtime(true);

//==================================================================================
// Create next month coreg welcome and noloc welcome campaign dbs
//==================================================================================


$fileHandle = basename($_SERVER['PHP_SELF'],'.php');
$logFile = $fileHandle.'.log';

Util::log_to_file($logFile, 'Started', '');

$dbConn = Database::get_pdo_conn(DEFAULT_DB_SETTING);

// Make next month welcome db
$nxtMnth = date("Ym", strtotime("+1 month"));
$start = date('Y-m-d H:i:s');

$welCamp = "juk_welcome_{$nxtMnth}";
$desc = "Jobungo UK Welcome Email";
$subject = "\$CITY\$ Job Alert\$SPACE_FOR_FNAME\$";
$url = "http://ses13.jobungo.co.uk/no_really_admin/create_campaign.php?" .
    "sub-id=" . $welCamp . "&" .
    "description=" . rawurlencode($desc) . "&" .
    "csv=test_users_1.csv" . "&" .
    "start-time=" . rawurlencode($start) . "&" .
    "template-id=2&" .
    "campaign-subject=" . rawurlencode($subject) . "&" .
    "search-country=gb&" .
    "number-processes=1&ses-account=dedicated_certified&site=jobungouk&" .
    "send-profile=jobungo_uk_transactional";

$welResp = Util::curlReq($url, '', '', TRUE, '', 'stats_candy:duckpizzabluenobelium');

$welNoLocCamp = "juk_welcome_noloc_{$nxtMnth}";
$desc = "Jobungo UK No Location Welcome Email";
$subject = "\$FNAME_COMMA_NONE\$25+ new jobs near you";
$url = "http://ses13.jobungo.co.uk/no_really_admin/create_campaign.php?" .
    "sub-id=" . $welNoLocCamp . "&" .
    "description=" . rawurlencode($desc) . "&" .
    "csv=test_users_1.csv" . "&" .
    "start-time=" . rawurlencode($start) . "&" .
    "template-id=13&" .
    "campaign-subject=" . rawurlencode($subject) . "&" .
    "search-country=gb&" .
    "number-processes=1&ses-account=dedicated_certified&site=jobungouk&" .
    "send-profile=jobungo_uk_transactional";

$welNoLocResp = Util::curlReq($url, '', '', TRUE, '', 'stats_candy:duckpizzabluenobelium');

if($welResp['status']!=200 || $welNoLocResp['status']!=200) {
    $msg = "Failed to create next month campaign(s)<br>";
    $msg .= "WELCOME: ";
    if($welResp['status']==200) {
        $msg .= "Successfully created {$welCamp} campaign<br>";
    } else {
        $msg .= "Failed to create {$welCamp}<br>";
    }
    $msg .= "WELCOME RESP: " . $welResp['response'] . "<br>";
    $msg .= "NOLOC WELCOME: ";
    if($welNoLocResp['status']==200) {
        $msg .= "Successfully created {$welNoLocCamp} campaign<br>";
    } else {
        $msg .= "Failed to create {$welNoLocCamp}<br>";
    }
    $msg .= "NOLOC WELCOME RESP: " . $welNoLocResp['response'] . "<br>";
    Util::log_to_file($logFile,'Failed', str_replace('<br>', ' ', $msg));
    //echo str_replace('<br>',"\n",$msg);
    Util::systemAlert($fileHandle . " {$nxtMnth} welcome campaigns", $msg, '', false);
} else {
    $msg = "Successfully created welcome campaigns<br>";
    $msg .= "WELCOME: Created {$welCamp} campaign<br>";
    $msg .= "WELCOME RESP: " . $welResp['response'] . "<br>";
    $msg .= "NOLOC WELCOME: Created {$welNoLocCamp} campaign<br>";
    $msg .= "NOLOC WELCOME RESP: " . $welNoLocResp['response'] . "<br>";
    Util::log_to_file($logFile,'Success', str_replace('<br>', ' ', $msg));
    //echo str_replace('<br>',"\n",$msg);
    Util::systemAlert($fileHandle . " {$nxtMnth} welcome campaigns", $msg);
}