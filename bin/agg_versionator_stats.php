#!/usr/bin/php -q
<?php

require_once( __DIR__ . "/../lib/core/initialize.inc");

if(Util::isRunningPID()) {
    echo "Already running.\n";
    exit;
}

$startTime = microtime(true);

//==================================================================================
// Aggregate Versionator Stats
//==================================================================================

//echo "Started: " . date('Y-m-d H:i:s') . "\n";

$fileHandle = basename($_SERVER['PHP_SELF'],'.php');
$logFile = $fileHandle.'.log';

$args = getopt("p:");

$prevDay = false;

# Must pass numeric day interval argument and in range 1-5
if(!empty($args['p'])) {
    if(is_numeric($args['p']) && $args['p']>0) {
        $prevDay = $args['p'];
    } else {
        exit( "Usage: " . $argv[ 0 ] . " -p [Prev Day Interval]\n" );
    }

}

Util::log_to_file($logFile, 'Started', '');

$dbConn = Database::get_pdo_conn(DEFAULT_DB_SETTING);

// Ensure no lock on table read on mckinley
//$dbConn->exec('SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;');

//Util::startTimer();

//$redis=new Redis();
//$redis->connect(‘/var/run/redis/redis.sock’);

// Aggregate current day
$stats = array();

if($prevDay) {
    // Aggregate previous day if param passed
    $frDt = date('Y-m-d', strtotime("-{$prevDay} day"));
} else {
    // Aggregate current day
    $frDt = date('Y-m-d');
}

$toDt = date('Y-m-d', strtotime($frDt . ' +1 day'));

//echo "frDt: {$frDt} toDt: {$toDt}\n";

// Aggregate search_with_results data
$sql = "SELECT email, ip, page_number, version_id, results_on_page
    FROM search_with_results WHERE date_searched>=? AND date_searched<?";

$stmt['results'] = $dbConn->prepare($sql);
$stmt['results']->execute(array($frDt, $toDt));

$searchCount = $stmt['results']->rowCount();

//echo "search_with_results count: " . $searchCount . "\n";

Util::log_to_file($logFile, 'Total search records', $searchCount);

//$prevDt = date('Y-m-d', strtotime("-1 day"));
// Remove unique search records for prev day to truncate table
$sql = "TRUNCATE search_unique_email;";

$stmt['deluniq'] = $dbConn->prepare($sql);
$stmt['deluniq']->execute();

$delUniqueCount = $stmt['deluniq']->rowCount();

//echo "Deleted unique searchs for {$prevDt} : {$delUniqueCount}\n";

Util::log_to_file($logFile, 'Deleted unique search records', "");

/*
  `version_id` int(11) NOT NULL,
  `page_number` smallint(6) NOT NULL,
  `unique_visitors` int(11) NOT NULL,
  `page_views` int(11) NOT NULL,
  `job_results` int(11) NOT NULL,
  `organic_clicks` int(11) NOT NULL,
  `sponsored_clicks` int(11) NOT NULL,
*/

 $uniqSql = "INSERT IGNORE INTO search_unique_email (search_date, version_id, page_number, email, ip)
                VALUES (?,?,?,?,?);
    ";

function initStat($key, $version, $page) {
    global $stats;

    if(!isset($stats[$key])) {
        $stats[$key] = array('ver_id'=>$version, 'page'=>$page);
    }

}

while($resultRow = $stmt['results']->fetch(PDO::FETCH_ASSOC)) {

    //print_r($resultRow);

    $stmt['unique'] = $dbConn->prepare($uniqSql);
    $stmt['unique']->execute(array($frDt, $resultRow['version_id'],
            $resultRow['page_number'], $resultRow['email'], $resultRow['ip']));

    if($stmt['unique']->rowCount()>0) {
        //echo "is unique\n";
        $unique = 1;
    } else {
        //echo "not unique\n";
        $unique = 0;
    }
    $key = dechex(crc32(strtolower($resultRow['version_id'].$resultRow['page_number'])));
    initStat($key, $resultRow['version_id'], $resultRow['page_number']);
    $stats[$key]['views']++;
    $stats[$key]['results'] += $resultRow['results_on_page'];
    $stats[$key]['uniques'] += $unique;
}

//echo 'stats: ';
//print_r($stats);

unset($stmt['results']);
unset($stmt['unique']);

// Aggregate no_result_details
/*
$sql = "SELECT email, ip, page_number, version_id
    FROM no_result_details WHERE date_searched>=? AND date_searched<?";

$stmt['noresults'] = $dbConn->prepare($sql);
$stmt['noresults']->execute(array($frDt, $toDt));

while($noResultRow = $stmt['noresults']->fetch(PDO::FETCH_ASSOC)) {

    $stmt['unique'] = $dbConn->prepare($uniqSql);
    $stmt['unique']->execute(array($frDt, $noResultRow['version_id'],
            $noResultRow['page_number'], $noResultRow['email'], $noResultRow['ip']));

    if($stmt['unique']->rowCount()>0) {
        $unique = 1;
    } else {
        $unique = 0;
    }

    $key = dechex(crc32(strtolower($noResultRow['version_id'].$noResultRow['page_number'])));
    initStat($key, $noResultRow['version_id'], $noResultRow['page_number']);
    $stats[$key]['views']++;
    $stats[$key]['uniques'] += $unique;
}

unset($stmt['noresults']);
unset($stmt['unique']);
*/

// Aggregate clicks
$sql = "SELECT page_number, version_id, sponsored
    FROM job_click WHERE date_clicked>=? AND date_clicked<?";

$stmt['clicks'] = $dbConn->prepare($sql);
$stmt['clicks']->execute(array($frDt, $toDt));

$clicksCount = $stmt['clicks']->rowCount();

//echo "Total clicks for {$frDt} : {$clicksCount}\n";

Util::log_to_file($logFile, 'Total click records', $clicksCount);

while($clickRow = $stmt['clicks']->fetch(PDO::FETCH_ASSOC)) {

    $sponClick = 0;
    $orgClick = 0;

    if($clickRow['sponsored']) {
        $sponClick = 1;
    } else {
        $orgClick = 1;
    }

    $key = dechex(crc32(strtolower($clickRow['version_id'].$clickRow['page_number'])));
    initStat($key, $clickRow['version_id'], $clickRow['page_number']);
    $stats[$key]['spon_click'] += $sponClick;
    $stats[$key]['org_click'] += $orgClick;

}

//print_r($stats);

$delSql = "DELETE FROM versionator_stats WHERE search_date=? AND version_id=?
            AND page_number=?
";

$insSql = "
    INSERT INTO versionator_stats (search_date, version_id, page_number,
        unique_visitors, page_views, job_results, organic_clicks, sponsored_clicks)
        VALUES (?,?,?,?,?,?,?,?);
";

foreach($stats as $verStat) {

    // Delete stat
    $stmt['delstat'] = $dbConn->prepare($delSql);
    $stmt['delstat']->execute(array(
        $frDt,
        $verStat['ver_id'],
        $verStat['page']
    ));

    $totViews = $verStat['views']
        ? $verStat['views']
        : 0;

    $totUniqViews = $verStat['uniques']
        ? $verStat['uniques']
        : 0;

    $totResults = $verStat['results']
        ? $verStat['results']
        : 0;

    $totOrgClicks = $verStat['org_click']
        ? $verStat['org_click']
        : 0;

    $totSponClicks = $verStat['spon_click']
        ? $verStat['spon_click']
        : 0;

    $stmt['insert'] = $dbConn->prepare($insSql);
    $stmt['insert']->execute(array(
        $frDt,
        $verStat['ver_id'],
        $verStat['page'],
        $totUniqViews,
        $totViews,
        $totResults,
        $totOrgClicks,
        $totSponClicks
    ));

}

$aggRecs = count($stats);

unset($stats);
unset($stmt);

$dur = Util::get_microtime_duration($startTime, microtime(true));
$doneDt = date('Y-m-d H:i:s');
$doneDt2 = date('Y-m-d');
//echo 'Done: ' . $doneDt . "\n";
//echo 'Duration: ' . $dur . "\n";
$mem = number_format(memory_get_usage()/1024,1).'kb  Peak: ('.number_format(memory_get_peak_usage()/1024,1).'kb)';
Util::log_to_file($logFile, 'Done', "agg dt: {$frDt} agg recs: {$aggRecs} mem: {$mem} dur: {$dur}");

$msg="Duration: {$dur}<br>
    Memory: {$mem}<br>
    Aggregate dt: {$frDt}<Br>
    Records Added: {$aggRecs}<br>
";

Util::systemAlert($fileHandle." {$doneDt2}", $msg);
