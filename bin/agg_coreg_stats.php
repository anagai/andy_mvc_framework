#!/usr/bin/php -q
<?php

require_once( __DIR__ . "/../lib/core/initialize.inc");

if(Util::isRunningPID()) {
    echo "Already running.\n";
    exit;
}

$startTime = microtime(true);

//==================================================================================
//Aggregate Coreg Daily and Partner Pivot Stats
//==================================================================================
//echo "Started: " . date('Y-m-d H:i:s') . "\n";

$fileHandle = basename($_SERVER['PHP_SELF'],'.php');
$logFile = $fileHandle.'.log';

$args = getopt("p:");

$prevDay = false;

# Must pass numeric day interval argument and in range 1-5
if(!empty($args['p'])) {
    if(is_numeric($args['p']) && $args['p']>0) {
        $prevDay = $args['p'];
    } else {
        exit( "Usage: " . $argv[ 0 ] . " -p [Prev Day Interval]\n" );
    }

}

Util::log_to_file($logFile, 'Started', '');

$dbConn = Database::get_pdo_conn(DEFAULT_DB_SETTING);

// Ensure no lock on table read on mckinley
//$dbConn->exec('SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;');

//Util::startTimer();

//$redis=new Redis();
//$redis->connect(‘/var/run/redis/redis.sock’);

$stats = array();

if($prevDay) {
    // Aggregate previous day if param passed
    $frDt = date('Y-m-d', strtotime("-{$prevDay} day"));
} else {
    // Aggregate current day
    $frDt = date('Y-m-d');
}

$toDt = date('Y-m-d', strtotime($frDt . '+1 day'));

unset($stmt);

//echo "frDt: {$frDt} toDt: {$toDt}\n";

// Aggregate search_with_results data
$sql = "SELECT partner, source, accepted, is_mailable, reject_reason_id, rejection_reason
    FROM coreg WHERE date_created>=? AND date_created<?";

$stmt['results'] = $dbConn->prepare($sql);
$stmt['results']->execute(array($frDt, $toDt));

$searchCount = $stmt['results']->rowCount();

//echo "coreg count: " . $searchCount . "\n";

Util::log_to_file($logFile, 'Total coreg to process', $searchCount);

function initStat($partner='', $source='') {

    return array(
        "partner"=>$partner,
        "source"=>$source,
        "Received"=>0,
        "Accepted"=>0,
        "Rejected"=>0,
        "NoLoc"=>0,
        "Mailable"=>0,
        "Invalid"=>0,
        "Dnm"=>0,
        "Within7"=>0,
        "Within60"=>0,
    );

}

$partnerStat = array();
$dailyStat = initStat();

while($resultRow = $stmt['results']->fetch(PDO::FETCH_ASSOC)) {

    // Aggregate daily stat
    $dailyStat["Received"]++;
    $dailyStat['Accepted'] += $resultRow['accepted'];
    $dailyStat['Rejected'] += !empty($resultRow['reject_reason_id'])
        ? 1
        : 0;
    $dailyStat['NoLoc'] += $resultRow['reject_reason_id']==6
        ? 1
        : 0;
    $dailyStat['Mailable'] += $resultRow['is_mailable'];
    $dailyStat['Invalid'] += $resultRow['reject_reason_id']>0 && $resultRow['reject_reason_id']<4
        ? 1
        : 0;
    $dailyStat['Dnm'] += $resultRow['reject_reason_id']==5 ||
        ($resultRow['reject_reason_id']>6 && $resultRow['reject_reason_id']<13)
        ? 1
        : 0;
    $dailyStat['Within7'] += $resultRow['reject_reason_id']==14
        ? 1
        : 0;
    $dailyStat['Within60'] += $resultRow['reject_reason_id']==13
        ? 1
        : 0;

    // Aggregate partner source stat
    $key = dechex(crc32(strtolower($resultRow['partner'].$resultRow['source'])));

    if(!isset($partnerStat[$key])) {
        // initialize partner stat record
        $partnerStat[$key] = initStat($resultRow['partner'], $resultRow['source']);
    }

    $partnerStat[$key]['Received']++;
    $partnerStat[$key]['Accepted'] += $resultRow['accepted'];
    $partnerStat[$key]['Rejected'] += !empty($resultRow['reject_reason_id'])
        ? 1
        : 0;
    $partnerStat[$key]['NoLoc'] += $resultRow['reject_reason_id']==6
        ? 1
        : 0;
    $partnerStat[$key]['Mailable'] += $resultRow['is_mailable'];

    $partnerStat[$key]['Invalid'] += $resultRow['reject_reason_id']>0 && $resultRow['reject_reason_id']<4
        ? 1
        : 0;
    $partnerStat[$key]['Dnm'] += $resultRow['reject_reason_id']==5 ||
        ($resultRow['reject_reason_id']>6 && $resultRow['reject_reason_id']<13)
        ? 1
        : 0;
    $partnerStat[$key]['Within7'] += $resultRow['reject_reason_id']==14
        ? 1
        : 0;
    $partnerStat[$key]['Within60'] += $resultRow['reject_reason_id']==13
        ? 1
        : 0;
}

//echo "daily stat<br>";
//print_r($dailyStat);
//echo "partnerstat<br>";
//print_r($partnerStat);
Util::log_to_file($logFile, 'DailyStat', print_r($dailyStat,true));
Util::log_to_file($logFile, 'PartnerStat', print_r($partnerStat,true));

// Only update if anything was processed
if($searchCount>0) {

    // Update coreg daily pivot data
    $findSql = "SELECT * from coregPivotDaily WHERE `Date`=?";
    $stmt['find'] = $dbConn->prepare($findSql);
    $stmt['find']->execute(array($frDt));

    if($stmt['find']->rowCount()>0) {
        $updSql = "UPDATE coregPivotDaily SET
            Received=?,
            Accepted=?,
            Rejected=?,
            NoLoc=?,
            Mailable=?,
            Invalid=?,
            Dnm=?,
            Within7=?,
            Within60=?
            WHERE `Date`=?;
        ";
        $stmt['update'] = $dbConn->prepare($updSql);
        $stmt['update']->execute(array(
            $dailyStat["Received"],
            $dailyStat['Accepted'],
            $dailyStat['Rejected'],
            $dailyStat['NoLoc'],
            $dailyStat['Mailable'],
            $dailyStat['Invalid'],
            $dailyStat['Dnm'],
            $dailyStat['Within7'],
            $dailyStat['Within60'],
            $frDt));

    } else {
        $insSql = "INSERT IGNORE INTO coregPivotDaily (
            `Date`,
            Received,
            Accepted,
            Rejected,
            NoLoc,
            Mailable,
            Invalid,
            Dnm,
            Within7,
            Within60) VALUES (?,?,?,?,?,?,?,?,?,?);
        ";
        $stmt['insert'] = $dbConn->prepare($insSql);
        $stmt['insert']->execute(array(
            $frDt,
            $dailyStat["Received"],
            $dailyStat['Accepted'],
            $dailyStat['Rejected'],
            $dailyStat['NoLoc'],
            $dailyStat['Mailable'],
            $dailyStat['Invalid'],
            $dailyStat['Dnm'],
            $dailyStat['Within7'],
            $dailyStat['Within60']
        ));
    }

    unset($stmt);

    // Update coreg partner pivot data
    foreach($partnerStat as $psRow) {
        $findSql = "SELECT * from coregPivotPartner WHERE `Date`=?
            AND Partner=? AND Source=?;
            ";
        $stmt['find'] = $dbConn->prepare($findSql);
        $stmt['find']->execute(array($frDt, $psRow['partner'], $psRow['source']));

        if($stmt['find']->rowCount()>0) {
            $updSql = "UPDATE coregPivotPartner SET
                Received=?,
                Accepted=?,
                Rejected=?,
                NoLoc=?,
                Mailable=?,
                Invalid=?,
                Dnm=?,
                Within7=?,
                Within60=?
                WHERE `Date`=? AND Partner=? AND Source=?;
            ";
            $stmt['update'] = $dbConn->prepare($updSql);
            $stmt['update']->execute(array(
                $psRow["Received"],
                $psRow['Accepted'],
                $psRow['Rejected'],
                $psRow['NoLoc'],
                $psRow['Mailable'],
                $psRow['Invalid'],
                $psRow['Dnm'],
                $psRow['Within7'],
                $psRow['Within60'],
                $frDt, $psRow['partner'], $psRow['source']));
        } else {
            $insSql = "INSERT IGNORE INTO coregPivotPartner (
                `Date`,
                Received,
                Partner,
                Source,
                Accepted,
                Rejected,
                NoLoc,
                Mailable,
                Invalid,
                Dnm,
                Within7,
                Within60) VALUES (?,?,?,?,?,?,?,?,?,?,?,?);
            ";
            $stmt['insert'] = $dbConn->prepare($insSql);
            $stmt['insert']->execute(array(
                $frDt,
                $psRow["Received"],
                $psRow['partner'],
                $psRow['source'],
                $psRow['Accepted'],
                $psRow['Rejected'],
                $psRow['NoLoc'],
                $psRow['Mailable'],
                $psRow['Invalid'],
                $psRow['Dnm'],
                $psRow['Within7'],
                $psRow['Within60']
            ));
        }
    }
}

$aggRecs = count($partnerStat);

unset($partnerStat);
unset($dailyStat);
unset($stmt);

$dur = Util::get_microtime_duration($startTime, microtime(true));
$doneDt = date('Y-m-d H:i:s');
$doneDt2 = date('Y-m-d');
//echo 'Done: ' . $doneDt . "\n";
//echo 'Duration: ' . $dur . "\n";
$mem = number_format(memory_get_usage()/1024,1).'kb  Peak: ('.number_format(memory_get_peak_usage()/1024,1).'kb)';
Util::log_to_file($logFile, 'Aggregated', "dt: {$frDt} recs: {$aggRecs}");
Util::log_to_file($logFile, 'Memory', $mem);
Util::log_to_file($logFile, 'Done', $dur);

$msg="Duration: {$dur}<br>
    Memory: {$mem}<br>
    Aggregate dt: {$frDt}<Br>
    Partner Pivot Records Updated: {$aggRecs}<br>
";

Util::systemAlert($fileHandle." {$doneDt2}", $msg);
