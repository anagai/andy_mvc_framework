#!/usr/bin/php -q
<?php

    // vim: ai ts=4 sts=4 et sw=4 tw=79

    /*
     * Created on Dec 6, 2012
     * Written By Sung Hwan Ahn
     * Observed By Andy Nagai
     *
     */

    require_once( __DIR__ . "/../lib/core/initialize.inc");

    //if(Util::isRunningPID()) {
    //    echo "Already running.\n";
    //    exit;
    //}

    echo "==============================" . "\n";
    echo "  Jobungo UK Coreg Injection  " . "\n";
    echo "==============================" . "\n\n";

    $fileHandle = basename($_SERVER['PHP_SELF'],'.php');
    $logFile = $fileHandle.'.log';
    $errFile = $fileHandle.'_error.log';

    date_default_timezone_set("America/Los_Angeles");

    Util::load_model('coreginjection', FALSE);

    //$injections_model = new CoregInjectionModel('201307_02_29');
    //$args = getopt("f:b:w:");
    $args = getopt("b:w:");
    if(empty($args['b'])) {
       //exit( "Usage: " . $argv[ 0 ] . " -f [injection_xxxxx] -b a -w 1\n" );
       exit( "Usage: " . $argv[ 0 ] . " -b a -w 1\n" );
    }

    echo "settings: " . DEFAULT_DB_SETTING . "\n";

    $conn = Database::get_pdo_conn(DEFAULT_DB_SETTING);

    //$injectTable = $args['f'];
    $injectTable = '';
    $batch = $args['b'];
    $nowrm = !empty($args['w'])
        ? true
        : false;

    echo 'inject: ' . $injectTable . ' batch: ' . $batch ." nowrm: {$nowrm}\n";

    //$limit = 10000;

    // get command line arguments
    /*
    $arguments = getopt("s:e:");

    if (isset($arguments["s"])) {
        $start = $process = $arguments["s"];
    } else {
        $start = $process = 0;
    }

    if (isset($arguments["e"])) {
        $end = $arguments["e"];
    } else {
        $end = $injections_model->count_all();
    }
*/
    $today = date('Ymd');

    Util::log_to_file($logFile, 'Started', 'inject: ' . $injectTable);
    $start = date('Y-m-d H:i:s');
    echo "Started: " . $start . "\n";
    //$injections = $injections_model->find_all_limit($start, $limit);

    // truncate coregInjection. Only hold 100 days of injections
    //$dt = date("Y-m-d", strtotime("-100 day"));
    //$sql = "DELETE FROM coregInjection WHERE dateInjected<?";

    //$stmt = $conn->prepare($sql);
    //$stmt->execute(array($dt));
    //$affected = $stmt->rowCount();

    //echo "Deleted > 100 day old corgInjection records: " . $affected ."\n";
    //echo "Processing batch: " . $batch . "\n";
    unset($stmt);

    $sql = "
        SELECT * FROM injection_temp WHERE injected=0 and batch=?
    ";

    $stmt = $conn->prepare($sql);
    $stmt->execute(array($batch));

    echo "To Inject: {$stmt->rowCount()}\n";

    //while ($end != $process) {
    echo $sql . "\n";
     $stats=array(
        'now'=>0,
        'ps'=>0,
        'start'=>(time()-1),
        'users'=>0
        );
    $cnt = 1;

    $ch = curl_init();

    function removeUnicode($inp) {
        $out = trim(preg_replace('/[^\x20-\x7e\t]/','',$inp));
        return $out;
    }

    while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        ++$stats['users'];
        //$process = $start + count($injections);

        //Util::log_to_file('coreg_injection_'. $today . '.log', 'Processing', $start . " - " . $process . " | End: " . $end);
        //print ("Processing: " . $start . " - " . $process . " | End: " . $end . "\n");

        //foreach ($injections as $injection) {

            $url = "http://www.jobungo.co.uk/partnersfeed?" .
            //$url = "http://andy-jobungouk.restdev.com/partnersfeed?" .
                   "email=" . urlencode(removeUnicode($row['email'])) . "&" .
                   "fname=" . urlencode(removeUnicode($row['first_name'])) . "&" .
                   "lname=" . urlencode(removeUnicode($row['last_name'])) . "&" .
                   "city=" . urlencode(removeUnicode($row['city'])) . "&" .
                   "zip=" . urlencode(removeUnicode($row['postal_code'])) . "&" .
                   "pid=" . urlencode($row['partner']) . "&" .
                   "source=" . urlencode($row['source']) . "&" .
                   "ip=" . urlencode($row['opt_in_ip']) . "&" .
                   "date=" . urlencode($row['date_opt_in']) . "&" .
                   "nowrm=" . $nowrm;
                   //"inj=" . urlencode($injectTable);

                $curl_options = array(
                    CURLOPT_URL => $url,
                    CURLOPT_HEADER => 0,
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_ENCODING => 'gzip,deflate',
            );

            curl_setopt_array( $ch, $curl_options );
            $output = curl_exec( $ch );

            //$output = Util::curl_request($url);
            Util::log_to_file($logFile, 'Processed', 'inject: ' . $injectTable  . ' | ' .
                $url . ' | ' . $row['email'] . " | " . $output );
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if($httpCode!==200) {
                Util::log_to_file($errFile, 'Error', 'inject: ' . $injectTable  . ' | ' .
                $url . ' | ' . $row['email'] . " | httpcode: {$httpCode} out: " . $output );
            }
        //}

        //$start += $limit;
        //$injections = $injections_model->find_all_limit($start, $limit);
        $injectId =  $row['id']
            ? $row['id']
            : 0;

        $sql = "UPDATE injection_temp SET injected=1 WHERE id=" . $injectId;
        $updStmt = $conn->prepare($sql);
        $updStmt->execute();

        //echo $row['email'] . ' - ' . $row['id'] . ' - ' . $cnt . "\n";

        //if($stats['now']!=time()){
                    $stats['now']=time();
                    $stats['ps']=($stats['users']/($stats['now']-$stats['start']));
                    printf("\rInject Rate: %.2f/s Emails: %d",
                            $stats['ps'],
                            $stats['users']
                    );
        //}


        $cnt++;
    }

    curl_close($ch);

    print ("\nStarted: " . $start . "\n");
    $ended = date('Y-m-d H:i:s');
    $dur = Util::get_microtime_duration(strtotime($start),strtotime($ended));
    print ("Ended: " . $ended . "\n");
    echo "Dur: " . $dur . "\n\n";

    Util::log_to_file($logFile, 'Ended',
        'injected: ' . $injectTable . ' rate: ' . $stats['ps'] . ' total: ' . $stats['users'] . ' dur: ' . $dur);

    echo "select partner, sum(accepted) as accepted, sum(mailable) as mailable, " .
        "count(if(rejectId=6,1,Null)) as 'no loc reject mailable', " .
        "count(if(rejectId>0,1,Null)) as 'total rejected' from coregInjection WHERE listName='{$injectTable}' group by partner;\n\n";

    echo "select count(a.id) as rejected, b.description  from coregInjection as a " .
         "inner join reject_reason as b on a.rejectId=b.id where listName='{$injectTable}' group by rejectId;\n";

    if(isset($database)) { $database->close_connection(); }
?>
