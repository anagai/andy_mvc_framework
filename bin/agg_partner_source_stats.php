#!/usr/bin/php -q
<?php

require_once( __DIR__ . "/../lib/core/initialize.inc");

if(Util::isRunningPID()) {
    //echo "Already running.\n";
    exit;
}

$startTime = microtime(true);

//==================================================================================
// Aggregate partner_source_stats data for monthly report
//==================================================================================
//echo "Started: " . date('Y-m-d H:i:s') . "\n";

$fileHandle = basename($_SERVER['PHP_SELF'],'.php');
$logFile = $fileHandle.'.log';

Util::log_to_file($logFile, 'Started', '');

$dbConn = Database::get_pdo_conn(DEFAULT_DB_SETTING);

// Ensure no lock on table read on mckinley
//$dbConn->exec('SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;');

//Util::startTimer();

//$redis=new Redis();
//$redis->connect(‘/var/run/redis/redis.sock’);

// Aggregate current day
$stats = array();

$frDt = date('Y-m-d', strtotime("-1 day"));
//$frDt = date('Y-m-d');
$toDt = date('Y-m-d', strtotime($frDt . '+1 day'));

unset($stmt);

//echo "frDt: {$frDt} toDt: {$toDt}\n";

function initStat($partner='', $source='') {

    return array(
        "partner"=>$partner,
        "source"=>$source,
        "page_views"=>0,
        "unique_views"=>0,
        "job_clicks"=>0,
        "unique_clicks"=>0
    );

}

$partnerStat = array();

//************ Aggregate search_with_results data ************

// delete prev day unique searches
$delSql = "DELETE FROM partner_source_search
    WHERE search_date>=? AND search_date<?";

$stmt['delete'] = $dbConn->prepare($delSql);
$stmt['delete']->execute(array($frDt, $toDt));

Util::log_to_file($logFile, "Deleted unique searches for {$frDt}", $stmt['delete']->rowCount());

$sql = "SELECT user_id, email, partner, source FROM search_with_results
    WHERE date_searched>=? AND date_searched<?";

$stmt['results'] = $dbConn->prepare($sql);
$stmt['results']->execute(array($frDt, $toDt));

$searchCount = $stmt['results']->rowCount();

//echo "search with result count: " . $searchCount . "\n";

Util::log_to_file($logFile, 'Total searches with results to process', $searchCount);

while($resultRow = $stmt['results']->fetch(PDO::FETCH_ASSOC)) {

    // Aggregate partner source stat
    if($resultRow['user_id']) {

        $key = dechex(crc32(strtolower($resultRow['partner'].$resultRow['source'])));

        if(!isset($partnerStat[$key])) {
            // initialize partner stat record
            $partnerStat[$key] = initStat($resultRow['partner'], $resultRow['source']);
        }

        $partnerStat[$key]['page_views']++;

        $uniqSql = "INSERT IGNORE INTO partner_source_search
            (search_date, partner, source, email) VALUES (?,?,?,?)";

        $stmt['unique'] = $dbConn->prepare($uniqSql);
        $stmt['unique']->execute(array($frDt, $resultRow['partner'],
            $resultRow['source'], $resultRow['email']));

        if($stmt['unique']->rowCount()>0) {
            $partnerStat[$key]['unique_views']++;
        }

    }
}

unset($stmt);

//************ Aggregate no_result_details data ******************

$sql = "SELECT user_id, email, partner, source FROM no_result_details
    WHERE date_searched>=? AND date_searched<?";

$stmt['noresults'] = $dbConn->prepare($sql);
$stmt['noresults']->execute(array($frDt, $toDt));

$searchCount = $stmt['noresults']->rowCount();

//echo "search with no results count: " . $searchCount . "\n";

Util::log_to_file($logFile, 'Total searches with no results to process', $searchCount);

while($resultRow = $stmt['noresults']->fetch(PDO::FETCH_ASSOC)) {

    // Aggregate partner source stat
    if($resultRow['user_id']) {

        $key = dechex(crc32(strtolower($resultRow['partner'].$resultRow['source'])));

        if(!isset($partnerStat[$key])) {
            // initialize partner stat record
            $partnerStat[$key] = initStat($resultRow['partner'], $resultRow['source']);
        }

        $partnerStat[$key]['page_views']++;

        $uniqSql = "INSERT IGNORE INTO partner_source_search
            (search_date, partner, source, email) VALUES (?,?,?,?)";

        $stmt['unique'] = $dbConn->prepare($uniqSql);
        $stmt['unique']->execute(array($frDt, $resultRow['partner'],
            $resultRow['source'], $resultRow['email']));

        if($stmt['unique']->rowCount()>0) {
            $partnerStat[$key]['unique_views']++;
        }

    }
}

unset($stmt);

//************ Aggregate job_click data ******************

// delete prev day unique job clicks
$delSql = "DELETE FROM partner_source_click
    WHERE search_date>=? AND search_date<?";

$stmt['delete'] = $dbConn->prepare($delSql);
$stmt['delete']->execute(array($frDt, $toDt));

Util::log_to_file($logFile, "Deleted unique clicks for {$frDt}", $stmt['delete']->rowCount());

$sql = "SELECT user_id, email, partner, source FROM job_click
    WHERE date_clicked>=? AND date_clicked<?";

$stmt['clicks'] = $dbConn->prepare($sql);
$stmt['clicks']->execute(array($frDt, $toDt));

$clickCount = $stmt['clicks']->rowCount();

//echo "job clicks count: " . $clickCount . "\n";

Util::log_to_file($logFile, 'Total job clicks to process', $clickCount);

while($resultRow = $stmt['clicks']->fetch(PDO::FETCH_ASSOC)) {

    // Aggregate partner source stat
    if($resultRow['user_id']) {

        $key = dechex(crc32(strtolower($resultRow['partner'].$resultRow['source'])));

        if(!isset($partnerStat[$key])) {
            // initialize partner stat record
            $partnerStat[$key] = initStat($resultRow['partner'], $resultRow['source']);
        }

        $partnerStat[$key]['job_clicks']++;

        $uniqSql = "INSERT IGNORE INTO partner_source_click
            (search_date, partner, source, email) VALUES (?,?,?,?)";

        $stmt['unique'] = $dbConn->prepare($uniqSql);
        $stmt['unique']->execute(array($frDt, $resultRow['partner'],
            $resultRow['source'], $resultRow['email']));

        if($stmt['unique']->rowCount()>0) {
            $partnerStat[$key]['unique_clicks']++;
        }

    }
}

//print_r($partnerStat);

$aggRecs = count($partnerStat);

// Update agg records
if($aggRecs>0) {

    foreach($partnerStat as $psRow) {

        $findSql = "SELECT * from partner_source_stats WHERE search_date=?
            AND partner=? AND source=?;
            ";

        $stmt['find'] = $dbConn->prepare($findSql);
        $stmt['find']->execute(array($frDt, $psRow['partner'], $psRow['source']));

        if($stmt['find']->rowCount()>0) {
            $updSql = "UPDATE partner_source_stats SET
                page_views=?,
                unique_views=?,
                job_clicks=?,
                unique_clicks=?
                WHERE search_date=? AND partner=? AND source=?;
            ";
            $stmt['update'] = $dbConn->prepare($updSql);
            $stmt['update']->execute(array(
                $psRow["page_views"],
                $psRow['unique_views'],
                $psRow['job_clicks'],
                $psRow['unique_clicks'],
                $frDt, $psRow['partner'], $psRow['source']));
        } else {
            $insSql = "INSERT IGNORE INTO partner_source_stats (
                search_date,
                partner,
                source,
                page_views,
                unique_views,
                job_clicks,
                unique_clicks
                ) VALUES (?,?,?,?,?,?,?);
            ";
            $stmt['insert'] = $dbConn->prepare($insSql);
            $stmt['insert']->execute(array(
                $frDt,
                $psRow["partner"],
                $psRow["source"],
                $psRow["page_views"],
                $psRow['unique_views'],
                $psRow['job_clicks'],
                $psRow['unique_clicks']
            ));

        }
    }
}

unset($partnerStat);
unset($stmt);

$dur = Util::get_microtime_duration($startTime, microtime(true));
$doneDt = date('Y-m-d H:i:s');
//echo 'Done: ' . $doneDt . "\n";
//echo 'Duration: ' . $dur . "\n";
$mem = number_format(memory_get_usage()/1024,1).'kb  Peak: ('.number_format(memory_get_peak_usage()/1024,1).'kb)';
Util::log_to_file($logFile, 'Aggregated', "dt: {$frDt} recs: {$aggRecs}");
Util::log_to_file($logFile, 'Memory', $mem);
Util::log_to_file($logFile, 'Done', $dur);

$msg="Duration: {$dur}<br>
    Memory: {$mem}<br>
    Aggregate dt: {$frDt}<Br>
    partner_source_stats records updated: {$aggRecs}<br>
";

Util::systemAlert($fileHandle." {$doneDt}", $msg);
