#!/usr/bin/php -q
<?php

require_once( __DIR__ . "/../lib/core/initialize.inc");

if(Util::isRunningPID()) {
    echo "Already running.\n";
    exit;
}

//===============================================
// export previous day clickers to csv
//===============================================
// email, first_name, city, postal_code, keyword, partner, source, date_opt_in, opt_in_ip

$fileHandle = basename($_SERVER['PHP_SELF'],'.php');
$logFile = "{$fileHandle}.log";

Util::log_to_file($logFile, 'Started', '');
$startTime = microtime(true);

$conn = Database::get_pdo_conn(DEFAULT_DB_SETTING);

$frDt = date('Y-m-d', strtotime("-1 day"));
$fileDt = date('Ymd', strtotime($frDt));

$sql = "SELECT email, first_name, city, postal_code,
    keyword, partner, source, date_opt_in, opt_in_ip, user_id
    FROM clickers90 WHERE date_updated='{$frDt}'";

$stmt['clickers'] = $conn->query($sql);

$total = $stmt['clickers']->rowCount();

$procCnt = 0;
$csvCnt = 0;

$userSql = "SELECT is_mailable from user WHERE id=?";
$csvFile = "mailable_clickers_{$fileDt}.csv";

$stmt['user'] = $conn->prepare($userSql);

$fp = fopen(CSV_PATH.$csvFile, 'w');

fputcsv($fp, array('email', 'first_name', 'city', 'postal_code',
    'keyword', 'partner', 'source', 'date_opt_in', 'opt_in_ip'));

$procStart = microtime(true);

while($row = $stmt['clickers']->fetch(PDO::FETCH_ASSOC)) {
    if($row['user_id'] && !empty($row['email'])) {
        $stmt['user']->execute(array($row['user_id']));
        $user = $stmt['user']->fetch(PDO::FETCH_ASSOC);
        if($user['is_mailable']) {
            array_pop($row);
            fputcsv($fp, $row);
            $csvCnt++;
        }
    }
    $nowTime = microtime(true);
    $procCnt++;
    $remain = $total - $procCnt;
    $rate = $procCnt/($nowTime-$procStart);
    echo "\rRate: " . $rate .
        "/sec {$procCnt} out of {$total} ETA: " . ($remain/$rate/60/60);
}
fclose($fp);

$diablo = Util::get_ssh_connection('DIABLO');
$ret = $diablo->put("/home/ses/juk_clicker/{$csvFile}", CSV_PATH.$csvFile, NET_SFTP_LOCAL_FILE);

echo 'file copied: ';
print_r($ret);

Util::log_to_file($logFile, 'Done', "clickers: {$total} " .
    "rate: {$rate}/sec csvcnt: {$csvCnt} dur: " . Util::get_microtime_duration($startTime, microtime(true)));
echo "\n";



