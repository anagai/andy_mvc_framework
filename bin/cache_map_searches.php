#!/usr/bin/php -q
<?php
// vim: ai ts=4 sts=4 et sw=4 tw=79

include_once( __DIR__ . "/../lib/core/initialize.inc" );

/* This is Steve's depricated code

$hits = array();

// Get last 120 hits from user_search_details
$sql = <<<END_OF_SQL
    SELECT job, city, state, zip
    FROM user_search_details
    ORDER BY id DESC
    LIMIT 120
END_OF_SQL;
$results = $db->query( $sql );
$fields = array(
    "job",
    "city",
    "state",
    "zip",
);
while( $row = $results->fetch_assoc() ) {
    $this_hit = array();

    // Populate fields
    foreach( $fields as $field ) {
        $this_hit[ $field ] = $row[ $field ];
    }

    // Store hit data
    $hits[] = $this_hit;
}

// Geocode with our own DB
$good_hits = array();
foreach( $hits as $hit ) {
    // Extract data for hit
    $job = $hit[ "job" ];
    $city = $hit[ "city" ];
    $state = $hit[ "state" ];
    $zip = $hit[ "zip" ];

    // Skip if no job or city
    if( !$job || !$city ) {
        continue;
    }

    // Build clauses
    $state_clause = $state ? "AND region = '$state'" : "";
    $zip_clause = $zip ? "AND region = '$zip'" : "";

    // Get long/lat
    $sql = <<<END_OF_SQL
        SELECT longitude, latitude
        FROM geocode_location
        WHERE city = "$city"
            $state_clause
            $zip_clause
        LIMIT 1
END_OF_SQL;
    $results = $db->query( $sql );
    $row = $results->fetch_assoc();
    $hit[ "longitude" ] = $row[ "longitude" ];
    $hit[ "latitude" ] = $row[ "latitude" ];

    // Create a pretty-printed location
    $hit[ "location" ] = $city . ($state ? ", $state" : "");

    // Stuff for later
    $good_hits[] = $hit;
}

// Cache JSON in settings: job, city, state, lat, long
$search_json = escape_string( json_encode( $good_hits ) );
$sql = <<<END_OF_SQL
    UPDATE settings
    SET value = "$search_json"
    WHERE `key` = "cached_map_results"
END_OF_SQL;
$db->query( $sql );
*/

// Andy 11/6/12 - Modified Tyrone's code.
// 1) This is cron job that runs every 5 minutes
// 2) Gets latest searches made in last 5 minutes and inserts into
//     cached_map_results
// 3) Truncates cached_map_results of old searches more then day ago

// Start Tyrone's new code.  Cache more data in order to flesh out localized
// maps with enough pins

// Get max date to start searching for search_with_result table records.
$max_dt = $db->getValue('SELECT max(date_created) as max_dt FROM cached_map_results');

echo $max_dt . '<br>';

$hits = array();

//Original sql
/*
SELECT query as 'job', city, date_searched
        FROM search_with_results
        WHERE date_searched > '$max_dt'
        AND query!=''
        AND city!=''
        GROUP BY city, query
        ORDER BY id DESC
*/

// Search most recent searches greater then last recorded search date
$sql = <<<END_OF_SQL
    SELECT keyword as 'job', city, datetime_updated
        FROM clickers90
        WHERE datetime_updated > '$max_dt'
        AND keyword!=''
        AND city!=''
        GROUP BY city, keyword
        ORDER BY id DESC
END_OF_SQL;
$results = $db->query( $sql );
$fields = array(
    "job",
    "city",
	"datetime_updated"
);
while( $row = $results->fetch_assoc() ) {
    $this_hit = array();

    // Populate fields
    foreach( $fields as $field ) {
        $this_hit[ $field ] = $row[ $field ];
    }

    // Store hit data
    $hits[] = $this_hit;
}

// Geocode with our own DB
$good_hits = array();
foreach( $hits as $hit ) {
    // Extract data for hit
    $job = $hit[ "job" ];
    $city = $hit[ "city" ];
	$dt = $hit['datetime_updated'];

    // Get long/lat for city
    $sql = <<<END_OF_SQL
        SELECT longitude, latitude
        FROM geocode_location
        WHERE city = "$city"
        LIMIT 1
END_OF_SQL;
    $results = $db->query( $sql );
    $row = $results->fetch_assoc();
    $hit[ "longitude" ] = $row[ "longitude" ];
    $hit[ "latitude" ] = $row[ "latitude" ];
	$hit[ "location" ] = $city;
	$hit[ 'date_searched'] = $dt;

    // Stuff for later
    $good_hits[] = $hit;
}

// {"job":"Customer
// Service","city":"Riverdale","state":"GA","zip":"","longitude":"-84.4522","latitude":"33.55610","location":"Riverdale,
// GA"}

// Cache in cached_map_results: job, city, state, lat, long

$sql = <<<END_OF_SQL
    INSERT INTO cached_map_results
        ( `job`, `city`,
        `longitude`, `latitude`, `location`, `date_created` )
    VALUES
        (?, ?,
        ?, ?, ?, ?)
END_OF_SQL;
$sth = $db->prepare( $sql );

foreach ($good_hits AS $hit) {
$job = $hit['job'];
$city = $hit['city'];
$longitude = $hit['longitude'];
$latitude = $hit['latitude'];
$location = $hit['location'];
$dt = $hit['date_searched'];

$sth->bind_param( "ssssss",
    $job,
    $city,
    $longitude, $latitude, $location, $dt );
$sth->execute();

}
$sth->close();

// Truncate search records
$sql = <<<END_OF_SQL
	DELETE FROM `cached_map_results`
		WHERE date_created < DATE_SUB(CURRENT_TIMESTAMP,INTERVAL 20 DAY)
END_OF_SQL;

$db->query( $sql );




?>
