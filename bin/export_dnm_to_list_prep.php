#!/usr/bin/php -q
<?php
// vim: ai ts=4 sts=4 et sw=4 tw=79

require_once( __DIR__ . "/../lib/core/initialize.inc");

if(Util::isRunningPID()) {
    echo "Already running.\n";
    exit;
}

echo "================================" . "\n";
echo "  Export JUK DNM data to diablo " . "\n";
echo "================================" . "\n\n";

$startTime = microtime(true);

$fileHandle = basename($_SERVER['PHP_SELF'],'.php');
$logFile = "{$fileHandle}.log";

$baconConn = Database::get_pdo_conn('bacon_rw_prod');

$diabloConn = Database::get_pdo_conn('diablo');

Util::log_to_file($logFile,'Started', '');

$stmt['last_id'] = $baconConn->prepare("SELECT * FROM last_dnm_exported");
$stmt['last_id']->execute();
$lastIds=$stmt['last_id']->fetchAll(PDO::FETCH_ASSOC);

foreach($lastIds as $lastId) {
    if($lastId['name']=='bounce') {
        $saved_last_bounced_id = $lastId['last_id'];
        $last_bounced_id = $lastId['last_id'];
    }
    if($lastId['name']=='complaint') {
        $saved_last_complained_id = $lastId['last_id'];
        $last_complained_id = $lastId['last_id'];
    }
    if($lastId['name']=='unsubscribe') {
        $saved_last_unsubscribed_id = $lastId['last_id'];
        $last_unsubscribed_id = $lastId['last_id'];
    }
}

if(empty($saved_last_complained_id) ||
    empty($saved_last_bounced_id) ||
    empty($saved_last_unsubscribed_id)
  ) {
Util::log_to_file($logFile,'Error', "Missing last_id from last_dnm_exported table. Need complaint, bounce and unsubscribe last id's set");
    exit('Missing last_id from last_dnm_exported table.');
}

$stmt['sites'] = $baconConn->prepare("SELECT id, name FROM sites");
$stmt['sites']->execute();
$sites=$stmt['sites']->fetchAll(PDO::FETCH_ASSOC);

// Needed so can add site name to diablo dnm tables
foreach($sites as $site) {
   $exportSites[$site['id']] = $site['name'];
}

// Export JO, WO and CA
//$exportSites[1] = 'jobungo';
//$exportSites[2] = 'wullo';
//$exportSites[4] = 'careeralerter';
//$siteList = implode(',', array_flip($exportSites));


//******** Export global removals ****************
/*
General::log_to_file($logFile, "Global Removal - Start Processing", '');

$sectionStart = microtime(true);

$total_exported_global_removal = 0;
$taskStart = microtime(true);
$sql = "SELECT id, email FROM global_removal WHERE id > ? ORDER BY id DESC;";
$stmt['global_removal'] = $baconConn->prepare($sql);
$stmt['global_removal']->execute(array($saved_last_global_removal_id));
$taskDur = General::get_duration($taskStart, microtime(true));
$global_removal_count = $stmt['global_removal']->rowCount();

General::log_to_file($logFile, "Global Removal - Retrieved Last Records", "last id: {$saved_last_global_removal_id} dur: {$taskDur} rows: {$global_removal_count}");

$index = 0;

if ($global_removal_count > 0) {

    General::log_to_file($logFile, "Global Removal - Exporting...", '');

    while($global_removal = $stmt['global_removal']->fetch(PDO::FETCH_ASSOC)) {
        $added = false;

        if($index==0) {
            $last_global_removal_id = $global_removal['id'];
        }

        $taskStart = microtime(true);

        $sql = "
            INSERT IGNORE INTO global_unsubscribed
            (email, reason, `when`)
            VALUES
            (?, 'Replicated from bacon', NOW());
        ";

        $stmt['insert'] = $diabloConn->prepare($sql);
        $stmt['insert']->execute(array($global_removal['email']));
        if($stmt['insert']->rowCount()>0) {
            $added = true;
            $total_exported_global_removal++;
        }
        General::log_to_file($logFile, "Global Removal - Processed", "em: {$global_removal['email']} dur: " .
            General::get_duration($taskStart, microtime(true)) . " added: {$added}");
        $index++;

    }
}

General::log_to_file($logFile, "Global Removal Done", "Dur: " .
    General::get_duration($sectionStart, microtime(true)) . " emails processed: {$index} emails added: " .
        "{$total_exported_global_removal} last id: {$saved_last_global_removal_id} ".
        "new last id: {$last_global_removal_id}");

unset($stmt);
*/

//******** Export unsubscribes ****************

Util::log_to_file($logFile,'Unsubscribes - Start Processing', "");

$sectionStart = microtime(true);

$total_exported_unsubscribed = 0;
$taskStart = microtime(true);
// NOTE: 1/16/15, removed site limitation AND site_id IN ({$siteList})
$sql = "
    SELECT id, site_id, original_message_id, email_address, details
    FROM unsubscribe
    WHERE id > ?
    ORDER BY id DESC;
    ";

//echo "unsub sql: " . $sql . "\n";

$stmt['unsubscribed'] = $baconConn->prepare($sql);
$stmt['unsubscribed']->execute(array($saved_last_unsubscribed_id));
$taskDur = Util::get_microtime_duration($taskStart, microtime(true));
$unsubscribed_count = $stmt['unsubscribed']->rowCount();

Util::log_to_file($logFile,'Unsubscribes - Retrieved Last Records', "last id: {$saved_last_unsubscribed_id} dur: {$taskDur} rows: {$unsubscribed_count}");

$index = 0;

if ($unsubscribed_count > 0) {

    Util::log_to_file($logFile,'Unsubscribes - Exporting...', "");

    while($unsubscribed = $stmt['unsubscribed']->fetch(PDO::FETCH_ASSOC)) {
        $added = false;

        if($index==0) {
            $last_unsubscribed_id = $unsubscribed['id'];
        }

        $taskStart = microtime(true);

        $sql = "
            INSERT IGNORE INTO site_unsubscribed
            (message_id, email, site, reason, `when`)
            VALUES
            (?, ?, ?, ?, NOW())
        ";

        $stmt['insert'] = $diabloConn->prepare($sql);
        $stmt['insert']->execute(array(
            $unsubscribed['original_message_id'],
            $unsubscribed['email_address'],
            $exportSites[$unsubscribed['site_id']],
            $unsubscribed['details']
            ));
        if($stmt['insert']->rowCount()>0) {
            $added = true;
            $total_exported_unsubscribed++;
        }
        Util::log_to_file($logFile,'Unsubscribes - Processed', "em: {$unsubscribed['email_address']} " .
            "siteid: {$unsubscribed['site_id']} site: {$exportSites[$unsubscribed['site_id']]} dur: " .
            Util::get_microtime_duration($taskStart, microtime(true)) . " added: {$added}");
        $index++;

    }
}

Util::log_to_file($logFile,'Unsubscribes Done', "Dur: " .
    Util::get_microtime_duration($sectionStart, microtime(true)) . " emails processed: {$index} emails added: " .
        "{$total_exported_unsubscribed} last id: {$saved_last_unsubscribed_id} " .
        "new last id: {$last_unsubscribed_id}");

//******** Export complaints ****************

Util::log_to_file($logFile,'Complaints - Start Processing', "");

$sectionStart = microtime(true);

$total_exported_complained = 0;
$taskStart = microtime(true);
// NOTE: 1/16/15, removed site limitation AND site_id IN ({$siteList})
$sql = "
    SELECT id, site_id, email_address
    FROM complaint
    WHERE id > ?
    ORDER BY id DESC;
    ";
$stmt['complained'] = $baconConn->prepare($sql);
$stmt['complained']->execute(array($saved_last_complained_id));
$taskDur = Util::get_microtime_duration($taskStart, microtime(true));
$complained_count = $stmt['complained']->rowCount();

Util::log_to_file($logFile,'Complaints - Retrieved Last Records', "last id: {$saved_last_complained_id} dur: {$taskDur} rows: {$complained_count}");

$index = 0;

if ($complained_count > 0) {
    Util::log_to_file($logFile,'Complaints - Exporting...', "");

    while($complained = $stmt['complained']->fetch(PDO::FETCH_ASSOC)) {
        $added = false;

        if($index==0) {
            $last_complained_id = $complained['id'];
        }

        $taskStart = microtime(true);

        $sql = "
            INSERT IGNORE INTO complaint
            (email, site_name, `when`)
            VALUES
            (?, ?, NOW())
        ";

        $stmt['insert'] = $diabloConn->prepare($sql);
        $stmt['insert']->execute(array(
            $complained['email_address'],
            $exportSites[$complained['site_id']]
            ));
        if($stmt['insert']->rowCount()>0) {
            $added = true;
            $total_exported_complained++;
        }
        Util::log_to_file($logFile, "Complaints - Processed", "em: {$complained['email_address']} " .
            "siteid: {$complained['site_id']} site: {$exportSites[$complained['site_id']]} dur: " .
            Util::get_microtime_duration($taskStart, microtime(true)) . " added: {$added}");
        $index++;

    }
}

Util::log_to_file($logFile, "Complaints Done", "Dur: " .
    Util::get_microtime_duration($sectionStart, microtime(true)) . " emails processed: {$index} emails added: " .
        "{$total_exported_complained} last id: {$saved_last_complained_id} " .
        "new last id: {$last_complained_id}");


//******** Export bounces ****************

Util::log_to_file($logFile, "Bounces - Start Processing", '');

$sectionStart = microtime(true);

$total_exported_bounced = 0;
$taskStart = microtime(true);
// NOTE: 1/16/15, removed site limitation AND site_id IN ({$siteList})
$sql = "
    SELECT id, site_id, email_address, details
    FROM bounce
    WHERE id > ?
    AND type = 'hard'
    ORDER BY id DESC;
    ";

//echo "bounces sql: " . $sql . "\n";

$stmt['bounced'] = $baconConn->prepare($sql);
$stmt['bounced']->execute(array($saved_last_bounced_id));
$taskDur = Util::get_microtime_duration($taskStart, microtime(true));
$bounced_count = $stmt['bounced']->rowCount();

Util::log_to_file($logFile, "Bounces - Retrieved Last Records", "last id: {$saved_last_bounced_id} dur: {$taskDur} rows: {$bounced_count}");

$index = 0;

if ($bounced_count > 0) {

    Util::log_to_file($logFile, "Bounces - Exporting...", '');

    while($bounced = $stmt['bounced']->fetch(PDO::FETCH_ASSOC)) {
        $added = false;

        if($index==0) {
            $last_bounced_id = $bounced['id'];
        }

        $taskStart = microtime(true);

        $sql = "
            INSERT IGNORE INTO bounce
            (email, site_name, `when`)
            VALUES
            (?, ?, NOW())
        ";

        $stmt['insert'] = $diabloConn->prepare($sql);
        $stmt['insert']->execute(array(
            $bounced['email_address'],
            $exportSites[$bounced['site_id']]
            ));
        if($stmt['insert']->rowCount()>0) {
            $added = true;
            $total_exported_bounced++;
        }
        Util::log_to_file($logFile, "Bounces - Processed", "em: {$bounced['email_address']} " .
            "siteid: {$bounced['site_id']} site: {$exportSites[$bounced['site_id']]} dur: " .
            Util::get_microtime_duration($taskStart, microtime(true)) . " added: {$added}");
        $index++;

    }
}

Util::log_to_file($logFile, "Bounces Done", "Dur: " .
    Util::get_microtime_duration($sectionStart, microtime(true)) . " emails processed: {$index} emails added: " .
        "{$total_exported_bounced} last id: {$saved_last_bounced_id} " .
        "new last id: {$last_bounced_id}");

if(!empty($last_complained_id)) {
    $stmt['last_id'] = $baconConn->prepare("UPDATE last_dnm_exported SET last_id=? WHERE name=?");
    $stmt['last_id']->execute(array($last_complained_id, 'complaint'));
}

if(!empty($last_bounced_id)) {
    $stmt['last_id'] = $baconConn->prepare("UPDATE last_dnm_exported SET last_id=? WHERE name=?");
    $stmt['last_id']->execute(array($last_bounced_id, 'bounce'));
}

if(!empty($last_unsubscribed_id)) {
    $stmt['last_id'] = $baconConn->prepare("UPDATE last_dnm_exported SET last_id=? WHERE name=?");
    $stmt['last_id']->execute(array($last_unsubscribed_id, 'unsubscribe'));
}

$durStr = Util::get_microtime_duration($startTime, microtime(true));

Util::log_to_file($logFile, "ALL Done", "Dur: {$durStr}");
