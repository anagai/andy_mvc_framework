#!/usr/bin/php -q
<?php

/*
 * Created on 7/09/13
 * Written By Sung Hwan Ahn / Andy Nagai
 *
 */


require_once( __DIR__ . "/../lib/core/initialize.inc");

if(Util::isRunningPID()) {
    echo "Already running.\n";
    exit;
}

$startTime = microtime(true);

//echo "================================================================================================\n";
//echo "  Aggregate coreg welcome campaign and user stats for partner/source (coregPivotWelPerfSource)\n";
//echo "================================================================================================\n\n";
echo "Started: " . date('Y-m-d H:i:s') . "\n";

include_once(HELPER_PATH . 'agg_coreg_welperf_source_helper.inc');

$args = getopt("p:");

$prevDay = false;

# Must pass numeric day interval argument and in range 1-5
if(!empty($args['p'])) {
    if(is_numeric($args['p']) && $args['p']>0) {
        $prevDay = $args['p'];
    } else {
        exit( "Usage: " . $argv[ 0 ] . " -p [Prev Day Interval]\n" );
    }

}

$fileHandle = basename($_SERVER['PHP_SELF'],'.php');
$logFile = $fileHandle.'.log';

Util::log_to_file($logFile, 'Started', '');

$dbConn = Database::get_pdo_conn(DEFAULT_DB_SETTING);

// Ensure no lock on table read on mckinley
$dbConn->exec('SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;');

//Util::startTimer();

// Make valid partner list
$sql = "SELECT partner as pid FROM coreg_partner GROUP BY partner";
$stmt = $dbConn->prepare($sql);
$stmt->execute();
while($pRow = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $partners[$pRow['pid']] = 1;
}

//Util::log_to_file('agg_coreg_welperf.log', 'coreg_partner', Util::elapsedTime());

//Util::startTimer();

// Get welcome campaign names
/*
$sql = "SELECT type, campaign_name as camp FROM email_version WHERE
        (type='welcome_email' AND name!='welcome_email_test') OR
        type='welcome_email_noloc'
    ";

$stmt = $dbConn->prepare($sql);
$stmt->execute();

$welCamps = $stmt->fetchAll(PDO::FETCH_ASSOC);
*/

//Util::log_to_file('agg_coreg_welperf.log', 'campaigns', Util::elapsedTime());

// Get connection to k2

if($prevDay) {
    // Aggregate previous day if param passed
    $dt = date('Y-m-d', strtotime("-{$prevDay} day"));
} else {
    // Aggregate current day
    $dt = date('Y-m-d');
}

//$dt = date('Y-m-d', strtotime('-1 day'));

$aggMnths = array(
    date('Ym', strtotime($dt)),
    date('Ym', strtotime("{$dt} first day of previous month")),
    date('Ym', strtotime("{$dt} first day of 2 months ago"))
    );

// For recent months prior to activation of monthly rotation use
// non-rotated campaigns for 8/14 - 10/14

$welCamps = array();

foreach($aggMnths as $aggMnth) {
    if($aggMnth == '201410' ||
       $aggMnth == '201409' ||
       $aggMnth == '201408') {
        if(!in_array('juk_welcome', $welCamps)) {
            $welCamps[] = 'juk_welcome';
        }
        if(!in_array('juk_welcome_noloc', $welCamps)) {
            $welCamps[] = 'juk_welcome_noloc';
        }
    } else {
        $welCamps[] = "juk_welcome_{$aggMnth}";
        $welCamps[] = "juk_welcome_noloc_{$aggMnth}";
    }
}

// This is for aggregating multiple days to prev day
//$dt = '2014-04-01';
//$toDt = date('Y-m-d');
//while($dt < $toDt) {

    $aggRows = array();

    echo "Aggregated: $dt \n";
    Util::log_to_file($logFile, 'Date Processing', $dt);
    //Get welcome stats

    //Util::startTimer();
    $aggRows = getWelcomeStats($partners, $welCamps, $dt);
    //Util::log_to_file('agg_coreg_welperf.log', 'welcomeStats', Util::elapsedTime());

    //Util::startTimer();
    $dnmData = getDnmStats($partners, $welCamps, $dt);
    //Util::log_to_file('agg_coreg_welperf.log', 'dnmStats', Util::elapsedTime());

    if(!empty($dnmData)) {

        foreach($dnmData as $type=>$typeGroup) {
            foreach($typeGroup as $pid=>$partnerGroup) {
                foreach($partnerGroup as $src=>$dnm) {
                    $aggRows[$type][$pid][$src]['bnce'] = (int)$dnm['bnce'];
                    $aggRows[$type][$pid][$src]['cmpt'] = (int)$dnm['cmpt'];
                    $aggRows[$type][$pid][$src]['unsub'] = (int)$dnm['unsub'];
                }
            }
        }
    }

    //Util::startTimer();
    $siteData = getSiteStats($dbConn, $partners, $dt);
    //Util::log_to_file('agg_coreg_welperf.log', 'SiteStats', Util::elapsedTime());

    //Util::printr($siteData);
    // Add site stats if there is row for type and pid
    if(!empty($siteData)) {
        foreach($siteData as $pid=>$partnerGroup) {
            foreach($partnerGroup as $src=>$site) {
                for($x=0; $x<=1; $x++) {
                    //if(isset($aggRows[$x][$pid])) {
                        $aggRows[$x][$pid][$src]['el1'] = (int)$site['el1'];
                        $aggRows[$x][$pid][$src]['el2'] = (int)$site['el2'];
                        $aggRows[$x][$pid][$src]['srch'] = (int)$site['srch'];
                        $aggRows[$x][$pid][$src]['jclik'] = (int)$site['jclik'];
                    //}
                }
            }
        }
    }

    //Util::printr($aggRows);

    if(count($aggRows)>0) {

        // Remove unknown in case agg script rerun to correct unknown partner
        $sql = "DELETE FROM coregPivotWelPerfSource WHERE Date=? AND Partner='unknown'";
        $delStmt = $dbConn->prepare($sql);
        $delStmt->execute(array($dt));

        foreach($aggRows as $type=>$typeGroup) {
            foreach($typeGroup as $pid=>$partnerGroup) {
                foreach($partnerGroup as $src=>$row) {

                    $aSent = !empty($row['sent'])
                        ? $row['sent']
                        : 0;

                    $aUnsent = !empty($row['unsent'])
                        ? $row['unsent']
                        : 0;

                    $aClik = !empty($row['clik'])
                        ? $row['clik']
                        : 0;

                    $aOpen = !empty($row['open'])
                        ? $row['open']
                        : 0;

                    $aBnce = !empty($row['bnce'])
                        ? $row['bnce']
                        : 0;

                    $aCmpt = !empty($row['cmpt'])
                        ? $row['cmpt']
                        : 0;

                    $aUnsub = !empty($row['unsub'])
                        ? $row['unsub']
                        : 0;

                    $aEl1 = !empty($row['el1'])
                        ? $row['el1']
                        : 0;

                    $aEl2 = !empty($row['el2'])
                        ? $row['el2']
                        : 0;

                    $aSrch = !empty($row['srch'])
                        ? $row['srch']
                        : 0;

                    $aJclik = !empty($row['jclik'])
                        ? $row['jclik']
                        : 0;

                    $sql = "
                        SELECT 1 FROM coregPivotWelPerfSource WHERE WelcomeType=? AND Date=?
                        AND Partner=? AND Source=? LIMIT 1
                        ";
                    $chkStmt = $dbConn->prepare($sql);

                    if($chkStmt->execute(array($type, $dt, $pid, $src))) {
                        $recCnt = $chkStmt->fetchColumn(0);
                        // If return something then record exists
                        if(!empty($recCnt)) {
                            // Update agg row
                            $sql = "
                                UPDATE coregPivotWelPerfSource SET Sent={$aSent}, Unsent={$aUnsent},
                                Clicks={$aClik}, Opens={$aOpen}, Bounced={$aBnce},
                                Complaints={$aCmpt}, Unsubscribed={$aUnsub}, El1={$aEl1},
                                El2={$aEl2}, JobSearch={$aSrch}, JobClicks={$aJclik}
                                WHERE WelcomeType=? AND Date=? AND Partner=? AND Source=?
                            ";

                        } else {
                            // New agg row
                            $sql = "
                                INSERT INTO coregPivotWelPerfSource (WelcomeType, Date, Partner, Source, Sent, Unsent,
                                Clicks, Opens, Bounced, Complaints, Unsubscribed, El1, El2,
                                JobSearch, JobClicks) VALUES(?, ?, ?, ?, {$aSent}, {$aUnsent},
                                {$aClik}, {$aOpen}, {$aBnce}, {$aCmpt}, {$aUnsub}, {$aEl1},
                                {$aEl2}, {$aSrch}, {$aJclik})
                            ";

                        }

                        //echo $sql . "\n";

                        $saveStmt = $dbConn->prepare($sql);
                        $saveStmt->execute(array($type, $dt, $pid, $src));

                    } else {
                        $err = $chkStmt->errorInfo();
                        echo $err[2];
                    }
                }
            }
        }

    }


    //echo Util::elapsedTime();
    //echo "aggregated {$dt}\n";
    //$dt = date('Y-m-d', strtotime($dt . ' +1 day'));

//}

$aggRecs = count($aggRows);
$dur = Util::get_microtime_duration($startTime, microtime(true));
$doneDt = date('Y-m-d');
Util::log_to_file($logFile, 'Aggregated: ', "dt: {$dt} recs: {$aggRecs}");
Util::log_to_file($logFile, 'Done', $dur . ' sec');
echo 'Done: ' . date('Y-m-d H:i:s') . "\n";
echo 'Duration: ' . $dur . ' sec' . "\n";
$msg="Duration: {$dur}<br>
    Aggregate dt: {$dt}<Br>
    Records updated: {$aggRecs}<br>
";

Util::systemAlert($fileHandle." {$doneDt}", $msg);
