#!/usr/bin/php -q
<?php

    /*
     * Created on Jul 23, 2012
     * Written By Sung Hwan Ahn
     * Observed By Andy Nagai
     *
     */

    // https://basecamp.com/1843241/projects/395698-wullo/messages/6079716-dnm-scrubbing

    // Every 15 minutes DNM scrubbing (EL1 user)
    // Mark do-not-mail all hard bounced
    // Mark do-not-mail all job sites complained
    // Mark do-not-mail its own site unsubscribe
    // Mark do-not-mail all global removals


    // Every 15 minutes DNM scrubbing (EL2, EL3, EL4 users)
    // Mark do-not-mail its own site hard bounced
    // Mark do-not-mail its own site complained
    // Mark do-not-mail its own site unsubscribe
    // Mark do-not-mail all global removals

    require_once( __DIR__ . "/../lib/core/initialize.inc" );

    if(Util::isRunningPID()) {
        echo "Already running.\n";
        exit;
    }

    $log_file_name = "scrub_dnm_list.log";

    // Check Before Mailable Numbers
    //$before_user_mailable = User::total_num_mailable();
    //Utility::log_to_file("scrub_dnm_list_full_scan.log", "Total Before User Mailable", $before_user_mailable);

    // Read the last added date from INI file
    $ini_array = parse_ini_file(CONFIG_PATH.'last_dnm_list.ini');

    $last_global_removal_date = $ini_array["global_removal"];
    $last_complaint_date = $ini_array["complained"];
    $last_bounce_date = $ini_array["bounced"];
    $last_unsubscribe_date = $ini_array["unsubscribed"];
    $last_blacklisted_domain_date = $ini_array['blacklisted_domain'];

    include_once(HELPER_PATH . 'scrub_dnm_list_helper.inc');

    /***********************
     *  Blacklisted Domain *
    ***********************/

    $bd_stats = update_users_with_blacklisted_domain($log_file_name, $last_blacklisted_domain_date);

    $latest_blacklisted_domain_date = !empty($bd_stats['bd_latest_date'])
    ? $bd_stats['bd_latest_date']
    : $last_blacklisted_domain_date;

    echo 'Blacklisted Domain latest date: '.$latest_blacklisted_domain_date . "\n";

    Util::log_to_file($log_file_name, "Total Blacklisted Domains Processed", $bd_stats['bd_count']);
    Util::log_to_file($log_file_name, "Total Scrubbed Blacklisted Domain Users", $bd_stats['bd_user_scrubbed_count']);

    /******************
     *  Global Removals   *
     ******************/

    $gr_stats = update_users_with_global_removal($log_file_name, $last_global_removal_date);

    $latest_global_removal_date = !empty($gr_stats['gr_latest_date'])
        ? $gr_stats['gr_latest_date']
        : $last_global_removal_date;

   echo 'Global Removal latest date: '.$latest_global_removal_date . "\n";

   Util::log_to_file($log_file_name, "Total Global Removals Processed", $gr_stats['gr_count']);
   Util::log_to_file($log_file_name, "Total Scrubbed Global Removal Users", $gr_stats['gr_user_scrubbed_count']);
   //echo ' Mem:'.number_format(memory_get_usage()/1024,1).'kb ('.number_format(memory_get_peak_usage()/1024,1)."kb)\n";

   /*************
    * Complaints  *
    *************/

   $complaint_stats = update_users_with_complaint($log_file_name, $last_complaint_date);

   $latest_complaint_date = !empty($complaint_stats['complaint_latest_date'])
   ? $complaint_stats['complaint_latest_date']
   : $last_complaint_date;

   echo 'Complaint latest date: '.$latest_complaint_date . "\n";

   Util::log_to_file($log_file_name, "Total Complaints Processed", $complaint_stats['complaint_count']);
   Util::log_to_file($log_file_name, "Total Scrubbed Complaint Users", $complaint_stats['complaint_user_scrubbed_count']);

   /*********
    * Bounces  *
    *********/

   $bounce_stats = update_users_with_bounce($log_file_name, $last_bounce_date);

   $latest_bounce_date = !empty($bounce_stats['bounce_latest_date'])
    ? $bounce_stats['bounce_latest_date']
    : $last_bounce_date;

   echo 'Bounce latest date: '.$latest_bounce_date . "\n";

   Util::log_to_file($log_file_name, "Total Bounces Processed", $bounce_stats['bounce_count']);
   Util::log_to_file($log_file_name, "Total Scrubbed Bounce Users", $bounce_stats['bounce_user_scrubbed_count']);

   /**************
    * Unsubscribed  *
   ***************/

   $unsub_stats = update_users_with_unsubscribe($log_file_name, $last_unsubscribe_date);

   $latest_unsubscribe_date = !empty($unsub_stats['unsub_latest_date'])
   ? $unsub_stats['unsub_latest_date']
   : $last_unsubscribe_date;

   echo 'Unsubscribe latest date: '.$latest_unsubscribe_date . "\n";

   Util::log_to_file($log_file_name, "Total Unsubscribes Processed", $unsub_stats['unsub_count']);
   Util::log_to_file($log_file_name, "Total Scrubbed Unsubscribe Users", $unsub_stats['unsub_user_scrubbed_count']);

   // Update last_dnm_list.ini file with latest dnm record dates
   $dnm_ini = array("last date added" => array("global_removal" => $latest_global_removal_date,
        "complained" => $latest_complaint_date,
        "bounced" => $latest_bounce_date,
        "blacklisted_domain" => $latest_blacklisted_domain_date,
        "unsubscribed" => $latest_unsubscribe_date));

   Util::write_ini_file($dnm_ini, CONFIG_PATH.'last_dnm_list.ini', true);

   //echo ' Mem:'.number_format(memory_get_usage()/1024,1).'kb ('.number_format(memory_get_peak_usage()/1024,1)."kb)\n";



//********** Following is the old original code *****************************

    /***************/
    /* Complainted */
    /***************/

    /*
    $total_scrubbed_complained_user = 0;

    if(isset($database)) { $database->close_connection(); }
    $database = new MySQLDatabase("shasta", "bacon");
    $num_last_complained_list = Complaint::count_last_added_list($last_complained_date);

    if ($num_last_complained_list > 0) {

        $start = 0;
        $limit = 5000;

        $index = 0;

        $last_complained_list = Complaint::get_last_added_list($last_complained_date, $start, $limit);

        $database->close_connection();
        $database = new MySQLDatabase(); // back to localhost

        while ($last_complained_list) {

            $process = $start + count($last_complained_list);

            print ("Complained Processing: " . $start . " - " . $process . " | End: " . $num_last_complained_list . "\n");

            foreach($last_complained_list as $last_complained) {

                if ($index == 0) $saved_last_complained_date = $last_complained->date_added;

                if ($user = User::find_by_email($last_complained->email_address)) {

                    if ($user->is_mailable == 1) {

                        $database->close_connection();
                        $database = new MySQLDatabase("shasta", "bacon");

                        $is_own_site_complained = Complaint::has_this_email($complained_user->email);

                        $database->close_connection();
                        $database = new MySQLDatabase(); // back to localhost

                        if ($user->is_mailable == 1 || $is_own_site_complained) {
                            $user->is_mailable = 0;
                            $user->update();
                            $total_scrubbed_complained_user++;
                            Utility::log_to_file($log_file_name, "Complained User EL" . $user->engagement_level, $user->email);
                        }
                    }
                }

                $index++;
            }

            $database->close_connection();
            $database = new MySQLDatabase("shasta", "bacon");

            $start += $limit;
            $last_complained_list = Complaint::get_last_added_list($last_complained_date, $start, $limit);

            $database->close_connection();
            $database = new MySQLDatabase(); // back to localhost
        }
    }

    print ("Total newly updated user from complaiend: " . $total_scrubbed_complained_user . "\n");
    Utility::log_to_file($log_file_name, "Total Complained Scanned", $index);
    Utility::log_to_file($log_file_name, "Total Scrubbed Complained User", $total_scrubbed_complained_user);
    */


    /****************/
    /* Unsubscribed */
    /****************/

    /*
    $total_scrubbed_unsubscribed_user = 0;

    if(isset($database)) { $database->close_connection(); }
    $database = new MySQLDatabase("shasta", "bacon");
    $num_last_unsubscribed_list = Unsubscribe::count_last_added_list($last_unsubscribed_date);

    if ($num_last_unsubscribed_list > 0) {

        $start = 0;
        $limit = 5000;

        $index = 0;

        $last_unsubscribed_list = Unsubscribe::get_last_added_list($last_unsubscribed_date, $start, $limit);

        $database->close_connection();
        $database = new MySQLDatabase(); // back to localhost

        while ($last_unsubscribed_list) {

            $process = $start + count($last_unsubscribed_list);

            print ("Unsubscribed Processing: " . $start . " - " . $process . " | End: " . $num_last_unsubscribed_list . "\n");

            foreach($last_unsubscribed_list as $last_unsubscribed) {

                if ($index == 0) $saved_last_unsubscribed_date = $last_unsubscribed->date_added;

                if ($user = User::find_by_email($last_unsubscribed->email_address)) {
                    if ($user->is_mailable == 1) {
                        $user->is_mailable = 0;
                        $user->update();
                        $total_scrubbed_unsubscribed_user++;
                        Utility::log_to_file($log_file_name, "Unsubscribed User", "{$user->email} | " . $index);
                    }
                }

                $index++;
            }

            $database->close_connection();
            $database = new MySQLDatabase("shasta", "bacon");

            $start += $limit;
            $last_unsubscribed_list = Unsubscribe::get_last_added_list($last_unsubscribed_date, $start, $limit);

            $database->close_connection();
            $database = new MySQLDatabase(); // back to localhost
        }
    }

    print ("Total newly updated user from unsubscribed: " . $total_scrubbed_unsubscribed_user . "\n");
    Utility::log_to_file($log_file_name, "Total Unsubscribed Scanned", $index);
    Utility::log_to_file($log_file_name, "Total Scrubbed Unsubscribed User", $total_scrubbed_unsubscribed_user);
    */

    /***********/
    /* Bounced */
    /***********/

    /*
    $total_scrubbed_bounced_user = 0;

    if(isset($database)) { $database->close_connection(); }
    $database = new MySQLDatabase("shasta", "bacon");
    $num_last_bounced_list = Bounce::count_last_added_list($last_bounced_date);

    // Scrubbing Bounced
    $num_update = 0;
    if ($num_last_bounced_list > 0) {

        $start = 0;
        $limit = 5000;

        $index = 0;

        $last_bounced_list = Bounce::get_last_added_list($last_bounced_date, $start, $limit);

        $database->close_connection();
        $database = new MySQLDatabase(); // back to localhost

        while ($last_bounced_list) {

            $process = $start + count($last_bounced_list);

            print ("Bounced Processing: " . $start . " - " . $process . " | End: " . $num_last_bounced_list . "\n");

            foreach($last_bounced_list as $last_bounced) {

                if ($index == 0) $saved_last_bounced_date = $last_bounced->date_added;

                if ($user = User::find_by_email($last_bounced->email_address)) {

                    if ($user->is_mailable == 1) {

                        $database->close_connection();
                        $database = new MySQLDatabase("shasta", "bacon");

                        $is_own_site_bounced = Bounce::has_this_email($bounced_user->email);

                        $database->close_connection();
                        $database = new MySQLDatabase(); // back to localhost

                        if ($user->is_mailable == 1 || $is_own_site_bounced) {
                            $user->is_mailable = 0;
                            $user->update();
                            $total_scrubbed_bounced_user++;
                            Utility::log_to_file($log_file_name, "Bounced User EL" . $user->engagement_level, $user->email);
                        }
                    }
                }

                $index++;
            }

            $database->close_connection();
            $database = new MySQLDatabase("shasta", "bacon");

            $start += $limit;
            $last_bounced_list = Bounce::get_last_added_list($last_bounced_date, $start, $limit);

            $database->close_connection();
            $database = new MySQLDatabase(); // back to localhost
        }
    }

    print ("Total newly updated user from bounced: " . $total_scrubbed_bounced_user . "\n");
    Utility::log_to_file($log_file_name, "Total Bounced Scanned", $index);
    Utility::log_to_file($log_file_name, "Total Scrubbed Bounced User", $total_scrubbed_bounced_user);
*/

    // Check After Mailable Numbers
    //$after_user_mailable = User::total_num_mailable();
    //Utility::log_to_file("scrub_dnm_list_full_scan.log", "Total After User Mailable", $after_user_mailable);

?>