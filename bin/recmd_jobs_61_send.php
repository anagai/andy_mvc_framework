#!/usr/bin/php -q
<?php

require_once( __DIR__ . "/../lib/core/initialize.inc");

if(Util::isRunningPID()) {
    echo "Already running.\n";
    exit;
}

echo "===============================================\n";
echo "  Send recommended jobs >60 campaign           \n";
echo "===============================================\n\n";

$fileHandle = basename($_SERVER['PHP_SELF'],'.php');
$logFile = "{$fileHandle}.log";

echo "Started...";
$startTime = date('Y-m-d H:i:s');

$conn = Database::get_pdo_conn(DEFAULT_DB_SETTING);

// sftp creds are in core/initialize.inc
$_servers = array(
    "blanc"=>array("dir"=>'/var/www/anagai-ses25/db/incoming/jobungo.co.uk'),
    "esam"=>array("dir"=>'/var/www/ses25/db/incoming/jobungo.co.uk'),
    "snowdon"=>array("dir"=>'/srv/www/ses25/current/db/incoming/jobungo.co.uk')
);

$sendServer = "snowdon";

$today = date('Ymd');

$srcFile = CSV_PATH . "recmd_jobs_61_w" . date('W') . ".csv";
$finalFile = CSV_PATH . 'recmd_jobs_61_w' . date('W') . '_final.csv';
//$finalFile = CSV_PATH . 'juk_test_kw.csv';

$sendTime = date("Y-m-d 17:30:00");
//$sendTime = date("Y-m-d 00:01:00");

$delCsv = true;
$addRpSeeds = false;
$useRpSched = false;
$rpSched = array();

$sub_id = "jukrcmd2{$today}";
$description = "JUK recommended jobs";
//$subject = "\$FNAME_COMMA_NONE\$check out these top \$KEYWORD_1\$ jobs in \$CITY\$";
$subject = "Recommended \$KEYWORD_1\$ Jobs\$SPACE_FOR_FNAME\$";


if(!file_exists($srcFile)) {
    $msg = "No CSV file to process: " . $srcFile . ". Cannot initialize campaign.<br>" .
        "CAMPAIGN: {$sub_id}<br>";
    Util::log_to_file($logFile,'Error', str_replace('<br>', ' ', $msg));
    Util::systemAlert($fileHandle.'.php',$msg,'',false);
    exit($msg."\n");
}

$srcCount = explode(' ',trim(`wc -l {$srcFile}`));
$totalUsers = $srcCount[0]-1;

$rpSeeds = array();

unset($stmt);

// Determine seed interval
if($addRpSeeds) {
    if($useRpSched === FALSE || in_array(date("Y-m-d"), $rpSched)) {
        $stmt['seeds'] = $conn->prepare('SELECT * FROM return_path_seeds;');
        if($stmt['seeds']->execute()) {
            $rpSeeds = $stmt['seeds']->fetchAll(PDO::FETCH_ASSOC);
        }
        $seedCount = count($rpSeeds);
        //echo 'seeds: ' . $seedCount . "users: " . $totalUsers . "\n";
        if($totalUsers>$seedCount) {
            $seedInterval = floor($totalUsers/$seedCount);
        } else {
            $seedInterval = 1;
        }
        Util::log_to_file($logFile,'Seeds to add', 'cnt: ' . $seedCount . ' interval: ' . $seedInterval);
        //echo 'seedinterval: ' . $seedInterval . "\n";
    }
}

$srcFp = fopen($srcFile,'r');
$finalFp = fopen($finalFile,'w');

$stats['users']=0;
$stats['seeds']=0;
$stats['skip']=0;
$stats['srccnt']=0;
$intervalCount=0;

echo 'Generating Final CSV file: '  . $finalFile . "....\n";
Util::log_to_file($logFile,'Final CSV file start', 'file: ' . $finalFile);

$updateSql = "
    UPDATE user set date_last_mailed=now() WHERE id=?;
";

while(!feof($srcFp)) {

    $line = fgetcsv($srcFp);

    if(!empty($line)) {

        $stats['srccnt']++;

        if($stats['srccnt']>1) {

           $userId = $line[0];

           $sql = "SELECT is_mailable as mailable FROM user WHERE id=?";

            $stmt['user'] = $conn->prepare($sql);
            $stmt['user']->execute(array($userId));
            if($stmt['user']->rowCount()>0) {
                $user = $stmt['user']->fetch(PDO::FETCH_ASSOC);
                if($user['mailable']==0) {
                    // User not mailable
                    $stats['skip']++;
                    continue;
                } else {
                    ++$stats['users'];

                    fputcsv($finalFp, $line);

                    $intervalCount++;

                    if(count($rpSeeds)>0 && $intervalCount==$seedInterval) {
                        $seed = array_shift($rpSeeds);
                        $seedRow = array('0', $seed['first_name'], $seed['last_name'],
                            $seed['email'], $seed['city'], '', $seed['postal_code'],
                            '0', $seed['keyword_1'], '', '', '', '', '', '', '', '209.37.24.74', 'RM', '');
                        fputcsv($finalFp, $seedRow);
                        $stats['seeds']++;
                        $intervalCount=0;
                    }

                    // update user last mailed date
                    $stmt['update'] = $conn->prepare($updateSql);
                    $stmt['update']->execute(array($userId));

                }
            } else {
                // User not found
                $stats['skip']++;
                continue;
            }

           //print_r($line);

        } else {
            $header = $line;
            fputcsv($finalFp, $header);
        }
    }

}

// Add any remaining seeds because final user count will be different
while(count($rpSeeds)>0) {
    $seed = array_shift($rpSeeds);
    $seedRow = array('0', $seed['first_name'], $seed['last_name'],
        $seed['email'], $seed['city'], '', $seed['postal_code'],
        '0', $seed['keyword_1'], '', '', '', '', '', '', '', '209.37.24.74', 'RM', '');
    fputcsv($finalFp, $seedRow);
    $stats['seeds']++;
}

fclose($srcFp);
fclose($finalFp);

echo "Final CSV file created\n";
echo 'srccnt: ' . $stats['srccnt'] . "\n";
Util::log_to_file($logFile,'Final CSV file created', "srccnt: {$srcCount[0]}
    finalcnt: {$stats['users']} skipped: {$stats['skip']} seeds: {$stats['seeds']}");
echo 'total source count: ' . $srcCount[0] . "\n";
echo 'total final count: ' . $stats['users'] . "\n";
echo 'total skipped: ' . $stats['skip'] . "\n";
echo 'total seeds: ' . $stats['seeds'] . "\n";



$sesConn = Util::get_ssh_connection($sendServer);

$remote_path = $_servers[$sendServer]['dir'] . "/{$sub_id}.csv";

$sesConn->put($remote_path, $finalFile, NET_SFTP_LOCAL_FILE);

echo "File transferred to {$sendServer} {$remote_path}\n\n";
Util::log_to_file($logFile,'Uploaded CSV', "Src: {$finalFile} Dest: {$remote_path} Server: {$sendServer}");

$key = sha1('hcAdI2k4HZ0XFIb_UifZmGCKkXGqs5LV:54.75.255.72');

$url = "http://" . constant(strtoupper($sendServer) . '_HOST') . "/api/campaign/?_[key]={$key}&" .
        "sub-id={$sub_id}&" .
        "description=" . urlencode($description) . "&" .
        "start-time=" . urlencode(Util::getUtcDt($sendTime)) . "&" .
        "template-id=juk_recommended_jobs_kw&" .
        "campaign-subject=" . urlencode($subject) . "&" .
        "total-min-results=0&" .
        "ses-account=dedicated_certified&site=jobungouk&" .
        "send-profile=jobungo_uk_digests";

$out = Util::curlReq($url);

echo "server: {$sendServer}\n";
echo "sub_id: {$sub_id}\n";
echo "description: " . $description . "\n";
//echo "csv: {$filename}\n";
echo "url: " . $url . "\n";
echo "send time: " . $sendTime . "\n\n";

$doneTime = date('Y-m-d H:i:s');

$dur = strtotime($doneTime) - strtotime($startTime);

if($out['status']!==200) {
    $msg = "Failed to initialize {$sub_id} campaign<br>";
    $msg .= 'STARTED: ' . $startTime . '<br>';
    $msg .= "SERVER: {$sendServer}<br>";
    $msg .= "URL: {$url}<br>";
    $msg .= "CURL OUT: {$out['response']}<br>";
    $msg .= "HTTP STATUS: {$out['status']}<br>";
    $msg .= "REMOTE FILE: {$remote_path}<br>";
    $msg .= "SOURCE: " . $srcFile . "<br>";
    $msg .= "FINAL: " . $finalFile . "<br>";
    $msg .= "SEND TIME: " . $sendTime . "<br>";
    $msg .= "DONE: " . $doneTime . "<br>";
    $msg .= "DURATION: " . $dur . "<br>";
    Util::log_to_file($logFile,'Failed', "Fail to schedule daily wullo digest emails to {$sendServer}: {$out['response']}");
    echo str_replace('<br>',"\n",$msg);
    Util::systemAlert($sub_id . ' campaign error', $msg,'',false);
} else {
    echo "Successfully scheduled on {$sendServer}";
    // delete csv files
    if($delCsv) {
        //@unlink($srcFile);
        @unlink($finalFile);
    }
    $msg = "Initialized {$sub_id} campaign starting at {$sendTime}<br>";
    $msg .= 'STARTED: ' . $startTime . '<br>';
    $msg .= "SERVER: {$sendServer}<br>";
    $msg .= "URL: {$url}<br>";
    $msg .= "REMOTE FILE: {$remote_path}<br>";
    $msg .= "SOURCE: " . $srcFile . "<br>";
    $msg .= "FINAL: " . $finalFile . "<br>";
    $msg .= "SEND TIME: " . $sendTime . "<br>";
    $msg .= "DONE: " . $doneTime . "<br>";
    $msg .= "DURATION: " . $dur . "<br>";
    Util::log_to_file($logFile,'Success', "System scheduled {$sendServer} daily wullo digest emails sending at ". $sendTime);
    echo str_replace('<br>',"\n",$msg);
    Util::systemAlert($sub_id . ' campaign initialized!',$msg);

}





