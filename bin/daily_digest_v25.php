#!/usr/bin/php -q
<?php

    /*
     * Created on 6/28/13
     * Written By Sung Hwan Ahn / Andy Nagai
     *
     */


    require_once( __DIR__ . "/../lib/core/initialize.inc");

    if(Util::isRunningPID()) {
        echo "Already running.\n";
        exit;
    }

    echo "===============================================" . "\n";
    echo "  ses v2 JUK daily digest mailing system (cron) " . "\n";
    echo "===============================================" . "\n\n";

    $ses_model = Util::load_model('ses');

    /*************************/
    /*** CREATING CSV FILE ***/
    /*************************/

    $today = strftime("%Y%m%d", time());

    $clickerDays = 35;

    $logFile = 'daily_digest.log';

    //***** Set campaign send time here. uk send time is equal to this time ********
    $startTime = strftime("%Y-%m-%d", time()) . " 06:30:00";

    //$filename = "juk_test_kw.csv";
    $filename = "daily_digest_{$today}.csv";



    $addRpSeeds = TRUE;
    $useRpSched = FALSE;

    $rpSched = array();

    echo "Creating CSV file {$filename} ..." . "\n";

    $conn = Database::get_pdo_conn(DEFAULT_DB_SETTING);

    $total_count = $ses_model->create_digest_v25_csv_file($conn, Util::getUtcDt($startTime), CSV_PATH . $filename, $clickerDays,
        $addRpSeeds, $useRpSched, $rpSched);

    if ($total_count < 1) {
        Util::log_to_file($logFile, 'No Users', "Nothing to send");
        exit("No need to send the digest email: 0 user\n");
    }

    echo "Created CSV File!\n\n";
    Util::log_to_file($logFile,'Created CSV', "System created {$filename} for digest mailing.");



    //***********************
    //*** UPLOAD CSV FILE ***
    //***********************

    echo "Connecting to Snowdon server...\n";
    //$filename = 'juk_test.csv';
    $sub_id = "juk{$today}";
    $description = "Jobungo UK Daily Digest for " . strftime("%Y-%m-%d", time());
    $subject = "\$NUM_JOBS_DISPLAYED\$+ new \$KEYWORD_1\$ jobs near \$CITY\$";

    $remote_path = "/srv/www/ses25/current/db/incoming/jobungo.co.uk/{$sub_id}.csv";

    $sesConn = Util::get_ssh_connection('SNOWDON');
    $sesConn->put($remote_path, CSV_PATH . $filename, NET_SFTP_LOCAL_FILE);

    echo "File transferred to {$remote_path}\n\n";
    Util::log_to_file($logFile,'Uploaded CSV', "System uploaded {$filename} to Snowdon server.");
/*
    if($conn=ssh2_connect('176.58.120.39',22,array(
        'client_to_server'=>array(
                'comp'=>'zlib,none'
        ),
        'server_to_client'=>array(
                'comp'=>'zlib,none'
        )
))){
        ssh2_auth_password($conn,'sftp','r3stm3d1a');
        //$sftp=ssh2_sftp($conn);
        ssh2_scp_send($conn, CSV_PATH . $filename, $remote_path);
        ssh2_exec($conn, 'exit');
        //if($fh=fopen("ssh2.sftp://{$sftp}/srv/www/ses25/current/db/incoming/wullo.com/testing.csv",'w')){
        //        fputs($fh,'this is a test: '.microtime(TRUE));
        //        fclose($fh);
        //}
}
*/


    //***************************
    //*** SETUP MAIL CAMPAIGN ***
    //***************************

    echo "Setup campaign...\n";

    $key = sha1('hcAdI2k4HZ0XFIb_UifZmGCKkXGqs5LV:54.75.255.72');

    $url = "http://snowdon.restdev.com/api/campaign/?_[key]={$key}&" .
        "sub-id={$sub_id}&" .
        "description=" . urlencode($description) . "&" .
        "start-time=" . urlencode(Util::getUtcDt($startTime)) . "&" .
        "template-id=juk_daily_digest&" .
        "campaign-subject=" . urlencode($subject) . "&" .
        "total-min-results=3&" .
        "ses-account=dedicated_certified&site=jobungouk&" .
        "send-profile=jobungo_uk_digests";

    $output = Util::curl_request($url);

    echo "sub_id: {$sub_id}\n";
    echo "description: " . $description . "\n";
    echo "csv: {$filename}\n";
    echo "start-time: " . $startTime . "\n";
    echo "url: " . $url . "\n\n";

    if (empty($output)) {
        //$ses_model->update_digest_user_last_emailed($start_time, $clickerDays);
        echo "Successfully schedueled daily Jobungo UK digest emails";
        Util::log_to_file($logFile,'Success', "System scheduled daily Jobungo UK digest emails sending at ". $startTime);

    } else {
        echo "Failed to schedule emails: {$output}";
        Util::log_to_file($logFile,'Failed', "Failed to schedule daily Jobungo UK digest emails: {$output}");
    };

    if(isset($database)) { $database->close_connection(); }

?>
