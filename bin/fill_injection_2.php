#!/usr/bin/php -q
<?php

    // vim: ai ts=4 sts=4 et sw=4 tw=79

    /*
     * Created on Dec 13, 2012
     * Written By Sung Hwan Ahn
     * Observed By Andy Nagai
     *
     */

    require_once( __DIR__ . "/../lib/core/initialize.inc");

    if(Util::isRunningPID()) {
        echo "Already running.\n";
        exit;
    }

    $today = date('Ymd');

    echo "===============================" . "\n";
    echo "  JUK Prepare Coreg Injection  " . "\n";
    echo "===============================" . "\n\n";

    Util::log_to_file('prepare_injection_'. $today . '.log', 'Started', date('Y-m-d H:i:s'));
    print ("Started: " . date('Y-m-d H:i:s') . "\n");

    $bulk_model = Util::load_model('injectionbulk');
    $injections_model = Util::load_model('coreginjection');

    // 1. Mark is_used from the previous injeciton data (please change table names to previous one)
    /*
    $num_updated = 0;
    $injections = $injections_model->find_all();
    foreach ($injections as $injection) {
        if ($my_bulk = $bulk_model->find_by_email($injection->email)) {
            $my_bulk->is_used = 1;
            $my_bulk->update();
            $num_updated++;
            Util::log_to_file('prepare_injection_'. $today . '.log', 'Updated', $my_bulk->email);
            print ("Updated: " . $num_updated . "\n");
        }
    }
    */

    // 2. Insert to current injection (please change table names to current one)

    $start = 0;
    $end = 10000;
    $limit = 5000;
    $num_inserted = 0;

    // Get the latest not used records
    $bulks = $bulk_model->find_recent_not_used($start, $limit);

    while ($num_inserted < $end) {

        foreach ($bulks as $bulk) {
            $injection = $injections_model->get_new();
            $injection->id = $bulk->id;
            $injection->email = $bulk->email;
            $injection->first_name = $bulk->first_name;
            $injection->last_name = $bulk->last_name;
            $injection->city = $bulk->city;
            $injection->postal_code = $bulk->postal_code;
            $injection->partner = "TPUK";
            $injection->source = $bulk->source;
            $injection->opt_in_ip = $bulk->opt_in_ip;
            $injection->date_opt_in = $bulk->date_opt_in;
            $injection->create();
            $num_inserted++;
            Util::log_to_file('prepare_injection_'. $today . '.log', 'Inserted', $bulk->email);
            print ("Inserted: " . $num_inserted . "\n");
        }

        $start += $limit;
        $bulks = $bulk_model->find_recent_not_used($start, $limit);
    }

    Util::log_to_file('prepare_injection_'. $today . '.log', 'Done', date('Y-m-d H:i:s'));
    print ("Done: " . date('Y-m-d H:i:s') . "\n");

    if(isset($databae)) { $database->close_connection(); }
?>
