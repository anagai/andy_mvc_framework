#!/usr/bin/php -q
<?php

require_once( __DIR__ . "/../lib/core/initialize.inc");

if(Util::isRunningPID()) {
    echo "Already running.\n";
    exit;
}

echo "===============================================\n";
echo "  Create recommended jobs csv lists            \n";
echo "===============================================\n\n";

// Make full CL60 list and split across four ways.

$fileHandle = basename($_SERVER['PHP_SELF'],'.php');
$logFile = "{$fileHandle}.log";

echo "Started...";
$startTime = Util::getUtcDt(date('Y-m-d H:i:s'));

$frstMsg = 'Started: ' . $startTime . '<br>';

$conn = Database::get_pdo_conn(DEFAULT_DB_SETTING);

$clickers = 60;
$dt = date('Y-m-d', strtotime("-{$clickers} day"));
$conn->exec('SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;');
//date(now() - interval {$clickerDays} day)
$sql = "
        SELECT u.id, u.first_name, u.last_name, u.email, u.city,
        u.postal_code, c.keyword as keyword_1, u.id as keyword_8,
        u.opt_in_ip, u.partner, u.source FROM clickers90 as c INNER JOIN user as u ON
        c.user_id=u.id WHERE c.date_updated >= ? AND
        u.is_mailable = 1 AND c.keyword!=''
        ";

unset($stmt);
echo 'execute sql started: ' . date('H:i:s') . "\n";
$taskStart = microtime(true);
$stmt['users'] = $conn->prepare($sql);
$stmt['users']->execute(array($dt));
$qryDur = microtime(true) - $taskStart;
Util::log_to_file($logFile, 'Query Dur', $qryDur);
$frstMsg .= 'Query Dur: ' . $qryDur . '<br>';
$totalRows = $stmt['users']->rowCount();
$totalPerCsv = ceil($totalRows/4);

$email_version_model = Util::load_model('emailversion');

// Get daily digest email template by weight
$email_version = $email_version_model->get_email_template("recommended_jobs");

if(!$email_version) {
    $template_id = 0;
} else {
    $template_id = $email_version->id;
}

$csvRowCnt=0;
$csvFileCnt=0;
$csvBatchCnt=0;
$totalUsers=0;
$incrDay=1;
$firstRow = array(
    'user_id', 'email_id', 'alert_id', 'first_name','last_name','email', 'city','state','zip',
    'loc','priority','keyword_1','keyword_2','keyword_3','keyword_4','keyword_5',
    'keyword_6','keyword_7','keyword_8','ip','partner','source','alerts'
    );

$hermesPath = '/srv/www/campaign_server_api/config_api/tmp/';

// The full CL60 list
$csvFullFileName = $hermesPath . "recmd_jobs_full_" . date('Ymd') . ".csv";

$fullfh=fopen($csvFullFileName, 'w');

while($user=$stmt['users']->fetch(PDO::FETCH_ASSOC)) {
    $csvRowCnt++;
    $csvBatchCnt++;
    $metroCity='';
    $metroState='';
    $stats[$csvFileCnt]['users']++;
    $totalUsers++;

    if($csvRowCnt==1) {
        fputcsv($fullfh, $firstRow);
    }

    if($csvRowCnt==1 || $csvBatchCnt>$totalPerCsv) {
        // Create new csv file
        if($fh) {
            fclose($fh);
            $st = "File: {$csvFileName} Total: " . ($stats[$csvFileCnt]['users']);
            echo $st . "\n";
            $stMsg .= $st .'<br>';
            Util::log_to_file($logFile,'Created CSV', $st);
        }

        $csvFileName = $hermesPath . "recmd_jobs_" . date('Ymd',strtotime('+' . $incrDay . ' day')) . ".csv";
        //$fh=fopen(CSV_PATH . $csvFileName, 'w');
        $fh=fopen($csvFileName, 'w');
        $stats[$csvFileCnt]['file'] = $csvFileName;

        fputcsv($fh, $firstRow);
        $csvBatchCnt=1;
        $csvFileCnt++;
        $incrDay++;
    }

    foreach($user as $k=>$v) {
        $user[$k]=trim(preg_replace('/[^\x20-\x7e\n\t]/','',$v));
    }

    $outRow = array(
        $user['id'], 0, 0, $user['first_name'], $user['last_name'], $user['email'],
        $user['city'], '', $user['postal_code'], '', '0', $user['keyword_1'], '', '', '', '', $template_id,
        '', $user['keyword_8'], $user['opt_in_ip'], $user['partner'], $user['source'], 1
    );

    fputcsv($fh, $outRow);
    fputcsv($fullfh, $outRow);
}

if($fh) {
    fclose($fh);
    $stats[$csvFileCnt]['users']++;
    $st = "File: {$csvFileName} Total: " . ($stats[$csvFileCnt]['users']);
    echo $st . "\n";
    $stMsg .= $st .'<br>';
    Util::log_to_file($logFile,'Created CSV', $st);
}

fclose($fullfh);

$st = "File: {$csvFullFileName} Total: " . $totalUsers;
    echo $st . "\n";
    $stMsg = $frstMsg . $st .'<br>' . $stMsg;
$doneDt = Util::getUtcDt(date('Y-m-d H:i:s'));
$stMsg .= 'Done: ' . $doneDt . '<br>';
$dur = strtotime($doneDt) - strtotime($startTime);
$stMsg .= 'Duration: ' . $dur . '<br>';

Util::log_to_file($logFile,'Created Full CSV', $st);
Util::systemAlert($fileHandle.'.php', $stMsg);