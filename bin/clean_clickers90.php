#!/usr/bin/php -q
<?php

    // vim: ai ts=4 sts=4 et sw=4 tw=79

    /*
     * Created on Dec 03, 2012
     * Written By Sung Hwan Ahn
     * Observed By Andy Nagai
     *
     */

    require_once( __DIR__ . "/../lib/core/initialize.inc");

    if(Util::isRunningPID()) {
        echo "Already running.\n";
        exit;
    }

    // This scrip will delete older then 90 days records
    // in the clickers90 table. That's it.

    echo "=========================================" . "\n";
    echo "  Jobungo UK Delete Old Clickers90 Rows  " . "\n";
    echo "=========================================" . "\n\n";

    $days_old = 90;

    $clickers90 = Util::load_model('clickers90');
    $before = $clickers90->count_all();
    $removed = $clickers90->delete_old_records($days_old);
    $after = $clickers90->count_all();

    Util::log_to_file('clickers90_delete.log', 'Before Total', number_format($before));
    Util::log_to_file('clickers90_delete.log', 'Total Removed',  number_format($removed));
    Util::log_to_file('clickers90_delete.log', 'After Total',  number_format($after));

    echo 'Total Before: ' . number_format($before) . "\n";
    echo 'Total Removed: ' . number_format($removed) . "\n";
    echo 'Total After: ' . number_format($after) . "\n";

    if(isset($databae)) { $database->close_connection(); }
?>
