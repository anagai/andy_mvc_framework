#!/usr/bin/php -q
<?php


    require_once( __DIR__ . "/../lib/core/initialize.inc" );

    if(Util::isRunningPID()) {
        echo "Already running.\n";
        exit;
    }

     # Get cli argument
    $args = getopt("f:");

    # Must pass numeric day interval argument and in range 1-5
    if(!empty($args['f'])) {
        $filename = $args['f'];
        if(!file_exists(CSV_PATH . $filename)) {
            exit("File does not exist in " . CSV_PATH);
        }
    } else {
       exit( "Usage: " . $argv[ 0 ] . " -f [File in /var/csv]\n" );
    }

    $logFile = "import_global_removal.log";

    Util::log_to_file($logFile, "Start Import", 'Importing ' . CSV_PATH . $filename);

    $cnt = 0;

    $dbh = Database::get_pdo_conn('bacon_rw_prod');

    if($fp = fopen(CSV_PATH . $filename,'r')) {
        while(!feof($fp)) {
            $email = fgets($fp);
            $email = trim($email);
            if(!empty($email)) {
                // Update date if email already exists
                $sql = "INSERT INTO global_removal (email, date_created, date_updated) VALUES (
                        ?, ?, ?) ON DUPLICATE KEY UPDATE date_updated=?
                ";
                $dt = date('Y-m-d H:i:s');
                $stmt = $dbh->prepare($sql);
                $stmt->execute(array($email, $dt, $dt, $dt));

                //echo "Add Global Removal: {$email} \n";

                Util::log_to_file($logFile, "Add Global Removal", $email);

                $cnt++;
            }
        }
        fclose($fp);
    } else {
        echo 'file not found';
    }

    //echo 'Total global removals added: ' . $cnt . "\n";
    Util::log_to_file($logFile, "End Import", 'Total added: ' . $cnt);

?>