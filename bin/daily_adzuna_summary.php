#!/usr/bin/php -q
<?php

require_once( __DIR__ . "/../lib/core/initialize.inc");

if(Util::isRunningPID()) {
    echo "Already running.\n";
    exit;
}

echo "==============================" . "\n";
echo "  Daily Adzuna summary email  " . "\n";
echo "==============================" . "\n\n";

$fileHandle = basename($_SERVER['PHP_SELF'],'.php');
$logFile = $fileHandle.'.log';
$errFile = $fileHandle.'_error.log';

Util::load_model('coreginjection', FALSE);


$conn = Database::get_pdo_conn(DEFAULT_DB_SETTING);

Util::log_to_file($logFile, 'Started', '');

$start = date('Y-m-d H:i:s');
echo "Started: " . $start . "\n";

$frDt = date("Y-m-d", strtotime("-1 day"));
$toDt = date("Y-m-d", strtotime("{$frDt} +1 day"));

$sql = "select count(1) from search_with_results where date_searched>=? " .
    "and date_searched<? and search_engine='adzuna';";

$stmt = $conn->prepare($sql);
$stmt->execute(array($frDt, $toDt));
$totalSearched = $stmt->fetchColumn();

$sql = "select count(1) from job_click where date_clicked>? " .
    "and date_clicked<? and engine='adzuna';";

$stmt = $conn->prepare($sql);
$stmt->execute(array($frDt, $toDt));
$totalClicked = $stmt->fetchColumn();

$dataDt = date("m/d/y, l", strtotime($frDt));

$msg = "DATE: {$dataDt}<br>";
$msg .= "PAGE VIEWS: {$totalSearched}<br>";
$msg .= "JOB CLICKS: {$totalClicked}<br>";

$to_array = array('tristan@restorationmedia.com','andy@restorationmedia.com');

//$to_array = array('andy@restorationmedia.com');

$resp = Util::send_sendgrid_email($to_array, 'mail@jobungo.co.uk', 'Jobungo UK', "JUK Adzuna metrics for " . $dataDt, '', $msg, 'jobungo_uk');

Util::log_to_file($logFile, 'Totals', "views: {$totalSearched} clicks: {$totalClicked} email sent: " . print_r($resp,true));

if(isset($database)) { $database->close_connection(); }
?>
