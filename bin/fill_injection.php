#!/usr/bin/php -q
<?php

    // vim: ai ts=4 sts=4 et sw=4 tw=79

    /*
     * Created on Dec 6, 2012
     * Written By Sung Hwan Ahn
     * Observed By Andy Nagai
     *
     */

    require_once( __DIR__ . "/../lib/core/initialize.inc");

    if(Util::isRunningPID()) {
        echo "Already running.\n";
        exit;
    }

    echo "=====================================================" . "\n";
    echo "  JUK Fill City and Postal Code for Coreg Injection  " . "\n";
    echo "=====================================================" . "\n\n";

    $injections_model = Util::load_model('coreginjection');

    $injections = $injections_model->find_all();
    foreach ($injections as $injection) {
        $gi = geoip_open(DATA_PATH."geo".DS."GeoIPCityeu.dat", GEOIP_STANDARD);
        $geo_record = geoip_record_by_addr($gi, $injection->opt_in_ip);
        if (!empty($geo_record->country_code) && $geo_record->country_code == "GB") {
            if (!empty($geo_record->city)) {
                $injection->city =     $geo_record->city;
                $injection->postal_code = $geo_record->postal_code;
            } else {
                $injection->city =     "London";
                $injection->postal_code = "E10";
            }
            $injection->update();
        }
    }

    if(isset($databae)) { $database->close_connection(); }
?>
