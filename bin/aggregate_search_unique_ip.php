#!/usr/bin/php -q
<?php

/*
 * Created on 7/09/13
 * Written By Sung Hwan Ahn / Andy Nagai
 *
 */


require_once( __DIR__ . "/../lib/core/initialize.inc");

if(Util::isRunningPID()) {
    echo "Already running.\n";
    exit;
}

echo "==========================================================" . "\n";
echo "  daily aggregate search unique ip's for versionator stats " . "\n";
echo "==========================================================" . "\n\n";
echo "Started " . date('m/d/Y H:i:s') . "\n";

$todayDt = date('Y-m-d', strtotime("-3 day"));

// 07/23/13 Andy: Unique view increment done by insert trigger in
// search_unique_ip table

/*
// Get previous day's search unique ip counts
$sql = "SELECT s.id as stats_id, COUNT( i.id ) AS uniques "
    . "FROM  versionator_stats AS s "
    . "INNER JOIN search_unique_ip AS i ON s.search_date = i.search_date "
    . "AND s.version_id = i.version_id "
    . "AND s.page_number = i.page_number "
    . "WHERE i.search_date < '{$todayDt}' "
    . "GROUP BY s.search_date, s.version_id, s.page_number";

$result = $db->query($sql);
$cnt = 0;
while($row = $db->fetch_array($result)) {
    $sql = "UPDATE versionator_stats SET unique_visitors=" . $row['uniques']
        . " WHERE id=" . $row['stats_id'];
    $db->query($sql);
    $cnt++;
}

Util::log_to_file('aggregate_search_unique_ip.log', 'Updated versionator_stats',
    $cnt . " unique counts updated");

*/

// Only hold a few days of searches. Only need prev day for aggregation.
$sql = "DELETE FROM search_unique_ip WHERE search_date < '{$todayDt}'";
$db->query($sql);


echo "Ended " . date('m/d/Y H:i:s') . "\n";


