#!/usr/bin/php -q
<?php



require_once( __DIR__ . "/../lib/core/initialize.inc");

if(Util::isRunningPID()) {
    echo "Already running.\n";
    exit;
}

echo "===============================================" . "\n";
echo "  ses v2 JUK daily digest mailing system (cron) " . "\n";
echo "===============================================" . "\n\n";

$logFile = basename($_SERVER['PHP_SELF'],'.php').'.log';

$ses_model = Util::load_model('ses');

/*************************/
/*** CREATING CSV FILE ***/
/*************************/

$today = strftime("%Y%m%d", time());

$clickerDays = 35;

$extractTotal=35000;

$logFile = 'daily_digest.log';

//***** Set campaign send time here. uk send time is equal to this time ********
$startTime = strftime("%Y-%m-%d", time()) . " 06:30:00";

//$filename = "juk_test_kw.csv";
$digestFile = "daily_digest_{$today}.csv";
$extractFile = "daily_digest_hermes_{$today}.csv";


$addRpSeeds = TRUE;
$useRpSched = FALSE;

$rpSched = array();

echo "Creating CSV file {$filename} ..." . "\n";

$conn = Database::get_pdo_conn(DEFAULT_DB_SETTING);

$sendTime = Util::getUtcDt($startTime);

$digestPath = CSV_PATH . $digestFile;
$extractPath = CSV_PATH . $extractFile;

unset($firstRow);
unset($keywords);
unset($stmt);
unset($validKeywords);
unset($stats);
unset($rpSeeds);
unset($userCount);
unset($intervalCount);
unset($seedCount);
unset($userStmt);
unset($updateStmt);
$intervalCount=0;
$rpSeeds = array();
$stats=array(
'now'=>0,
'ps'=>0,
'skip'=>0,
'start'=>(time()-1),
'seeds'=>0,
'users'=>0
);

$curDt = date('Y-m-d');

$conn->exec('SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;');

$sql .= "
    SELECT u.id, u.first_name, u.last_name, u.email, u.city,
    u.postal_code, c.keyword as keyword_1, u.id as keyword_8,
    u.opt_in_ip, u.partner, u.source FROM clickers90 as c INNER JOIN user as u ON
    c.user_id=u.id WHERE c.date_updated >
    date(now() - interval {$clickerDays} day) AND
    u.is_mailable = 1 AND c.keyword!='' AND
    (u.date_last_mailed < ? OR u.date_last_mailed='0000-00-00 00:00:00')
    ";

//echo $sql . "\n";

$stmt['data'] = $conn->prepare($sql);
$stmt['data']->execute(array($curDt));

$updateSql = "
    UPDATE user set date_last_mailed=:sendTime WHERE id=:user_id;
";

$totalUsers = $stmt['data']->rowCount();

$digestTotal = $totalUsers - $extractTotal;

if($addRpSeeds) {
    if($useRpSched === FALSE || in_array(date("Y-m-d"), $rpSched)) {
        $stmt['seeds'] = $conn->prepare('SELECT * FROM return_path_seeds;');
        if($stmt['seeds']->execute()) {
            $extractSeeds = $stmt['seeds']->fetchAll(PDO::FETCH_ASSOC);
            $digestSeeds = $extractSeeds;
        }

        $seedCount = count($extractSeeds);

        //echo 'seeds: ' . $seedCount . "users: " . $userCount . "\n";
        if($digestTotal>$seedCount) {
            $digestSeedInterval = floor($digestTotal/$seedCount);
        } else {
            $digestSeedInterval = 1;
        }

        if($extractTotal>$seedCount) {
            $extractSeedInterval = floor($extractTotal/$seedCount);
        } else {
            $extractSeedInterval = 1;
        }
        //echo 'seedinterval: ' . $seedInterval . "\n";
    }
}

$email_version_model = Util::load_model('emailversion');

// Get daily digest email template by weight
$email_template = $email_version_model->get_email_template("daily_digest");

$extractRowCnt=0;
$digestRowCnt=0;

$extractFp=fopen($extractPath, 'w');
$digestFp=fopen($digestPath, 'w');

while(($row=$stmt['data']->fetch(PDO::FETCH_ASSOC)) !== FALSE) {
    ++$stats['users'];

    foreach($row as $k=>$v) {
        //$row[$k]=htmlentities(trim(preg_replace('/[^\x20-\x7e\n\t]/','',iconv('','ASCII//TRANSLIT//IGNORE',$v))),ENT_QUOTES);
        $row[$k]=trim(preg_replace('/[^\x20-\x7e]/','', str_replace('"', '', stripslashes($v))));
    }

    if($extractRowCnt<$extractTotal) {
        $extractRowCnt++;
        ++$stats['extract_users'];

        $emId = md5($row['email'].microtime(true).mt_rand(1,1000000));

        if($extractRowCnt==1) {

            $header=array('user_id', 'email_id', 'alert_id', 'first_name','last_name','email', 'city','state','zip',
                'loc','priority','keyword_1','keyword_2','keyword_3','keyword_4','keyword_5',
                'keyword_6','keyword_7','keyword_8','ip','partner','source','alerts');

           fputcsv($extractFp, $header);

        }

        $line = array(
            $row['id'],
            $emId,
            0,
            $row['first_name'],
            $row['last_name'],
            $row['email'],
            $row['city'],
            '',
            $row['postal_code'],
            '',
            0,
            $row['keyword_1'],'','','','',$email_template->id,'',$row['keyword_8'],
            $row['opt_in_ip'],
            $row['partner'],
            $row['source'],
            1
        );

        fputcsv($extractFp, $line);

        ++$intervalCount;

        // Add RP Seeds

        if(count($extractSeeds)>0 && $intervalCount==$extractSeedInterval) {
            $seed = array_shift($extractSeeds);
            $emId = md5(crc32($seed['email']).microtime(true));
            $seedRow = array('0', $emId, '0', $seed['first_name'], $seed['last_name'],
            $seed['email'], $seed['city'], '', $seed['postal_code'], '',
                '0', $seed['keyword_1'], '', '', '', '', '', '', '', '', 'RM', '','1');
            fputcsv($extractFp, $seedRow);
            ++$stats['extract_seeds'];
            $intervalCount=0;
        }

    } else {

        $digestRowCnt++;
        ++$stats['digest_users'];
        $outRow = array(
            $row['id'], $row['first_name'], $row['last_name'], $row['email'],
            $row['city'], '', $row['postal_code'], '0', $row['keyword_1'],
            '', '', '', '', $email_template->id, '', $row['keyword_8'], $row['opt_in_ip'], $row['partner'],
            $row['source']);

        if($digestRowCnt==1) {
            $firstRow = array(
                'user_id', 'first_name', 'last_name', 'email', 'city',
                'state', 'zip', 'priority', 'keyword_1', 'keyword_2',
                'keyword_3', 'keyword_4', 'keyword_5', 'keyword_6',
                'keyword_7', 'keyword_8', 'ip', 'partner', 'source'
                );
            //fputcsv($fh, array_keys($outRow));
            fputcsv($digestFp, $firstRow);
            $intervalCount = 0;
        }

        fputcsv($digestFp, $outRow);

        ++$intervalCount;

        // Add RP Seeds

        if(count($digestSeeds)>0 && $intervalCount==$digestSeedInterval) {
            $seed = array_shift($digestSeeds);
            $seedRow = array('0', $seed['first_name'], $seed['last_name'],
                $seed['email'], $seed['city'], '', $seed['postal_code'],
                '0', $seed['keyword_1'], '', '', '', '', '', '', '', '', 'RM', '');
            fputcsv($digestFp, $seedRow);
            ++$stats['digest_seeds'];
            $intervalCount=0;
        }

        if($stats['now']!=time()){
                $stats['now']=time();
                $stats['ps']=($stats['users']/($stats['now']-$stats['start']));
                /*
                printf("\r    %.2f/s Skip:%d Seeds:%d Users:%d T:%d",
                        $stats['ps'],
                        $stats['skip'],
                        $stats['seeds'],
                        $stats['users'],
                        $stats['users'] + $stats['seeds']
                );
                */
        }

        // Update user last emailed date
        $updateStmt = $conn->prepare($updateSql);
        $updateStmt->execute(array(':sendTime'=>$sendTime, ':user_id'=>intval($row['id'])));
    }

}

fclose($digestFp);
fclose($extractFp);
//print_r($stats);

$elapsed = time()-$stats['start'];
//$lines = $stats['users'] - $stats['skip'] + $stats['seeds'];
$statLine = "Elapsed: {$elapsed} Total users: {$totalUsers} Digest users: {$stats['digest_users']} " .
    " Digest seeds: {$stats['digest_seeds']} Extract users: {$stats['extract_users']} Extract seeds: {$stats['extract_seeds']}";

echo $statLine . "\n";
Util::log_to_file($logFile,'csv created', $statLine);

if ($totalUsers < 1) {
    Util::log_to_file($logFile, 'No Users', "Nothing to send");
    exit("No need to send the digest email: 0 user\n");
}

echo "Created digest CSV File: {$digestPath}\n";
echo "Created extract CSV File: {$extractPath}\n";
Util::log_to_file($logFile,'Created digest CSV', "file: {$digestPath}");
Util::log_to_file($logFile,'Created extract CSV', "file: {$extractPath}");

$hermesPath = '/srv/www/campaign_server_api/config_api/tmp/' . $extractFile;
echo "Extractd file transferred to: {$hermesPath}\n";
Util::log_to_file($logFile,'Copied extract file:', "loc: {$hermesPath}");

copy($extractPath, $hermesPath);



//***********************
//*** UPLOAD CSV FILE ***
//***********************

echo "Connecting to Snowdon server...\n";
//$filename = 'juk_test.csv';
$sub_id = "juk{$today}";
$description = "Jobungo UK Daily Digest for " . strftime("%Y-%m-%d", time());
$subject = "\$NUM_JOBS_DISPLAYED\$+ new \$KEYWORD_1\$ jobs near \$CITY\$";

$remote_path = "/srv/www/ses25/current/db/incoming/jobungo.co.uk/{$sub_id}.csv";

$sesConn = Util::get_ssh_connection('SNOWDON');
$sesConn->put($remote_path, CSV_PATH . $digestFile, NET_SFTP_LOCAL_FILE);

echo "File transferred to {$remote_path}\n\n";
Util::log_to_file($logFile,'Uploaded CSV', "System uploaded {$digestFile} to Snowdon server.");
/*
if($conn=ssh2_connect('176.58.120.39',22,array(
    'client_to_server'=>array(
            'comp'=>'zlib,none'
    ),
    'server_to_client'=>array(
            'comp'=>'zlib,none'
    )
))){
    ssh2_auth_password($conn,'sftp','r3stm3d1a');
    //$sftp=ssh2_sftp($conn);
    ssh2_scp_send($conn, CSV_PATH . $filename, $remote_path);
    ssh2_exec($conn, 'exit');
    //if($fh=fopen("ssh2.sftp://{$sftp}/srv/www/ses25/current/db/incoming/wullo.com/testing.csv",'w')){
    //        fputs($fh,'this is a test: '.microtime(TRUE));
    //        fclose($fh);
    //}
}
*/


//***************************
//*** SETUP MAIL CAMPAIGN ***
//***************************

echo "Setup campaign...\n";

$key = sha1('hcAdI2k4HZ0XFIb_UifZmGCKkXGqs5LV:54.75.255.72');

$url = "http://snowdon.restdev.com/api/campaign/?_[key]={$key}&" .
    "sub-id={$sub_id}&" .
    "description=" . urlencode($description) . "&" .
    "start-time=" . urlencode(Util::getUtcDt($startTime)) . "&" .
    "template-id=juk_daily_digest&" .
    "campaign-subject=" . urlencode($subject) . "&" .
    "total-min-results=3&" .
    "ses-account=dedicated_certified&site=jobungouk&" .
    "send-profile=jobungo_uk_digests";

$output = Util::curl_request($url);

echo "sub_id: {$sub_id}\n";
echo "description: " . $description . "\n";
echo "csv: {$digestFile}\n";
echo "start-time: " . $startTime . "\n";
echo "url: " . $url . "\n\n";

if (empty($output)) {
    //$ses_model->update_digest_user_last_emailed($start_time, $clickerDays);
    echo "Successfully schedueled daily Jobungo UK digest emails";
    Util::log_to_file($logFile,'Success', "System scheduled daily Jobungo UK digest emails sending at ". $startTime);

} else {
    echo "Failed to schedule emails: {$output}";
    Util::log_to_file($logFile,'Failed', "Failed to schedule daily Jobungo UK digest emails: {$output}");
};

if(isset($database)) { $database->close_connection(); }

?>
