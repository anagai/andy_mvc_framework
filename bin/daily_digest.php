#!/usr/bin/php -q
<?php

    /*
     * Created on 6/28/13
     * Written By Sung Hwan Ahn / Andy Nagai
     *
     */


    require_once( __DIR__ . "/../lib/core/initialize.inc");

    if(Util::isRunningPID()) {
        echo "Already running.\n";
        exit;
    }

    echo "===============================================" . "\n";
    echo "  ses v2 JUK daily digest mailing system (cron) " . "\n";
    echo "===============================================" . "\n\n";

    /*************************/
    /*** CREATING CSV FILE ***/
    /*************************/

    $today = strftime("%Y%m%d", time());

    $clickerDays = 30;

    $ses_model = Util::load_model('ses');

    //***** Set campaign send time here. uk send time is -1 this time ********
    $start_time = strftime("%Y-%m-%d", time()) . " 04:00:00";
    $num_users = $ses_model->count_digest_mailing($clickerDays);

    if ($num_users == 0) {
        Util::log_to_file('daily_digest.log', 'No Users', "No need to send the digest email: 0 user");
        exit("No need to send the digest email: 0 user\n");
    }

    $filename = "digest_{$today}_{$num_users}.csv";

    echo "CSV File Name: {$filename}\n";

    echo "Creating CSV file..." . "\n";

    $ses_model->create_digest_csv_file(CSV_PATH . $filename, $clickerDays);
    $return_path_seeds_model = new ReturnPathSeeds();

    $return_path_seeds = $return_path_seeds_model->find_all_resource();

    $tot_seeds = 0;

    //$seed_schedule = array();

    //if ($return_path_seeds !== FALSE &&
    //    in_array(date("Y-m-d"), $seed_schedule)) {
    if ($return_path_seeds !== FALSE) {
        $fp = fopen(CSV_PATH . $filename, "a");
        while ($seed = $db->fetch_array($return_path_seeds)) {
            $num_users++;
            $tot_seeds++;
            $line = '"0","' .
                $seed['first_name'] .'","' .
                $seed['last_name'] . '","' .
                $seed['email'] . '","' .
                $seed['city'] . '","","' .
                $seed['postal_code'] . '","0","' .
                $seed['keyword_1'] . '","","","","","","","",' .
                '"209.37.24.74","RM",""';
            $line .= "\n";
            fputs($fp, $line);
        }
        fclose($fp);
    }

    echo 'Total seeds added: ' . $tot_seeds . "\n";

    echo "Created CSV File!\n\n";
    Util::log_to_file("daily_digest.log",'Created CSV', "System created {$filename} for digest mailing.");

    //***********************
    //*** UPLOAD CSV FILE ***
    //***********************

    echo "Connecting to K2 server...\n";
    $sesConn = Util::get_ssh_connection('k2');
    $sesConn->put(K2_DIR . 'var/csv/' . $filename, CSV_PATH . $filename, NET_SFTP_LOCAL_FILE);

    echo "File transferred to /srv/ses_v2/var/csv/{$filename}\n\n";
    Util::log_to_file("daily_digest.log",'Uploaded CSV', "System uploaded {$filename} to K2 server.");

    //***************************
    //*** SETUP MAIL CAMPAIGN ***
    //***************************

    echo "Setup campaign...\n";

    $sub_id = "JUK{$today}";
    $description = "Jobungo UK Daily Digest for " . strftime("%Y-%m-%d", time()) . " ({$num_users} users)";
    $subject = "\$NUM_JOBS_DISPLAYED\$+ new \$KEYWORD_1\$ jobs near \$CITY\$";

    $url = "http://ses13.jobungo.co.uk/no_really_admin/create_campaign.php?" .
           "sub-id={$sub_id}&" .
           "description=" . urlencode($description) . "&" .
           "csv={$filename}&" .
           "start-time=" . urlencode($start_time) . "&" .
           "template-id=3&" .
           "campaign-subject=" . urlencode($subject) . "&" .
           "total-min-results=3&" .
           "search-country=gb&number-processes=100&ses-account=dedicated_certified&site=jobungouk&send-profile=jobungo_uk_digests&search-provider=indeed&".
           "campaign-keyword-1=&campaign-keyword-1-days-back=1&campaign-keyword-1-radius=25&campaign-keyword-1-min-results=3&campaign-keyword-1-max-results=10&" .
           "campaign-keyword-2=&campaign-keyword-2-days-back=&campaign-keyword-2-radius=&campaign-keyword-2-min-results=&campaign-keyword-2-max-results=&" .
           "campaign-keyword-3=&campaign-keyword-3-days-back=&campaign-keyword-3-radius=&campaign-keyword-3-min-results=&campaign-keyword-3-max-results=&" .
           "campaign-keyword-4=&campaign-keyword-4-days-back=&campaign-keyword-4-radius=&campaign-keyword-4-min-results=&campaign-keyword-4-max-results=&" .
           "campaign-keyword-5=&campaign-keyword-5-days-back=&campaign-keyword-5-radius=&campaign-keyword-5-min-results=&campaign-keyword-5-max-results=&";

    $curl_options = array(
        CURLOPT_URL => $url,
        CURLOPT_HEADER => 0,
        CURLOPT_USERPWD =>"stats_candy:icedteaisnotcoffee",
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_TIMEOUT => 5,
    );

    $ch = curl_init();
    curl_setopt_array( $ch, $curl_options );
    $output = curl_exec( $ch );
    curl_close($ch);

    echo "sub_id: {$sub_id}\n";
    echo "description: {$description}\n";
    echo "csv: {$filename}\n";
    echo "start-time: {$start_time}\n\n";

    if (strrpos($output, "reation spawned")) {
        $start_time = $start_time;
        $ses_model->update_digest_user_last_emailed($start_time, $clickerDays);
        echo "Successfully schedueled daily Jobungo UK digest emails";
        Util::log_to_file("daily_digest.log",'Success', "System scheduled daily Jobungo UK digest emails sending at ". $start_time);

    } else {
        echo "Failed to schedule emails: {$output}";
        Util::log_to_file("daily_digest.log",'Failed', "Failed to schedule daily Jobungo UK digest emails: {$output}");
    };

    if(isset($database)) { $database->close_connection(); }

?>
