#!/usr/bin/php -q
<?php

    // vim: ai ts=4 sts=4 et sw=4 tw=79

    /*
     * Created on Dec 13, 2012
     * Written By Sung Hwan Ahn
     * Observed By Andy Nagai
     *
     */

    require_once( __DIR__ . "/../lib/core/initialize.inc");

    if(Util::isRunningPID()) {
        echo "Already running.\n";
        exit;
    }

    $today = date('Ymd');

    echo "==========================================" . "\n";
    echo "  JUK Fill Empty Date in Complaint Table  " . "\n";
    echo "==========================================" . "\n\n";

    Util::log_to_file('fill_complaint_date'. $today . '.log', 'Started', date('Y-m-d H:i:s'));
    print ("Started: " . date('Y-m-d H:i:s') . "\n");

    $complaint_model = Util::load_model('complaint');
    $sessent_model = Util::load_model('sessent');

    // Get the empty date_received
    $num_updated = 0;
    $complaints = $complaint_model->get_empty_date_received();
    foreach ($complaints as $complaint) {
        if ($sessent = $sessent_model->find_by_email('juk_welcome', $complaint->email_address)) {
            $complaint->date_received = date('Y-m-d H:i:s', strtotime($sessent->date_sent . ' + 1 hour'));
            $complaint->date_added = $complaint->date_received;
            $complaint->details = "filled date by a script";
            $complaint->update();
            $num_updated++;
            Util::log_to_file('fill_complaint_date'. $today . '.log', 'Updated', $bounce->email_address);
            print ("Updated: " . $num_updated . "\n");
        }
    }

    Util::log_to_file('fill_complaint_date'. $today . '.log', 'Done', date('Y-m-d H:i:s'));
    print ("Done: " . date('Y-m-d H:i:s') . "\n");

    if(isset($databae)) { $database->close_connection(); }
?>
