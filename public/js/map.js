// ****** Juk.map module *******
// Google map api scripts needed for whose searching maps
// on home and searching pages.
// This can be a stand alone module. Does not need Juk to be defined.

// Create empty object literal if Juk does not exist.
var Juk = Juk || {};

Juk.map = function(window, $, undefined) {
	
	var _map = {};

	_map.set_marker_timers = function( map, lat, long, base_url, no_loc, no_center_pin ) {

		if(no_loc==undefined) {
	    	no_loc = '';
	    } else {
	    	no_loc = 'TRUE';
	    }
		var url = "/ajax/get_cached_searches.php?lat=" + lat + "&long=" + long + "&nolocation=" + no_loc;
	    $.getJSON( url, function( data ) {
	        var ms_per_drop = 1500;
	        var total_pulled = 0;
	
	        // center pin
	        
	        if(no_center_pin==undefined) {
	        	_map.place_marker( map,
	        			{"longitude":long,"latitude":lat},
	        			"/images/orange-pin.png", true, base_url);
	        }
	
	        // Set a bunch of timers
	       
	        $.each( data, function( idx, val ) {
	            // lengthen timeout for each pin so dropped one at a time
	        	var thisDelay = idx * ms_per_drop + 1500;
	            setTimeout( function() {
	                _map.place_marker( map, val,
	                "/images/map-pins.png", false, base_url );
	            }, thisDelay );
	
	            total_pulled += 1;
	        } );
	        // Give us a little buffer
	        total_pulled += 1;
	
	        // Do it again!
	        // Get a another set of random pins and do again
	        setTimeout( function() {
	            _map.set_marker_timers( map, lat, long, base_url, no_loc, no_center_pin )
	        }, total_pulled * ms_per_drop );
	       
	        
	    } );
	};
	
	_map.place_marker = function( map, data, ico, is_center, base_url ) {
		    
		var url = base_url + "/jobs"
	        + "/q-" + escape( data[ "job" ] )
	        + "/l-" + escape( data[ "location" ]) + '/';
	    		
	    var lat = data[ "latitude" ];
	    var lng = data[ "longitude" ];
	    var myLatlng = new google.maps.LatLng( lat, lng );
	
	    var marker = new google.maps.Marker({
	        position: myLatlng,
	        map: map,
	        animation: google.maps.Animation.DROP,
	        title: '',
	        icon: ico
	    });
	   
	
	    // Add an entry to #user_list
	    // TODO: Make this a link!
	    
	    var box_text = data[ "job" ] + " in " + data[ "location" ];
	    $( "#user_list" )
	        .children( "ul" )
	        .prepend( "<li><a href='" + url + "' target='_blank'>"
	            + box_text + "</a></li>\n" );
	
	    // Expire the pin!. 
	    // Pin will disappear after 4 minutes
	    // Do not wipe out center pin
	    if(!is_center) {
	    	setTimeout( function() {
	    		marker.setMap( null );
	    		marker = null;
	    	}, 240000 );
	    }
	};
	
	return _map;
	
}(this, jQuery);