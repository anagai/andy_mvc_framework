// vim: ai ts=4 sts=4 et sw=4 tw=79

	/**
	 * Created on Oct 16, 2012
	 * Written By Sung Hwan Ahn
	 * Converted to Module - Andy Nagai 11/24/2012
	 *
	 */


// ******* Juk Module ********
// Common scripts go into Juk. All javascript should be namespaced
// in order to avoid conflicts in the global scope.

// Is a self executing closure function. Returns closure object
// with private and public members. Other functional modules can be added
// elsewhere when needed. e.g. Juk.map, Juk.validate
var Juk = function (window, $, undefined) {

	// Stop execution if Juk has been defined by another library
	if(window.Juk != undefined) {
		throw new Error('Juk is already defined. Cannot proceed.');
	}

	var _Juk = {};


	/**
	 * Add search parameters to form action so can have friendly url
	 * in address bar after submit for jobs page
	 *
	 */
	_Juk.update_form_action = function(frm) {

		$("#" + frm).submit(
			function(event) {
				event.preventDefault();
				var kw = $("#keyword").val();
				var loc = $("#location").val();
				// url encode special characters
				kw = encodeURIComponent(kw);
				loc = encodeURIComponent(loc);

				// Encode url space code %20 to plus sign
				kw = kw.replace('%20', '+');
				loc = loc.replace('%20', '+');

				var redirect = '/jobs/q-'+kw+'/l-'+loc;

				location.href = redirect;
			}
		);
	};

	/**
	 * Attach autocomplete behavior to keyword and location fields
	 */

	_Juk.init_autocomplete = function() {

		if($('#keyword').length) _Juk.makeSearch('keyword', 'keyword');
		if($('#location').length) _Juk.makeSearch('location', 'location');
	};

	/**
	 * Attach job click handler to all job title and more links
	 */

	_Juk.init_result_click = function() {

		$(".lnk_job").click(
			function() {
				// Any repetitive handler code should go in its own function.
				// Otherwise each link will have its own copy of the same code.
				_Juk.send_result_click(this);
			}
		);

		$(".lnk_more_job").click(
			function() {
				_Juk.send_result_click(this);
			}
		);
	};

	_Juk.send_result_click = function(elem) {

		// Extract number portion from job link id property
		var elem_id = $(elem).attr('id');
		var patt = /_(\d+)/;
		var match = patt.exec(elem_id);
		var job_index = match[1]
			? match[1]
			: '';

		//var job_index_prefix = "lnk_job_";
		//var job_index = $(elem).attr('id').substring(job_index_prefix.length);

		var rc_keyword = $("#keyword").val();
		var rc_location = $("#location").val();
		//var rc_daysback = $(".days_back.active").attr('rel');
		var rc_daysback = $("#hid_days_back").val();
		var rc_radius = $("#hid_radius").val();
		var rc_page_number = $("#page_number").val();
		var rc_version_id = $("#version_id").val();

		var result_key = $("#result_key_" + job_index).val();
		var result_title = $("#result_title_" + job_index).val();
		var result_company = $("#result_company_" + job_index).val();
		var result_location = $("#result_location_" + job_index).val();
		var result_list_position = $("#result_position_" + job_index).val();
		var result_age = $("#result_age_" + job_index).val();
		var result_sponsored = $("#result_sponsored_" + job_index).val();
		//var result_channel = $("#result_channel_"+job_index).val();
		var result_search_engine = $("#result_search_engine_" + job_index).val();

		var jqxhr = $.post("/ajax/jobs_ajax.php", {method:"job_click",
										keyword:rc_keyword,
										location:rc_location,
										daysback:rc_daysback,
										radius:rc_radius,
										page_number:rc_page_number,
										result_version: rc_version_id,
										result_key:result_key,
										result_title:result_title,
										result_company:result_company,
										result_location:result_location,
										result_list_position:result_list_position,
										result_age:result_age,
										result_sponsored:result_sponsored,
										channel: '',
										search_engine:result_search_engine},
			function(result) {
				if ($.trim(result) != "okay") {
					alert(result);
				}
			}
		);

		jqxhr.error(function(jqXHR, textStatus, errorThrown){});
		jqxhr.complete(function(jqXHR, responseText) {});

	};

	//Holds autocomplete list data
	var resp_array = new Array();

	_Juk.makeSearch = function(inp, type) {

		var inp_id = $('#' + inp + '_id');

		$('#' + inp).autocomplete({ //Add autocomplete functionality to input box
			delay: 0,
			minLength: 2, //minium characters to perform ajax lookup
			source: function( request, response ) {
				//request object has entered keyword
				//response object is what populates autocomplete list
				_Juk.getData(response, request.term, type);

			},
			select: function( event, ui ) {
				//When item is selected in autocomplete list set id in hidden field
				if(ui.item.id) {
					inp_id.val(ui.item.id);
					//console.log('id: ' + ui.item.id + ' kw: ' + inp_id.val());
					ui.item.option.selected = true;
					// Need to trigger change event when user uses enter key to select from list
					$(this).trigger('change');
				}
			},
			change: function( event, ui ) {

				//When user manually types search term need to match against result set and set id in hidden field
				if ( !ui.item ) {
					var valid = false;

					var match = _Juk.matchItem($(this).val());

					if(match.id) {
						inp_id.val(match.id);
						$(this).val(match.tx);
						//ui.item.option.selected = true;
						valid = true;
					}

					//There is no match so clear hidden and input fields
					/*
					if(!valid && type!='keyword') {
						inp_id.val('');
						$(this).val('');
					}
					*/
				}

			}

		});
	};


	//Find matching item by name in result set
	_Juk.matchItem = function(term, resp) {
		var ret = {};
		for(var i=0, len=resp_array.length; i < len; i++) {
			//console.log('term: ' + term + 'city: ' + resp_array[i][1]);
			if(resp_array[i][1].toLowerCase() === term.toLowerCase()) {
				//console.log('match found');
				ret.id = resp_array[i][0];
				ret.tx = resp_array[i][1];
				return ret;
			}

		}

		//no match
		return '';
	};

	//Make ajax call to get list
	_Juk.getData = function(response, term, req_type) {

		$.ajax({
			url: '/ajax/jobs_ajax.php',
			type: 'POST',
			dataType: 'json',
			context: this,
			data: {srch: term, method: req_type},
			error: function() {

				//console.log('error');

			},
			success: function(resp){

				//Put response data into an array
				if(resp && resp.length) {

						var data = resp;
						var len = data.length;
						for(var i = 0; i < len; i++) {

							resp_array[i] = [data[i].id, data[i].tx];

						}



					//Generate the autocomplete list.
					response($.map( data, function( item ) {

						return {
							label: item.tx,
							value: item.tx,
							id: item.id,
							option: this
						}
					}));



				} else {
					response();
				}
			} //end of success block

		}); //End of ajax block

	};

	/**
	 * job alert signup ajax request
	 * Written by Andy
	 */
	_Juk.alert_signup = function() {


		if(!$('#txt_signup_email').val()) {
			alert('You must enter an email!');
		} else {

			// 06/27/13 Andy: Was passing keyword. not needed
			// kw: $('#keyword').val(),

			$.post("/ajax/jobs_ajax.php", {
		        email: $('#txt_signup_email').val(),
		        loc: $('#txt_signup_loc').val(),
		        kw: $('#keyword').val(),
		        method: 'signup'},
			function(response) {

		        if(response.status == 'success') {

					alert('Thank you for signing up for job alerts!');

					// Close signup form and remove roadblock
				    //$('#myModal').modal('toggle');
					//$('.modal-backdrop').removeClass();

					// Reload page to record valid user's search and use
					// non-organic result version with no signup form
					document.location.reload();

		        } else if(response.status == 'bad_email') {
		        	alert('Email is invalid');
		        }

			}, "json"
			).error(function() {
				alert('failed');
			});

		}

	};

	_Juk.popup_alert_signup = function() {

		if(!$('#txt_popup_signup_email').val()) {
			alert('You must enter an email!');
		} else if(!$('#chkagree').prop("checked")) {
			alert('You must agree to disclaimer in order to receive daily job alerts.');
		} else {

			// 06/27/13 Andy: Was passing keyword. not needed
			// kw: $('#keyword').val(),

			$.post("/ajax/jobs_ajax.php", {
				email: $('#txt_popup_signup_email').val(),
				loc: $('#location').val(),
				kw: $('#keyword').val(),
				method: 'signup'},
			function(response) {

				if(response.status == 'success') {

					alert('Thank you for signing up for job alerts!');

					// Close signup form and remove roadblock
					$('#myModal').modal('toggle');
					$('.modal-backdrop').removeClass();

					// Reload page to record valid user's search and use
					// non-organic result version with no signup form
					document.location.reload();

				} else if(response.status == 'bad_email') {
					alert('Email is invalid');
				}

			}, "json"
			).error(function() {
				alert('failed');
			});

		}

	};

	return _Juk;


// Use this to pass in window object. Certain browsers
// have window object by different name. jQuery object passed in
// so doesn't have to look for it up scope chain.
}(this,jQuery);
