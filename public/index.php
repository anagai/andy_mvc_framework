<?php
require_once( __DIR__ . '/../lib/core/initialize.inc' );

$app->route();

// Redirect US users to jobongo.com. Except if testing from office or partnersfeed.
/*
if($app->valid_us_ip() && $app->get_view() != 'partnersfeed' &&
		$_SERVER[ "REMOTE_ADDR" ] != "38.122.21.66" &&
		$_SERVER[ "REMOTE_ADDR" ] != "127.0.0.1" &&
		$_SERVER[ "REMOTE_ADDR" ] != "68.96.93.160") { //Sung's home IP

		header('Location: http://www.jobungo.com');
}
*/

// Maintenance switch set in Application->init()
if(MAINTENANCE === false) {
	$app->render();
} else {
	$app->set_view('maintenance');
	$app->render();
	//echo "Site is under maintenance. We will be up shortly!";
}

// If switch is true in initialize.inc then print debug statements
// Can print debug statements from anywhere with Util::debug();
if(DEBUG) {
	$app->print_debug_stmt();
	// Never display sql on production
	if(!PROD_SERVER) {
		$app->print_debug_sql_stmt();
	}
	$app->print_load_stats();
}
?>
