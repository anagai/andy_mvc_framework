<?php

// output specifc job result version css file content specified in css=file
// in url

header('Content-type: text/css');

/*
if(session_id() == '' ) {
    Util::my_session_start();
}
*/

$css = '';

// This is a system constant. linux and pc use different separator dashes.
$ds = DIRECTORY_SEPARATOR;

//$file_root = 'C:\Users\Restoration Media\projects\jobungouk\lib\core\..\..\\';

$file_root = __DIR__ . $ds . ".." . $ds . ".." . $ds;

if(!empty($_GET['css'])) {

	$css = $file_root . 'lib' . $ds . 'template' . $ds .'result' . $ds .'css' . $ds . $_GET['css'];
	// Kick out if we're getting a file that's not in our directory!
	// -suy, 20130702
	// TODO: Centralize the location of $sentinel and set it correctly!
	$sanitized_css = realpath( $css );
	#$sentinel = '/srv/dev-jobungouk/';
	# Extract out parent path
	$sentinel = substr($file_root, 0, strpos($file_root, 'public/'));
	#print 'sentinel: ' . $sentinel . "\n";
	#print 'sanit: ' . $sanitized_css . "\n";
        #print 'css: ' . $css . "\n";
	if( strncmp( $sanitized_css, $sentinel, strlen( $sentinel ) ) ) {
		$css = '';
	}

	// Kick out if the file we're retrieving doesn't end in .css!
	// -suy, 20130703
	if( !preg_match( '/\.css$/', $css ) ) {
		$css = '';
	}

	// Make sure file exists
	if(!file_exists($sanitized_css)) {
		$css = '';
	}
}

// Default to base.css if no css file found
if(empty($css)) {
	$css = $file_root . 'public' . $ds . 'css' . $ds .'base.css';
}

$path = pathinfo($css);

// Print out css filename at the top of output
echo "/*  " . $path['filename'] ."." . $path['extension'] . "  */\n";

// Output the css file
readfile($css);

?>
