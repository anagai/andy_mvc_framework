<?php
	// vim: ai ts=4 sts=4 et sw=4 tw=79

	/*
	 * Created on Oct 17, 2012
	 * Written By Sung Hwan Ahn / Andy Nagai
	 *
	 */


	require_once( __DIR__ . "/../../" ."lib/core/initialize.inc");

	// AJAX Calling Method
	switch ($_POST['method']) {
		case 'job_click':
			job_click($_POST['keyword'], $_POST['location'], $_POST['daysback'],
				$_POST['radius'], $_POST['page_number'], $_POST['result_key'],
				$_POST['result_title'], $_POST['result_company'],
				$_POST['result_location'], $_POST['result_list_position'],
				$_POST['result_age'], $_POST['result_sponsored'],
				$_POST['channel'], $_POST['search_engine'],
				$_POST['result_version']);
			break;
		case 'keyword':
			autofill_keyword($_POST['srch']);
			break;
		case 'location':
			autofill_location($_POST['srch']);
			break;
		case 'signup':
			if($status = email_alert_signup()) {

				if($status === true) {
					echo '{"status":"success"}';
				} else {
					echo '{"status":"'. $status . '"}';
				}

			} else {
				echo "failed";
			}
			break;
		case 'accept_cookie':
			accept_cookie();
			break;
	}

	// Job List Click Result Tracking
	function job_click($keyword, $location, $daysback, $radius, $page_number,
		$result_key, $result_title, $result_company, $result_location,
		$result_list_position, $result_age, $result_sponsored, $channel,
		$search_engine, $result_version) {

		$job_click = new JobClick();
		$browser = new Browser();

		// parse search location to city, state, zip
		//$loc = SearchLocation::parse_location($location);

		if(isset($_SESSION['uid'])) {
			$user_model = Util::load_model('user');
			$user = $user_model->find_by_id($_SESSION['uid']);
			$job_click->user_id = $user->id;
			$job_click->email = $user->email;
			$job_click->partner = $user->partner;
			$job_click->source = $user->source;
			//$job_click->subcode = $user->subcode;
		}

		if(!$page_number) {
			$page_number = 1;
		}

		$job_click->query = $keyword;
		$job_click->location = $location;
		$job_click->daysback = $daysback;
		$job_click->radius = $radius;
		$job_click->ip = $_SERVER['REMOTE_ADDR'];
		$job_click->device = $browser->device;
		$job_click->browser = $browser->name;
		$job_click->browser_version = $browser->version;
		$job_click->is_mobile = $browser->is_mobile;
		//$job_click->channel = $channel;
		$job_click->page_number = $page_number;
		$job_click->version_id = $result_version;
		$job_click->engine = $search_engine;
		$job_click->job_key = $result_key;
		$job_click->job_title = $result_title;
		$job_click->job_company = $result_company;
		$job_click->job_location = $result_location;
		$job_click->position = $result_list_position;
		$job_click->age = $result_age;
		$job_click->sponsored = $result_sponsored;
		$job_click->sub_id = "RM" . strftime("%Y%m%d", time());
		$job_click->date_clicked = strftime("%Y-%m-%d %H:%M:%S", time());

		$job_click->create();

		// Update versionator_stats click count
		$model = Util::load_model('versionatorstats');
		//$model->save_versionator_click_stats($result_version, $page_number,
		//	$result_sponsored);

		// Update partner_source_stats click count for registered user
		if(isset($_SESSION['uid'])) {
			$ps_model = Util::load_model('partnersourcestats');
			$ps_model->setState('user', $user);
			//$ps_model->save_partner_source_click_stats();
			//$ps_model->save_click_unique_email();
		}

		echo "okay";
	}

	function autofill_keyword($term) {
		$validkeyword = Util::load_model('validkeyword');
		echo $validkeyword->find_keyword_list($term);
	}

	function autofill_location($term) {
		$geocode = Util::load_model('geocodelocation');
		echo $geocode->find_location_list($term);
	}

	function email_alert_signup() {

		$user_model = Util::load_model('user');
		$unsubscribe_model = Util::load_model('unsubscribe');
		$geocode_model = Util::load_model('geocodelocation');

		$email = Util::getVar('email', '', 'POST');
		$keyword = Util::getVar('kw', '', 'POST');
		$city = Util::getVar('loc', '', 'POST');

		$loc = '';
		$postal = '';

		//print_r($user);


		if(!empty($email)) {

			// - Andy 20130806, Also check if bad top level domain and
			// use builtin email validation
			if(!filter_var($email, FILTER_VALIDATE_EMAIL) ||
				Util::is_bad_TLD($email)) {
				return 'bad_email';
			}

			$user = $user_model->find_by_email($email);

			$now_dt = date('Y-m-d H:i:s');

			// get user's current location by IP. Will default to London
			// if not valid UK IP

			if(empty($city)) {
				$loc = $geocode_model->get_loc_by_ip();
				$postal = $loc['postal'];
				$city = $loc['city'];
			} else {
				$postal = $geocode_model->get_postal_by_city($city);
			}

			//if(empty($postal) || empty($city)) {
			//	$postal = 'E10';
			//	$city = 'London';
			//}

			$city = ucwords($city);

			if(!empty($user)) {
				// Existing user

				// If user is un-mailable remove from unsubscribe and update
				// as mailable
				if($user->is_mailable == 0) {
					// Remove from unsubscribe
					$unsubscribe_model->remove_from_unsubscribe($email);
					$user->is_mailable = 1;
				}

				$user->has_engaged = 1;
				$user->update();

				// Set user to session
				$_SESSION['uid'] = $user->id;

			} else {

				// ***** Email not found. Create new user
				$new_user = new User();
				$new_user->email = $email;
				$email_exp = explode('@', $email);
				$new_user->email_domain = $email_exp[1];
				$new_user->partner = 'JUO';
				$new_user->source = 'jobungo.co.uk';
				$new_user->is_mailable = 1;
				$new_user->date_added = $now_dt;
				$new_user->date_opt_in = $now_dt;
				$new_user->opt_in_ip = $_SERVER['REMOTE_ADDR'];
				$new_user->has_engaged = 1;
				$new_user->city = $city;
				$new_user->postal_code = $postal;
				$new_user->create();

				// Add encrypted user id as reg_key
				$new_user->reg_key = Util::mcryptEncode($new_user->id);
				$new_user->update();

				// Set user to session
				$_SESSION['uid'] = $new_user->id;

				// ***** Update or create clickers90 record
				//modify_clickers90($email, $loc, $keyword, $now_dt);

				$clickers90_model = Util::load_model('clickers90');

				if(!empty($city)) {
					$loc = $city;
				} else {
					$loc = $postal;
				}

                $clickers90_model->modify_clickers90($new_user->id,
                    $keyword, $loc);
			}
		}
		return true;
	}

	function accept_cookie() {

		$browser = new Browser();
		$accept_cookie = new AcceptCookie();

		if(isset($_SESSION['uid'])) {
			$user_model = Util::load_model('user');
			$user = $user_model->find_by_id($_SESSION['uid']);
			$accept_cookie->email = $user->email;
		}

		$accept_cookie->ip = $_SERVER['REMOTE_ADDR'];
		$accept_cookie->device = $browser->device;
		$accept_cookie->browser = $browser->name;
		$accept_cookie->browser_version = $browser->version;
		$accept_cookie->user_agent = $_SERVER['HTTP_USER_AGENT'];
		$accept_cookie->is_mobile = $browser->is_mobile;
		$accept_cookie->date_accepted = date('Y-m-d H:i:s');

		$accept_cookie->create();

		echo "success";
	}
