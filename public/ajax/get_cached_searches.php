<?php
// vim: ai ts=4 sts=4 et sw=4 tw=79

require_once( __DIR__ . "/../../" ."lib/core/initialize.inc");

$longitude = $_GET['long'];
$latitude = $_GET['lat'];
$nolocation = $_GET['nolocation'];
$radius = 30;

$lng_min = $longitude - $radius*2 / abs(cos(deg2rad($latitude)) * 69);
$lng_max = $longitude + $radius*2 / abs(cos(deg2rad($latitude)) * 69);
$lat_min = $latitude - ($radius / 69);
$lat_max = $latitude + ($radius / 69);

// Get cached JSON
$searches = array();
if($nolocation=='TRUE'){
$sql = <<<END_OF_SQL
        SELECT * FROM cached_map_results ORDER BY id DESC
        limit 100
END_OF_SQL;
} else {
$sql = <<<END_OF_SQL
        SELECT * FROM cached_map_results
        WHERE latitude > $lat_min and latitude < $lat_max
        AND longitude > $lng_min and longitude < $lng_max
        ORDER BY id DESC
        limit 100
END_OF_SQL;

}

$results = $db->query( $sql );
while($row = $results->fetch_assoc()){
        array_push($searches,$row);
}
echo json_encode($searches);

?>
