<?php
// vim: ai ts=4 sts=4 et sw=4 tw=79

$controller = Util::load_controller('jobs');

$out = $controller->runTask();

$template = $app->get_page_template();

//include_once(TEMPLATE_PATH . $template .'_header.inc');

if($out['browser']->is_mobile || $_REQUEST['v']==3) {
    // mobile version of rec jobs page
    $out['components']['css'] = 'c100.css';
    include_once(JRP_VERSION_PATH . 'header' . DS . 'h100.inc');
    include_once(JRP_VERSION_PATH . 'body' . DS . 'rj100.inc');
    include_once(JRP_VERSION_PATH . 'footer' . DS . 'f100.inc');
} else {
    // all other versions
    include_once(JRP_VERSION_PATH . 'header' . DS . 'h4.inc');


?>
<!-- container -->
    <div class="recommended-jobs">
        <div class="recommended-column-container">
            <div class="recommended-jobs-header">
                <h1><?php
                $trimName = trim($out['username']);
                if(empty($trimName)) {
                    echo 'Welcome!';
                } else {
                    echo "Welcome, {$trimName}!";
                }
                ?></h1>
                <div>We found new <?=$out['keyword']?> jobs near <?=$out['location'] ?> that we'd like to recommend.</div>
            </div>
            <div class="recommended-column-headers">
                <div class="recommended-column-header-2">
                    <h3>Recommended Jobs<?php echo (!empty( $out['location'] ) ? ' near ' . $out['location'] : ''); ?></h3>
                </div>
                <div class="sponsored-column-header-2">
                    <h4>Sponsored Ads</h4>
                </div>
            </div>
            <div class="primary-col-2 recommended-column">
                <style type='text/css'>
                    #indJobContent {
                            padding-bottom:0;
                    }
                    #indJobContent a {
                            color:#1223CC;
                            font-size:16px;
                            line-height:23px;
                            font-weight:bold;
                    }
                    #indJobContent .company_location {
                        font-size:13px;
                        overflow:hidden;
                        display:block;
                        color:#444444;
                        line-height:18px;
                    }

                    #indJobContent .company {
                        font-weight:bold;
                    }
                    #indJobContent.wide .indeedjob {
                            display:block;
                            float:left;
                            margin:0;
                            padding:0;
                            width:100%;
                    }
                    #indeedresults .indeedjob {
                            padding:12px 1em !important;
                            border-bottom: 1px solid #EEEEEE;
                   }
                   #indeedresults .indeedjob:not(:last-child) {
                            border-bottom: 1px solid #EEEEEE;
                     }

                    #indeed_widget_wrapper {
                            position:relative;
                            font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;
                            font-size:13px;
                            font-weight:normal;
                            color:#4C493E;
                            line-height:18px;
                            padding:0 0 10px 0;
                            margin:0;
                            height:auto;
                            width:100%;
                    }
                    #indeed_widget_header {
                            font-size:18px;
                            color:#222;
                            padding-bottom:5px;
                    }
                    #indeed_search_wrapper {
                            clear:both;
                            font-size:12px;
                            margin-top:5px;
                            padding-top:5px;
                            /* border-top:1px solid #DDD;
                            */
                    }
                    #indeed_search_wrapper label {
                            font-size:12px;
                            line-height:inherit;
                            text-align:left;
                    }
                    #indeed_search_wrapper input[type='text'] {
                            width:100px;
                    }
                    #indeed_search_wrapper.stacked input[type='text'] {
                            width:150px;
                    }
                    #indeed_search_wrapper.stacked label {
                            display:block;
                            padding-bottom:5px;
                    }
                    #indeed_search_wrapper.stacked label span {
                            display:block;
                    }
                    #indeed_search_footer {
                            width:295px;
                            padding-top:5px;
                            height:15px;
                    }
                    #indeed_link {
                            position:absolute;
                            bottom:1px;
                            right:5px;
                            clear:both;
                            font-size:11px;
                    }
                    #indeed_link a {
                            text-decoration:none;
                            color:#000000;
                    }
                    #results .job {
                            padding:10px 20px;
                            border-bottom:1px solid #EEEEEE;
                    }
                    #results #job_9 {
                            border-bottom:1px solid #EEEEEE;
                    }
                </style>
                <div>
                    <script type='text/javascript'>
                        var ind_pub = '1448762077525160';
                        var ind_el = 'indJobContent';
                        var ind_pf = 'indeed';
                        var ind_q = '';
                        var ind_l = '<?=$out['location']?>';
                        var ind_chnl = 'jrollwidget';
                        var ind_n = 10;
                        var ind_d = 'http://www.indeed.co.uk';
                        var ind_t = 40;
                        var ind_c = 30;
                    </script>
                    <script type='text/javascript' src='http://www.indeed.co.uk/ads/jobroll-widget-v3.js'></script>
                    <div id="indeed_widget_wrapper" style="margin: 0;">
                        <div id='indJobContent'></div>
                    </div>
                </div>
            </div>

            <div class="ad-col-2 sponsored-widget" style="background-color: #f9f9f9">
                <script type="text/javascript" charset="utf-8">
                    (function(G,o,O,g,L,e){G[g]=G[g]||function(){(G[g]['q']=G[g]['q']||[]).push(
                    arguments)},G[g]['t']=1*new Date;L=o.createElement(O),e=o.getElementsByTagName(
                    O)[0];L.async=1;L.src='//www.google.com/adsense/search/async-ads.js';
                    e.parentNode.insertBefore(L,e)})(window,document,'script','_googCsa');
                </script>
                <div style="padding: 0 10px;">
                    <div id="adcontainer1"></div>
                </div>
                <script type="text/javascript" charset="utf-8">
                    var pageOptions = {
                      'pubId': 'pub-2202016470033432',
                      'query': '<?=$out['keyword']?> jobs <?=$out['location'] ?>',
                      'channel': '4021913302',
                      'hl': 'en',
                      'location': false,
                      'plusOnes': false,
                      'sellerRatings': false,
                      'siteLinks': false,
                      'linkTarget' : '_blank'
                    };

                    var adblock1 = {
                      'container': 'adcontainer1',
                      'number': 5,
                      'width': '100%',
                      'fontSizeTitle': 16,
                      'colorTitleLink': '#1223CC',
                      'colorText': '#AAAAAA',
                      'colorDomainLink': '#409059',
                      'colorBackground': '#F9F9F9',
                      'detailedAttribution': false
                    };
                _googCsa('ads', pageOptions, adblock1);
                </script>
            </div>

        </div>
        <div class="recommended-column-btns">
            <div class="recommended-column-btn-2">
                <a href="/jobs.php/q-<?=$out['keyword']?>/l-<?=$out['location']?>/">View More Jobs</a>
            </div>
            <div class="sponsored-column-btn" style="background-color: #f9f9f9; width: 291px; height: 82px; border-right: 1px solid #eee;">&nbsp;</div>
        </div>
    </div>
    <div class="footer-last-call container popular-keywords-3 clearfix" style="border-bottom: 0;  padding-left: 20px">
        <div>
            <ul>
                <li>Popular Keywords &rarr;</li>
                <li><a href="/jobs/q-Accounting/l-<?= $out['location'] ?>/">Accounting</a> / <a href="/jobs/q-Finance/l-<?= $out['location'] ?>/">Finance</a></li>
                <li><a href="/jobs/q-Administrative/l-<?= $out['location'] ?>/">Administrative</a> / <a href="/jobs/q-Clerical/l-<?= $out['location'] ?>/">Clerical</a></li>
                <li><a href="/jobs/q-Customer+Service/l-<?= $out['location'] ?>/">Customer Service</a> &nbsp;&nbsp;<span class="label">FEATURED</span></li>
            </ul>
        </div>
        <div>
            <ul>
                <li><a href="/jobs/q-Health+Care/l-<?= $out['location'] ?>/">Health Care</a> / <a href="/jobs/q-Nursing/l-<?= $out['location'] ?>/">Nursing</a></li>
                <li><a href="/jobs/q-Human+Resources/l-<?= $out['location'] ?>/">Human Resources</a> &nbsp;&nbsp;<span class="label">FEATURED</span></li>
                <li><a href="/jobs/q-Marketing/l-<?= $out['location'] ?>/">Marketing</a> / <a href="/jobs/q-Advertising/l-<?= $out['location'] ?>/">Advertising</a></li>
                <li><a href="/jobs/q-Restaurant/l-<?= $out['location'] ?>/">Restaurant</a> / <a href="/jobs/q-Hotel/l-<?= $out['location'] ?>/">Hotel</a></li>
            </ul>
        </div>
        <div>
            <ul>
                <li><a href="/jobs/q-Retail/l-<?= $out['location'] ?>/">Retail</a> &nbsp;&nbsp;<span class="label">FEATURED</span></li>
                <li><a href="/jobs/q-Sales/l-<?= $out['location'] ?>/">Sales</a></li>
                <li><a href="/jobs/q-Truck+Driving/l-<?= $out['location'] ?>/">Truck Driving</a></li>
                <li><a href="/jobs/q-Part-time/l-<?= $out['location'] ?>/">Part-time</a> / <a href="/jobs/q-Full-time/l-<?= $out['location'] ?>/">Full-time</a></li>
            </ul>
        </div>
    </div>
</div>
<?php

//include_once(TEMPLATE_PATH . $template . '_footer.inc');

include_once(JRP_VERSION_PATH . 'footer' . DS . 'f1.inc');

}

?>

<script>
	//***** Scripts to setup DOM on page load

	// Modify form action property with friendly format url
	// This is so when submit will display friendly url in address bar
	Juk.update_form_action('jobs_form');

	// Add autocomplete behavior to keyword and location fields
	Juk.init_autocomplete();

</script>