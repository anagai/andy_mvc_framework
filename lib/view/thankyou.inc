<?php
// vim: ai ts=4 sts=4 et sw=4 tw=79

//$controller = Util::load_controller('home');

//$out = $controller->runTask();

$template = $app->get_page_template();

include_once(TEMPLATE_PATH . $template .'_header.inc');
?>
    <div class="home-section">
        <div class="container-home container-inner">

            <p class="popup-txt">Thank you for contacting Jobungo.<br />
            We will get back to you as soon as possible.</p>

        </div>
    </div>

<?php include_once(TEMPLATE_PATH . $template . '_footer.inc');  ?>