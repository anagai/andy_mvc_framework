<?php
// vim: ai ts=4 sts=4 et sw=4 tw=79

$controller = Util::load_controller('employerscontact');

$out = $controller->runTask();

$template = $app->get_page_template();

include_once(TEMPLATE_PATH . $template .'_header.inc');

?>

<!-- container -->
    <div class="home-section">
        <div class="sixteen columns container-home  ">

            <p class="job-search-txt">Employer's Contact</p>

            <div class="ten columns">

            <form action="/employers_contact" method="post" id="employercontact">

                <label for="regularInput">Contact Name</label>
                <input class="smallfield required" type="text" maxlength="90" name="name" />

                <label for="regularInput">Company</label>
                <input class="smallfield required" type="text" name="company" id="company" />

                <label for="regularInput">City</label>
                <input class="smallfield required" type="text" name="city" id="city"/>

                <label for="regularInput">Phone</label>
                <input class="smallfield required" type="text" name="phone" id="phone" />

                <label for="regularInput">Email</label>
                <input class="smallfield required" type="email" name="email" id="email" />

                <label for="regularInput">Message</label>
                <textarea cols="35" rows="5" class="empmessagebox required" name="message" id="message"></textarea>
                <br>
                <input type="submit" name="submit" value="Submit" class="sendmessage" id="submit" />

                <input type="hidden" name="task" value="send" />

            </form>

            </div>
            <div class="one columns omega">&nbsp;</div>
            <div class="five columns omega">
             <strong>Expand your reach with Jobungo.</strong><br />
             Interested in learning more about Jobungo's services for employers? Submit your details and we'll be in touch.<br /><br />
            <a href="mailto:mail@jobungo.co.uk" title="Contact Jobungo">mail@jobungo.co.uk</a>
            </div>


        </div>
     </div>

<?php include_once(TEMPLATE_PATH . $template . '_footer.inc');  ?>

<script>
	//***** Scripts to setup DOM on page load

</script>