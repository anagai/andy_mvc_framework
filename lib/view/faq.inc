<?php
// vim: ai ts=4 sts=4 et sw=4 tw=79

//$page = Util::load_controller('home');

//$out = $page->controller();

$template = $app->get_page_template();

include_once(TEMPLATE_PATH . $template .'_header.inc');

?>

<!-- container -->

	<div class="home-section">
		<div class="sixteen columns container-home  ">
			<div class="sixteen columns">
				<section class="faq-header">
					<h2>Frequently Asked Questions</h2>
				</section>
			</div>
			<div class="ten columns">
				<section id="faq1" class="faq-question jumptarget">
					<h4>How do I find a job using Jobungo?</h4>
					<p>From our <a href="/">Home Page</a>, simply type in your "Keywords" and "Location" into the search boxes, and click the "Search" button.</p>
				</section>
				<section id="faq2" class="faq-question jumptarget">
					<h4>How do I apply for a job?</h4>
					<p>Search for jobs and click on any job listing, then follow the application instructions on the hiring company website.</p>
				</section>
				<section id="faq3" class="faq-question jumptarget">
					<h4>Does Jobungo cost money to use?</h4>
					<p>Jobungo is completely free to use for your job search.</p>
				</section>
				<section id="faq4" class="faq-question jumptarget">
					<h4>What is my username and password?</h4>
					<p>We do not require users to sign up to use Jobungo. Therefore, there is no username or password. However, you may be required to register on an employer’s website outside of Jobungo in order to apply for certain jobs.</p>
				</section>
				<section id="faq5" class="faq-question jumptarget">
					<h4>Are you a staffing service?</h4>
					<p>Jobungo does not place candidates with companies.</p>
				</section>
				<section id="faq6" class="faq-question jumptarget">
					<h4>Can you tell me the status of my application?</h4>
					<p>To check the status of your application, you will need to contact the Human Resources department or company that you have applied to.</p>
				</section>
				<section id="faq7" class="faq-question jumptarget">
					<h4>How can I unsubscribe?</h4>
					<p>You can unsubscribe by clicking <a href="/unsubscribe">here</a>. Please allow up to 72 hours for your request to take effect.</p>
				</section>
				<section id="faq8" class="faq-question jumptarget">
					<h4>Can I use Jobungo if I do not have Internet access?</h4>
					<p>You must have access to a computer or mobile device with an Internet connection to use Jobungo.co.uk.</p>
				</section>
				<section id="faq9" class="faq-question jumptarget">
					<h4>Do you have an App?</h4>
					<p>You do not need to download a special mobile app because Jobungo is optimized to work on your smartphone or tablet browser automatically.</p>
				</section>
				<section id="faq10" class="faq-question jumptarget">
					<h4>I see an outdated or suspicious job posting. How can I report this?</h4>
					<p>We appreciate your feedback to help make Jobungo even better. Please let us know by sending a message on the <a href="/contact">Contact Us</a> page. We actively investigate user reports and remove outdated or suspicious postings.</p>
				</section>
			</div>
			<div class="one columns omega">&nbsp;</div>
			<div class="five columns omega">
				<!--
				<ul class="faq-menu">
					<li><a href="#faq1">What is Jobungo?</a></li>
					<li><a href="#faq2">Is Jobungo free to use?</a></li>
					<li><a href="#faq3">Do I need to sign up for an account?</a></li>
					<li><a href="#faq4">Is Jobungo a staffing agency?</a></li>
					<li><a href="#faq5">How do I find job using Jobungo?</a></li>
					<li><a href="#faq6">How do I apply for job?</a></li>
					<li><a href="#faq7">Can I submit my resume to Jobungo?</a></li>
					<li><a href="#faq8">Can Jobungo tell me status of my application?</a></li>
					<li><a href="#faq9">What information do Jobungo collect?</a></li>
					<li><a href="#faq10">How do I unsubscribe from Jobungo emails?</a></li>
					<li><a href="#faq11">Advertising</a></li>
				</ul>
				-->
				<ul class="faq-menu">
					<li style="font-size: 16px; line-height: 24px; padding: 20px; background-color: #ddd; -webkit-border-radius: 3px -moz-border-radius: 3px; border-radius: 3px;">Not finding the answers you need? <a href="/contact">Contact us</a></li>
				</ul>
			</div>  
			
     	</div>
	 </div>

<?php include_once(TEMPLATE_PATH . $template . '_footer.inc');  ?>

<script>
	//***** Scripts to setup DOM on page load

</script>
