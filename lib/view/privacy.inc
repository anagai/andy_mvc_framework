<?php
// vim: ai ts=4 sts=4 et sw=4 tw=79

//$controller = Util::load_controller('home');

//$out = $controller->runTask();

$template = $app->get_page_template();

include_once(TEMPLATE_PATH . $template .'_header.inc');

?>

<!-- container -->
<div class="home-section">
        <div class="sixteen columns container-home container-inner privacy">

            <p class="popup-txt">Privacy Policy</p>

            <h3>Updated June 2, 2014</h3>

            <p>Jobungo.co.uk (“Jobungo”) takes your privacy very seriously and has created this Privacy Policy in order to disclose how we process consumer data collected about you. If you have any comments, or concerns, please contact Jobungo at Suite 510, 77 Victoria Street, Westminster, London SW1H 0HW or via our <a href="/contact" title="Contact Jobungo">contact form</a> here.</p>

            <h3>Introduction</h3>
            <p>This Privacy Policy covers how Jobungo processes the personal information that we collect when you are on Jobungo’s website and when you use Jobungo’s services. This policy does not apply to the practices of third parties that Jobungo does not own or control, including websites that we may link to, or to individuals that Jobungo does not employ or manage. Any personal data collected by Jobungo is used in accordance with UK data protection legislation.</p>

            <h3>The Data Protection Act 1998</h3>
            <p>Under the Data Protection Act 1998, we comply with certain requirements which are designed to ensure that any data you provide to us is processed fairly, and with due care and attention. We aim to provide a safe and secure experience for all of our users.</p>

            <h3>Privacy Policy Updates</h3>
            <p>Due to the Internet’s rapidly evolving nature, Jobungo may need to update this Privacy Policy from time to time. If so, Jobungo will post its updated Privacy Policy on our website located at www.Jobungo.co.uk. Jobungo encourages you to review this Privacy Policy regularly for any changes. Your continued use of this site and/or continued provision of personally identifiable information to Jobungo will be subject to the then current Privacy Policy.</p>

            <h3>Information We Collect</h3>
            <p>Jobungo may collect personally identifiable information when you register with Jobungo, when you use certain Jobungo services such as applying for a job or set up job alerts, and when you visit the Jobungo website.</p>
            <ul>
                  <li>When you register with Jobungo, set up a job alert, or use any other service provided by Jobungo, we may ask for personal information such as, but not limited to, your email address.</li>
                  <li>Jobungo gathers general information about users, for example, what services users access the most and which areas of the Jobungo site are most frequently visited. Such data is used in the aggregate to help us to understand how the Jobungo website is used.</li>
                  <li>Jobungo automatically receives and records information on our web server logs from your browser including your Internet Protocol (“IP”) address. Every computer on the internet has an IP address which is a unique number by which it can be identified.</li>
                  <li>Jobungo may set and access cookies on your computer. A cookie is a small amount of data, which often includes an anonymous unique identifier, which is sent to your browser from a website's computers and stored on your computer's hard drive. Each website can send its own cookies to your browser if your browser's preference settings allow it. To protect your privacy, your browser will only permit a website to access the cookies it has already sent to you. You can choose whether or not to accept cookies, however, if you do not accept Jobungo’s cookies, some of our services may not be available to you.</li>
            </ul>

            <h3>How We Use Your Information</h3>
            <p>Personal data will be collected and processed by Jobungo for the following purposes:</p>
            <ul>
                  <li>To understand your needs and provide you with a better service on the Jobungo website.</li>
                  <li>Jobungo may contact you based on your interests such as job postings and career development opportunities.</li>
                  <li>Jobungo’s site may require you to provide Jobungo with contact information (such as your name and email address) and demographic information to send you job alerts.</li>
                  <li>Jobungo may use demographic and/or profile data, including but not limited to search history, to tailor your experience on Jobungo’s site, show you content that Jobungo thinks you may be interested in, and display content according to your preferences.</li>
                  <li>To administer the website.</li>
                  <li>We may use other companies as necessary to perform services necessary to our operations. In the course of providing these services, those companies may have access to your personal information in accordance with this Privacy Policy. Every effort will be made to safeguard your privacy when it may be accessed by other companies, including but not limited to technology providers; however, Jobungo will not be liable for any damages that may result from the misuse of your personal information by these companies.</li>
                  <li>Jobungo reserves the right to disclose any information as required by law, responding to subpoenas, court orders, or legal process.</li>
            </ul>

            <h3>Consent</h3>
            <p>By providing personal data to Jobungo, users consent to the processing of such data by Jobungo as described in this Privacy Policy. Users can alter their preferences as described below.</p>

            <h3>Disclosure of Data to Third Parties</h3>
            <p>Jobungo may send personally identifiable information about you when:</p>
            <ul>
                  <li>We have your consent to share the information;</li>
                  <li>We need to share your information to provide the service you have requested;</li>
                  <li>We need to send the information to companies who work on behalf of Jobungo to provide a product or service to you. Unless otherwise stated, these companies do not have the right to use the personally identifiable information we provide to them beyond what is necessary to assist us and you;</li>
                  <li>We respond to subpoenas, court orders or legal process</li>
            </ul>

            <h3>Use of Data for Direct Marketing Purposes</h3>
            <p>We do not share contact information with third parties for their direct marketing purposes without your consent.</p>

            <h3>Online Behavioral Advertising</h3>
            <p>We partner with third-party advertising networks to display advertising on the Jobungo website. These ad network partners may utilize cookies to better provide advertisements about goods and services that may be of interest to you. They do not collect and do not have access to any personally identifiable information. They may, however, anonymously track your Internet usage across other websites in their networks beyond our website.</p>

            <h3>Controlling Your Personal Information</h3>
            <p>You may choose to restrict the collection or use of your personal information. Whenever you are asked to fill in a form on this website, please look for a box that you can click to indicate that you do not want the information to be used by anybody for direct marketing purposes.</p>
            <p>If you have previously agreed to us using your personal information for direct marketing purposes, you may change your mind at any time by contacting at Jobungo at Suite 510, 77 Victoria Street, Westminster, London SW1H 0HW or, by email at <a href="mailto:mail@jobungo.co.uk" title="Contact Jobungo">mail@jobungo.co.uk</a>, or via our <a href="/contact" title="Contact Jobungo">contact form</a>.</p>
            <p>You may request details of personal information which we hold about you under the Data Protection Act 1998. If you would like a copy of the information held on you, at your expense, please contact Suite 510, 77 Victoria Street, Westminster, London SW1H 0HW or, by email at <a href="mailto:mail@jobungo.co.uk" title="Contact Jobungo">mail@jobungo.co.uk</a>, or via our <a href="/contact" title="Contact Jobungo">contact form</a>.</p>
            <p>If you believe that any information we are holding on you is incorrect or incomplete, please contact Jobungo at Suite 510, 77 Victoria Street, Westminster, London SW1H 0HW or, by email at <a href="mailto:mail@jobungo.co.uk" title="Contact Jobungo">mail@jobungo.co.uk</a>, or via our <a href="/contact" title="Contact Jobungo">contact form</a>. We will promptly correct any information found to be incorrect.</p>

            <h3>Unsubscribe</h3>
            <p>Jobungo respects your email preferences and will never provide your email address to a third party without your permission, except as may be prescribed elsewhere in this Privacy Policy. If you provide us your email address, Jobungo may email you job alerts, account notifications, news, offers, surveys, promotions, and other communications.</p>
            <p>If you do not want to receive particular types of email messages from Jobungo, follow the unsubscribe instructions contained in the email message to notify us of our preferences, or click here.</p>

            <h3>Security</h3>
            <p>Jobungo is committed to ensuring that your information is secure. In order to prevent unauthorized access or disclosure of your information, we have put in place suitable physical, electronic and managerial procedures to safeguard and secure the information we collect online. or</p>

            <h3>Facebook Information Collection and Use</h3>
            <p>Through our use of Facebook products and services, including but not limited to Facebook Connect and Facebook Platform, we may receive personally identifiable information about you and your friends who are registered on Facebook (“Friend”). We may use this information to customize and improve the services that we provide to you. By visiting Jobungo, information that we receive from Facebook about you and your Friends may appear on our site. In addition, if a user visits our site and you are listed as a Friend of that user on Facebook, your information may also appear on our site. You can learn more here.</p>

            <h3>Children’s Privacy</h3>
            <p>Jobungo’s site is not intended for and may not be used by children under the age of 13. Jobungo is committed to protecting the privacy needs of children and we encourage parents and guardians to take an active role in their children’s online activities. Jobungo does not knowingly collect personally identifiable information from children under the age of 13, nor does Jobungo target its services or site to children under the age of 13. We recommend that minors between the ages of 13 and 17 ask and receive permission from their parent or guardian prior to using Jobungo or sending information about themselves or anyone else over the Internet.</p>

            <h3>Protect Yourself</h3>
            <p>Jobungo uses automated processes and third party aggregators to collect and present information that has been created by third parties, so we cannot verify the accuracy or legitimacy of all job postings displayed on our site. We strongly encourage you to exercise caution when providing information about yourself to others online, including when you apply for jobs found on Jobungo.</p>
            <p>Never provide your social security number, credit card information, bank account details, or unnecessary personal information to a potential employer. Never participate in a financial transaction of any sort with a potential employer. Always be cautious of opportunities that sound too good to be true or require you to act as a financial intermediary, particularly for an overseas person.</p>

            <h3>Links to Third Party Sites</h3>
            <p>The Jobungo site may provide links to external websites. Such links do not constitute an endorsement by Jobungo of those external websites. You acknowledge that Jobungo is providing these links to you only as a convenience, and further agree that Jobungo is not responsible for the content of such external websites. Your use of external websites is subject to the terms of use and privacy policies located on the external websites.</p>

            <h3>Assignment</h3>
            <p>If there is a change of control in Jobungo’s business (whether by merger, sale, or otherwise) or if there is an asset sale, the user information, including personal information, could be sold as part of that transaction and your personally identifying information potentially could be used by the purchaser. However, if that business materially changes this statement or the information handling practices as described in this Privacy Policy, you will be notified by email and/or through a notice posted on the site, and you may opt-out of the use of your existing information in a new manner. This Privacy Policy inures to the benefit of any successors or assigns of Jobungo or the assets of Jobungo.</p>

            <h3>Conditions of Use</h3>
            <p>If you decide to use Jobungo’s site or services, your use and any possible dispute over privacy is subject to this Privacy Policy. If you have questions about this Privacy Policy, you can contact us at <a href="mailto:mail@jobungo.co.uk" title="Contact Jobungo">mail@jobungo.co.uk</a>.</p>

    </div>
    </div>

<?php include_once(TEMPLATE_PATH . $template . '_footer.inc');  ?>

<script>
	//***** Scripts to setup DOM on page load

</script>