<?php
// vim: ai ts=4 sts=4 et sw=4 tw=79

//$controller = Util::load_controller('home');

//$out = $controller->runTask();

$template = $app->get_page_template();

include_once(TEMPLATE_PATH . $template .'_header.inc');

?>

<!-- container -->
<div class="home-section">
        <div class="sixteen columns container-home container-inner ">

            <p class="popup-txt">COOKIE NOTICE</p>

            <strong>Updated April 3, 2014</strong><br /><br />

            <strong>User Consent</strong><br />
            By using or accessing Jobungo UK’s website at Jobungo.co.uk, you are consenting to Jobungo UK’s use of Cookies.<br /><br />

            <strong>Types of Cookies</strong><br />
            We use cookies for many purposes. Cookies are text-only pieces of information that our website may transfer to your hard drive or other website browsing equipment for record-keeping purposes. Cookies allow the website to remember important information that will make your use of the site more convenient. A cookie will typically contain the name of the domain from which the cookie has come, the “lifetime” of the cookie, and a randomly generated unique number or other value. Cookies will generally not carry any personally identifiable information.<br /><br />

            Like most websites, we use cookies for a variety of purposes in order to improve your experience on Jobungo.co.uk, including to store your website preferences, to analyze user activity to make improvements to the website, and for marketing and referral tracking purposes.<br /><br />

            <strong>Advertising &amp; Cookies</strong><br />
            We use third-party advertising companies to serve advertisements relevant to your interests when you visit our website. These companies may use third-party cookies to record information (excluding your name, address, email address, or telephone number) about your visits to this and other websites in order to provide advertisements about goods and services of interest to you on others websites.<br /><br />

            <strong>Your Privacy</strong><br />
            Please see our <a href="/privacy">Privacy Policy</a> to understand what information we collect, why we collect it, how we use it, and how we protect it.<br /><br />

            <strong>Disabling Cookies</strong><br />
            Most browsers give you the ability to manage cookies to suit your preferences. In some browsers you can set up rules to manage cookies on a site-by-site basis, giving you more fine-grained control over your privacy. What this means is that you can disallow cookies from all sites except those that you trust.<br /><br />
    </div>
    </div>

<?php include_once(TEMPLATE_PATH . $template . '_footer.inc');  ?>

<script>
	//***** Scripts to setup DOM on page load

</script>
