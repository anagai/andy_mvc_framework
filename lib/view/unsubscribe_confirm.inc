<?php
// vim: ai ts=4 sts=4 et sw=4 tw=79

$template = $app->get_page_template();

include_once(TEMPLATE_PATH . $template .'_header.inc');

?>

	<div class="home-section">
		<div class="container-home container-inner">
			<div class="sixteen columns search-form-home">
				<p class="popup-txt">We're sorry to see you go!</p> <strong>Your unsubscribe request for <?=$_SESSION['email_temp']?> has been received.</strong> <em>Please allow up to 48 hours for your request to be processed.</em></p>
				<p><strong>P.S. Hopefully you've found the job of your dreams! If not, do one final search below:</strong></p>
				<form id="home_form" name="home_form" action="/jobs" method="post" style="position: relative;" >
					<?php
						/*
						// START FUTUREADS
						if( $from_future_ads ) {
					?>
						<input type="hidden" name="fa" value="<?=$from_future_ads?>">
						<input type="hidden" name="sub_id" value="<?=$sub_id?>">
					<?php
						} // END FUTUREADS
						*/

					include_once(TEMPLATE_PATH . 'component/search_fields.inc');
					?>
				</form>
			</div>
		</div>
	</div>


<?php

include_once(TEMPLATE_PATH . $template . '_footer.inc');

unset($_SESSION['email_temp']);

?>

<script>
	//***** Scripts to setup DOM on page load

	// Modify form action property with friendly format url
	// This is so when submit will display friendly url in address bar
	update_form_action('home_form');

	// Add autocomplete behavior to keyword and location fields
	init_autocomplete();

</script>