<?php
// vim: ai ts=4 sts=4 et sw=4 tw=79

$g_uid = Util::getVar('uid', '', 'GET');
$email_template = Util::getVar('et', 0, 'GET');
$keyword = Util::getVar('kw', '', 'GET');
$redirect = Util::getVar('rd', '', 'GET');
$job_key = Util::getVar('jk', '', 'GET');
$sub_id = Util::getVar('sub_id', '', 'GET');
$ver_id = Util::getVar('v', 0, 'GET');
$source = Util::getVar('src', '', 'GET');
$email = Util::getVar('email', '', 'GET');

// convert any space in email after decode back to plus
$email = str_replace(" ", "+", $email);

$do_redirect = true;

/**
 * Email click tracking handler. All email links are directed to here.
 * Metrics recorded for all clicks from emails into
 * email_clicker_tracking table
 *
 * metrics collected
 *
 * jobs view:
 *
 *  email template id
 *  result version id
 *  user id - email
 *  action - "jobs"
 *
 * All other views:
 *
 *  email template id
 *  user id - email
 *  action - view name. e.g. "contact", "faq"
 *
 */

function unsubUser($em, $subid='') {
	$unsub_model = Util::load_model('unsubscribe');
	if(!empty($em)) {
		$_SESSION['email_temp'] = $em;
	}
	// unsub code will redirect to unsubscribe_confirm
	$unsub_model->unsubscribe_email($em, $subid);
}

if(!empty($g_uid)) {

	// decrypt the uid
	//$uid = Util::mcryptDecode($g_uid);
	$uid = $g_uid;

	$user_model = Util::load_model('user');

	$user = $user_model->find_by_id($uid);

	unset($_SESSION['noloc_user']);

	$qrystr = '';

	//Util::debug($user);
	//Util::printr($_GET);
	//echo 'et: ' . $email_template . ' g_uid: ' . $g_uid . ' id: ' . $uid . ' rd: ' . $redirect . '<br>';
	//Util::printr($user);


	// If no email template id passed or no redirect or is not an existing
	// user then kickout to home
	if(empty($user) || !$redirect) {
		Util::redirect_to('home');
	}

	// Set uid to session
	$_SESSION['uid'] = $user->id;
	$_SESSION['location'] = $user->city
		? trim($user->city)
		: trim($user->postal_code);
	$_SESSION['username'] = $user->first_name;
	$_SESSION['fromemail'] = 1;
	// Get user location by ip if needed
	if(empty($_SESSION['location'])) {
		$geocode = Util::load_model('geocodelocation');
		$loc = $geocode->get_loc_by_ip(FALSE);

		if(!empty($loc['city']) || !empty($loc['postal'])) {
			$user_city = ucwords($loc['city']);
			$user_postal = strtoupper($loc['postal']);

			$_SESSION['location'] = $user_city
				? $user_city
				: $user_postal;

			// Update no loc user with loc
			$user->city = $user_city;
			$user->postal_code = $user_postal;
			$user->update();

			// Remove no loc status from coreg record
			$user_model->update_noloc_coreg($user->coreg_id, $user_city, $user_postal);
		}
	}


	// Set no loc flag to get user location on redirect if needed
	if(empty($_SESSION['location'])) {
		$_SESSION['noloc_user'] = 1;
	}

	//Util::printr($_SESSION);



	// Use specific version if v=x is passed from email and persist
	// during user's session
	if(!empty($ver_id)) {
		$_SESSION['uid_ver'] = $ver_id;
	}

	$today_dt = date('Y-m-d H:i:s');

	// jobs and recommended_jobs share same jobs template code.
	if($redirect == 'jobs' || $redirect == 'recommended_jobs') {
		// Record metrics for job result links
		$result_version_model = Util::load_model('resultversion');

		// Use same method used on jobs view to get version.
		$version = $result_version_model->get_jrp_version();

		// Create new email_click_tracking record
		// NOTE: disabled 10/16/14. data not being used anywhere
		/*
		$new_click_tracking = new EmailClickTracking();
		$new_click_tracking->email_version_id = $email_template;

		// Only record result version if going to jobs page
		if($redirect == 'jobs') {
			$new_click_tracking->result_version_id = $version->id;
		}

		$new_click_tracking->user_id = $user->id;
		$new_click_tracking->partner = $user->partner;
		$new_click_tracking->source = $user->source;
		$new_click_tracking->action = $redirect;
		$new_click_tracking->date_created = $today_dt;
		$new_click_tracking->create();
		*/

		if($keyword) {
			$qry['kw'] = $keyword;
		}

		if($job_key) {
			$qry['jk'] = $job_key;
		}

		if(isset($qry) && $qrystr = http_build_query($qry)) {
			$qrystr = '?' .$qrystr;
		} else {
			$_SESSION['keyword'] = '';
		}

		// Redirect to jobs page
		Util::redirect_to($redirect . $qrystr);

	} else {
		// Record metrics for all other views

		// Convert short url redirect names to actual view names
		switch($redirect) {
			case 'emp':
				$redirect = 'employers_contact';
				break;
			case 'unsub':
				// Do not redirect to a unsub view.
				// unsub code will redirect to unsubscribe_confirm
				$do_redirect = false;
				break;
		}

		// If supposed to do redirect and redirect view is not existing file then 404
		if($do_redirect === true && !file_exists(VIEW_PATH . $redirect .".inc")) {
			Util::redirect_to('404');
		}

		// Create new email_click_tracking record
		// NOTE: disabled 10/16/14. data not being used anywhere
		/*
		$new_click_tracking = new EmailClickTracking();
		$new_click_tracking->email_version_id = $email_template;
		$new_click_tracking->user_id = $user->id;
		$new_click_tracking->partner = $user->partner;
		$new_click_tracking->source = $user->source;
		$new_click_tracking->action = $redirect;
		$new_click_tracking->date_created = $today_dt;
		$new_click_tracking->create();
		*/

		// Handle instant unsubscribe. Email added to bacon unsubscribe
		// and user is_mailable set to zero.
		if($redirect == 'unsub') {

			unsubUser($user->email, $sub_id);
			/*
			$unsub_model = Util::load_model('unsubscribe');
			if(!empty($email)) {
				$_SESSION['email_temp'] = $email;
			} else {
				$_SESSION['email_temp'] = $user->email;
			}
			// unsub code will redirect to unsubscribe_confirm
			$unsub_model->unsubscribe_email($_SESSION['email_temp'], $sub_id);
			*/

		}

		// Redirect to final destination if supposed to redirect to view
		// set in url parameter rd
		if($do_redirect === true) {
			Util::redirect_to($redirect);
		}

	}

} else {
	// There is no uid.
	if($source=='sg' && !empty($email) && $redirect=='unsub') {
		// If sg unsub click
		unsubUser($email);
	} else {
		// everything else goes to home
		Util::redirect_to('home');
	}
}


?>
