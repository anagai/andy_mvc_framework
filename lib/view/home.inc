<?php
// vim: ai ts=4 sts=4 et sw=4 tw=79

$controller = Util::load_controller('home');

$out = $controller->runTask();

$template = $app->get_page_template();

// Include google maps api file for this view
$app->add_script('/js/map.js');

include_once(TEMPLATE_PATH . $template .'_header.inc');

?>

<!-- container -->
<div class="home-section">
	<div class="container-home">
		<div class="sixteen columns search-form-home">
			<form id="home_form" name="home_form" method="get" action="/jobs">
				<?php
				include_once(TEMPLATE_PATH . 'component/search_fields.inc');
				?>
				<!--<div class="seven columns alpha">
					<label style="color:#444;font-size: 18px; margin-bottom: 5px;">Keywords <span>job title, skills, or company</span></label>
					<input type="text"  name="q" id="keyword" value="">
					<input type="hidden" id="keyword_id">
				</div>
				<div class="seven columns">
					<label style="color:#444;font-size: 18px; margin-bottom: 5px;">Location <span>city or postcode</span></label>
					<input type="text" name="l" id="location" value="<?= $out['location'] ?>">
					<input type="hidden" id="location_id">
				</div>
				<div class="two columns omega">
					<label style="font-size: 18px; margin-bottom: 5px;">&nbsp;</label>
					<button class="full-width" type="submit" name="job_search"><img src="/images/search-icon.png"></button>
				</div>-->
				<input type="hidden" name="task" value="search">
			</form>
		</div>
		<div class="map-overlay-container sixteen columns row">
			<a href="/searching">
			<div class="map-overlay">
				<img class="map-overlay-1" src="/images/map-overlay.png">
				<img class="map-overlay-2" src="/images/map-overlay-2.png">
			</div>
			<div id="map_canvas"></div>
			</a>
		</div>
	</div>
</div>

<!--
<h2><?= $app->get_page_title() ?></h2>
<form id="home_form" name="home_form" method="post" action="/jobs">
	Keyword: <input type="text" name="q" id="keyword" value="" />
	<input type="hidden" id="keyword_id">
    Location: <input type="text" name="l" id="location" value="<?= $out['location'] ?>" />
    <input type="hidden" id="location_id">
    <input type="submit" name="job_search" value="Search" />
</form>
-->

<?php include_once(TEMPLATE_PATH . $template . '_footer.inc'); ?>

<script>
	//***** Scripts to setup DOM on page load

	// Modify form action property with friendly format url
	// This is so when submit will display friendly url in address bar
	Juk.update_form_action('home_form');

	// Add autocomplete behavior to keyword and location fields
	Juk.init_autocomplete();

	// Need to find approximate current users location
	// 1) uid is passed we can use uk city
	// 2) If ip lookup can get city

	var latlng = new google.maps.LatLng( <?= $out['longlat']->latitude ?>, <?= $out['longlat']->longitude ?> );

    var myOptions = {
        zoom: 8,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,

        disableDefaultUI: true,
        disableDoubleClickZoom: false,
		navigationControl: false,
    	mapTypeControl: false,
    	scaleControl: false,
        draggable: false,
		scrollwheel: false,
    };
    var map = new google.maps.Map(
        document.getElementById( "map_canvas" ), myOptions );

    Juk.map.set_marker_timers( map, '<?=  $out['longlat']->latitude ?>', '<?= $out['longlat']->longitude ?>', '<?= Util::base_url() ?>' );

</script>
