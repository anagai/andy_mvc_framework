<?php
// vim: ai ts=4 sts=4 et sw=4 tw=79

$controller = Util::load_controller('unsubscribe');

$out = $controller->runTask();

$template = $app->get_page_template();

include_once(TEMPLATE_PATH . $template .'_header.inc');

?>

<!-- container -->

	<div class="home-section">
		<div class="container-home container-inner ">
			<div class="ten columns alpha">
				<p class="popup-txt">Manage Subscriptions</p>
				<p><strong>We're sorry to see you go!</strong> If you wish to unsubscribe from future emails and career alerts from Jobungo please provide your email address below and click "Unsubscribe"</p>
				<form action="/unsubscribe" method="post">
					<label style="color:#444;font-size: 18px; margin-bottom: 5px;">Email</label>
					<input class="unsubfield" name="email" type="text" value="" tabindex="2" />
					<button type="submit" tabindex="3">Unsubscribe</button>
					<input type="hidden" name="task" value="unsub" />
				</form>
				<p><em>Please allow up to 72 hours for your removal request to be processed.</em></p>
			</div>
			<div class="six columns omega">

			</div>
    	</div>
	</div>

<?php include_once(TEMPLATE_PATH . $template . '_footer.inc');  ?>

<script>
	//***** Scripts to setup DOM on page load

</script>