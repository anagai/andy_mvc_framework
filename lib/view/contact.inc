<?php
// vim: ai ts=4 sts=4 et sw=4 tw=79

$controller = Util::load_controller('contact');

$out = $controller->runTask();

$template = $app->get_page_template();

include_once(TEMPLATE_PATH . $template .'_header.inc');

?>

<!-- container -->
<div class="home-section">

		<div class="sixteen columns container-home">

			<p class="job-search-txt">Contact Us</p>

    	<div class="ten columns">

        	<form action="/contact" method="post" id="contactusform">

            <label for="regularInput">Name</label>
            <input class="contactfield required" id="name" type="text" name="name"
                value="" />

            <label for="regularInput">Email</label>
            <input class="contactfield required" type="email" name="email"
                value="" />

            <label for="regularInput">Message</label>
            <textarea cols="35" rows="5"
                class="messagebox required" id="message" name="message"></textarea>
          <br>
            <input type="submit" name="submit" value="Contact Us" class="sendmessage" id="submit" />

          <input type="hidden" name="task" value="send" />
        </form>

     </div>
     <div class="one columns omega">&nbsp;</div>
     <div class="five columns omega">
         <strong>Expand your reach with Jobungo.</strong><br />
         Interested in learning more about Jobungo's services for employers? Submit your details and we'll be in touch.<br /><br />
         <a href="mailto:mail@jobungo.co.uk" title="Contact Jobungo">mail@jobungo.co.uk</a>
       </div>
    </div>
    </div>

<?php include_once(TEMPLATE_PATH . $template . '_footer.inc');  ?>

<script>
    //***** Scripts to setup DOM on page load

</script>