<?php

    /*
     * Created on Dec 10, 2012
     * Written By Sung Hwan Ahn
     * Observed By Andy Nagai
     *
     */

    // vim: ai ts=4 sts=4 et sw=4 tw=79

    include_once(CORE_PATH . 'access.inc');

    // Validate Access (Hash and IP)
    $hash = Util::getVar('hash');
    $access_ip = $_SERVER["REMOTE_ADDR"];

    $dashboard_access = new Access();

    if ($hash != $dashboard_access->da_hash) {
        Util::redirect_to("home.php");
    }

    if ($access_ip != $dashboard_access->da_ip &&
        $access_ip != $dashboard_access->office_ip) {
        Util::redirect_to("home.php");
    }

    $start_date = Util::getVar('sd');
    $end_date = Util::getVar('ed');

    $result_model = Util::load_model('result');
    $visitors = $result_model->get_daily_unique_visitor($start_date, $end_date);
    $json = json_encode($visitors);

    echo $json;
?>
