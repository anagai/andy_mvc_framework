<?php

    /*
     * Created on Dec 12, 2012
     * Written By Sung Hwan Ahn
     * Observed By Andy Nagai
     *
     */

    // vim: ai ts=4 sts=4 et sw=4 tw=79

    include_once(CORE_PATH . 'access.inc');

    // Validate Access (Hash and IP)
    $hash = Util::getVar('hash');
    $access_ip = $_SERVER["REMOTE_ADDR"];

    $dashboard_access = new Access();

    if ($hash != $dashboard_access->da_hash) {
        Util::redirect_to("home.php");
    }

    if ($access_ip != $dashboard_access->da_ip &&
        $access_ip != $dashboard_access->office_ip) {
        Util::redirect_to("home.php");
    }

    $campaign_name = Util::getVar('cn');

    $campaign_model = Util::load_model('campaign');
    $campaign = $campaign_model->find_by_campaign_name($campaign_name);

    $json = json_encode($campaign);

    echo $json;
?>
