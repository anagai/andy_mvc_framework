<?php

    /*
     * Created on June 24, 2013
     * Written By Sung Hwan Ahn / Andy Nagai
     *
     */

    // vim: ai ts=4 sts=4 et sw=4 tw=79

    // Validate Access (Hash and IP)
    $action = Util::getVar('act');

    // Only allow access if host server is restdev or request coming from
    // office.
    Util::authenticate_request();

    switch($action) {
        case 'verdet':
            $detList = array();
            $from = date('Y-m-d', strtotime(Util::getVar('fr')));
            $to = date('Y-m-d', strtotime(Util::getVar('to')));
            $version = (int)Util::getVar('ver');
            $sql = "SELECT page_number as page, sum(unique_visitors) as "
                . "uniques, sum(page_views) as views, "
                . "sum(job_results) as jobs, sum(organic_clicks) as oc, "
                . "sum(sponsored_clicks) as sc FROM versionator_stats "
                . "WHERE search_date >= '{$from}' AND search_date <= '{$to}' "
                . "AND version_id=" . $version . " GROUP BY page_number";

            $result = $db->query($sql);

            while($row = $db->fetch_array($result)) {
                $detList[] = $row;
            }

            $json = json_encode($detList);

            echo $json;
        break;

    }


?>
