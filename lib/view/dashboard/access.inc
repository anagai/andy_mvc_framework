<?php

    /*
     * Created on Dec 10, 2012
     * Written By Sung Hwan Ahn
     * Observed By Andy Nagai
     *
     */

    // vim: ai ts=4 sts=4 et sw=4 tw=79

    include_once(CORE_PATH . 'access.inc');

    // Get the parameters
    parse_str($_SERVER["QUERY_STRING"], $params);

    // Clean backslash (\)
    foreach($params as $key=>$value) {
        $params[$key] = str_replace("\\", "", $value);
    }

    (isset($params["hash"])) ? $hash = urldecode($params["hash"]) : $hash = "";
    (isset($params["rd"])) ? $rd = urldecode($params["rd"]) : $rd = "";

    // Get the dashboard access info
    $dashboard_access = new Access();
    $access_ip = $_SERVER["REMOTE_ADDR"];

    // Validate hash
    if ($hash != $dashboard_access->da_hash) {
        $json = '{"result":0}';
    }

    // Validate remote ip address
    else if ($access_ip != $dashboard_access->da_ip &&
             $access_ip != $dashboard_access->office_ip) {
        $json = '{"result":0}';
    }

    // Parse Direct URL
    else if ($rd != "") {
        $redirect = explode("/", $rd); // jobungo_uk/daily_unique_visitors.php
        $redirect_url = $redirect[1];
        $json = '{"result":1,"redirect_url":"'. $redirect_url . '"}';
    } else {
        $json = '{"result":0}';
    }

    echo $json;
?>
