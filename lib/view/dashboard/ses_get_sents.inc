<?php

    /*
     * Created on Dec 12, 2012
     * Written By Sung Hwan Ahn
     * Observed By Andy Nagai
     *
     */

    // vim: ai ts=4 sts=4 et sw=4 tw=79

    include_once(CORE_PATH . 'access.inc');

    // Validate Access (Hash and IP)
    $hash = Util::getVar('hash');
    $access_ip = $_SERVER["REMOTE_ADDR"];

    $dashboard_access = new Access();

    if ($hash != $dashboard_access->da_hash) {
        Util::redirect_to("home.php");
    }

    if ($access_ip != $dashboard_access->da_ip &&
        $access_ip != $dashboard_access->office_ip) {
        Util::redirect_to("home.php");
    }

    $campaign_name = Util::getVar('cn');
    $search_text = Util::getVar('st');
    $search_field = Util::getVar('sf');
    $start_date = Util::getVar('sd');
    $end_date = Util::getVar('ed');
    $per_page = Util::getVar('pp');
    $offset = Util::getVar('ps');

    $search_text = str_replace(' ','+',$search_text);

    $ses_sent_model = Util::load_model('sessent');
    $ses_sents = $ses_sent_model->get_sents(
            $campaign_name,
            $search_text,
            $search_field,
            $start_date,
            $end_date,
            $per_page,
            $offset);

    $json = json_encode($ses_sents);

    echo $json;
?>
