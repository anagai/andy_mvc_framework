<?php

    /*
     * Created on June 24, 2013
     * Written By Sung Hwan Ahn / Andy Nagai
     *
     */

    // vim: ai ts=4 sts=4 et sw=4 tw=79

    $action = Util::getVar('act');

    // Only allow access if host server is restdev or request coming from
    // office.
    Util::authenticate_request();

    switch($action) {

        case 'verlist':
            $model = Util::load_model('resultversion');
            $versions = $model->get_all();

            $result = array();

            while ($version = $db->fetch_array($versions)) {
                $result[] = $version;
            }

            $json = json_encode($result);

            echo $json;
        break;
        case 'getver':
            $verId = Util::getVar('ver');
            $model = Util::load_model('resultversion');
            $version = $model->find_by_id($verId);
            echo json_encode($version);
        break;
        case 'updver':
            $verId = Util::getVar('ver');

            $table = new ResultVersion();
            $version = $table->find($verId);

            $version->alt_version_id = Util::getVar('altver');
            $version->name = Util::getVar('name');
            $version->description = Util::getVar('desc');
            $version->weight = Util::getVar('weight');
            $version->header_file = Util::getVar('header');
            $version->css_file = Util::getVar('css');
            $version->body_file = Util::getVar('body');
            $version->footer_file = Util::getVar('footer');
            $version->traffic_type = Util::getVar('ttype');
            $version->page_type = Util::getVar('ptype');
            $version->items_per_page = Util::getVar('perpge');
            $version->organic_per_page = Util::getVar('orgpp');
            $version->sponsored_per_page = Util::getVar('spnpp');
            $version->ad_per_page = Util::getVar('adpp');
            $version->result_pattern = Util::getVar('patt');
            $version->date_updated = Util::getVar('updt');
            $version->date_expire = Util::getVar('expdt');
            $version->modal_enabled = Util::getVar('modal');
            $version->is_active = Util::getVar('active');
            $ret = $version->update();

            echo '{"success":"1"}';

        break;
        case 'newver':
            $verId = Util::getVar('ver');

            $version = new ResultVersion();

            $version->alt_version_id = Util::getVar('altver');
            $version->name = Util::getVar('name');
            $version->description = Util::getVar('desc');
            $version->weight = Util::getVar('weight');
            $version->header_file = Util::getVar('header');
            $version->css_file = Util::getVar('css');
            $version->body_file = Util::getVar('body');
            $version->footer_file = Util::getVar('footer');
            $version->traffic_type = Util::getVar('ttype');
            $version->page_type = Util::getVar('ptype');
            $version->items_per_page = Util::getVar('perpge');
            $version->organic_per_page = Util::getVar('orgpp');
            $version->sponsored_per_page = Util::getVar('spnpp');
            $version->ad_per_page = Util::getVar('adpp');
            $version->result_pattern = Util::getVar('patt');
            $version->date_updated = Util::getVar('updt');
            $version->date_expire = Util::getVar('expdt');
            $version->modal_enabled = Util::getVar('modal');
            $version->is_active = Util::getVar('active');
            $ret = $version->create();

            echo '{"success":"1"}';

        break;
        case 'delver':
            $verId = Util::getVar('ver');
            $table = new ResultVersion();
            $version = $table->find($verId);
            $version->delete();

            echo '{"success":"1"}';

        break;
    }


?>
