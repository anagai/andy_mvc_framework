<?php

    /*
     * Created on June 24, 2013
     * Written By Sung Hwan Ahn / Andy Nagai
     *
     */

    // vim: ai ts=4 sts=4 et sw=4 tw=79

    // Only allow access if host server is restdev or request coming from
    // office.
    Util::authenticate_request();

    $model = Util::load_model('resultversion');
    $versions = $model->get_all();

    $result = array();

    while ($version = $db->fetch_array($versions)) {
        $result[] = $version;
    }

    $json = json_encode($result);

    echo $json;
?>
