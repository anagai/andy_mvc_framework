<?php

    /*
     * Created on Dec 13, 2012
     * Written By Sung Hwan Ahn
     * Observed By Andy Nagai
     *
     */

    // vim: ai ts=4 sts=4 et sw=4 tw=79

    // Only allow access if host server is restdev or request coming from
    // office.
    Util::authenticate_request();

    $custom_clickers = Util::getVar('cc');

    $coreg_model = Util::load_model('coreg');
    $user_model = Util::load_model('user');
    $clickers_model = Util::load_model('clickers90');
    $result_model = Util::load_model('result');
    $bounce_model = Util::load_model('bounce');
    $complaint_model = Util::load_model('complaint');
    $unsubscribe_model = Util::load_model('unsubscribe');

    $overview = new stdClass;

    // Coreg

    //echo "Coreg:<br>";
    //$start = microtime(TRUE);
    $overview->coreg_total = $coreg_model->count_all();
    $overview->coreg_accepted = $coreg_model->count_accepted();
    $overview->coreg_rejected = $overview->coreg_total - $overview->coreg_accepted;
    $overview->coreg_mailable = $coreg_model->count_mailable();
    //echo 'Time:'.number_format(microtime(TRUE)-$start,7) . "<br><Br>";

    // User

    //echo "User:<br>";
    //$start = microtime(TRUE);
    $overview->user_total = $user_model->count_all();
    $overview->user_mailable = $user_model->count_mailable();
    //echo 'Time:'.number_format(microtime(TRUE)-$start,7) . "<br><Br>";

    // Clickers

    //echo "Clickers:<br>";
    //$start = microtime(TRUE);
    $overview->clickers90 = $clickers_model->count_clickers(90);
    $overview->c90_mailable = $clickers_model->count_mailable(90);
    $overview->c90_valid_kw = $clickers_model->count_valid_kw(90);

    $overview->clickers60 = $clickers_model->count_clickers(60);
    $overview->c60_mailable = $clickers_model->count_mailable(60);
    $overview->c60_valid_kw = $clickers_model->count_valid_kw(60);

    $overview->clickers45 = $clickers_model->count_clickers(45);
    $overview->c45_mailable = $clickers_model->count_mailable(45);
    $overview->c45_valid_kw = $clickers_model->count_valid_kw(45);

    $overview->clickers15 = $clickers_model->count_clickers(15);
    $overview->c15_mailable = $clickers_model->count_mailable(15);
    $overview->c15_valid_kw = $clickers_model->count_valid_kw(15);

    $overview->custom_clickers = $clickers_model->count_clickers($custom_clickers);
    $overview->cc_mailable = $clickers_model->count_mailable($custom_clickers);
    $overview->cc_valid_kw = $clickers_model->count_valid_kw($custom_clickers);
    //echo 'Time:'.number_format(microtime(TRUE)-$start,7) . "<br><Br>";

    // Result
    //echo "Result:<br>";
    //$start = microtime(TRUE);
    $overview->result_search = $result_model->count_result_search();
    $overview->no_result_search = $result_model->count_no_result_search();
    $overview->total_search = $overview->result_search + $overview->no_result_search;
    $overview->job_clicks = $result_model->count_job_clicks();
    //echo 'Time:'.number_format(microtime(TRUE)-$start,7) . "<br><Br>";

    // DNM
    //echo "DNM:<br>";
    //$start = microtime(TRUE);
    $overview->bounce = $bounce_model->count_all_bounce();
    $overview->complaint = $complaint_model->count_all_complaint();
    $overview->unsubscribe = $unsubscribe_model->count_all_unsubscribe();
    //echo 'Time:'.number_format(microtime(TRUE)-$start,7) . "<br><Br>";

    $json = json_encode($overview);

    echo $json;
?>
