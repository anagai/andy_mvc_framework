<?php
// vim: ai ts=4 sts=4 et sw=4 tw=79

//$controller = Util::load_controller('home');

//$out = $controller->runTask();

$template = $app->get_page_template();

include_once(TEMPLATE_PATH . $template .'_header.inc');

?>

<!-- container -->
<div class="home-section">
		<div class="container-home container-inner">

			<p class="popup-txt">About Us</p>

			<p>Jobungo is committed to helping job seekers find jobs. With an average of 120,000 new jobs posted everyday by employers throughout the UK, Jobungo is the site for active job seekers. Jobungo provides job seekers with a comprehensive search engine that allows users to search by keyword, location, and trending jobs.  Users can generate a list of available jobs and refine by distance and date to provide the most accurate, up-to-date, and relevant job listings. Finding a job can be a stressful process for anyone, but Jobungo strives to make the search as easy as possible!</p>

			<p>Jobungo boasts over 20 different categories of job listings from entry-level positions to executive management, allowing job seekers from all walks of life to benefit from its comprehensive search engine, including those interested in seasonal or freelance employment. Jobungo's search engine derives its results from every nook and cranny of the Internet, including recruiter websites, job boards, classified postings, and other search engines. No local job listing escapes detection from Jobungo's search engine, ensuring that users receive the most complete and accurate job listings available on the Internet.</p>

			<!--<p>In addition to providing a powerful search engine, Jobungo also offers a bank of valuable advice and tips about the job application and interview process in our <a href="http://www.jobungo.com/resources/">Resources</a> section. Job seekers at Jobungo can peruse our articles about job hunting and interviews, learning all they need to know about cover letters, common resume mistakes, and how to dress to impress potential employers. Everyone can benefit from a little sage advice and Jobungo strives to provide its users with the most accurate and informative resources so that they can secure the job of their dreams!</p>
		-->

			<p>Here at Jobungo we love ideas and value creativity, especially when it can help our job seekers find jobs.  If you have suggestions on how we can improve our services, please <a href="/contact">let us know!</a></p>
		</div>
	</div>

<?php include_once(TEMPLATE_PATH . $template . '_footer.inc');  ?>

<script>
	//***** Scripts to setup DOM on page load

</script>