<?php
// vim: ai ts=4 sts=4 et sw=4 tw=79

$controller = Util::load_controller('jobs');

$out = $controller->runTask();

$result_version_model = Util::load_model('resultversion');

// Get component files for active version by traffic and weight
$components = $result_version_model->get_versionator_files();

// Merge components array into main output array
$out = array_merge($out, $components);

//Util::debug($components);

include_once($out['components']['header']);

include_once($out['components']['body']);

include_once($out['components']['footer']);

// ****** Jobs page is versionated. Header and footer html included in each version file.


// JRP - Jobs Result Page
/*
if($out["jobs"]->total < 1) {
	include_once(VIEW_PATH . 'nojobs.inc');
} else {
	include_once(JRP_VERSION_PATH . $out['jrp_version']->file_name);
}
*/

?>
<script>
	//***** Scripts to setup DOM on page load

	// Modify form action property with friendly format url
	// This is so when submit will display friendly url in address bar
	Juk.update_form_action('jobs_form');

	// Add autocomplete behavior to keyword and location fields
	Juk.init_autocomplete();

	// Set click handler to all job title and more links
	Juk.init_result_click();

	//$('#myModal').modal('toggle');
	//$('#popupModal').modal('toggle');
	//$('#share-results').modal('toggle');

</script>
