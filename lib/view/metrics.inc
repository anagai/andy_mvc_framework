	<script type="text/javascript" src="/js/rm-analytics/rm.analytics.js"></script>
	<script>
		<?
		$js = 'rma("jobungo.co.uk").init("'.(preg_match('/(dev\.|local\.|stage\.)/', $_SERVER['HTTP_HOST']) ? 'dev' : 'prod').'", function(m){
			m.alias({
				//site data
				site_version:           "'.$out['version']->id.'",
				site_channel_id:        "'.$out['channel'].'",
				//mobile check
				is_mobile:              '.var_export($Browser->is_mobile ?: false, true).',
				//user data
				user_sub_id:            "'.$out['sub_id'].'",
				user_ip:                "'.$out['request_ip'].'",
				user_email:             "'.$out['email'].'",
				user_first_name:        "'.$out['first_name'].'",
				user_last_name:         "'.$out['last_name'].'",
				user_city:              "'.$out['city'].'",
				user_state:             "",
				user_zip:               "'.$out['postal_code'].'",
				user_opt_in_date:       "'.$out['date_opt_in'].'",
				user_opt_in_ip:         "'.$out['opt_in_ip'].'",
				user_keyword:           "'.$out['keyword'].'",
				user_agent:             "'.$Browser->agent.'",
				//partner info
				partner_id:             "'.$out['partner'].'",
				partner_source:         "'.$out['source'].'",
			});

			m.pageView({
				page_url: document.URL,
				is_result_page: '.var_export($out['is_result_page'] ?: "false", true).',
				result_provider_name: "indeed",
				result_page_num: '.(!is_object($out['pagination']) ? $out['pagination']->current_page : 0).',
				result_num_visible: '.($out['page_total'] ?: 0).',
				result_num_total: '.($out['result_total'] ?: 0).',
				result_filter_days_ago: '.($out['days_back'] ?: 0).',
				result_filter_radius: '.($out['radius'] ?: 0).',
				result_filter_keyword: "'.$out['keyword'].'",
				result_filter_location: "'.$out['location'].'"
			});

			m.addClickTracking(".lnk_job", {
				page_url: document.URL,
				result_provider_name: "indeed",
				result_page_num: '.(!is_object($out['pagination']) ? $out['pagination']->current_page : 0).',
				result_num_visible: '.($out['page_total'] ?: 0).',
				result_num_total: '.($out['result_total'] ?: 0).',
				result_filter_days_ago: '.($out['days_back'] ?: 0).',
				result_filter_radius: '.($out['radius'] ?: 0).',
				result_filter_keyword: "'.$out['keyword'].'",
				result_filter_location: "'.$out['location'].'"
			}, {
				result_id: "key",
				result_url: "url",
				result_title: "title",
				result_description: "description",
				result_location: "location",
				result_source: "source",
				result_company: "company",
				result_publisher_id: "pid",
				result_type: "type",
				result_position: "pos",
				result_added: "added"
			}, '.var_export($Browser->is_mobile ?: false, true).');

			m.addWindowBlurFix({
					page_url: document.URL,
					result_page_num: '.(!is_object($out['pagination']) ? $out['pagination']->current_page : 0).',
					result_num_visible: '.($out['page_total'] ?: 0).',
					result_num_total: '.($out['result_total'] ?: 0).',
					result_filter_days_ago: '.($out['days_back'] ?: 0).',
					result_filter_radius: '.($out['radius'] ?: 0).',
					result_filter_keyword: "'.$out['keyword'].'",
					result_filter_location: "'.$out['location'].'"
			});

			m.addAdIframeClickTracking(".iframe-ad");

		});';

		$obf = new JsObf($js, array('move_strings', 'hex_strings', 'minify', 'hexify', 'do_eval'));
		print $obf->mangle();

		?>
	</script>
