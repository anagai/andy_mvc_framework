<?php

$template = $app->get_page_template();

// Include google maps api file for this view
$app->add_script('/js/map.js');

include_once(TEMPLATE_PATH . $template .'_header.inc');

?>

<!-- Google Analytics -->
<script type="text/javascript">
/*
 var _gaq = _gaq || [];
 _gaq.push(['_setAccount', 'UA-8592459-11']);
 _gaq.push(['_setDomainName', '.jobungo.com']);
 _gaq.push(['_trackPageview']);

 (function() {
  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
 })();
 */

/*
function set_marker_timers( map, base_url ) {
    var url = "/ajax/get_cached_searches.php?lat=39.5&long=-96.35&nolocation=TRUE";
    $.getJSON( url, function( data ) {
        var ms_per_drop = 1500;
        var total_pulled = 0;

        // Set a bunch of timers
        $.each( data, function( idx, val ) {
            var thisDelay = idx * ms_per_drop;
            setTimeout( function() {
                place_marker( map, val, base_url );
            }, thisDelay );

            total_pulled += 1;
        } );
        // Give us a little buffer
        total_pulled += 1;

        // Do it again!
        setTimeout( function() {
            set_marker_timers( map, base_url )
        }, total_pulled * ms_per_drop );
    } );
}

function place_marker( map, data, base_url ) {
    var url = base_url + "/jobs"
    + "/q-" + escape( data[ "job" ] )
    + "/l-" + escape( data[ "location" ]) + '/';

    var lat = data[ "latitude" ];
    var lng = data[ "longitude" ];
    var myLatlng = new google.maps.LatLng( lat, lng );

    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        animation: google.maps.Animation.DROP,
        title: data[ "location "],
        icon: "/images/map-pins.png"
    });

    google.maps.event.addListener( marker, "click", function() {
        // Never mind; won't be doing this for now...
        //window.open( url, "_blank" );
    } );

    // Add an entry to #user_list
    // TODO: Make this a link!
    var box_text = data[ "job" ] + " in " + data[ "location" ];
    $( "#user_list" )
        .children( "ul" )
        .prepend( "<li><a href='" + url + "' target='_blank'>"
            + box_text + "</a></li>\n" );

    // Expire the pin!
    setTimeout( function() {
        marker.setMap( null );
        marker = null;
    }, (5 * 60 * 1000) + 500 );
}

$( document ).ready( function() {
    init_gmap();
} );
*/

</script>

    <div class="home-section">
        <div class="container-home">
            <div class="searching-map sixteen columns row">
                <h2>Who's Searching Now</h2>
                <div class="twelve columns alpha">
                    <div id="map_canvas" style="width: 100%; height: 480px; float:left; border:1px solid #CCC; "></div>
                </div>
                <div class="four columns omega">
                    <div id="user_list"><ul class="scroll_list"> </ul></div>
                </div>
            </div>
            <div class="sixteen columns search-form-home">
                <form action="/jobs" method="post" id="home_form">
                    <?php
                    include_once(TEMPLATE_PATH . 'component/search_fields.inc');
                    ?>
                </form>
            </div>
        </div>
    </div>

<?php include_once(TEMPLATE_PATH . $template . '_footer.inc');  ?>

<script type="text/javascript">

    //***** Scripts to setup DOM on page load

    // Modify form action property with friendly format url
    // This is so when submit will display friendly url in address bar
    Juk.update_form_action('home_form');

    // Add autocomplete behavior to keyword and location fields
    Juk.init_autocomplete();

    // Start populating who's searching map
    // Centered on East Kilbride:
    // Lat: 55.7714 Long: -4.1950
    var lat = 55.7714;
    var lon = -4.1950;
    var latlng = new google.maps.LatLng( lat, lon );
    var myOptions = {
        zoom: 5,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,

        disableDefaultUI: true,
        disableDoubleClickZoom: false,
        navigationControl: false,
        mapTypeControl: false,
        scaleControl: false,
        draggable: false,
        scrollwheel: false,
    };
    var map = new google.maps.Map(
        document.getElementById( "map_canvas" ), myOptions );

    Juk.map.set_marker_timers( map, lat, lon, '<?= Util::base_url() ?>', true, true );

</script>
