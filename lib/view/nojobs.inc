<?php
// vim: ai ts=4 sts=4 et sw=4 tw=79

//$controller = Util::load_controller('home');

//$out = $controller->runTask();

$out['keyword'] = Util::get_session('keyword');

$out['location'] = Util::get_session('location');


$template = 'home';

include_once(TEMPLATE_PATH . $template .'_header.inc');
?>
<!-- container -->
<div class="home-section">
		<div class="container-home container-inner">

			<p class="popup-txt">Sorry, We Couldn't Find Any Jobs!</p>
			<div class="sixteen columns search-form-home">
				<form action="/jobs" method="post" id="home_form" >
					<?php
					include_once(TEMPLATE_PATH . 'component/search_fields.inc');
					?>
					<!--<div class="seven columns alpha">
						<label style="color:#444;font-size: 18px; margin-bottom: 5px;">Keywords <span>job title, skills, or company</span></label>
						<input id="keyword" name="q" type="text" tabindex="1">
						<input type="hidden" id="keyword_id">
					</div>
					<div class="seven columns">
						<label style="color:#444;font-size: 18px; margin-bottom: 5px;">Location <span>city or postcode</span></label>
						<input id="location" name="l" type="text" tabindex="2">
						<input type="hidden" id="location_id">
					</div>
					<div class="two columns omega">
						<label style="font-size: 18px; margin-bottom: 5px;">&nbsp;</label>
						<button class="full-width" type="submit" tabindex="3"><img src="/images/search-icon.png"></button>
					</div>-->
				</form>
			</div>
			<div id="resultscontent" class="titlecenter">

				<div id="searchtip" style="height:150px; clear:both; margin-bottom:15px;">
					<span class="popup-txt">Search Tips</span>
						<ul>
							<li><img src="/images/lightbulb.png" width="16" />&nbsp;Keep it simple. Don't add unnecessary commas, spaces, and other special characters.</li>
							<li><img src="/images/lightbulb.png" width="16" />&nbsp;Try broadening your search by removing words from the Keywords box or expanding your search radius.</li>
							<li><img src="/images/lightbulb.png" width="16" />&nbsp;Double check your location. Enter a valid city or postcode.</li>
							<li><img src="/images/lightbulb.png" width="16" />&nbsp;Search for one job/position at a time. </li>
						</ul>
				</div>
			</div>
		</div>
	</div>

<?php include_once(TEMPLATE_PATH . $template . '_footer.inc');  ?>

<script type="text/javascript">

	//***** Scripts to setup DOM on page load

	// Modify form action property with friendly format url
	// This is so when submit will display friendly url in address bar
	Juk.update_form_action('home_form');

	// Add autocomplete behavior to keyword and location fields
	Juk.init_autocomplete();

</script>