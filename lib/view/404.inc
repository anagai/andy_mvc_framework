<?php
// vim: ai ts=4 sts=4 et sw=4 tw=79

header("HTTP/1.0 404 Not Found");

$template = $app->get_page_template();

include_once(TEMPLATE_PATH . $template .'_header.inc');

?>

<!-- container -->
<div class="home-section">
		<div class="container-home container-inner">
			
			<p class="popup-txt">404 Error</p>
			<strong>Something seems to be wrong and we can't find the page you're looking for.</strong> Why don't you try searching for a job instead?<br /><br />
			
			<form action="/jobs" method="post" id="home_form">
					<?php 
					include_once(TEMPLATE_PATH . 'component/search_fields.inc');
					?>
					<!--<div class="seven columns alpha">
						<label style="color:#444;font-size: 18px; margin-bottom: 5px;">Keywords <span>job title, skills, or company</span></label>
						<input id="keyword" name="q" type="text" tabindex="1">
					</div>
					<div class="seven columns">
						<label style="color:#444;font-size: 18px; margin-bottom: 5px;">Location <span>city or postcode</span></label>
						<input id="location" name="l" type="text" tabindex="2">
					</div>
					<div class="two columns omega">
						<label style="font-size: 18px; margin-bottom: 5px;">&nbsp;</label>
						<button class="full-width" type="submit" tabindex="3"><img src="/images/search-icon.png"></button>
					</div>--><br><br>
				</form>
			</div>
    </div>

<?php include_once(TEMPLATE_PATH . $template . '_footer.inc');  ?>

<script type="text/javascript">

	//***** Scripts to setup DOM on page load

	// Modify form action property with friendly format url
	// This is so when submit will display friendly url in address bar
	Juk.update_form_action('home_form');

	// Add autocomplete behavior to keyword and location fields
	Juk.init_autocomplete();
	
</script>