<?php
    // vim: ai ts=4 sts=4 et sw=4 tw=79

    $detailLog = 'partners_feed.log';
    $errLog = 'partners_feed_error.log';

    $mainStart = microtime(true);

    //Util::log_to_file($detailLog, 'Start', $_SERVER['REQUEST_URI']);

    // Die for maintenance or if feed disabled!
    if( MAINTENANCE == TRUE || COREG_FEED == FALSE ) {
        die( "Rejected: under maintenance" );
        Util::log_to_file('partners_feed.log', 'Maintenance mode', '');
    }

    // Log coreg query string!
    if( FALSE ) {
        $now = date( "Y-m-d H:i:s" );
        $fh = fopen( "/tmp/coreg_unparsed.csv", "a" );
        fwrite( $fh, $now . "\t" . $_SERVER[ "QUERY_STRING" ] . "\n" );
        fclose( $fh );
    }

    // Load all needed models
    $partnersfeed_model = Util::load_model('partnersfeed');
    $coreg_partner_model = Util::load_model('coregpartner');
    $ses_model = Util::load_model('ses');
    $user_model = Util::load_model('user');
    //Util::log_to_file($detailLog, 'Getting reject reasons...', '');
    $reject_reasons = $partnersfeed_model->get_reject_reasons();
    //Util::log_to_file($detailLog, 'Reject reasons found', "cnt: " . count($reject_reasons));
    //Util::printr($reject_reasons);

    // Set default values
    $is_accepted = TRUE;
    $is_mailable = TRUE;
    $noloc_coreg = FALSE;
    $is_dnm_reject = FALSE;
    $is_invalid = FALSE;
    $is_within7 = FALSE;
    $is_within60 = FALSE;
    $now_dt = date('Y-m-d H:i:s');
    $reject_reason_id = 0;
    $rejection_reason = '';
    $fldList = array();
    $incrList = array();

    // Parse coreg query string into an array
    parse_str($_SERVER["QUERY_STRING"], $params);

    // Clean query string values.
    // 1) Urldecode
    // 2) Strip back slashes
    // 3) trim
    // 4) Replaces spaces in email with plus sign
    //Util::log_to_file($detailLog, 'Cleaning params...', '');
    $params = $partnersfeed_model->clean_param_value($params);

    // Kick out with no coreg record if do not have following url parameters:
    // email
    // ip - opt in ip
    // date - opt in date
    // pid - partner
    //Util::log_to_file($detailLog, 'Checking required params...', '');
    $partnersfeed_model->check_required_params($params);

    // Set needed variables
    $email = $params["email"];
    $email_split = explode("@", $email);
    $email_domain = $email_split[1];
    $partner = Util::trimPartner($params["pid"]);
    $opt_in_ip = $params['ip'];
    $opt_in_date = $params['date'];
    $source = !empty($params["source"]) ? Util::trimSource($params["source"]) : '';
    $subcode = !empty($params["subcode"]) ? $params["subcode"] : '';
    $injList = !empty($params["inj"]) ? $params["inj"] : '';

    // Kick out with no coreg record if any of the required parameters are invalid
    //Util::log_to_file($detailLog, 'Validating params...', "em: {$email} pid: {$partner} " .
    //    "src: {$source} ip: {$opt_in_ip} dt: {$opt_in_date}");
    $reject_key = $partnersfeed_model->validate_params($params, $reject_reasons);

    //echo "reject key: " . $reject_key . '<br>';

    if($reject_key !== TRUE) {
        $reject_reason_id = $reject_reasons[$reject_key]['id'];
        $rejection_reason = $reject_reasons[$reject_key]['desc'];
        $is_accepted = FALSE;
        $is_mailable = FALSE;
        $is_invalid = TRUE;
        //Util::log_to_file($detailLog, 'Params invalid', "em: {$email} accepted: {$is_accepted} mailable: {$is_mailable} " .
        //    "rejid: {$reject_reason_id} reas: {$rejection_reason}");
        goto REJECTED_END;
    } else {
        //Util::log_to_file($detailLog, 'Params are valid', "em: {$email} accepted: {$is_accepted} mailable: {$is_mailable}");
    }

    //Util::log_to_file($detailLog, 'Checking partner exists...', "em: {$email} pid: {$partner} src: {$source}");
    // If partner does not exists then create coreg_partner record
    if (!empty($partner) && !$coreg_partner_model->is_exist($partner, $source, $subcode)) {
        //Util::log_to_file($detailLog, 'Creating new partner', "em: {$email} pid: {$partner} src: {$source}");
        $partnersfeed_model->create_coreg_partner($partner, $source, $subcode);

    }

    $zip = isset($params['zip'])
        ? $params['zip']
        : '';

    $city = isset($params['city'])
        ? $params['city']
        : '';

    // premium partner list
    $premPartners[] = 'VHMW';
    $premPartners[] = 'ANT';
    $premPartners[] = 'MJHUK';

    $isPrem = false;

    foreach($premPartners as $premPar) {
        $premPar = preg_quote($premPar);
        if(preg_match("/^{$premPar}($|:)/i", $partner)) {
            //echo "Found!\n";
            $isPrem = true;
            break;
        }
    }

    // only premium partners go into normal coreg workflow
    $nowrm = $isPrem == true
        ? 0
        : 1;

    // non-premium partner list used in determining which welcome campaign to use
    // in welcome send functions.
    $nonPremPartners = array('R3IUK','R3IUK2','RVUK','PMUK','R3I5','LSMUK',
            'VIPUK','BBCUK','BBCUK2','BBCUK3','DMUK','LMUK','PMUK2','PMUK3',
            'RSTUK');

    // nowrm flag override
    //$nowrm = !empty($params['nowrm'])
    //    ? 1
    //    : 0;


    // Set postal code and city data for this user
    //Util::log_to_file($detailLog, 'Set location...', "em: {$email} zip: {$zip} city: {$city}");
    $params = $partnersfeed_model->set_location($params);

    //Util::debug($params);

    // Reject if no location data
    if(empty($params['zip']) && empty($params['city'])) {

        // Rejection Reason: 'No location'
        /*
        $reject_reason_id = $reject_reasons['no_loc']['id'];
        $rejection_reason = $reject_reasons['no_loc']['desc'];
        Util::log_to_file('partners_feed.log',
            'Co-reg Rejected', $rejection_reason . " '{$email}'");
        $is_accepted = FALSE;
        $is_mailable = FALSE;
        goto REJECTED_END;
        */

        // No location users are rejected but mailable
        //Util::log_to_file('partners_feed.log',
         //   'Co-reg Rejected', "No Location User '{$email}'");

        // Rejection Reason: 'No location'
        $reject_reason_id = $reject_reasons['no_loc']['id'];
        $rejection_reason = $reject_reasons['no_loc']['desc'];
        $is_accepted = FALSE;
        $is_mailable = TRUE;
        $noloc_coreg = TRUE;
        Util::log_to_file($detailLog, 'Rejected - No location but mailable...', "em: {$email} accepted: {$is_accepted} " .
            "mailable: {$is_mailable} rejid: {$reject_reason_id} reas: {$rejection_reason}");
    }

    // Find reason to reject. Will be rejected for these reasons
    // 1) Partner is inactive
    // 2) Is not a top level domain (must be .uk, .com, .co, .net, .org, .edu, .info, .me or .name)
    // 3) email is in global removal, complaint, bounced or unsubscribe list
    //Util::log_to_file($detailLog, 'Checking partner and dnm...', "em: {$email} pid: {$partner} src: {$source}");

    $reject_key = $partnersfeed_model->validate_dnm_partner_domain($params, $partner);

    if($reject_key !== TRUE) {
        $reject_reason_id = $reject_reasons[$reject_key]['id'];
        $rejection_reason = $reject_reasons[$reject_key]['desc'];
        $is_accepted = FALSE;
        $is_mailable = FALSE;
        $is_dnm_reject = TRUE;
        Util::log_to_file($detailLog, 'Rejected - Failed dnm check, not mailable', "em: {$email} accepted: {$is_accepted} " .
            "mailable: {$is_mailable} rejid: {$reject_reason_id} reas: {$rejection_reason}");
        goto REJECTED_END;
    }

    // Handle duplicate user
    // When user object returned means user was already sent welcome email
    //Util::log_to_file($detailLog, 'Getting user...', "em: {$email}");
    if ($user = $user_model->find_by_email($email)) {
        Util::log_to_file($detailLog, 'User found', "em: {$email} uid: {$user->id}");

        if(!$user->is_mailable) {
            $reject_reason_id = $reject_reasons['not_mailable']['id'];
            $rejection_reason = $reject_reasons['not_mailable']['desc'];
            $is_accepted = FALSE;
            $is_mailable = FALSE;
            Util::log_to_file($detailLog, 'Rejected - User not mailable', "em: {$email} accepted: {$is_accepted} " .
                "mailable: {$is_mailable} rejid: {$reject_reason_id} reas: {$rejection_reason}");
            goto REJECTED_END;
        }

        // If user already have location this coreg is not no loc reject
        if($noloc_coreg) {
            if(!empty($user->city) || !empty($user->postal_code)) {
                $reject_reason_id = 0;
                $rejection_reason = '';
                # set back to pre noloc state
                $is_accepted = TRUE;
                $noloc_coreg=FALSE;
                $params["city"] = $user->city;
                $params["zip"] = $user->postal_code;
                //Util::log_to_file($detailLog, 'Using user location. Is not no loc coreg.',
                //    "em: {$email} uid: {$user->id} accepted: {$is_accepted} mailable: {$is_mailable} " .
                //    "city: {$params['city']} zip: {$params['zip']}");
            }
        }

        // If was mailed 35 or more days ago we can send another email
        if (strtotime($user->date_last_mailed) <= strtotime("-35 day")) {

            // if unpaid partner, and mailed more than 35 days ago then is Accepted, update user info and send email
            if (!$coreg_partner_model->is_paid($user->partner)) {
                Util::log_to_file($detailLog, 'Accepted - Sent > 35 days ago non-paid partner',
                    "em: {$email} uid: {$user->id} accepted: {$is_accepted} mailable: {$is_mailable} " .
                    "city: {$params['city']} zip: {$params['zip']}");
                // Update user info with latest coreg data
                $user->first_name = (isset($params["fname"])) ? $params["fname"] : '';
                $user->last_name = (isset($params["lname"])) ? $params["lname"] : '';

                if(!empty($params["city"]) || !empty($params["zip"])) {
                    $user->city = $params["city"];
                    $user->postal_code = $params["zip"];
                }

                $user->partner = $partner;
                $user->source = $source;
                $user->subcode = $subcode;
                $user->sub_id = (isset($params["sub_id"])) ? $params["sub_id"] : '';
                $user->opt_in_ip = $params["ip"];
                $user->date_opt_in = $params["date"];

                //Util::log_to_file('partners_feed.log', 'Accepted', '60 days ago non-paid partner user: ' . $email);

            } else {
                // Any paid partner that was sent  35 or more days ago is rejected but mailable
                // NOTE: Do not have any paid partners. This here just in case there will be
                $reject_reason_id = $reject_reasons['more_seven']['id'];
                $rejection_reason = $reject_reasons['more_seven']['desc'];
                //Util::log_to_file('partners_feed.log',
                //    'Co-reg Rejected', $rejection_reason . ", but mailable '{$email}'");
                $is_accepted = FALSE;
                $is_within60 = TRUE;
                Util::log_to_file($detailLog, 'Rejected - Sent > 35 days ago paid partner, is mailable',
                    "em: {$email} uid: {$user->id} accepted: {$is_accepted} mailable: {$is_mailable} " .
                    "rejid: {$reject_reason_id} reas: {$rejection_reason}");
                goto REJECTED_END;
            }

        } else {
            // User was emailed in the past 35 days. Reject and do not send email.

            $reject_reason_id = $reject_reasons['sent_seven']['id'];
            $rejection_reason = $reject_reasons['sent_seven']['desc'];
            //Util::log_to_file('partners_feed.log',
            //    'Co-reg Rejected', $rejection_reason . " '{$email}'");
            $is_accepted = FALSE;
            $is_mailable = FALSE;
            $is_within7 = TRUE;
            Util::log_to_file($detailLog, 'Rejected - Sent within 35 days',
                "em: {$email} uid: {$user->id} accepted: {$is_accepted} mailable: {$is_mailable} " .
                "rejid: {$reject_reason_id} reas: {$rejection_reason}");
            goto REJECTED_END;
            //exit("Rejected");
        }

    } else {
        //Util::log_to_file($detailLog, 'Creating new user...',
        //       "em: {$email} accepted: {$is_accepted} mailable: {$is_mailable}");
        // Save to DB
        $new_user = new User();

        $new_user->email = $email;
        $new_user->email_domain = $email_domain;
        $new_user->first_name = (isset($params["fname"])) ? $params["fname"] : "";
        $new_user->last_name = (isset($params["lname"])) ? $params["lname"] : "";
        $new_user->city = (!empty($params["city"])) ? $params["city"] : "";
        $new_user->postal_code = (!empty($params["zip"])) ? $params["zip"] : "";
        $new_user->partner = $partner;
        $new_user->source = $source;
        $new_user->subcode = $subcode;
        $new_user->sub_id = (isset($params["sub_id"])) ? $params["sub_id"] : "";
        $new_user->opt_in_ip = $params["ip"];
        $new_user->date_opt_in = $params["date"];
        $new_user->is_mailable = 1; // default
        $new_user->date_added = $now_dt;
        $new_user->nowrm = $nowrm;

        if ($new_user->create()) {
            //Util::log_to_file('partners_feed.log', 'Accepted', 'New user: ' . $email);
            // Add encrypted user id
            $new_user->reg_key = Util::mcryptEncode($new_user->id);
            $new_user->update();
            $newUserId = $new_user->id;
            Util::log_to_file($detailLog, 'Accepted - New user created',
                "em: {$email} uid: {$newUserId} accepted: {$is_accepted} mailable: {$is_mailable}");
        } else {
            // Rejection reason: Failed to store user
            $reject_reason_id = $reject_reasons['no_rec']['id'];
            $rejection_reason = $reject_reasons['no_rec']['desc'];
            $is_accepted = false;
            $is_mailable = false;
            //Util::log_to_file('partners_feed.log',
            //    'Co-reg Failed', $rejection_reason . " '{$email}'");
            Util::log_to_file($detailLog, 'Failed to create new user',
                "em: {$email} pid: {$partner} src: {$source} accepted: {$is_accepted} " .
                "mailable: {$is_mailable} rejid: {$reject_reason_id} reas: {$rejection_reason}");
            goto REJECTED_END;
        }

        $user = $new_user; // to process both new and mailable users
    }

    // When user is rejected go here
    REJECTED_END:


    // Create coreg record if rejected or accepted
    $co_reg = new CoReg();

    $co_reg->email = $email;
    $co_reg->email_domain = $email_domain;
    $co_reg->first_name = (isset($params["fname"])) ? $params["fname"] : '';
    $co_reg->last_name = (isset($params["lname"])) ? $params["lname"] : '';
    $co_reg->city = (!empty($params["city"])) ? $params["city"] : '';
    $co_reg->postal_code = (!empty($params["zip"])) ? $params["zip"] : '';
    $co_reg->partner = $partner;
    $co_reg->source = $source;
    $co_reg->subcode = $subcode;
    $co_reg->sub_id = (isset($params["sub_id"])) ? $params["sub_id"] : '';
    $co_reg->opt_in_ip = $params["ip"];
    $co_reg->date_opt_in = $params["date"];
    $co_reg->accepted = $is_accepted;
    if(!empty($reject_reason_id)) {
        $co_reg->reject_reason_id = $reject_reason_id;
    }
    $co_reg->rejection_reason = $rejection_reason;
    $co_reg->is_mailable = $is_mailable;
    $co_reg->date_created = $now_dt;

    // We create the coreg record exactly once: doing a create-update
    // pair grows more expensive as the size of the user table increases.

    $conn = $db->get_pdo_conn(DEFAULT_DB_SETTING);

    try {

        if (!$co_reg->create()) {
            //Util::log_to_file('partners_feed.log', 'Rejected', "Failed to store coreg record '{$email}'.");
            $is_accepted = FALSE;
            $is_mailable = FALSE;
            Util::log_to_file($detailLog, 'Rejected - Failed to create coreg record',
                    "em: {$email} uid: {$user->id} pid: {$partner} src: {$source} " .
                    "accepted: {$is_accepted} mailable: {$is_mailable} " .
                    "rejid: {$reject_reason_id} reas: {$rejection_reason}");
        } else {
            /*
            //Add coreg pivot data

            $aggField['Accepted'] = $is_accepted
                ? 1
                : 0;
            $aggField['Rejected'] = !empty($reject_reason_id)
                ? 1
                : 0;
            $aggField['NoLoc'] = $noloc_coreg
                ? 1
                : 0;
            $aggField['Mailable'] = $is_mailable
                ? 1
                : 0;
            $aggField['Invalid'] = $is_invalid
                ? 1
                : 0;
            $aggField['Dnm'] = $is_dnm_reject
                ? 1
                : 0;
            $aggField['Within7'] = $is_within7
                ? 1
                : 0;
            $aggField['Within60'] = $is_within60
                ? 1
                : 0;

            //******** update coreg daily pivot data *********

            unset($stmt);
            $curDt = date('Y-m-d');

            $taskStart = microtime(true);

            $findSql = "SELECT * from coregPivotDaily WHERE `Date`=?";
            $stmt['find'] = $conn->prepare($findSql);
            $stmt['find']->execute(array($curDt));

            if($stmt['find']->rowCount()>0) {
                // Update pivot data
                $dailyRow = $stmt['find']->fetch(PDO::FETCH_ASSOC);

                $dailyRow['Received']++;
                $dailyRow['Accepted'] += $aggField['Accepted'];
                $dailyRow['Rejected'] += $aggField['Rejected'];
                $dailyRow['NoLoc'] += $aggField['NoLoc'];
                $dailyRow['Mailable'] += $aggField['Mailable'];
                $dailyRow['Invalid'] += $aggField['Invalid'];
                $dailyRow['Dnm'] += $aggField['Dnm'];
                $dailyRow['Within7'] += $aggField['Within7'];
                $dailyRow['Within60'] += $aggField['Within60'];

                $updSql = "UPDATE coregPivotDaily SET
                    Received=?,
                    Accepted=?,
                    Rejected=?,
                    NoLoc=?,
                    Mailable=?,
                    Invalid=?,
                    Dnm=?,
                    Within7=?,
                    Within60=? WHERE `Date`=?;
                    ";
                $stmt['update'] = $conn->prepare($updSql);
                $stmt['update']->execute(array(
                    $dailyRow['Received'],
                    $dailyRow['Accepted'],
                    $dailyRow['Rejected'],
                    $dailyRow['NoLoc'],
                    $dailyRow['Mailable'],
                    $dailyRow['Invalid'],
                    $dailyRow['Dnm'],
                    $dailyRow['Within7'],
                    $dailyRow['Within60'],
                    $curDt
                    ));
            } else {
                // Insert pivot data
                $insSql = "INSERT IGNORE INTO coregPivotDaily (
                    Received,
                    `Date`,
                    Accepted,
                    Rejected,
                    NoLoc,
                    Mailable,
                    Invalid,
                    Dnm,
                    Within7,
                    Within60) VALUES (1,?,?,?,?,?,?,?,?,?);
                ";
                $stmt['insert'] = $conn->prepare($insSql);
                $stmt['insert']->execute(array(
                    $curDt,
                    $aggField['Accepted'],
                    $aggField['Rejected'],
                    $aggField['NoLoc'],
                    $aggField['Mailable'],
                    $aggField['Invalid'],
                    $aggField['Dnm'],
                    $aggField['Within7'],
                    $aggField['Within60']
                    ));
            }

            //$dur = microtime(true) - $taskStart;
            //Util::log_to_file($detailLog, 'Updated coregPivotDaily',
            //    "em: {$email} accepted: {$is_accepted} mailable: {$is_mailable} dur: {$dur}");

            unset($stmt);

            //********* Update coreg partner pivot data ***********

            $taskStart = microtime(true);

            $findSql = "SELECT * from coregPivotPartner WHERE `Date`=? AND
                Partner=? AND Source=?";
            $stmt['find'] = $conn->prepare($findSql);
            $stmt['find']->execute(array($curDt, $partner, $source));

            if($stmt['find']->rowCount()>0) {
                $partnerRow = $stmt['find']->fetch(PDO::FETCH_ASSOC);
                // Update pivot data
                $partnerRow['Received']++;
                $partnerRow['Accepted'] += $aggField['Accepted'];
                $partnerRow['Rejected'] += $aggField['Rejected'];
                $partnerRow['NoLoc'] += $aggField['NoLoc'];
                $partnerRow['Mailable'] += $aggField['Mailable'];
                $partnerRow['Invalid'] += $aggField['Invalid'];
                $partnerRow['Dnm'] += $aggField['Dnm'];
                $partnerRow['Within7'] += $aggField['Within7'];
                $partnerRow['Within60'] += $aggField['Within60'];

                $updSql = "UPDATE coregPivotPartner SET
                    Received=?,
                    Accepted=?,
                    Rejected=?,
                    NoLoc=?,
                    Mailable=?,
                    Invalid=?,
                    Dnm=?,
                    Within7=?,
                    Within60=? WHERE `Date`=? AND Partner=? AND Source=?;
                    ";
                $stmt['update'] = $conn->prepare($updSql);
                $stmt['update']->execute(array(
                    $partnerRow['Received'],
                    $partnerRow['Accepted'],
                    $partnerRow['Rejected'],
                    $partnerRow['NoLoc'],
                    $partnerRow['Mailable'],
                    $partnerRow['Invalid'],
                    $partnerRow['Dnm'],
                    $partnerRow['Within7'],
                    $partnerRow['Within60'],
                    $curDt, $partner, $source
                    ));
            } else {
                // Insert pivot data
                $insSql = "INSERT IGNORE INTO coregPivotPartner (
                    Received,
                    `Date`,
                    Partner,
                    Source,
                    Accepted,
                    Rejected,
                    NoLoc,
                    Mailable,
                    Invalid,
                    Dnm,
                    Within7,
                    Within60) VALUES (1,?,?,?,?,?,?,?,?,?,?,?);
                ";
                $stmt['insert'] = $conn->prepare($insSql);
                $stmt['insert']->execute(array(
                    $curDt,
                    $partner,
                    $source,
                    $aggField['Accepted'],
                    $aggField['Rejected'],
                    $aggField['NoLoc'],
                    $aggField['Mailable'],
                    $aggField['Invalid'],
                    $aggField['Dnm'],
                    $aggField['Within7'],
                    $aggField['Within60']
                    ));
            }

            $dur = microtime(true) - $taskStart;
            //Util::log_to_file($detailLog, 'Updated coregPivotPartner',
            //    "em: {$email} accepted: {$is_accepted} mailable: {$is_mailable} dur: {$dur}");
            */
        }

        // Insert into coreg_injection for injection feeds
        if(!empty($injList)) {

            $sql = "
            INSERT INTO coregInjection (listName, userId, email, partner, source,
            accepted, mailable, rejectId) VALUES (?, ?, ?, ?, ?, ?, ?, ?)
            ";
            $taskStart = microtime(true);
            $injStmt = $conn->prepare($sql);
            if(empty($user)) {
                $uID = 0;
            } else {
                $uID = $user->id;
            }
            $injStmt->execute(array($injList, $uID, $email, $partner,
                $source, $is_accepted, $is_mailable, $reject_reason_id));
            $dur = microtime(true) - $taskStart;
            //Util::log_to_file($detailLog, 'Inserted into coregInjection',
            //        "em: {$email} accepted: {$is_accepted} mailable: {$is_mailable} injlist: {$injList} dur: {$dur}");
        }



        // Update new user rec with coreg record id
        if(isset($newUserId)) {
            $user->coreg_id = $co_reg->id;
        }

        if(isset($params['et'])) {
            $email_template = $params['et'];
        } else {
            $email_template = '';
        }

        /*
        if($is_mailable) {
            //Util::log_to_file("welcome_email_mailable.log", "", "em: {$email} Partner: {$partner} " .
            //    "accepted: {$is_accepted} rejid: {$reject_reason_id} reason: {$rejection_reason}");
        } else {
            //Util::log_to_file("welcome_email_not_mailable.log", "", "em: {$email} Partner: {$partner} " .
            //    "accepted: {$is_accepted} rejid: {$reject_reason_id} reason: {$rejection_reason}");
        }

        $sql = "INSERT INTO feed_log (email, partner, source, mailable, accepted, rejectId, reason, injectlist, created)
                    values (?,?,?,?,?,?,?,?,?);
        ";

        unset($logStmt);

        $logStmt = $conn->prepare($sql);
        $logStmt->execute(array(
            $email,
            $partner,
            $source,
            $is_mailable,
            $is_accepted,
            $reject_reason_id,
            $rejection_reason,
            $injList,
            $now_dt
            ));
        */

    } catch(Exception $e) {
        Util::log_to_file($errLog, 'PDO Error',
            "em: {$email} pid: {$partner} src: {$source} " .
            "accepted: {$is_accepted} mailable: {$is_mailable} err: {$e->getMessage()}");
    }

    if($is_mailable) {

        if($noloc_coreg) {

            if (($sendOut = $ses_model->send_noloc_welcome_email($user, $email_template, $nonPremPartners))===true) {
                $user->date_last_mailed = $now_dt;
                $user->date_coreg_sent = $now_dt;
                //Set flag to not send welcome reminder if passed
                $user->nowrm = $nowrm;
                $user->update();
                Util::log_to_file("welcome_email.log", "Success", "Sent no location welcome email '{$user->email}'. " .
                    "Partner: {$user->partner} nowrm: {$nowrm}");
                //Util::log_to_file($detailLog, 'Success - Welcome email sent', "em: {$email} pid: {$partner} src: {$source} " .
                //    "accepted: {$is_accepted} mailable: {$is_mailable}");
            } else {
                Util::log_to_file("welcome_email.log", "Failed", "Failed to send no location welcome email '{$user->email}'. " .
                    "Partner: {$user->partner} nowrm: {$nowrm} output: {$sendOut}");
                //Util::log_to_file($detailLog, 'Failed sending welcome email', "em: {$email} pid: {$partner} src: {$source} " .
                //    "accepted: {$is_accepted} mailable: {$is_mailable}");
            }

        } else {

            if (($sendOut = $ses_model->send_welcome_email($user, $email_template, $nonPremPartners))===true) {
                $user->date_last_mailed = $now_dt;
                $user->date_coreg_sent = $now_dt;
                //Set flag to not send welcome reminder if passed
                $user->nowrm = $nowrm;
                $user->update();
                Util::log_to_file("welcome_email.log", "Success", "Sent welcome email {$user->email} " .
                    "pid: {$user->partner} src: {$source} nowrm: {$nowrm}");
                //Util::log_to_file($detailLog, 'Success - No Loc welcome email sent', "em: {$email} pid: {$partner} src: {$source} " .
                //    "accepted: {$is_accepted} mailable: {$is_mailable}");
            } else {
                Util::log_to_file("welcome_email.log", "Failed", "Failed to send welcome email {$user->email} " .
                    "pid: {$user->partner} src: {$source} nowrm: {$nowrm} output: {$sendOut}");
                //Util::log_to_file($detailLog, 'Failed sending No Loc welcome email', "em: {$email} pid: {$partner} src: {$source} " .
                //    "accepted: {$is_accepted} mailable: {$is_mailable}");
            }

        }


    }

    $dur = microtime(true) - $mainStart;

    if ($is_accepted) {
        echo "Accepted";
        Util::log_to_file($detailLog, 'Done', "Accepted - pid {$params['pid']} (ID:{$co_reg->id}) em: {$email} " .
            "pid: {$partner} src: {$source} accepted: {$is_accepted} mailable: {$is_mailable} dur: {$dur} nowrm: {$nowrm}");
    } else {
        echo "Rejected";
        Util::log_to_file($detailLog, 'Done', "Rejected  - pid {$params['pid']} (ID:{$co_reg->id}) em: {$email} " .
            "pid: {$partner} src: {$source} accepted: {$is_accepted} mailable: {$is_mailable} dur: {$dur} nowrm: {$nowrm}");
    }

    //Util::log_to_file($detailLog, 'Done', "em: {$email} pid: {$partner} src: {$source} " .
    //    "accepted: {$is_accepted} mailable: {$is_mailable} dur: {$dur}");

    unset($conn);
?>
