<?php
// vim: ai ts=4 sts=4 et sw=4 tw=79

//$page = Util::load_controller('home');

//$out = $page->controller();

$template = $app->get_page_template();

include_once(TEMPLATE_PATH . $template .'_header.inc');

?>

		<div class="home-section">
			<div class="container-home container-inner" style="text-align: center;">
				
				<p class="popup-txt">Undergoing routine maintenance.</p>
				<p style="margin-bottom: 0;">Be back shortly...</p>
				
			</div>
		</div>

<?php include_once(TEMPLATE_PATH . $template . '_footer.inc');  ?>