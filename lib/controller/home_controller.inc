<?php
	// vim: ai ts=4 sts=4 et sw=4 tw=79

	/**
	 * Created on Oct 23, 2012
	 * Written By Andy Nagai
	 *
	 */

	class HomeController extends Controller {

		function __construct() {

			$this->_initController();

		}

		protected function _initState() {

		}

		protected function _display() {

			// If coming from email get location from user record.
			// Otherwise get location by IP
			if($uid = Util::get_session('uid')) {

				// If uid then get user location
				$user_model = Util::load_model('user');

				$user = $user_model->find_by_id($uid);

				// Location search is either city or postal code.
				// City returns more search results
				$location = $user->city
					? $user->city
					: $user->postal_code;

				$geocode = Util::load_model('geocodelocation');

				$longlat = $geocode->get_longlat_by_city($location);

				$this->addOut('longlat', $longlat);

			} else {

				// Find user's location by IP
				// For non GP countries will display London as default city
				$geocode = Util::load_model('geocodelocation');
				$loc = $geocode->get_loc_by_ip();

				// Location search is either city or postal code.
				$location = $loc['city']
					? $loc['city']
					: $loc['postal'];

				$longlat = $geocode->get_longlat_by_city($location);
				//Util::debug($location);
				//Util::debug($longlat);
				$this->addOut('longlat', $longlat);
			}

			$this->addOut('location', $location);

		}

		protected function _search() {

			$this->_display();

		}

	}