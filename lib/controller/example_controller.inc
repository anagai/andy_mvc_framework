<?php
	// vim: ai ts=4 sts=4 et sw=4 tw=79

	/**
	 * Created on Oct 9, 2012
	 * Written By Andy Nagai
	 *
	 */

	class ExampleController extends Controller {

		function __construct() {

			$this->_initController();

		}

		/**
		 * Setup objects needed in different view states.
		 * If only used in specific state then don't define it here.
		 *
		 */
		protected function _initState() {

			// Get url parameters. This should go in beginning of method
			$param = Util::getVar();




			// Set objects to state object so can be used
			// in different view states. This should go at end of method.
			$this->setState();

		}

		/**
		 * The view state when user first comes to page
		 *
		 */
		protected function _display() {

			// Retrieve objects from state that were set in _initState()
			$val = $this->getState();



			// Output to view. Should go at end of method.
			$this->addOut();
		}

	}