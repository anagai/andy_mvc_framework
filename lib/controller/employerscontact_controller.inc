<?php
    // vim: ai ts=4 sts=4 et sw=4 tw=79

    /**
     * Created on Nov 21, 2012
     * Written By Andy Nagai
     *
     */

    class EmployersContactController extends Controller {

        function __construct() {

            $this->_initController();

        }

        protected function _initState() {

        }

        protected function _display() {

        }

        protected function _send() {

			Util::load_phpmailer();

            $to = "mail@jobungo.co.uk";
            $name_field = $_POST['name'];
            $email_field = $_POST['email'];
            $contactmessage = $_POST['message'];
            $employer_name = $_POST['company'];
            $employer_address = $_POST['city'];
            $employer_phone = $_POST['phone'];
            $subject = "Employer Contact Form Request from $employer_name";
            $employerbody = "Employer Information:\n$employer_name\n$employer_address\n$employer_phone" ;

            $body = "From: $name_field\n\nE-Mail: $email_field\n\nMessage:\n$contactmessage\n\n$employerbody";

            $mail = new PHPMailer();

            $mail->IsSMTP();
            //$mail->SMTPDebug = true; // for error checking
            $mail->Host = "173.204.123.41";
            $mail->Port = 25;

            $mail->FromName = $name_field;
            $mail->From = $email_field;
            $mail->AddAddress($to, "Employer Contact");
            $mail->AddReplyTo($email_field, $name_field);
            $mail->Sender = $email_field; // return path

            $mail->Subject = $subject;
            $mail->IsHTML(true);

            // HTML Version
            $mail->Body = $body;

            // Text Version
            $mail->AltBody = $body;

            $result = $mail->Send();

            Util::redirect_to('thankyou');
        }
    }