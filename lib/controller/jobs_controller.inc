<?php
    // vim: ai ts=4 sts=4 et sw=4 tw=79

    /**
     * Created on Oct 9, 2012
     * Written By Andy Nagai / Sung Ahn
     *
     */

    class JobsController extends Controller {

        private $_main_job_list = array();

        function __construct() {

            $this->_initController();
        }

        // Initialize variables needed across different view states
        protected function _initState() {

            // Set value from user input or url input with ucfist
            $keyword = Util::getVar('q');
            $location = Util::getVar('l');

            // Set Default values used in all states
            $days_back_def = Util::get_session('days_back')
                ? Util::get_session('days_back')
                : 7;
            $radius_def = Util::get_session('radius')
                ? Util::get_session('radius')
                : 25;

            if(Util::getVar('full')) {
                $_SESSION['full_site'] = 1;
            }

            // This is set in _set_jrp_version();
            // Number of jobs to display
            //$jobs_per_page = 12;

            // Number of paging links on page
            $paging_per_page = 10;

            $days_back = Util::getVar('d', $days_back_def);
            $radius = Util::getVar('r', $radius_def);
            $pg = Util::getVar('pg', '', 'GET');
            //$uid = Util::getVar('uid', '', 'GET');
            $sub_id = Util::getVar('sub_id', '', 'GET');
            $job_key = Util::getVar('jk', '', 'GET');
            $email_kw = Util::getVar('kw', '', 'GET');
            // Set current view to state.
            $this->setState('view', Util::get_view());

            //$email_template = Util::getVar('et', '', 'GET');

            // Load needed models
            $user_model = Util::load_model('user');
            $this->setState('result_version_model', Util::load_model('resultversion'));

            // *** Get the uid after this point from session always

            // Get user record if there is an uid in session
            if(Util::is_session('uid')) {
                $user = $user_model->find_by_id(Util::get_session('uid'));

                // More effiecient to update single column then all columns
                // in the normal orm class update()
                if($user->has_engaged == 0) {
                    $user_model->update_as_engaged(Util::get_session('uid'));
                }

                $this->setState('user', $user);

                // Set session keyword when user clicks on a keyword link.
                // This is only passed from email digest with valid uid and et. The uid and et
                // checking done in /redir
                if(!empty($email_kw)) {
                    $keyword = $email_kw;
                }

                // User clicked on a job title link with job key. This is used to generate
                // Featured job
                if($job_key) {
                    $this->setState('job_key' , $job_key);
                }

                // Use user's location if no location searched and not in
                // session
                if(empty($location) && !Util::get_session('location')) {
                    if($user->city) {
                        $location = $user->city;
                    } else {
                        $location = $user->postal_code;
                    }
                }

                if(isset($_SESSION['username'])) {
                    $this->setState('username', $_SESSION['username']);
                }

            }

            // Set the job result page version to use
            $this->_set_jrp_version($pg);

            // Set keyword session value if q was passed in url GET or friendly format
            if($keyword !== null) {
                $_SESSION['keyword'] = ucfirst($keyword);
            }

            // Set location session value if l was passed in url GET or friendly format
            if($location !== null) {
                $_SESSION['location'] = ucfirst($location);
            }

            // Set radius session value if r was passed in url GET or friendly format
            if($radius !== null) {
                $_SESSION['radius'] = $radius;
            }

            // Set days back session value if d was passed in url GET or friendly format
            if($days_back !== null) {
                $_SESSION['days_back'] = $days_back;
            }

            if(Util::is_session('uid')) {

                if(!empty($_SESSION['location']) && empty($user->city) && empty($user->postal_code)) {

                    $geocode = Util::load_model('geocodelocation');
                    $loc = $geocode->verify_location($_SESSION['location']);

                    // Only update if found valid uk location
                    if($loc !== FALSE) {

                        // Update user location
                        $user_model->update_user_loc($user->id, $loc['city'], $loc['postal']);

                        // Remove no loc rejection status from coreg
                        $user_model->update_noloc_coreg($user, $loc['city'], $loc['postal']);
                    }

                }

            }

            $browser = new Browser;

            // Set objects to state for use elsewhere

            //$this->setState('uid', $uid);
            $this->setState('sub_id', $sub_id);
            // keyword, location, radius, days back set to state so can detect if they were
            // even passed in _display(). Everywere else these values
            // are retrieved from session.
            $this->setState('keyword', $keyword);
            $this->setState('location', $location);
            $this->setState('days_back', $days_back);
            $this->setState('radius', $radius);
            $this->setState('pg', $pg);
            $this->setState('result_total', 0);
            $this->setState('page_total', 0);

            // Set defaults to state
            $this->setState('days_back_def', $days_back_def);
            $this->setState('radius_def', $radius_def);

            // This is set in _set_jrp_version();
            //$this->setState('jobs_per_page', $jobs_per_page);

            $this->setState('paging_per_page', $paging_per_page);
            $this->setState('browser', $browser);

        }

        // ************ Task Methods

        // Default view on first page load
        protected function _display() {

            $d = $this->getState('days_back') === null
                ? 'is null'
                : 'val: ' . $this->getState('days_back');

            $r = $this->getState('radius') === null
                ? 'is null'
                : 'val: ' . $this->getState('radius');

            // Checks if q, l or pg parameters were even passed through GET or friendly link
            // If nothing was passed then try to find location with user data or IP.
            // If paging pg is passed then means there is session data and do not need to go through this again.
            if($this->getState('keyword') === null && $this->getState('location') === null &&
                    $this->getState('pg') === null && $this->getState('radius') === null &&
                    $this->getState('days_back') === null) {

                // Set session variables to default
                $_SESSION['days_back'] = $this->getState('days_back_def');
                $_SESSION['radius'] = $this->getState('radius_def');

                $user = $this->getState('user');

                // Only set location if one of these conditions.
                // 1) First time visit to page. Meaning no location for session is set.
                if(!Util::get_session('location')) {

                    // Get user location data if arrived from email link
                    // If passed uid then there will be a $user object
                    if(!empty($user)) {

                        // Location search is either city or postal code.
                        // City returns more search results
                        $location = $user->city
                            ? $user->city
                            : $user->postal_code;

                        $_SESSION['location'] = $location;

                    } else {

                        // There is no uid so find user location by IP
                        $geocode = Util::load_model('geocodelocation');
                        $loc = $geocode->get_loc_by_ip();

                        // Location search is either city or postal code.
                        $location = $loc['city']
                            ? $loc['city']
                            : $loc['postal'];

                        $_SESSION['location'] = $location;
                        //Util::debug($loc);
                    }

                }

            }

            // Get indeed data and save result data
            $this->_get_data_save_disp_loc();

            // Update clickers90
            if(Util::is_session('uid')) {

                $clickers90_model = Util::load_model('clickers90');

                $clickers90_model->modify_clickers90(Util::get_session('uid'),
                        Util::get_session('keyword'), Util::get_session('location'));

            }

            // Get featured job of passed job key jk=99999
            /*
            if($this->getState('job_key')) {

                $list = $this->_merge_featured_job();

                // Set the revised job list to state
                $this->addOut('jobs', $list);
            }
*/
            $result_version_model = $this->getState('result_version_model');

            $this->setState('components', $result_version_model->verify_versionator_components($this->getState('jrp_version')));

            $this->_common_out();
        }

        // View after Submit
        protected function _search() {

            // Get indeed data and save result data
            $this->_get_data_save_disp_loc();

            // Update clickers90
            if(Util::is_session('uid')) {

                $clickers90_model = Util::load_model('clickers90');

                $clickers90_model->modify_clickers90(Util::get_session('uid'),
                        Util::get_session('keyword'), Util::get_session('location'));

            }

            $result_version_model = $this->getState('result_version_model');

            $this->setState('components', $result_version_model->verify_versionator_components($this->getState('jrp_version')));

            $this->_common_out();
        }

        // ************ Helper Methods

        /**
         * Get result version to use. Use email or organic version.
         *
         */
        private function _set_jrp_version($pg) {

            $browser = new Browser;

            $result_version_model = $this->getState('result_version_model');

            // Get the jrp version id

            // v - passed in url is version override. Display whatever version specified
            // full - url value passed from mobile version 'full site' link to set version to organic for full
            // size viewing experience on mobile.
            $url_version_id = Util::getVar('v', '', 'GET', 'int');
			//$is_full_site = Util::getVar('full', '', 'GET');

            //if (!$jrp_version_id) {

			//if($url_version_id) {
				// version override
				$jrp_version = $result_version_model->get_jrp_version($pg, $url_version_id);
			//} elseif($sess_ver_id = Util::get_session('fullsite_version_id')) {
				// If from mobile and click on full site link. User will experience full site experience
				// for entire visit to site.
			//	$jrp_version = $result_version_model->get_jrp_version($pg, $sess_ver_id);
			//} elseif($is_full_site) {
				//Use the weighted organic traffic type on mobile that request full size site
			//	$jrp_version = $result_version_model->get_jrp_version_by_traffic('organic');
			//	$_SESSION['fullsite_version_id'] = $version->id;
			//} else {

			//}

            // Set pub_id type to use in indeed request.
            // Types: organic, sponsored
            //-Andy 20130731, Criteria for which publisher key to use changed.
            // Organic users use organic key. Non-organic users use sponsored
            // key
            //if($jrp_version->traffic_type == 'organic') {
            //    $pub_id_type = "organic";
            //} else{
                $pub_id_type = "sponsored";
            //}

            if($jrp_version->organic_per_page > 0) {
                $jobs_per_page = $jrp_version->organic_per_page;
            } else {
                $jobs_per_page = $jrp_version->sponsored_per_page;
            }

            $this->setState('indeed_pub_id_type', $pub_id_type);

            // Set traffic type to use in indeed channel request.
            // Types: organic, email, mobile
            $this->setState('indeed_traffic_type', $jrp_version->traffic_type);

            // Set jobs per page value
            $this->setState('jobs_per_page', $jobs_per_page);

            $this->setState('jrp_version', $jrp_version);

        }

        /**
         * Get indeed data, save search results and set
         * display location text
         *
         */
        private function _get_data_save_disp_loc() {

            $geocode_model = Util::load_model('geocodelocation');

            // Get jobs from indeed
            // Only make api request if keyword or location set in session

            $view = $this->getState('view');

            // NOTE: Do not make indeed requests or record metrics if page
            // loaded by google bot.
            $userAgent = $_SERVER['HTTP_USER_AGENT'];
            if(strpos($userAgent, 'Mediapartners-Google') === FALSE &&
                $view!='recommended_jobs') {

                if(Util::is_session('keyword') || Util::is_session('location')) {
                    $this->_build_versionator_result();
                }

                if($this->getState('job_key')) {

                    //$list = $this->_merge_featured_job();
                    $this->_merge_featured_job();
                    // Set the revised job list and page count to state
                    //$this->addOut('jobs', $list);

                }

                //Util::printr($list);

                // Record with result if actual results on page.
                if($this->getState('page_total') > 0) {
                    $this->_save_with_result();
                } else {
                    $this->_save_with_no_result();
                }

                // Update versionator_stats and search_unique_ip tables
                //$this->_save_versionator_stats();

                //$this->_save_partner_source_stats();
                $this->setState('jobs', $this->_main_job_list);
            } else {
                // Set empty array as job list
                $this->setState('jobs', array());
            }

            // Set display location as city if user entered a valid postcode. Otherwise display
            // whatever location user entered.

            if($loc = Util::get_session('location')) {
                $loc_exp = explode(' ', $loc);
                if(Util::valid_postal($loc_exp[0])) {
                    if($city = $geocode_model->get_city_by_postal($loc_exp[0])) {
                        $loc = $city;
                    }
                }

                $loc = ' near '.ucwords($loc);

            } else {

                $loc = ' in the United Kingdom';

            }

            $this->setState('display_location', $loc);

        }

        /**
         * Main Job result generation method
         *
         */
        public function _build_versionator_result() {

            $result_version_model = $this->getState('result_version_model');

            $version = $this->getState('jrp_version');

            //$this->_get_indeed_data();
            $this->_get_job_data();

            //$jobs = $this->getState('jobs');

            // Convert pattern string into an array
            // layout_pattern: type-provider-ad_id
            // type = O - organic, S - Sponsored, A - Ad
            // provider = I - indeed, G - Google adsense
            // e.g. organic job (type => 'O', provider => 'I')
            // e.g. ad (type => 'A', provider => 'G', ad_id => '12345678')

            $pattern = explode(',', $version->result_pattern);
			$pattern_job_total = 0;

            foreach($pattern as $item) {

                $pat_item = explode('-', $item);

                $type = isset($pat_item[0])
                    ? trim($pat_item[0])
                    : '';

                $provider = isset($pat_item[1])
                    ? trim($pat_item[1])
                    : '';

                $ad_id = isset($pat_item[2])
                    ? trim($pat_item[2])
                    : '';

                $layout_pattern[] = array('type' => $type,
                        'provider' => $provider, 'ad_id' => $ad_id);

            }

            $result = array();

            //$jobs->job_list = array_slice($jobs->job_list, 0, -4);

            //Util::debug($jobs->job_list);

            //$job_total = count($jobs->job_list);
            $job_total = $this->getState('page_total');


            $max_ads = 999;

            // Set max number of ads if under certain limits
            // < 5 jobs - display no ads
            // < 10 jobs - display max one add
            // < 15 jobs - display max two adds
            if($job_total < 5) {
            	$max_ads = 0;
            } elseif($job_total < 10) {
            	$max_ads = 1;
            } elseif($job_total < 15) {
            	$max_ads = 2;
            }

            $job_cnt = 0;
			$ad_cnt = 1;

            foreach($layout_pattern as $patt_item) {

                if($patt_item['type'] == 'O' || $patt_item['type'] == 'S') {
                    if(!empty($this->_main_job_list[$job_cnt]) && $job_cnt < $job_total) {
                        //$result[] = $jobs->job_list[$job_cnt];
                        $result[] = $this->_main_job_list[$job_cnt];
                        $job_cnt++;
                    }
                } elseif($patt_item['type'] == 'A') {

                    // NOTE: Display all ads in pattern regardless of how many jobs on page.
					// UPDATE: Only display ads if under max allowed
                	if($ad_cnt <= $max_ads) {

	                    if(empty($patt_item['ad_id'])) {
	                        // Use test ad id if none specified.
	                        $patt_item['ad_id'] = '2061405453';
	                    }

	                    $result[] = array('id' => 'ad', 'ad_id' => $patt_item['ad_id']);
						$ad_cnt++;

                	}
                    // Break out of loop when displayed all jobs in list. Only need break here.
                    //if($job_cnt >= $job_total) {
                    //    break;
                    //}

                }

            }

            $this->_main_job_list = $result;

            //Util::debug($jobs->job_list);
            //Util::printr($jobs->job_list);
            //$this->setState('jobs', $jobs);

        }

        private function _get_indeed_data() {

            // Number of jobs listed on page
            $per_page = $this->getState('jobs_per_page');
            // Number of paging links on page
            $page_list = $this->getState('paging_per_page');

            // Indeed Job by versionator partner api key name
            //$jrp_version = $this->getState('jrp_version');

            $pub_id_type = $this->getState('indeed_pub_id_type');
            $traffic_type = $this->getState('indeed_traffic_type');

            $indeed = new Indeed($pub_id_type, $traffic_type);

            $indeed->keyword = Util::get_session('keyword');
            $indeed->location = Util::get_session('location');

            $indeed->daysback = Util::get_session('days_back', $this->getState('days_back_def'));
            $indeed->radius = Util::get_session('radius', $this->getState('radius_def'));

            $indeed->limit = $per_page; // 10 jobs display per page

	    $this->setState('indeed_channel', $indeed->channel);

            // Page Controls

            // 1. the current page number ($current_page)
            $page = $this->getState('pg')
                ? $this->getState('pg')
                : 1;

            // 2. set job page and start
            $start = $page==1
                ? 0
                : $page * $per_page - $per_page;

            $indeed_offset = Util::get_session('indeed_offset', 0);

            if($page==1) {
                $indeed->start = 0;
                //$indeed->start = $indeed_offset;
                //$indeed->limit = $per_page - $indeed_offset;
            } else {
                $indeed->start = $start-$indeed_offset;
            }

            //echo "indeed offset: {$indeed_offset} start: {$indeed->start} limit: {$indeed->limit}<br>";

            // 3. get the job list
            $indeed->job_detail();

            return $indeed;

            // 4. total record count ($toal_count)
            //$total_count = $indeed->total;
            /*
            // Activate pagination if there are records
            if ($total_count > 0) {
            	$version = $this->getState('jrp_version');
                $pagination = new Pagination($page, $per_page, $total_count, $page_list, $version->traffic_type);
                $this->setState("pagination", $pagination);
            }

            // Set total and job result object to state
            $this->setState('result_total', $total_count);
            $this->setState('page_total', (int)count($indeed->job_list));
            $this->setState("jobs", $indeed);
            */

            //Util::printr($indeed);
            //Util::debug($indeed);


        }

        private function _get_adzuna_data() {
            // Number of jobs listed on page
            $per_page = $this->getState('jobs_per_page');
            // Number of paging links on page
            $page_list = $this->getState('paging_per_page');

            // Indeed Job by versionator partner api key name
            //$jrp_version = $this->getState('jrp_version');

            $adzuna = new Adzuna();

            $adzuna->keyword = Util::get_session('keyword');
            $adzuna->location = Util::get_session('location');

            $adzuna->daysback = Util::get_session('days_back', $this->getState('days_back_def'));
            $adzuna->radius = Util::get_session('radius', $this->getState('radius_def'));

            // Number of adzuna jobs to retrieve. Cannot be > the per page limit.
            $adzuna->limit = 15;

            // 3. get the job list
            $adzuna->job_detail();

            return $adzuna;

        }

        private function _get_job_data() {

            // Number of jobs listed on page
            $per_page = $this->getState('jobs_per_page');
            // Number of paging links on page
            $page_list = $this->getState('paging_per_page');

            $traffic_type = $this->getState('indeed_traffic_type');

            $this->setState('search_engine', 'indeed');

            $adzuna_count = 0;
            $indeed_count = 0;
            $fill_count = 0;

            $page = $this->getState('pg')
                ? $this->getState('pg')
                : 1;

            if($page==1) {
                $kw = Util::get_session('keyword');
                $fromEmFlag = Util::get_session('fromemail');
                if(!empty($kw) && ($traffic_type=='email' || ($traffic_type=='mobile' && !empty($fromEmFlag)))) {
                    $adzuna = $this->_get_adzuna_data();
                    $adzuna_count = count($adzuna->job_list);
                    if($adzuna_count>0) {
                        //$_SESSION['indeed_offset'] = $adzuna_count;
                        $this->setState('search_engine', 'adzuna');
                    } else {
                        //unset($_SESSION['indeed_offset']);
                    }
                } else {
                    // make sure offset not set
                    //unset($_SESSION['indeed_offset']);
                }

            }

            $indeed = $this->_get_indeed_data();

            if($page==1) {
                if(!empty($kw) && ($traffic_type=='email' || ($traffic_type=='mobile' && !empty($fromEmFlag)))) {
                    if($adzuna_count > 0 && $adzuna_count<$per_page) {
                        //Add backfill
                        $fill_request = $per_page - $adzuna_count;
                        $this->_main_job_list = array_slice($indeed->job_list, 0, $fill_request);
                        $fill_count = count($this->_main_job_list);
                        //offset substracted from > page 1 indeed start parameter
                        //e.g. page 1 - 3 indeed jobs, page 2 - indeed start is 15-3=12
                        $_SESSION['indeed_offset'] = $per_page-$fill_count;
                    } elseif($adzuna_count==$per_page) {
                        //Backfill not needed
                        $this->_main_job_list = array();
                        $_SESSION['indeed_offset'] = $per_page;
                    } else {
                        //if zero adzuna jobs use whole indeed job list
                        $this->_main_job_list = $indeed->job_list;
                        unset($_SESSION['indeed_offset']);
                    }
                } else {
                    // non-adzuna first page
                    $this->_main_job_list = $indeed->job_list;
                    unset($_SESSION['indeed_offset']);
                }
            } else {
                $this->_main_job_list = $indeed->job_list;
            }

            $indeed_count = $indeed->total;

            $total_count = $adzuna_count+$indeed_count;

            //echo "traffic type: {$traffic_type}<br>" ;
            //echo "adzuna: {$adzuna_count} indeed: {$indeed_count}<br>";
            //echo "total: " . $total_count . "<br>";
            //echo "indeed start: {$indeed->start} indeed limit: {$indeed->limit} indeed total: {$indeed->total}<br>";
            //echo "indeed offset: {$_SESSION['indeed_offset']}";

            if($page==1) {
                $_SESSION['total_jobs'] = $total_count;
                $per_page = $this->getState('jobs_per_page');

            } else {
                if(Util::get_session('total_jobs')!='') {
                    $total_count = Util::get_session('total_jobs');
                }
            }

            //print_r($adzuna->job_list);
            //print_r($this->_main_job_list);

            if($adzuna_count>0) {
                $this->_main_job_list = array_merge($adzuna->job_list, $this->_main_job_list);
            }



            // Activate pagination if there are records
            if ($total_count > 0) {
                $version = $this->getState('jrp_version');
                $pagination = new Pagination($page, $per_page, $total_count, $page_list, $version->traffic_type);
                $this->setState("pagination", $pagination);
            }

            // Set total and job result object to state
            $this->setState('result_total', $total_count);
            $this->setState('page_total', (int)count($this->_main_job_list));
        }

        private function _get_indeed_featured() {
            $pub_id_type = $this->getState('indeed_pub_id_type');
            $traffic_type = $this->getState('indeed_traffic_type');
            $indeed = new Indeed($pub_id_type, $traffic_type);
            $job = $indeed->get_job_by_key($this->getState('job_key'));

            return $job;
        }

        private function _get_popular_search_counts() {

        	$links = array();

			$kw_list = array(
				'Customer Service',
				'Healthcare',
				'Driver',
				'Data Entry',
				'Warehouse',
				'Sales Associate'
				);

			foreach($kw_list as $kw) {

				if($pop_link = $this->_build_popular_link_item($kw)) {
					$links[] = $pop_link;
				}

            }

            return $links;

        }

        private function _build_popular_link_item($kw) {

            //$days_back = 3;

            $count = number_format($this->_get_job_count($kw));

            if($count > 0) {
                $keyword = str_replace(' ', '+', $kw);
                //$location = str_replace(' ' , '+', Util::get_session('location'));
                //$url = '/jobs/q-' . $keyword . '/l-' . $location . '/d-' . (string)$days_back . '/r-' . Util::get_session('radius', $this->getState('radius_def') . '/');
                $url = '/jobs/q-' . $keyword . '/';
                $ret = array('keyword' => $kw, 'url' => $url, 'count' => $count);
            } else {
                $ret = false;
            }

            return $ret;

        }

        /**
         * Get job count. Check if in cache otherwise make indeed api
         * request
         *
         * @param string $keyword
         * @return int
         */
        private function _get_job_count($keyword) {

            $days_back = 7;

            if(extension_loaded('memcache')){
                $memcached_model = Util::load_model('memcached');
                $location = Util::get_session('location');
                $radius = Util::get_session('radius', $this->getState('radius_def'));

                $cache_key = $memcached_model->make_cache_key('indeed', $keyword, '', $days_back, '');
                // Connect to memcached server
                $cache_obj = memcache_connect("localhost", 11211);

                // Check cache

                if(!$count = memcache_get($cache_obj, $cache_key)) {
                    // Not in cache make api request
                    $count = $this->_get_indeed_count($keyword, $days_back);
                    // Save to cache. Expire in one hour
                    memcache_add($cache_obj, $cache_key, $count, false, 86400);
                    //Util::debug($keyword . ' not in cache');
                } else {
                    //Util::debug($keyword . ' from cache');
                }
            } else {
                // If no memcached installed just make api call.
                $count = $this->_get_indeed_count($keyword, $days_back);
            }

            return $count;

        }

        private function _get_indeed_count($keyword, $days_back) {

            $indeed = new Indeed('organic', '', 'xml');

            $indeed->keyword = $keyword;
            $indeed->location = '';

            // Only get new jobs
            $indeed->daysback = $days_back;
            $indeed->radius = 0;

            $indeed->limit = $this->getState('jobs_per_page');

            $indeed->start = 0;

            $indeed->job_count();

            //Util::debug($indeed);

            return $indeed->total;
        }

        private function _merge_featured_job() {

            // Get indeed job by job key
            $featured_job = $this->_get_indeed_featured();
            //Util::debug($featured_job);

            //$list = $this->getState('jobs');
            //$jobs = $list->job_list;

            // Get job key set in state
            $job_key = $this->getState('job_key');

            // if job is found in job result flag
            $job_found = false;

            //Util::debug($jobs);
            //for($i = 0, $len = count($jobs); $i < $len; $i++) {

            for($i = 0, $len = count($this->_main_job_list); $i < $len; $i++) {
                if($this->_main_job_list[$i]['id'] == $job_key) {
                    $job_found = $i;
                    break;
                }
            }

            // If the featured job already exists in main result then remove it
            if($job_found !== false) {
                unset($this->_main_job_list[$job_found]);
                $this->_main_job_list = array_values($this->_main_job_list);
            }

            if(!empty($featured_job)) {
                // Set featured flag so can display as featured in view
                $featured_job['featured'] = true;
                // Need to stick in indexed array before merge
                $feat_arr[] = $featured_job;

                // Merge to top of array stack
                $this->_main_job_list = array_merge($feat_arr, $this->_main_job_list);

                // Add one to total results on page if was not removed from
                // existing result
                if($job_found === false) {
                    $this->setState('page_total', $this->getState('page_total') + 1);
                }
            }

            //Util::debug($jobs);

            // Add the revised job array back to job_list object
            //$list->job_list = $jobs;

            // This is remote possibility that there is no main job results
            // Will ensure at least the featured job will display
            if($this->getState('result_total') < 1) {
                $this->setState('result_total', 1);
                //$list->total = 1;
            }

            //return $list;
        }

        private function _save_with_result() {
            $jobs_model = Util::load_model('jobs');
            $jobs_model->setState('user', $this->getState('user'));
            $jobs_model->setState('sub_id', $this->getState('sub_id'));
            $jobs_model->setState('keyword', Util::get_session("keyword"));
            $jobs_model->setState('location', Util::get_session("location"));
            $jobs_model->setState('radius', Util::get_session('radius', $this->getState('radius_def')));
            $jobs_model->setState('days_back', Util::get_session('days_back', $this->getState('days_back_def')));
            $jobs_model->setState('page', $this->getState('pg'));
            $jobs_model->setState('page_total', $this->getState('page_total'));
            $jobs_model->setState('result_total', $this->getState('result_total'));
            $jobs_model->setState('channel', $this->getState('jobs')->channel);
            $jobs_model->setState('version_id', $this->getState('jrp_version')->id);
            $jobs_model->setState('search_engine', $this->getState('search_engine'));
            $jobs_model->save_result_search();
        }

        private function _save_with_no_result() {
            $jobs_model = Util::load_model('jobs');
            $jobs_model->setState('user', $this->getState('user'));
            $jobs_model->setState('sub_id', $this->getState('sub_id'));
            $jobs_model->setState('keyword', Util::get_session("keyword"));
            $jobs_model->setState('location', Util::get_session("location"));
            $jobs_model->setState('radius', Util::get_session('radius', $this->getState('radius_def')));
            $jobs_model->setState('days_back', Util::get_session('days_back', $this->getState('days_back_def')));
            $jobs_model->setState('page', $this->getState('pg'));
            $jobs_model->setState('version_id', $this->getState('jrp_version')->id);
            $jobs_model->save_no_result_search();
        }

        private function _save_versionator_stats() {
            $model = Util::load_model('versionatorstats');
            $model->setState('version_id', $this->getState('jrp_version')->id);
            $model->setState('page', $this->getState('pg'));
            $model->setState('job_results', $this->getState('page_total'));
            $model->setState('request_ip', $_SERVER['REMOTE_ADDR']);
            $model->save_versionator_search_stats();
            $model->save_search_unique_ip();
        }

        private function _save_partner_source_stats() {
            $model = Util::load_model('partnersourcestats');
            $model->setState('user', $this->getState('user'));
            $model->save_partner_source_search_stats();
            $model->save_search_unique_email();
        }

        // Common output shared by different view states
        private function _common_out() {

            $this->addOut('jrp_version', $this->getState("jrp_version"));
            $this->addOut('keyword', Util::get_session("keyword"));
            $this->addOut('location', Util::get_session("location"));
            $this->addOut('display_location', $this->getState('display_location'));
            $this->addOut('radius', Util::get_session('radius', $this->getState('radius_def')));
            $this->addOut('days_back', Util::get_session('days_back', $this->getState('days_back_def')));
            $this->addOut('days_back_array', $this->_get_days_back_list());
            $this->addOut('radius_array', $this->_get_radius_list());
            $this->addOut('page', $this->getState('pg'));
            $this->addOut('page_total', $this->getState('page_total'));
            $this->addOut('result_total', $this->getState('result_total'));
            $this->addOut('popular_items', $this->_get_popular_search_counts());

            //if($this->getState('result_total') > 0) {
            if($this->getState('page_total') > 0) {
                $this->addOut('jobs', $this->getState("jobs"));
                $this->addOut('pagination', $this->getState("pagination"));
            } else {
                // If no jobs return list object with total=0
                //$list = new stdClass();
                //$list->total = 0;
                $this->addOut('jobs', array());
            }

            $this->addOut('components', $this->getState('components'));
            $this->addOut('version_id', $this->getState('jrp_version')->id);
            $this->addOut('view', $this->getState('view'));
            $this->addOut('username', $this->getState('username'));
            $this->addOut('browser', $this->getState('browser'));

	    $this->addMetricsData((array) $this->getState('user'));
	    $this->addMetricsData(array(
		'is_result_page' => true,
		'channel' => $this->getState('indeed_channel'),
		'request_ip' => $_SERVER['REMOTE_ADDR']
	    ));
        }

        private function _get_days_back_list() {

            return array('1'=>"1 day",
                         '3'=>"3 days",
                         '7'=>"7 days", // default
                         '14'=>"14 days",
                         '30'=>"30 days");
        }

        private function _get_radius_list() {

            return array('5'=>"5 miles",
                         '10'=>"10 miles",
                         '25'=>"25 miles", // default
                         '50'=>"50 miles",
                         '100'=>"100 miles");
        }

    }
