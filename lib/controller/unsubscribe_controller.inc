<?php
	// vim: ai ts=4 sts=4 et sw=4 tw=79

	/**
	 * Created on Nov 21, 2012
	 * Written By Andy Nagai
	 *
	 */

	class UnsubscribeController extends Controller {

		function __construct() {

			$this->_initController();

		}

		protected function _initState() {

		}

		protected function _display() {

		}

		protected function _unsub() {

			$user_model = Util::load_model('user');

			$email = Util::getVar('email', '', 'POST');

			$_SESSION['email_temp'] = $email;

			if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
				$user = $user_model->find_by_email($email);

				if(!empty($user)) {

					// Add email to unsubscribe on bacon
					$now_dt = date('Y-m-d H:i:s');
					$new_unsub = new Unsubscribe();
					$new_unsub->site_id = SITE_ID;
					$new_unsub->email_address = $email;
					$email_exp = explode('@', $email);
					$new_unsub->domain = $email_exp[1];
					$new_unsub->partner = $user->partner;
					$new_unsub->source = $user->source;
					$new_unsub->details = 'Added via site';
					$new_unsub->date_received = $now_dt;
					$new_unsub->date_added = $now_dt;
					$new_unsub->create();

					// Set user is_mailable to zero.
					$user->is_mailable = 0;
					$user->update();
				}

			}

			Util::redirect_to('unsubscribe_confirm');

		}

	}