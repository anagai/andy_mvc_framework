<?php
    // vim: ai ts=4 sts=4 et sw=4 tw=79

    /**
     * Created on Nov 21, 2012
     * Written By Andy Nagai
     *
     */

    class ContactController extends Controller {

        function __construct() {

            $this->_initController();

        }

        protected function _initState() {

        }

        protected function _display() {

        }

        protected function _send() {

			Util::load_phpmailer();
            $receiver = "mail@jobungo.co.uk";
            $sender_name = $_POST['name'];
            $sender_email = $_POST['email'];
            $contactmessage = $_POST['message'];
            $subject = "Jobungo UK Contact Form Request from $sender_email";
            $body = "From: $sender_name\n\nE-Mail: $sender_email\n\nMessage:\n$contactmessage";

            $mail = new PHPMailer();

            $mail->IsSMTP();
            //$mail->SMTPDebug = true; // for error checking
            $mail->Host = "173.204.123.41";
            $mail->Port = 25;

            $mail->FromName = $sender_name;
            $mail->From = $sender_email;
            $mail->AddAddress($receiver, "Contact Us");
            $mail->AddReplyTo($sender_email, $sender_name);
            $mail->Sender = $sender_email; // return path

            $mail->Subject = $subject;
            $mail->IsHTML(true);

            // HTML Version
            $mail->Body = $body;

            // Text Version
            $mail->AltBody = $body;

            $result = $mail->Send();

            Util::redirect_to('thankyou');
        }
    }
