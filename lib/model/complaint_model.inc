<?php
    // vim: ai ts=4 sts=4 et sw=4 tw=79

    /**
     * Created on Nov 7, 2012
     * Written By Andy Nagai
     *
     */

    class ComplaintModel extends Model {

        protected $_pdo_bacon_dbh;

        function __construct() {

            $this->_table_name = 'complaint';
            $this->_table_class_name = str_replace('_', '', $this->_table_name);
            $this->_db = Util::get_bacon_db();

        }

        public function is_complaint_email($email) {

            // Email is a complaint if not complained from asoftv site
            $exists = $this->result_exists("SELECT id FROM " . $this->_table_name .
                " WHERE email_address = '" . $this->_db->escape_value($email) .
                "' AND site_id <> 3 LIMIT 1");

            // returns true if email is valid complaint
            return $exists;
        }

        public function set_pdo_dbh() {
            if(PROD_SERVER) {
                $this->_pdo_bacon_dbh = Util::get_pdo_db_conn('bacon_rw_prod');
            } else {
                $this->_pdo_bacon_dbh = Util::get_pdo_db_conn('bacon_rw_dev');
            }
        }

        /**
         * Get records with no domain or partner added in past half hour
         *
         */
        public function get_empty_domain_partner_records() {

            static $sth;

            $sth = $this->_pdo_bacon_dbh->prepare('SELECT id, email_address, domain, partner FROM ' . $this->_table_name . ' WHERE
            	date_added > now() - interval 30 minute AND
             	site_id = :site_id AND (domain=\'\' OR partner=\'\')');

            $this->_db->pdo_execute($sth, array(':site_id' => SITE_ID));

            return $sth;

        }

        public function update_domain_partner($id, $domain, $partner, $source) {

            static $sth;

            $update_sql = '';

            if($domain) {
                $update_sql = ', domain = :domain';
                $update_params[':domain'] = $domain;
            }

            if($partner) {
                $update_sql .= ', partner = :partner, source = :source';
                $update_params[':partner'] = $partner;
                $update_params[':source'] = $source;
            }

            $update_sql = substr($update_sql,2);

            $sql = 'UPDATE ' . $this->_table_name . ' SET ' . $update_sql . ' WHERE id = ' . $id;

            $sth = $this->_pdo_bacon_dbh->prepare($sql);

            $this->_db->pdo_execute($sth, $update_params);

        }

        public function count_all_complaint() {

            $sql = "SELECT COUNT(*) FROM " . $this->_table_name .
            " WHERE site_id = " . SITE_ID;
            $result_set = $this->_db->query($sql);
            $row = $this->_db->fetch_array($result_set);
            return array_shift($row);
        }

        public function count_by_campaign($campaign_name) {

            $sql = "SELECT COUNT(*) FROM " . $this->_table_name .
            " WHERE site_id = " . SITE_ID .
            " AND campaign_name = '" . $this->_db->escape_value($campaign_name) . "'";
            $result_set = $this->_db->query($sql);
            $row = $this->_db->fetch_array($result_set);
            return array_shift($row);
        }

        public function get_total_complaints($campaign_name, $search_text, $search_field, $start_date, $end_date) {

            $sql = "SELECT COUNT(*) FROM " . $this->_table_name;
            $sql .= " WHERE site_id = " . SITE_ID;
            $sql .= " AND campaign_name = '" . $this->_db->escape_value($campaign_name) . "'";
            $sql .= $this->write_list_sql($search_text, $search_field, $start_date, $end_date);

            $result_set = $this->_db->query($sql);
            $row = $this->_db->fetch_array($result_set);
            return array_shift($row);
        }

        public function get_complaints($campaign_name, $search_text, $search_field, $start_date, $end_date, $per_page, $offset) {

            $sql = "SELECT * FROM " . $this->_table_name;
            $sql .= " WHERE site_id = " . SITE_ID;
            $sql .= " AND campaign_name = '" . $this->_db->escape_value($campaign_name) . "'";
            $sql .= $this->write_list_sql($search_text, $search_field, $start_date, $end_date);

            return $this->find_by_sql($sql . " ORDER BY id DESC LIMIT {$per_page} OFFSET {$offset}");
        }

        private function write_list_sql($search_text, $search_field, $start_date, $end_date) {

            $sql = "";

            if ($start_date != "") {
                $sql .= " AND date_added >= '{$start_date}'";
            }

            if ($end_date != "") {
                $end_date = strftime ("%Y-%m-%d %H:%M:%S", strtotime('+1 day', strtotime($end_date)));
                $sql .= " AND date_added < '{$end_date}'";
            }

            if ($search_text != "" && $search_field == "email") {
                $sql .= " AND email_address LIKE '{$search_text}%'";
            }

            if ($search_text != "" && $search_field == "partner") {
                $sql .= " AND partner LIKE '{$search_text}'";
            }

            return $sql;
        }

        public function get_empty_date_received() {

            $result_array = $this->find_by_sql(
                    "SELECT * FROM " . $this->_table_name .
                    " WHERE date_received = '0000-00-00 00:00:00'");
            return !empty($result_array) ? $result_array : false;
        }

        public function get_last_added_records($last_date_added) {

        	static $sth;

        	$dbh = Util::get_pdo_bacon_conn();

        	if(!isset($sth)) {
        		$sth = $dbh->prepare( 'SELECT email_address, date_added' .
                ' FROM ' . $this->_table_name .
                ' WHERE date_added >= :date_added' .
                ' GROUP BY email_address' .
                ' ORDER BY date_added DESC');
        	}

        	$sth->bindParam(':date_added', $last_date_added, PDO::PARAM_STR);

        	$sth->execute();

        	return $sth;

        }

    }
