<?php
// vim: ai ts=4 sts=4 et sw=4 tw=79

/*
 * Created on Oct 8, 2012
 * Written By Sung Hwan Ahn / Andy Nagai
 *
 */

class SesModel extends Model {

    function __construct() {

        // All models should have this variable set for default db
        $this->_db = Util::get_dbo();

        // For remote db connections create a second db connection object
        // e.g. $_shasta_db = $_db->open_connection('shasta')
    }

    // Send Welcome Emails
    public function send_welcome_email($user, $et_id = '', &$nonPremPartners) {

        /*
        $email_version_model = Util::load_model('emailversion');


        if(!empty($et_id)) {
            $email_template = $email_version_model->find_by_id($et_id);
        } else {
            $email_template = $email_version_model->get_email_template("welcome_email");
        }
        */

        //$encoded_uid = base64_encode($user->email);
        //$salted_email = $user->email . ':' . strtotime($user->date_added);
        // 06/28/13 Andy: Changed to send strong encrypted user id. Passing
        // salt in this manner not secure at all.
        // $encoded_uid = Util::mcryptEncode($user->id);

        // 10/30/14
        // Removed passing of template id: "k6=" . urlencode($email_template->id) . "&" .

        $isNonPrem = false;

        foreach($nonPremPartners as $par) {
            $par = preg_quote($par);
            if(preg_match("/^{$par}($|:)/i", $user->partner)) {
                //echo "Found!\n";
                $isNonPrem = true;
                break;
            }
        }

        if($isNonPrem===true) {
            $campName = 'JUK_WELCOME_NP_' . date('Ym');
        } else {
            $campName = 'JUK_WELCOME_' . date('Ym');
        }

        $urlArr = array(
            "cn"=>$campName,
            "u"=>$user->id,
            "fn"=>$user->first_name,
            "e"=>$user->email,
            "c"=>$user->city,
            "z"=>$user->postal_code,
            "k8"=>$user->id,
            "pr"=>$user->partner,
            "sr"=>$user->source
        );

        if(strpos($_SERVER['REDIRECT_SCRIPT_URI'], 'stage.') === false &&
            strpos($_SERVER['REDIRECT_SCRIPT_URI'], 'dev.') === false) {
            $url = "http://hyperion.restdev.com/api/send/";
        } else {
            $url = "http://trans.dev.cedar.restdev.com/api/send/";
        }

        $url .= "?" . http_build_query($urlArr);

        /*
        $url =  "http://ses13.jobungo.co.uk/send_email.php?" .
                "cn=" . urlencode($campName) . "&" .
                "u=" . urlencode($user->email) . "&" .
                "fn=" . urlencode($user->first_name) . "&" .
                "ln=" . urlencode($user->last_name) . "&" .
                "e=" . urlencode($user->email) . "&" .
                "c=" . urlencode($user->city) . "&" .
                "z=" . urlencode($user->postal_code) . "&" .
                "k8=" . urlencode($user->id) . "&" .
                "i=" . urlencode($user->opt_in_ip) . "&" .
                "pr=" . urlencode($user->partner) . "&" .
                "sr=" . urlencode($user->source);
        */

        $taskStart = microtime(true);
        $output = Util::curl_request($url);
        $dur = microtime(true) - $taskStart;
        Util::log_to_file("welcome_email_curl.log", "loc welcome", "out: {$output} em: {$user->email} " .
            "pid: {$user->partner} src: {$user->source} camp: {$campName} dur: {$dur} url: {$url}");
        $outDec = json_decode($output, true);

        if(isset($outDec['success'])) {
            return true;
        } else {
            return $output;
        }

        //return $output->{"success"};
    }

    public function send_noloc_welcome_email($user, $et_id = '', &$nonPremPartners) {
        /*
        $email_version_model = Util::load_model('emailversion');

        if(!empty($et_id)) {
            $email_template = $email_version_model->find_by_id($et_id);
        } else {
            $email_template = $email_version_model->get_email_template("welcome_email_noloc");
        }
        */

        //$encoded_uid = base64_encode($user->email);
        //$salted_email = $user->email . ':' . strtotime($user->date_added);
        // 06/28/13 Andy: Changed to send strong encrypted user id. Passing
        // salt in this manner not secure at all.
        //$encoded_uid = Util::mcryptEncode($user->id);

        // 10/30/14
        // Removed passing of template id: "k6=" . urlencode($email_template->id) . "&" .

        // 4/21/15, Send with different campaign for non-premium partners
        $isNonPrem = false;

        foreach($nonPremPartners as $par) {
            $par = preg_quote($par);
            if(preg_match("/^{$par}($|:)/i", $user->partner)) {
                //echo "Found!\n";
                $isNonPrem = true;
                break;
            }
        }

        if($isNonPrem===true) {
            $campName = 'JUK_WELCOME_NP_NOLOC_' . date('Ym');
        } else {
            $campName = 'JUK_WELCOME_NOLOC_' . date('Ym');
        }


        $urlArr = array(
            "cn"=>$campName,
            "u"=>$user->id,
            "fn"=>$user->first_name,
            "e"=>$user->email,
            "k8"=>$user->id,
            "pr"=>$user->partner,
            "sr"=>$user->source
        );

        if(strpos($_SERVER['REDIRECT_SCRIPT_URI'], 'stage.') === false &&
            strpos($_SERVER['REDIRECT_SCRIPT_URI'], 'dev.') === false) {
            $url = "http://hyperion.restdev.com/api/send/";
        } else {
            $url = "http://trans.dev.cedar.restdev.com/api/send/";
        }

        $url .= "?" . http_build_query($urlArr);

        /*

        $url =  "http://ses13.jobungo.co.uk/send_email.php?" .
                "cn=" . urlencode($campName) . "&" .
                "u=" . urlencode($user->email) . "&" .
                "fn=" . urlencode($user->first_name) . "&" .
                "ln=" . urlencode($user->last_name) . "&" .
                "e=" . urlencode($user->email) . "&" .
                "c=" . urlencode($user->city) . "&" .
                "z=" . urlencode($user->postal_code) . "&" .
                "k8=" . urlencode($user->id) . "&" .
                "i=" . urlencode($user->opt_in_ip) . "&" .
                "pr=" . urlencode($user->partner) . "&" .
                "sr=" . urlencode($user->source);
        */

        $taskStart = microtime(true);
        $output = Util::curl_request($url);
        $dur = microtime(true) - $taskStart;
        Util::log_to_file("welcome_email_curl.log", "noloc welcome", "out: {$output} em: {$user->email} " .
            "pid: {$user->partner} src: {$user->source} camp: {$campName} dur: {$dur} url: {$url}");

        $outDec = json_decode($output, true);

        if(isset($outDec['success'])) {
            return true;
        } else {
            return $output;
        }

    }

    // Send Temporay Welcome Emails
    public function send_temp_welcome_email($user) {

        $email_version_model = Util::load_model('emailversion');
        $email_template = $email_version_model->get_email_template("welcome_email");

        $encoded_uid = Util::mcryptEncode($user->id);

        $click_url = "http://dev-jobungouk.restdev.com/redir/?" .
            "uid=" . $encoded_uid . "&" .
            "et=" . $email_template->id . "&" .
            "rd=home";

        $url =  "http://ses13.jobungo.co.uk/send_email.php?" .
                "cn=" . urlencode($email_template->campaign_name) . "&" .
                "u=" . urlencode($user->email) . "&" .
                "fn=" . urlencode($user->first_name) . "&" .
                "ln=" . urlencode($user->last_name) . "&" .
                "e=" . urlencode($user->email) . "&" .
                "c=" . urlencode($user->city) . "&" .
                "z=" . urlencode($user->postal_code) . "&" .
                "k1=" . urlencode($click_url) . "&" .
                "k6=" . urlencode($email_template->id) . "&" .
                "i=" . urlencode($user->opt_in_ip) . "&" .
                "pr=" . urlencode($user->partner) . "&" .
                "sr=" . urlencode($user->source);

        $output = Util::curl_request($url);
        //Util::log_to_file("welcome_email_curl.log", "noloc welcome", "{$output} - '{$user->email}'. Partner: {$user->partner}");
        $output = json_decode($output);

        return $output->{"success"};
    }

    public function count_digest_mailing($clickerDays=0) {

        $sql = "SELECT count(*) FROM clickers90 as c INNER JOIN user as u"
            . " ON c.user_id=u.id WHERE c.date_updated >"
            . " date(now() - interval {$clickerDays} day) AND"
            . " u.is_mailable = 1 AND c.keyword!=''";

        $result_set = $this->_db->query($sql);
        $row = $this->_db->fetch_array($result_set);
        return array_shift($row);
    }

    public function create_digest_csv_file($filename, $clickerDays=0) {

        // Convert any windows backslashes to forward slashes
        $filename = str_replace("\\", "/", $filename);

        // Delete this file if it exists.
        @unlink($filename);

        $email_version_model = Util::load_model('emailversion');

        // Get daily digest email template by weight
        $email_template = $email_version_model->get_email_template("daily_digest");

        $sql = "SELECT 'user_id', 'first_name', 'last_name', 'email', "
            . "'city', 'state', 'zip', 'priority', 'keyword_1', "
            . "'keyword_2', 'keyword_3', 'keyword_4', 'keyword_5', "
            . "'keyword_6', 'keyword_7', 'keyword_8', 'ip', 'partner', "
            . "'source' "
            . "UNION ALL "
            . "SELECT u.id, u.first_name, u.last_name, u.email, "
            . "u.city, '', u.postal_code, 0, c.keyword, '', '', "
            . "'', '', '{$email_template->id}', '', u.id, "
            . "u.opt_in_ip, u.partner, u.source "
            . "INTO OUTFILE '" . $filename . "' "
            . "FIELDS ENCLOSED BY '\"' "
            . "TERMINATED BY ',' "
            . "LINES TERMINATED BY '\n' "
            . "FROM clickers90 as c INNER JOIN user as u ON "
            . "c.user_id=u.id WHERE c.date_updated > "
            . "date(now() - interval {$clickerDays} day) AND "
            . "u.is_mailable = 1 AND c.keyword!=''";


        $this->_db->query($sql);
    }

    public function update_digest_user_last_emailed($start_time, $clickerDays) {

        global $database;

        $sql = "UPDATE user as u INNER JOIN clickers90 as c ON "
            . "u.id=c.user_id SET u.date_last_mailed "
            . "= '" . $start_time . "' WHERE c.date_updated > "
            . "date(now() - interval {$clickerDays} day) AND "
            . "u.is_mailable = 1 AND c.keyword!=''";

        $database->query($sql);
    }

    public function create_digest_v25_csv_file($conn, $sendTime='', $outFile='', $clickerDays,
        $addRpSeeds=FALSE, $useRpSched=FALSE, $rpSched=array()) {

        $fh=fopen($outFile, 'w'); //* php://output , /tmp/el3_test.csv

        unset($firstRow);
        unset($keywords);
        unset($stmt);
        unset($validKeywords);
        unset($stats);
        unset($rpSeeds);
        unset($userCount);
        unset($intervalCount);
        unset($seedCount);
        unset($userStmt);
        unset($updateStmt);
        $intervalCount=0;
        $rpSeeds = array();
        $stats=array(
        'now'=>0,
        'ps'=>0,
        'skip'=>0,
        'start'=>(time()-1),
        'seeds'=>0,
        'users'=>0
        );

        $curDt = date('Y-m-d');

        $conn->exec('SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;');

        $sql .= "
            SELECT u.id, u.first_name, u.last_name, u.email, u.city,
            u.postal_code, c.keyword as keyword_1, u.id as keyword_8,
            u.opt_in_ip, u.partner, u.source FROM clickers90 as c INNER JOIN user as u ON
            c.user_id=u.id WHERE c.date_updated >
            date(now() - interval {$clickerDays} day) AND
            u.is_mailable = 1 AND c.keyword!='' AND
            (u.date_coreg_sent < ? OR u.date_coreg_sent='0000-00-00 00:00:00')
            ";

        //echo $sql . "\n";

        $userStmt = $conn->prepare($sql);
        $userStmt->execute(array($curDt));

        $updateSql = "
            UPDATE user set date_last_mailed=:sendTime WHERE id=:user_id;
        ";

        if($addRpSeeds) {
            if($useRpSched === FALSE || in_array(date("Y-m-d"), $rpSched)) {
                $stmt = $conn->prepare('SELECT * FROM return_path_seeds;');
                if($stmt->execute()) {
                    $rpSeeds = $stmt->fetchAll(PDO::FETCH_ASSOC);
                }
                $seedCount = count($rpSeeds);
                $userCount = $userStmt->rowCount();
                //echo 'seeds: ' . $seedCount . "users: " . $userCount . "\n";
                if($userCount>$seedCount) {
                    $seedInterval = floor($userCount/$seedCount);
                } else {
                    $seedInterval = 1;
                }
                //echo 'seedinterval: ' . $seedInterval . "\n";
            }
        }

        $email_version_model = Util::load_model('emailversion');

        // Get daily digest email template by weight
        $email_template = $email_version_model->get_email_template("daily_digest");

        unset($stmt);

        while(($row=$userStmt->fetch(PDO::FETCH_ASSOC)) !== FALSE) {
            ++$stats['users'];

            foreach($row as $k=>$v) {
                //$row[$k]=htmlentities(trim(preg_replace('/[^\x20-\x7e\n\t]/','',iconv('','ASCII//TRANSLIT//IGNORE',$v))),ENT_QUOTES);
                $row[$k]=trim(preg_replace('/[^\x20-\x7e]/','', str_replace('"', '', stripslashes($v))));
            }

            $outRow = array(
                $row['id'], $row['first_name'], $row['last_name'], $row['email'],
                $row['city'], '', $row['postal_code'], '0', $row['keyword_1'],
                '', '', '', '', $email_template->id, '', $row['keyword_8'], $row['opt_in_ip'], $row['partner'],
                $row['source']);

            if(empty($firstRow)) {
                $firstRow = array(
                    'user_id', 'first_name', 'last_name', 'email', 'city',
                    'state', 'zip', 'priority', 'keyword_1', 'keyword_2',
                    'keyword_3', 'keyword_4', 'keyword_5', 'keyword_6',
                    'keyword_7', 'keyword_8', 'ip', 'partner', 'source'
                    );
                //fputcsv($fh, array_keys($outRow));
                fputcsv($fh, $firstRow);
            }

            fputcsv($fh, $outRow);

            // Update user last emailed date
            $updateStmt = $conn->prepare($updateSql);
            $updateStmt->execute(array(':sendTime'=>$sendTime, ':user_id'=>intval($row['id'])));

            ADDSEED:

            ++$intervalCount;

            // Add RP Seeds

            if(count($rpSeeds)>0 && $intervalCount==$seedInterval) {
                $seed = array_shift($rpSeeds);
                $seedRow = array('0', $seed['first_name'], $seed['last_name'],
                    $seed['email'], $seed['city'], '', $seed['postal_code'],
                    '0', $seed['keyword_1'], '', '', '', '', '', '', '', '209.37.24.74', 'RM', '');
                fputcsv($fh, $seedRow);
                ++$stats['seeds'];
                $intervalCount=0;
            }

            if($stats['now']!=time()){
                    $stats['now']=time();
                    $stats['ps']=($stats['users']/($stats['now']-$stats['start']));
                    /*
                    printf("\r    %.2f/s Skip:%d Seeds:%d Users:%d T:%d",
                            $stats['ps'],
                            $stats['skip'],
                            $stats['seeds'],
                            $stats['users'],
                            $stats['users'] + $stats['seeds']
                    );
                    */
            }

        }

        fclose($fh);
        //print_r($stats);

        $elapsed = time()-$stats['start'];
        $lines = $stats['users'] - $stats['skip'] + $stats['seeds'];
        $statLine = "Elapsed: {$elapsed} Total Lines: {$lines} Users: {$stats['users']} Skip: {$stats['skip']} Seeds: {$stats['seeds']}";

        echo $statLine . "\n";
        Util::log_to_file("daily_digest.log",'csv created', $statLine);

        $actRows = $stats['users'] - $stats['skip'];

        return $actRows;


    }

    public static function count_welcome_reminder_mailing($dayInterval=0) {

        global $database;

        $todayDt = date('Y-m-d');

        // Reminders sent every fourth day
        //$sendInterval = 4 * $dayInterval;

        //-Andy 20130726, Send interval changed back to consecutive days
        $sendInterval = $dayInterval;

        // Only find
        $sql = "SELECT COUNT(*) FROM user WHERE DATEDIFF('{$todayDt}'," .
            " date_added)={$sendInterval} AND has_engaged=0 AND" .
            ' is_mailable=1';

        $result_set = $database->query($sql);
        $row = $database->fetch_array($result_set);
        return array_shift($row);
    }

    public function create_welcome_reminder_csv_file($filename, $dayInterval=0) {

        $todayDt = date('Y-m-d');

        // Reminders sent every fourth day
        //$sendInterval = 4 * $dayInterval;

        //-Andy 20130726, Send interval changed back to consecutive days
        $sendInterval = $dayInterval;

        // Convert any windows backslashes to forward slashes
        $filename = str_replace("\\", "/", $filename);

        // Delete this file if it exists.
        @unlink($filename);

        $email_version_model = Util::load_model('emailversion');

        // Get daily digest email template by weight
        $email_template =
            $email_version_model->get_email_template("welcome_reminder_{$dayInterval}");

        $sql = "SELECT 'user_id', 'first_name', 'last_name', 'email', "
            . "'city', 'state', 'zip', 'priority', 'keyword_1', "
            . "'keyword_2', 'keyword_3', 'keyword_4', 'keyword_5', "
            . "'keyword_6', 'keyword_7', 'keyword_8', 'ip', 'partner', "
            . "'source' "
            . "UNION ALL "
            . "SELECT id, first_name, last_name, email, "
            . "city, '', postal_code, 0, '', '', '', "
            . "'', '', '{$email_template->id}', '', id, "
            . "opt_in_ip, partner, source "
            . "INTO OUTFILE '" . $filename . "' "
            . "FIELDS ENCLOSED BY '\"' "
            . "TERMINATED BY ',' "
            . "LINES TERMINATED BY '\n' "
            . "FROM user "
            . "WHERE DATEDIFF('{$todayDt}',"
            . "date_added)={$sendInterval} AND has_engaged=0 AND "
            . "is_mailable=1";


        $this->_db->query($sql);
    }

    public function update_user_welcome_reminder_last_emailed($start_time, $dayInterval=0) {

        $todayDt = date('Y-m-d');


        // Reminders sent every fourth day
        //$sendInterval = 4 * $dayInterval;

        //-Andy 20130726, Send interval changed back to consecutive days
        $sendInterval = $dayInterval;

        $sql = "UPDATE user "
            . "SET date_last_mailed "
            . "= '" . $start_time . "' WHERE DATEDIFF('{$todayDt}',"
            . "date_added)={$sendInterval} AND has_engaged=0 AND "
            . "is_mailable=1";

        $this->_db->query($sql);
    }

    public function create_welrem_v25_csv_file($conn, $sendTime='', $outFile='', $dayInterval=1,
        $addRpSeeds=FALSE, $useRpSched=FALSE, $rpSched=array(), $seedRange=FALSE) {

        $todayDt = date('Y-m-d');

        $fh=fopen($outFile, 'w'); //* php://output , /tmp/el3_test.csv

        unset($firstRow);
        unset($stmt);
        unset($stats);
        unset($rpSeeds);
        unset($userCount);
        unset($intervalCount);
        unset($seedCount);
        unset($userStmt);
        unset($updateStmt);

        $intervalCount=0;
        $rpSeeds = array();
        $stats=array(
        'now'=>0,
        'ps'=>0,
        'skip'=>0,
        'start'=>(time()-1),
        'seeds'=>0,
        'users'=>0
        );

        $conn->exec('SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;');

        $fromDt = date('Y-m-d', strtotime("-{$dayInterval} day"));
        $toDt = date('Y-m-d', strtotime($fromDt." +1 day"));

        $sql .= "
            SELECT u.id, u.first_name, u.last_name, u.email, u.city,
            u.postal_code, u.id as keyword_8, u.opt_in_ip, u.partner,
            u.source FROM user as u WHERE date_coreg_sent>=? AND date_coreg_sent<? AND has_engaged=0 AND
            is_mailable=1 AND nowrm=0 AND (u.city!='' OR u.postal_code!='');
        ";

        /*
        $sql .= "
            SELECT u.id, u.first_name, u.last_name, u.email, u.city,
            u.postal_code, u.id as keyword_8, u.opt_in_ip, u.partner,
            u.source FROM user as u WHERE DATEDIFF('{$todayDt}',
            date_coreg_sent)={$dayInterval} AND has_engaged=0 AND
            is_mailable=1 AND (u.city!='' OR u.postal_code!='');
            ";
        */

        //echo $sql . "\n";

        $userStmt = $conn->prepare($sql);
        $userStmt->execute(array($fromDt, $toDt));

        $updateSql = "
            UPDATE user set date_last_mailed=:sendTime WHERE id=:user_id;
        ";

        if($addRpSeeds) {
            if($useRpSched === FALSE || in_array(date("Y-m-d"), $rpSched)) {
                $seedSql = 'SELECT * FROM return_path_seeds';
                if($seedRange && isset($seedRange['st'])) {
                    $seedSql .= ' LIMIT ' . $seedRange['st'] . ',' . $seedRange['lim'];
                }
                $stmt = $conn->prepare($seedSql);
                if($stmt->execute()) {
                    $rpSeeds = $stmt->fetchAll(PDO::FETCH_ASSOC);
                }
                $seedCount = count($rpSeeds);
                $userCount = $userStmt->rowCount();
                //echo 'seeds: ' . $seedCount . "users: " . $userCount . "\n";
                if($userCount>$seedCount) {
                    $seedInterval = floor($userCount/$seedCount);
                } else {
                    $seedInterval = 1;
                }
                //echo 'seedinterval: ' . $seedInterval . "\n";
            }
        }

        $email_version_model = Util::load_model('emailversion');

        // Get daily digest email template by weight
        $email_template = $email_version_model->get_email_template("welcome_reminder_{$dayInterval}");

        unset($stmt);

        while(($row=$userStmt->fetch(PDO::FETCH_ASSOC)) !== FALSE) {
            ++$stats['users'];

            foreach($row as $k=>$v) {
                $row[$k]=htmlentities(trim(preg_replace('/[^\x20-\x7e\n\t]/','',iconv('','ASCII//TRANSLIT//IGNORE',$v))),ENT_QUOTES);
            }

            $outRow = array(
                $row['id'], $row['first_name'], $row['last_name'], $row['email'],
                $row['city'], '', $row['postal_code'], '0', '',
                '', '', '', '', $email_template->id, '', $row['keyword_8'], $row['opt_in_ip'], $row['partner'],
                $row['source']);

            if(empty($firstRow)) {
                $firstRow = array(
                    'user_id', 'first_name', 'last_name', 'email', 'city',
                    'state', 'zip', 'priority', 'keyword_1', 'keyword_2',
                    'keyword_3', 'keyword_4', 'keyword_5', 'keyword_6',
                    'keyword_7', 'keyword_8', 'ip', 'partner', 'source'
                    );
                fputcsv($fh, $firstRow);
            }

            fputcsv($fh, $outRow);

            // Update user last emailed date
            $updateStmt = $conn->prepare($updateSql);
            $updateStmt->execute(array(':sendTime'=>$sendTime, ':user_id'=>intval($row['id'])));

            ADDSEED:

            ++$intervalCount;

            // Add RP Seeds

            if(count($rpSeeds)>0 && $intervalCount==$seedInterval) {
                $seed = array_shift($rpSeeds);
                $seedRow = array('0', $seed['first_name'], $seed['last_name'],
                    $seed['email'], $seed['city'], '', $seed['postal_code'],
                    '0', '', '', '', '', '', '', '', '', '209.37.24.74', 'RM', '');
                fputcsv($fh, $seedRow);
                ++$stats['seeds'];
                $intervalCount=0;
            }

            if($stats['now']!=time()){
                    $stats['now']=time();
                    $stats['ps']=($stats['users']/($stats['now']-$stats['start']));
                    /*
                    printf("\r    %.2f/s Skip:%d Seeds:%d Users:%d T:%d",
                            $stats['ps'],
                            $stats['skip'],
                            $stats['seeds'],
                            $stats['users'],
                            $stats['users'] + $stats['seeds']
                    );
                    */
            }

        }

        fclose($fh);
        //print_r($stats);

        $elapsed = time()-$stats['start'];
        $lines = $stats['users'] - $stats['skip'] + $stats['seeds'];
        $statLine = "Elapsed: {$elapsed} Total Lines: {$lines} Users: {$stats['users']} Skip: {$stats['skip']} Seeds: {$stats['seeds']}";

        echo $statLine . "\n";
        Util::log_to_file("welcome_reminder_{$dayInterval}.log",'csv created', $statLine);

        $actRows = $stats['users'] - $stats['skip'];

        return $actRows;


    }

    public function create_noloc_welrem_v25_csv_file($conn, $sendTime='', $outFile='', $dayInterval=1,
        $addRpSeeds=FALSE, $useRpSched=FALSE, $rpSched=array(), $seedRange=FALSE) {

        $todayDt = date('Y-m-d');

        $fh=fopen($outFile, 'w'); //* php://output , /tmp/el3_test.csv

        unset($firstRow);
        unset($stmt);
        unset($stats);
        unset($rpSeeds);
        unset($userCount);
        unset($intervalCount);
        unset($seedCount);
        unset($userStmt);
        unset($updateStmt);

        $intervalCount=0;
        $rpSeeds = array();
        $stats=array(
        'now'=>0,
        'ps'=>0,
        'skip'=>0,
        'start'=>(time()-1),
        'seeds'=>0,
        'users'=>0
        );

        $conn->exec('SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;');

        $fromDt = date('Y-m-d', strtotime("-{$dayInterval} day"));
        $toDt = date('Y-m-d', strtotime($fromDt." +1 day"));

        /*
        $sql .= "
            SELECT u.id, u.first_name, u.last_name, u.email, u.city,
            u.postal_code, u.id as keyword_8, u.opt_in_ip, u.partner,
            u.source FROM user as u WHERE DATEDIFF('{$todayDt}',
            date_coreg_sent)={$dayInterval} AND has_engaged=0 AND
            is_mailable=1 AND u.city='' AND u.postal_code='';
            ";
        */

        $sql .= "
            SELECT u.id, u.first_name, u.last_name, u.email, u.city,
            u.postal_code, u.id as keyword_8, u.opt_in_ip, u.partner,
            u.source FROM user as u WHERE date_coreg_sent>=? AND date_coreg_sent<? AND has_engaged=0 AND
            is_mailable=1 AND nowrm=0 AND u.city='' AND u.postal_code='';
        ";

        echo $sql . "\n";

        $userStmt = $conn->prepare($sql);
        $userStmt->execute(array($fromDt, $toDt));

        $updateSql = "
            UPDATE user set date_last_mailed=:sendTime WHERE id=:user_id;
        ";

        if($addRpSeeds) {
            if($useRpSched === FALSE || in_array(date("Y-m-d"), $rpSched)) {
                $seedSql = 'SELECT * FROM return_path_seeds';
                if($seedRange && isset($seedRange['st'])) {
                    $seedSql .= ' LIMIT ' . $seedRange['st'] . ',' . $seedRange['lim'];
                }
                $stmt = $conn->prepare($seedSql);
                if($stmt->execute()) {
                    $rpSeeds = $stmt->fetchAll(PDO::FETCH_ASSOC);
                }
                $seedCount = count($rpSeeds);
                $userCount = $userStmt->rowCount();
                //echo 'seeds: ' . $seedCount . "users: " . $userCount . "\n";
                if($userCount>$seedCount) {
                    $seedInterval = floor($userCount/$seedCount);
                } else {
                    $seedInterval = 1;
                }
                //echo 'seedinterval: ' . $seedInterval . "\n";
            }
        }

        $email_version_model = Util::load_model('emailversion');

        // Get daily digest email template by weight
        $email_template = $email_version_model->get_email_template("welcome_noloc_reminder_{$dayInterval}");

        unset($stmt);

        while(($row=$userStmt->fetch(PDO::FETCH_ASSOC)) !== FALSE) {
            ++$stats['users'];

            foreach($row as $k=>$v) {
                $row[$k]=htmlentities(trim(preg_replace('/[^\x20-\x7e\n\t]/','',iconv('','ASCII//TRANSLIT//IGNORE',$v))),ENT_QUOTES);
            }

            $outRow = array(
                $row['id'], $row['first_name'], $row['last_name'], $row['email'],
                '', '', '', '0', '',
                '', '', '', '', $email_template->id, '', $row['keyword_8'], $row['opt_in_ip'], $row['partner'],
                $row['source']);

            if(empty($firstRow)) {
                $firstRow = array(
                    'user_id', 'first_name', 'last_name', 'email', 'city',
                    'state', 'zip', 'priority', 'keyword_1', 'keyword_2',
                    'keyword_3', 'keyword_4', 'keyword_5', 'keyword_6',
                    'keyword_7', 'keyword_8', 'ip', 'partner', 'source'
                    );
                fputcsv($fh, $firstRow);
            }

            fputcsv($fh, $outRow);

            // Update user last emailed date
            $updateStmt = $conn->prepare($updateSql);
            $updateStmt->execute(array(':sendTime'=>$sendTime, ':user_id'=>intval($row['id'])));

            ADDSEED:

            ++$intervalCount;

            // Add RP Seeds

            if(count($rpSeeds)>0 && $intervalCount==$seedInterval) {
                $seed = array_shift($rpSeeds);
                $seedRow = array('0', $seed['first_name'], $seed['last_name'],
                    $seed['email'], '', '', '',
                    '0', '', '', '', '', '', '', '', '', '209.37.24.74', 'RM', '');
                fputcsv($fh, $seedRow);
                ++$stats['seeds'];
                $intervalCount=0;
            }

            if($stats['now']!=time()){
                    $stats['now']=time();
                    $stats['ps']=($stats['users']/($stats['now']-$stats['start']));
                    /*
                    printf("\r    %.2f/s Skip:%d Seeds:%d Users:%d T:%d",
                            $stats['ps'],
                            $stats['skip'],
                            $stats['seeds'],
                            $stats['users'],
                            $stats['users'] + $stats['seeds']
                    );
                    */
            }

        }

        fclose($fh);
        //print_r($stats);

        $elapsed = time()-$stats['start'];
        $lines = $stats['users'] - $stats['skip'] + $stats['seeds'];
        $statLine = "Elapsed: {$elapsed} Total Lines: {$lines} Users: {$stats['users']} Skip: {$stats['skip']} Seeds: {$stats['seeds']}";

        echo $statLine . "\n";
        Util::log_to_file("welcome_noloc_reminder_{$dayInterval}.log",'csv created', "File: {$outFile} {$statLine}");

        $actRows = $stats['users'] - $stats['skip'];

        return $actRows;


    }

}
