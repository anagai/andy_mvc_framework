<?php
	// vim: ai ts=4 sts=4 et sw=4 tw=79

	/**
	 * Created on Nov 1, 2012
	 * Written By Sung Hwan Ahn / Andy Nagai
	 *
	 */

	class GlobalRemovalModel extends Model {

		function __construct() {

			$this->_table_name = 'global_removal';
			$this->_table_class_name = str_replace('_', '', $this->_table_name);
			$this->_db = Util::get_bacon_db();

		}

		public function has_this_email ($email) {

			// Point database object to bacon
			$result_array = $this->find_by_sql("SELECT id FROM " . $this->_table_name . " WHERE email = '" . $this->_db->escape_value($email) . "' LIMIT 1");

			return !empty($result_array) ? true : false;
		}


		public function get_last_added_records($last_date_added) {

			static $sth;

			$dbh = Util::get_pdo_bacon_conn();

			if(!isset($sth)) {
				$sth = $dbh->prepare('SELECT date_updated, email FROM ' . $this->_table_name .
	                 ' WHERE date_updated >= :date_updated' .
	                 ' ORDER BY date_updated DESC');
			}

			$sth->bindParam(':date_updated', $last_date_added, PDO::PARAM_STR);

			$sth->execute();

			return $sth;

		}



	}