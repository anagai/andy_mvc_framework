<?php
    // vim: ai ts=4 sts=4 et sw=4 tw=79

    /*
     * Created on July 5, 2013
     * Written By Sung Hwan Ahn / Andy Nagai
     *
     */

    class PartnerSourceStatsModel extends Model {

        function __construct() {

            // All models should have this variable set for default db
            $this->_db = Util::get_dbo();
            $this->_table_name = 'partner_source_stats';
            $this->_table_class_name = str_replace('_', '', $this->_table_name);

            // For remote db connections create a second db connection object
            // e.g. $_shasta_db = $_db->open_connection('shasta')
        }

        // Increment or create page view by date, partner and source.
        public function save_partner_source_search_stats() {
            $today_dt = date('Y-m-d');

            $user = $this->getState('user');

            if(!empty($user) && !empty($user->partner)) {

                $sql = "INSERT INTO {$this->_table_name} (search_date, "
                . "partner, source, page_views) VALUES "
                . "('{$today_dt}','{$user->partner}','{$user->source}',1) "
                . "ON DUPLICATE KEY UPDATE page_views=page_views+1";

                $this->_db->query($sql);
            }



        }

        // Increment or create job click by date, partner and source.
        public function save_partner_source_click_stats() {

            $today_dt = date('Y-m-d');

            $user = $this->getState('user');

            if(!empty($user) && !empty($user->partner)) {

                // Will update partner_source_stats record. Should always update
                // existing row. The INSERT new row should never occur. Just put
                // in as failsafe and do not have to do a record lookup.
                $sql = "INSERT INTO {$this->_table_name} (search_date, "
                    . "partner, source, page_views, job_clicks) VALUES "
                    . "('{$today_dt}','{$user->partner}','{$user->source}',1,1) "
                    . "ON DUPLICATE KEY UPDATE job_clicks=job_clicks+1";
                $this->_db->query($sql);

            }

        }

        // Save unique search by date, partner, source and email
        public function save_search_unique_email() {

            $today_dt = date('Y-m-d');

            $user = $this->getState('user');

            if(!empty($user) && !empty($user->partner)) {

                $sql = "INSERT INTO partner_source_search "
                    . "(search_date, partner, source, email) VALUES "
                    . "('{$today_dt}','{$user->partner}','{$user->source}',"
                    . "'{$user->email}') "
                    . "ON DUPLICATE KEY UPDATE search_date=search_date";

                $this->_db->query($sql);

            }

        }

        // Save unique click by date, partner, source and email
        public function save_click_unique_email() {

            $today_dt = date('Y-m-d');

            $user = $this->getState('user');

            if(!empty($user) && !empty($user->partner)) {

                $sql = "INSERT INTO partner_source_click "
                    . "(search_date, partner, source, email) VALUES "
                    . "('{$today_dt}','{$user->partner}','{$user->source}',"
                    . "'{$user->email}') "
                    . "ON DUPLICATE KEY UPDATE search_date=search_date";

                $this->_db->query($sql);

            }

        }

    }