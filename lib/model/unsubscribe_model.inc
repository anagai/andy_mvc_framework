<?php
    // vim: ai ts=4 sts=4 et sw=4 tw=79

    /**
     * Created on Nov 7, 2012
     * Written By Andy Nagai
     *
     */

    class UnsubscribeModel extends Model {

        function __construct() {

            $this->_table_name = 'unsubscribe';
            $this->_table_class_name = str_replace('_', '', $this->_table_name);
            $this->_db = Util::get_bacon_db();

        }

        public function count_by_campaign($campaign_name) {

            $sql = "SELECT COUNT(*) FROM " . $this->_table_name .
                   " WHERE site_id = " . SITE_ID .
                   " AND campaign_name = '" . $this->_db->escape_value($campaign_name) . "'";
            $result_set = $this->_db->query($sql);
            $row = $this->_db->fetch_array($result_set);
            return array_shift($row);
        }

        public function find_by_email($email='') {
            $result_array = $this->find_by_sql("SELECT id FROM " . $this->_table_name .
                " WHERE email_address = '" . $this->_db->escape_value($email) .
                "' AND site_id = " . SITE_ID . " LIMIT 1");
            return !empty($result_array) ? array_shift($result_array) : false;
        }

        public function is_unsubscribe_email($email) {

            // Email is unsubscribed if in unsubscribe table for specific site
            $exists = $this->result_exists("SELECT id FROM " . $this->_table_name .
                " WHERE email_address = '" . $this->_db->escape_value($email) .
                "' AND site_id = " . SITE_ID . " LIMIT 1");

            // returns true if email is valid unsubscribe
            return $exists;
        }

        public function unsubscribe_email($email, $sub_id = '') {

            $user_model = Util::load_model('user');

            $now_dt = date('Y-m-d H:i:s');

            if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $user = $user_model->find_by_email($email);

                if(!empty($user)) {

                    // Add email to unsubscribe on bacon
                    $now_dt = date('Y-m-d H:i:s');
                    $new_unsub = new Unsubscribe();
                    $new_unsub->site_id = SITE_ID;
                    $new_unsub->email_address = $email;
                    $email_exp = explode('@', $email);
                    $new_unsub->domain = $email_exp[1];
                    $new_unsub->campaign_name = $sub_id;
                    $new_unsub->partner = $user->partner;
                    $new_unsub->source = $user->source;
                    $new_unsub->details = 'Added via site';
                    $new_unsub->date_received = $now_dt;
                    $new_unsub->date_added = $now_dt;
                    $new_unsub->create();

                    // Set user is_mailable to zero.
                    $user->is_mailable = 0;
                    $user->update();
                }

            }

            Util::redirect_to('unsubscribe_confirm');

        }

        public function remove_from_unsubscribe($email) {
            $this->_db->query('DELETE FROM ' . $this->_table_name . ' WHERE site_id = ' . SITE_ID . " AND email_address = '" . $this->_db->escape_value($email) . "'");
        }

        public function count_all_unsubscribe() {

            $sql = "SELECT COUNT(*) FROM " . $this->_table_name .
            " WHERE site_id = " . SITE_ID;
            $result_set = $this->_db->query($sql);
            $row = $this->_db->fetch_array($result_set);
            return array_shift($row);
        }

        public function get_total_unsubscribes($campaign_name, $search_text, $search_field, $start_date, $end_date) {

            $sql = "SELECT COUNT(*) FROM " . $this->_table_name;
            $sql .= " WHERE site_id = " . SITE_ID;
            $sql .= " AND campaign_name = '" . $this->_db->escape_value($campaign_name) . "'";
            $sql .= $this->write_list_sql($search_text, $search_field, $start_date, $end_date);

            $result_set = $this->_db->query($sql);
            $row = $this->_db->fetch_array($result_set);
            return array_shift($row);
        }

        public function get_unsubscribes($campaign_name, $search_text, $search_field, $start_date, $end_date, $per_page, $offset) {

            $sql = "SELECT * FROM " . $this->_table_name;
            $sql .= " WHERE site_id = " . SITE_ID;
            $sql .= " AND campaign_name = '" . $this->_db->escape_value($campaign_name) . "'";
            $sql .= $this->write_list_sql($search_text, $search_field, $start_date, $end_date);

            return $this->find_by_sql($sql . " ORDER BY id DESC LIMIT {$per_page} OFFSET {$offset}");
        }

        private function write_list_sql($search_text, $search_field, $start_date, $end_date) {

            $sql = "";

            if ($start_date != "") {
                $sql .= " AND date_added >= '{$start_date}'";
            }

            if ($end_date != "") {
                $end_date = strftime ("%Y-%m-%d %H:%M:%S", strtotime('+1 day', strtotime($end_date)));
                $sql .= " AND date_added < '{$end_date}'";
            }

            if ($search_text != "" && $search_field == "email") {
                $sql .= " AND email_address LIKE '{$search_text}%'";
            }

            if ($search_text != "" && $search_field == "partner") {
                $sql .= " AND partner LIKE '{$search_text}'";
            }

            return $sql;
        }

        public function get_last_added_records($last_date_added) {

        	static $sth;

        	$dbh = Util::get_pdo_bacon_conn();

        	if(!isset($sth)) {
        		$sth = $dbh->prepare( 'SELECT email_address, date_added' .
        				' FROM ' . $this->_table_name .
        				' WHERE date_added >= :date_added' .
        				' AND site_id = ' . SITE_ID .
        				' GROUP BY email_address' .
        				' ORDER BY date_added DESC');
        	}

        	$sth->bindParam(':date_added', $last_date_added, PDO::PARAM_STR);

        	$sth->execute();

        	return $sth;

        }
    }