<?php

    /**
     * Created on Dec 11, 2012
     * Written By Sung Hwan Ahn / Andy Nagai
     *
     */

    // vim: ai ts=4 sts=4 et sw=4 tw=79

    class CampaignModel extends Model {

        function __construct() {
            $this->_db = Util::get_ses_db();
            $this->_table_name = 'campaign';
            $this->_table_class_name = str_replace('_', '', $this->_table_name);
        }

        public function find_by_campaign_name($campaign_name) {

            $result_array = $this->find_by_sql(
                    "SELECT id FROM " . $this->_table_name .
                    " WHERE id <> 1" .
                    " AND database_name='" . $this->_db->escape_value($campaign_name) . "'".
                    " LIMIT 1");
            return !empty($result_array) ? array_shift($result_array) : false;
        }

        public function get_campaings($search_text, $search_field, $start_date, $end_date, $per_page, $offset) {

            $sql = "SELECT * FROM " . $this->_table_name;

            $sql .= $this->write_list_sql($search_text, $search_field, $start_date, $end_date);

            return $this->find_by_sql($sql . " ORDER BY id DESC LIMIT {$per_page} OFFSET {$offset}");
        }

        private function write_list_sql($search_text, $search_field, $start_date, $end_date) {

            $sql = "";

            if ($start_date != "") {
                (strrpos($sql, "WHERE")) ? $sql .= " AND " : $sql .= " WHERE ";
                $sql .= "date_send_started >= '{$start_date}'";
            }

            if ($end_date != "") {
                (strrpos($sql, "WHERE")) ? $sql .= " AND " : $sql .= " WHERE ";
                $end_date = strftime ("%Y-%m-%d %H:%M:%S", strtotime('+1 day', strtotime($end_date)));
                $sql .= "date_send_started < '{$end_date}'";
            }

            if ($search_text != "" && $search_field == "campaign_name") {
                (strrpos($sql, "WHERE")) ? $sql .= " AND " : $sql .= " WHERE ";
                $sql .= "name LIKE '{$search_text}%'";
            }

            return $sql;
        }
    }
