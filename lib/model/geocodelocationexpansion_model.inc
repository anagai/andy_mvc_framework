<?php
	// vim: ai ts=4 sts=4 et sw=4 tw=79

	/*
	 * Created on Oct 18, 2012
	 * Written By Sung Hwan Ahn / Andy Nagai
	 *
	 */

	class GeocodeLocationExpansionModel extends Model {

		function __construct() {

			// All models should have this variable set for default db
			$this->_db = Util::get_dbo();
			$this->_table_name = 'geocode_location_expansion';
			$this->_table_class_name = str_replace('_', '', $this->_table_name);

			// For remote db connections create a second db connection object
			// e.g. $_shasta_db = $_db->open_connection('shasta')
		}

		public function get_postal_by_city($city) {

			$myLocation = $this->find_by_sql("SELECT postal_code FROM " . $this->_table_name .
					" WHERE city = '" . $this->_db->escape_value($city) . "' AND postal_code != '' LIMIT 1");

			if (!empty($myLocation)) {
				$myLocation = array_shift($myLocation);
				$myZipCode = $myLocation->postal_code;
			} else {
				$myZipCode = false;
			}

			return $myZipCode;
		}

		public function get_city_by_postal($postal) {
			$postal = $this->_db->escape_value($postal);
			$myLocation = $this->find_by_sql("SELECT city FROM " . $this->_table_name . " WHERE country='GB' AND postal_code = '{$postal}' LIMIT 1");

			if (!empty($myLocation)) {
				$myLocation = array_shift($myLocation);
				$myCity = $myLocation->city;
			} else {
				$myCity = '';
			}

			return $myCity;
		}

		public function valid_city($city) {

			$exists = $this->result_exists("SELECT id FROM " . $this->_table_name . " WHERE country = 'GB' AND city = '" . $this->_db->escape_value($city) . "' LIMIT 1");

			return $exists;
		}
	}