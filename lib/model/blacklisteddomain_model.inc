<?php
    // vim: ai ts=4 sts=4 et sw=4 tw=79

    /**
     * Created on Jan 1, 2013
     * Written By Andy Nagai
     *
     */

    class BlacklistedDomainModel extends Model {

        protected $_pdo_bacon_dbh;

        function __construct() {

            $this->_table_name = 'blacklisted_domain';
            $this->_table_class_name = str_replace('_', '', $this->_table_name);
            $this->_db = Util::get_bacon_db();

        }
		
        public function get_last_added_records($last_date_added) {
        
        	static $sth;
        
        	$dbh = Util::get_pdo_bacon_conn();
        		
        	if(!isset($sth)) {
        		$sth = $dbh->prepare('SELECT date_added, domain_name FROM ' . $this->_table_name .
        				' WHERE date_added >= :date_added' .
        				' AND site_id=' . SITE_ID .
        				' ORDER BY date_added DESC');
        	}
        		
        	$sth->bindParam(':date_added', $last_date_added, PDO::PARAM_STR);
        		
        	$sth->execute();
        
        	return $sth;
        		
        }
        
        public function is_blacklisted_domain($email_domain) {
        	
        	$exists = $this->result_exists('SELECT id FROM ' . $this->_table_name . ' WHERE site_id=' . SITE_ID .
        			' AND domain_name="' . $this->_db->escape_value($email_domain) . '"');
        	return $exists;
        }
        
    }
