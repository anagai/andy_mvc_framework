<?php
	// vim: ai ts=4 sts=4 et sw=4 tw=79

	/*
	 * Created on Oct 18, 2012
	 * Written By Sung Hwan Ahn / Andy Nagai
	 *
	 */

	class GeocodeLocationModel extends Model {

		function __construct() {

			// All models should have this variable set for default db
			$this->_db = Util::get_dbo();
			$this->_table_name = 'geocode_location';
			$this->_table_class_name = str_replace('_', '', $this->_table_name);

			// For remote db connections create a second db connection object
			// e.g. $_shasta_db = $_db->open_connection('shasta')
		}

		public function valid_city($city) {

			$exists = $this->result_exists("SELECT id FROM " . $this->_table_name . " WHERE country = 'GB' AND city = '" . $this->_db->escape_value($city) . "' LIMIT 1");

			return $exists;
		}

		public function find_location_list($term) {

			$data = array();

			$result_array = $this->find_by_sql("SELECT id, city FROM " . $this->_table_name . " WHERE country = 'GB' AND city LIKE '" . $this->_db->escape_value($term) . "%' GROUP BY city");

			foreach($result_array as $row) {
				$data[] = Array('id' => $row->id, 'tx'=> $row->city);
			}

			$ret = count($data) >0 ? json_encode($data) : false;

			return !empty($result_array) ? $ret : false;
		}

		public function get_city_by_postal($postal) {
			$postal = $this->_db->escape_value($postal);
			$myLocation = $this->find_by_sql("SELECT city FROM " . $this->_table_name . " WHERE country='GB' AND postal_code = '{$postal}' LIMIT 1");

			if (!empty($myLocation)) {
				$myLocation = array_shift($myLocation);
				$myCity = $myLocation->city;
			} else {
				$myCity = '';
			}

			return $myCity;
		}

		public function get_postal_by_city($city) {
			$city = $this->_db->escape_value($city);
			$myLocation = $this->find_by_sql("SELECT postal_code FROM " . $this->_table_name . " WHERE country = 'GB' AND
					city = '{$city}' AND postal_code != '' LIMIT 1");

			if (!empty($myLocation)) {
				$myLocation = array_shift($myLocation);
				$myZipCode = $myLocation->postal_code;
			} else {
				$myZipCode = '';
			}

			return $myZipCode;
		}

		public function is_valid_city($city) {

			$result_array = $this->find_by_sql("SELECT id FROM " . $this->_table_name . " WHERE country = 'GB' AND city='" . $this->_db->escape_value($city) . "' LIMIT 1");
			return !empty($result_array) ? true : false;
		}

		public function get_longlat_by_city($city) {
			$city = $this->_db->escape_value($city);
			$myLocation = $this->find_by_sql("SELECT longitude, latitude FROM " . $this->_table_name . " WHERE country = 'GB' AND
					city = '{$city}' LIMIT 1");


					$longlat = array_shift($myLocation);

					return $longlat;
		}

		public function get_loc_by_ip($use_default = true) {

			$ret['city'] = '';
			$ret['postal'] = '';

			$gi = geoip_open(DATA_PATH."geo".DS."GeoIPCityeu.dat", GEOIP_STANDARD);

			// Get geo record by user's curent ip
			$geo_record = geoip_record_by_addr($gi, $_SERVER["REMOTE_ADDR"]);
			//$geo_record = geoip_record_by_addr($gi, '136.170.0.0');
			// Shipley - 136.170.0.0
			// London - 128.40.228.145
			// Edinburgh, Scotland - 193.62.154.252
			// Belfast, Northern Ireland - 194.46.0.230
			// Paris, France (code - FR) - 91.121.112.154

			// Only accept UK ip
			if (!empty($geo_record->country_code) && $geo_record->country_code == "GB"
					&& (!empty($geo_record->city) || !empty($geo_record->postal_code))) {

				//$geocode_location->region = $geo_record->region;
				$ret['city'] = $geo_record->city;
				$ret['postal'] = $geo_record->postal_code;

				if ($geo_record->postal_code == "") {
					$ret['postal'] = $this->get_postal_by_city($ret['city']);
				}

			} else {

				if($use_default) {
					// If not valid UK IP default to London
					$ret['city'] = DEFAULT_CITY;
					$ret['postal'] = DEFAULT_POSTCODE;
				}
			}

			//Util::debug($geo_record);

			geoip_close($gi);

			return $ret;
		}

		public function get_loc_by_opt_in_ip($opt_in_ip = '') {

			$ret['city'] = '';
			$ret['postal'] = '';

			$gi = geoip_open(DATA_PATH."geo".DS."GeoIPCityeu.dat", GEOIP_STANDARD);

			// Get geo record by opt in ip
			$geo_record = geoip_record_by_addr($gi, $opt_in_ip);

			// Only accept UK IP
			if (!empty($geo_record) && !empty($geo_record->country_code) && $geo_record->country_code == "GB") {

				//$geocode_location->region = $geo_record->region;
				if(!empty($geo_record->city)) {
					$ret['city'] = $geo_record->city;
				}

				if(!empty($geo_record->postal_code)) {
					$ret['postal'] = $geo_record->postal_code;
				}

				if (!$ret['postal'] && $ret['city']) {
					$ret['postal'] = $this->get_postal_by_city($ret['city']);
				}

			}

			//Util::debug($geo_record);

			geoip_close($gi);

			if(!$ret['postal'] && !$ret['city']) {
				$ret = false;
			}

			return $ret;
		}

		public function is_us_ip() {

			$gi = geoip_open(DATA_PATH."geo".DS."GeoIPCityusw.dat", GEOIP_STANDARD);

			// Get geo record by user's curent ip
			$geo_record = geoip_record_by_addr($gi, $_SERVER["REMOTE_ADDR"]);
			//$geo_record = geoip_record_by_addr($gi, '127.0.0.1');
			//$geo_record = geoip_record_by_addr($gi, '173.204.123.43');

			return !empty($geo_record) ? true : false;

		}

		function verify_location($loc) {

            $city = '';
            $postal_code = '';

            // Get only first part of postal code if in two parts
            $loc_exp = explode(' ', $loc);
            $is_postal = Util::valid_postal($loc_exp[0]);

            if($is_postal) {
                // Validate postalcode by finding a city in geocode_location
                if($city = $this->get_city_by_postal($loc_exp[0])) {
                    $postal_code = $loc_exp[0];
                }
            } else {

                if($this->is_valid_city($loc)) {
                    $city = $loc;
                }

                $postal_code = $this->get_postal_by_city($loc);

            }

            if(empty($city) && empty($postal_code)) {
                $loc = $this->get_loc_by_ip(false);
                $city = $loc['city'];
                $postal_code = $loc['postal'];
            }

            if(!empty($city) || !empty($postal_code)) {
                $ret['city'] = ucwords($city);
                $ret['postal'] = strtoupper($postal_code);
            } else {
                // Should never go here. Default set to London and E11
                // if no location data found in get_loc_by_ip()
                $ret = false;
            }

            return $ret;

        }
	}