<?php
    // vim: ai ts=4 sts=4 et sw=4 tw=79

    /*
     * Created on July 5, 2013
     * Written By Sung Hwan Ahn / Andy Nagai
     *
     */

    class VersionatorStatsModel extends Model {

        function __construct() {

            // All models should have this variable set for default db
            $this->_db = Util::get_dbo();
            $this->_table_name = 'versionator_stats';
            $this->_table_class_name = str_replace('_', '', $this->_table_name);

            // For remote db connections create a second db connection object
            // e.g. $_shasta_db = $_db->open_connection('shasta')
        }

        public function save_versionator_search_stats() {
            $today_dt = date('Y-m-d');

            $page = (int)$this->getState('page');
            if($page == 0) {
                $page = 1;
            }

            $jobs = (int)$this->getState('job_results');

            // Will create new record if not duplicate. If dupe will just
            // update the counter fields.
            $sql = "INSERT INTO " . $this->_table_name . " (search_date, "
                . "version_id, page_number, page_views, job_results) VALUES "
                . "('{$today_dt}'," . (int)$this->getState('version_id')
                . "," . $page . ",1, {$jobs}) "
                . "ON DUPLICATE KEY UPDATE page_views=page_views+1, "
                . "job_results=job_results+{$jobs}";

            $this->_db->query($sql);

        }

        public function save_versionator_click_stats($version, $page,
            $is_sponsored) {

            $today_dt = date('Y-m-d');
            $page = (int)$page;
            if($page == 0) {
                $page = 1;
            }


            // Will update versionator stats record. Should always update
            // existing row. The INSERT new row should never occur. Just put
            // in as failsafe and do not have to do a record lookup.
            $type_init_values = $is_sponsored
                ? '1,0'
                : '0,1';
             $type_incr = $is_sponsored
                ? 'sponsored_clicks=sponsored_clicks+1'
                : 'organic_clicks=organic_clicks+1';
            $sql = "INSERT INTO " . $this->_table_name . " (search_date, "
                . "version_id, page_number, sponsored_clicks, "
                . "organic_clicks) VALUES "
                . "('{$today_dt}'," . (int)$version
                . ",{$page},{$type_init_values}) "
                . "ON DUPLICATE KEY UPDATE {$type_incr}";
            $this->_db->query($sql);

        }

        public function save_search_unique_ip() {

            $today_dt = date('Y-m-d');
            $version = (int)$this->getState('version_id');
            $page = (int)$this->getState('page');

            if($page == 0) {
                $page = 1;
            }

            $ip = $this->getState('request_ip');

            $sql = "INSERT INTO search_unique_ip "
                . "(search_date, version_id, page_number, ip) VALUES "
                . "('{$today_dt}', {$version}, {$page}, '{$ip}') "
                . "ON DUPLICATE KEY UPDATE search_date=search_date";

            $this->_db->query($sql);

        }

    }