<?php
	// vim: ai ts=4 sts=4 et sw=4 tw=79

	/**
	 * Created on Nov 26, 2012
	 * Written By Andy Nagai
	 *
	 */

	class MemcachedModel extends Model {
		
		/**
		 * Clean and generate memcached key
		 *
		 * @param string $key
		 * @return string
		 */
		public function make_cache_key($provider = 'indeed', $key, $loc, $days_back = '3', $radius) {
				
			// Used to cache the memcached key
			static $cached_key = '';
								
			// Cache the the common key elements so don't have to duplicate efforts.
			if(!$cached_key) {
				$loc = $this->clean_cache_key($loc);
				$days_back = $this->clean_cache_key((string)$days_back);
				$radius = $this->clean_cache_key($radius);
				$cached_key = "$provider-%s-$loc-$days_back-$radius";
			}
				
			$keyword = $this->clean_cache_key($key);
		
			// Add the keyword to the cached memcached key
			$key = sprintf($cached_key, $keyword);

			//Util::debug('key: ' . $key);
			
			return $key;
				
				
		}
		
		/**
		 * Clean cached key item
		 * 
		 */
		public function clean_cache_key($key) {
			// lower case and trim.
			$key = strtolower(trim($key));
			// Remove any non alpha and numeric characters.
			$key = preg_replace('/[^a-z0-9]/', '', $key);
			return $key;
		}
		
	}