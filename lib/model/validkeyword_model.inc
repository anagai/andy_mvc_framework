<?php
	// vim: ai ts=4 sts=4 et sw=4 tw=79

	/*
	 * Created on Oct 8, 2012
	 * Written By Sung Hwan Ahn / Andy Nagai
	 *
	 */

	class ValidKeywordModel extends Model {

		function __construct() {

			// All models should have this variable set for default db
			$this->_db = Util::get_dbo();
			$this->_table_name = 'valid_keyword';
			$this->_table_class_name = str_replace('_', '', $this->_table_name);

			// For remote db connections create a second db connection object
			// e.g. $_shasta_db = $_db->open_connection('shasta')
		}

		public function is_valid_keyword($keyword) {

			$is_exist = $this->result_exists("SELECT keyword FROM " . $this->_table_name . " WHERE keyword='" . $this->_db->escape_value($keyword) . "' LIMIT 1");
			return $is_exist;

		}

		public function find_keyword_list($term) {

			$result_array = $this->find_by_sql("SELECT * FROM " . $this->_table_name . " WHERE keyword LIKE '" . $this->_db->escape_value($term) . "%' LIMIT 5");

			foreach($result_array as $row) {
				$data[] = array('id'=>$row->id, 'tx'=>$row->keyword);
			}

			$ret = json_encode($data);

			return !empty($result_array) ? $ret : false;
		}


	}