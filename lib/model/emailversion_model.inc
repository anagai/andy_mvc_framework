<?php
    // vim: ai ts=4 sts=4 et sw=4 tw=79

    /**
     * Created on Nov 1, 2012
     * Written By Sung Hwan Ahn / Andy Nagai
     *
     */

    class EmailVersionModel extends Model {

        function __construct() {
            $this->_db = Util::get_dbo();
            $this->_table_name = 'email_version';
            $this->_table_class_name = str_replace('_', '', $this->_table_name);

            // For remote db connections create a second db connection object
            // e.g. $_shasta_db = $_db->open_connection('shasta')
        }

        public function find_by_type($type='', $status='') {

            $status_sql = '';
            if (!empty($status)) {
                $status_sql = " AND weight = 100 AND is_active = 1";
            }

            $result_array = $this->find_by_sql(
                "SELECT * FROM " . $this->_table_name .
                " WHERE type='" . $this->_db->escape_value($type) . "'" .
                $status_sql);

            return !empty($result_array) ? $result_array : false;
        }

        public function find_email_type_weights($type='') {
            $result_array = $this->find_by_sql(
                "SELECT id, campaign_name, weight" .
                " FROM " . $this->_table_name .
                " WHERE type='" . $this->_db->escape_value($type) . "'");
            return !empty($result_array) ? $result_array : false;
        }

        public function find_by_type_all($type='') {

            $result_array = $this->find_by_sql(
                "SELECT * FROM " . $this->_table_name .
                " WHERE type='" . $this->_db->escape_value($type) . "'" .
                " ORDER BY is_active DESC");

            return !empty($result_array) ? $result_array : false;
        }

        /**
         * Choose email template randomly by weight
         * If a template weight is set to 50 then will be used 50% of the time
         *
         * @param string $type
         * @return unknown
         */
        public function get_email_template($type='') {

            $email_templates = $this->find_email_type_weights($type);

            $random_template = mt_rand(1, 100);

            $add_rate = 0;
            foreach ($email_templates as $email_template) {
                if ($random_template <= ($email_template->weight + $add_rate))
                    return $email_template;
                else
                    $add_rate += $email_template->weight;
            }

            return false;

        }

    }