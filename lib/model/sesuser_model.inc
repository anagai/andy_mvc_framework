<?php
    // vim: ai ts=4 sts=4 et sw=4 tw=79

    /*
     * Created on Dec 12, 2012
     * Written By Sung Hwan Ahn / Andy Nagai
     *
     */

    class SESUserModel extends Model {

        function __construct() {

            $this->_db = Util::get_ses_db();
            $this->_table_name = 'user';
            $this->_table_class_name = 'ses' . str_replace('_', '', $this->_table_name);
        }

        public function get_total_users($campaign_name, $search_text = '', $search_field = '') {

            $this->_db = Util::get_ses_db($campaign_name);

            $sql = "SELECT COUNT(*) FROM " . $this->_table_name;
            $sql .= $this->write_list_sql($search_text, $search_field);

            $result_set = $this->_db->query($sql);
            $row = $this->_db->fetch_array($result_set);
            return array_shift($row);
        }

        public function get_users($campaign_name, $search_text, $search_field, $per_page, $offset) {

            $this->_db = Util::get_ses_db($campaign_name);

            $sql = "SELECT * FROM " . $this->_table_name;
            $sql .= $this->write_list_sql($search_text, $search_field);

            return $this->find_by_sql($sql . " ORDER BY id DESC LIMIT {$per_page} OFFSET {$offset}");
        }

        private function write_list_sql($search_text, $search_field) {

            $sql = "";

            if ($search_text != "" && $search_field == "email") {
                (strrpos($sql, "WHERE")) ? $sql .= " AND " : $sql .= " WHERE ";
                $sql .= "email LIKE '" . $this->_db->escape_value($search_text) . "%'";
            }

            if ($search_text != "" && $search_field == "name") {
                (strrpos($sql, "WHERE")) ? $sql .= " AND " : $sql .= " WHERE ";
                $sql .= "first_name LIKE '" . $this->_db->escape_value($search_text) . "%'" .
                        " OR last_name LIKE '" . $this->_db->escape_value($search_text) . "%'" .
                        " OR CONCAT_WS(' ', first_name, last_name) LIKE '" . $this->_db->escape_value($search_text) . "%'";
            }

            if ($search_text != "" && $search_field == "partner") {
                (strrpos($sql, "WHERE")) ? $sql .= " AND " : $sql .= " WHERE ";
                $sql .= "partner LIKE '" . $this->_db->escape_value($search_text) . "'";
            }

            return $sql;
        }
    }