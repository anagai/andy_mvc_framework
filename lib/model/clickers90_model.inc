<?php

    // vim: ai ts=4 sts=4 et sw=4 tw=79

    /*
     * Created on Dec 3, 2012
     * Written By Sung Hwan Ahn / Andy Nagai
     *
     */

    class Clickers90Model extends Model {

        function __construct() {

            // All models should have this variable set for default db
            $this->_db = Util::get_dbo();
            $this->_table_name = 'clickers90';
            $this->_table_class_name = str_replace('_', '', $this->_table_name);
        }

        public function find_by_email($email='') {
            $result_array = $this->find_by_sql("SELECT * FROM " . $this->_table_name . " WHERE email='" . $this->_db->escape_value($email) . "' LIMIT 1");
            return !empty($result_array) ? array_shift($result_array) : false;
        }

        function modify_clickers90($uid, $keyword, $loc) {

            $geocode_model = Util::load_model('geocodelocation');
            // 1) Should always be a valid location. Default to user's location
            //    if geocode and ip lookup returns nothing.
            // 2) get current clickers90 is_keyword_valid
            // 3) verify searched keyword valid or not
            // 4) If searched keyword valid update keyword and is_keyword_valid
            // 5) if searched keyword in-valid and existing in-valid or blank then update
            // 6) If no searched keyword do not update
            $valid_location = $geocode_model->verify_location($loc);

            $now_dt = date('Y-m-d');
            $now_dt_time = date('Y-m-d H:i:s');

            $update_keyword = '';
            $update_valid_flag = 0;
            $kw_update = true;

            $user_model = Util::load_model('user');

            $user = $user_model->find_by_id($uid);

            // Find clickers90 record by email
            $current_clicker = $this->find_by_email($user->email);

            // Set when to update the user's keyword in clickers90
            // For first time engagement from email will record with
            // passed in keyword. Every subsequent visit will only
            // update the clickers90 keyword if its in valid_keyword table
            if($keyword) {

                $valid_keyword_model = Util::load_model('validkeyword');

                // keyword is valid. always update clickers90 keyword
                if($is_valid = $valid_keyword_model->is_valid_keyword($keyword)) {
                    $update_keyword = $keyword;
                    $update_valid_flag = 1;
                } elseif(empty($current_clicker) ||
                        (!empty($current_clicker) && $current_clicker->is_keyword_valid != 1 &&
                            !empty($current_clicker->keyword))) {
                    //Keyword is invalid

                    // if first engagement or existing clicker with invalid
                    // keyword flag and keyword then update. Updates
                    // keyword and set valid flag to 0
                    $update_keyword = $keyword;
                    $update_valid_flag = 0;
                } else {
                    $kw_update = false;
                }

            } else {
                $kw_update = false;
            }

            // If cannot find valid location up to IP lookup then use user's
            // orginal location data
            if($valid_location === FALSE) {
                $location['city'] = $user->city;
                $location['postal'] = $user->postal_code;
            } else {
                $location['city'] = $valid_location['city'];
                $location['postal'] = $valid_location['postal'];
            }

            // Create new clickers90 record
            if(empty($current_clicker)) {
                // Only record if there is an actual location
                if(!empty($location['city']) || !empty($location['postal'])) {

                    $new_clicker = new Clickers90();
                    $new_clicker->user_id = $user->id;
                    $new_clicker->email = $user->email;
                    $new_clicker->first_name = $user->first_name;
                    $new_clicker->city = $location['city'];
                    $new_clicker->postal_code = $location['postal'];
                    $new_clicker->keyword = $update_keyword;
                    $new_clicker->is_keyword_valid = $update_valid_flag;
                    $new_clicker->partner = $user->partner;
                    $new_clicker->source = $user->source;
                    $new_clicker->date_opt_in = $user->date_opt_in;
                    $new_clicker->opt_in_ip = $user->opt_in_ip;
                    $new_clicker->date_updated = $now_dt;
                    $new_clicker->datetime_updated = $now_dt_time;
                    $new_clicker->date_added = $now_dt;
                    $new_clicker->create();

                }
                //Util::debug($new_clicker);
            } else {
                // Update clickers90

                if(!empty($location['city']) || !empty($location['postal'])) {
                    $current_clicker->city = $location['city'];
                    $current_clicker->postal_code = $location['postal'];
                }

                if($kw_update) {
                    $current_clicker->keyword = $update_keyword;
                    $current_clicker->is_keyword_valid = $update_valid_flag;
                }
                $current_clicker->date_updated = $now_dt;
                $current_clicker->datetime_updated = $now_dt_time;
                $current_clicker->update();

            }

        }

        public function count_clickers($days_old) {

            $today = date('Y-m-d');
            $sql = "SELECT COUNT(*) FROM " . $this->_table_name .
                   " WHERE date_updated > DATE('" . $today . "' - INTERVAL " . $days_old . " DAY)";
            $result_set = $this->_db->query($sql);
            $row = $this->_db->fetch_array($result_set);
            return array_shift($row);
        }

        public function count_mailable($days_old) {

            $today = date('Y-m-d');

            $sql = "SELECT COUNT(*) FROM " . $this->_table_name . " as c" .
                   " INNER JOIN user as u ON c.user_id=u.id WHERE" .
                   " c.date_updated > DATE('{$today}' - INTERVAL" .
                   " {$days_old} DAY) AND u.is_mailable = 1";

            $result_set = $this->_db->query($sql);
            $row = $this->_db->fetch_array($result_set);
            return array_shift($row);
        }

        public function count_valid_kw($days_old) {

            $today = date('Y-m-d');

            $sql = "SELECT COUNT(*) FROM " . $this->_table_name . " as c" .
                   " INNER JOIN user as u ON c.user_id=u.id WHERE" .
                   " c.date_updated > DATE('{$today}' - INTERVAL" .
                   " {$days_old} DAY) AND c.is_keyword_valid = 1 AND u.is_mailable = 1";

            $result_set = $this->_db->query($sql);
            $row = $this->_db->fetch_array($result_set);
            return array_shift($row);
        }

        public function delete_old_records($days_old) {

            $today = date('Y-m-d');
            $sql = "DELETE FROM " . $this->_table_name .
                   " WHERE date_updated < DATE('" . $today . "' - INTERVAL " . $days_old . " DAY)";
            $result_set = $this->_db->query($sql);
            return $this->_db->affected_rows();
        }


    }