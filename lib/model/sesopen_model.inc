<?php
    // vim: ai ts=4 sts=4 et sw=4 tw=79

    /*
     * Created on Dec 12, 2012
     * Written By Sung Hwan Ahn / Andy Nagai
     *
     */

    class SESOpenModel extends Model {

        function __construct() {

            $this->_db = Util::get_ses_db();
            $this->_table_name = 'open';
            $this->_table_class_name = 'ses' . str_replace('_', '', $this->_table_name);
        }

        public function get_total_opens($campaign_name, $search_text, $search_field, $start_date, $end_date) {

            $this->_db = Util::get_ses_db($campaign_name);

            $sql = "SELECT COUNT(DISTINCT user_id) FROM " . $this->_table_name;
            $sql .= $this->write_list_sql($search_text, $search_field, $start_date, $end_date);

            $result_set = $this->_db->query($sql);
            $row = $this->_db->fetch_array($result_set);
            return array_shift($row);
        }

        public function get_opens($campaign_name, $search_text, $search_field, $start_date, $end_date, $per_page, $offset) {

            $this->_db = Util::get_ses_db($campaign_name);

            $sql = "SELECT * FROM " . $this->_table_name;
            $sql .= $this->write_list_sql($search_text, $search_field, $start_date, $end_date);

            return $this->find_by_sql($sql . " GROUP BY user_id ORDER BY id DESC LIMIT {$per_page} OFFSET {$offset}");
        }

        private function write_list_sql($search_text, $search_field, $start_date, $end_date) {

            $sql = "";

            if ($start_date != "") {
                (strrpos($sql, "WHERE")) ? $sql .= " AND " : $sql .= " WHERE ";
                $sql .= "date_added >= '{$start_date}'";
            }

            if ($end_date != "") {
                (strrpos($sql, "WHERE")) ? $sql .= " AND " : $sql .= " WHERE ";
                $end_date = strftime ("%Y-%m-%d %H:%M:%S", strtotime('+1 day', strtotime($end_date)));
                $sql .= "date_added < '{$end_date}'";
            }

            if ($search_text != "" && $search_field == "email") {
                (strrpos($sql, "WHERE")) ? $sql .= " AND " : $sql .= " WHERE ";
                $sql .= "email LIKE '{$search_text}%'";
            }

            if ($search_text != "" && $search_field == "partner") {
                (strrpos($sql, "WHERE")) ? $sql .= " AND " : $sql .= " WHERE ";
                $sql .= "partner LIKE '{$search_text}'";
            }

            return $sql;
        }
    }