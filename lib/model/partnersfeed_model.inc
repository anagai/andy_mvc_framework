<?php
	// vim: ai ts=4 sts=4 et sw=4 tw=79

	/**
	 * Created on Oct 8, 2012
	 * Written By Sung Hwan Ahn / Andy Nagai
	 *
	 */
	class PartnersFeedModel extends Model {

		function __construct() {

		}

		public function clean_param_value($params) {

			foreach($params as $key=>$value) {

				$value = trim(stripslashes(urldecode($value)));

				// replace any spaces with plus for emails
				if($key=='email') {
					$value = str_replace(' ','+',$value);
				}

				if($key=='zip') {
					$value = strtoupper($value);
				}

				if($key=='city') {
					$value = ucwords($value);
				}

				$params[$key] = $value;
			}

			return $params;
		}

		public function check_required_params($params) {

			$required_fields = array("email", "ip", "date",	"pid");

			foreach ($required_fields as $required_field) {
				if (empty($params[$required_field])) {
					//Util::log_to_file('partners_feed.log', 'Co-reg Rejected',
					//	"field {$required_field} required. uri: {$_SERVER['REQUEST_URI']}");
					Util::log_to_file('partners_feed.log', 'Missing parameter',
						"param: {$required_field} uri: {$_SERVER['REQUEST_URI']}");
					exit("Rejected: Field '$required_field' required.");
				}
			}

		}
		public function validate_params($params, $reject_reasons) {
			// Failing any one validation returns reject reason key

			if (!filter_var($params['email'], FILTER_VALIDATE_EMAIL)) {
				// Rejection reason: 'Invalid email';
				Util::log_to_file('partners_feed.log', 'Co-reg Rejected',
					"{$reject_reasons['inv_email']['desc']} '{$params['email']}'");
				return 'inv_email';
				echo '<br>invalid email';
			}

			if (!filter_var($params['ip'], FILTER_VALIDATE_IP)) {
				// Rejection reason: 'Invalid IP'
				Util::log_to_file('partners_feed.log', 'Co-reg Rejected',
					"{$reject_reasons['inv_ip']['desc']} '{$params['ip']}', '{$params['email']}'");
				return 'inv_ip';
			}

			if (!preg_match("/^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/", $params["date"])) {
				// Rejection reason: 'Invalid date'
				Util::log_to_file('partners_feed.log', 'Co-reg Rejected',
					"{$reject_reasons['inv_dt']['desc']} '{$params['date']}', '{$params['email']}'");
				return 'inv_dt';
			}

			return TRUE;

		}

		public function create_coreg_partner($partner, $source, $subcode) {

			$coreg_partner_model = Util::load_model('coregpartner');
			$now_dt = date('Y-m-d H:i:s');

			// If new partner send email alert
			if(!$coreg_partner_model->get_status($partner)) {
				$coreg_partner_model->send_new_partner_alert($partner);
				$is_paid = $coreg_partner_model->check_paid_partner($partner);
			}

			$new_coreg_partner = new CoregPartner();
			$new_coreg_partner->partner = $partner;
			$new_coreg_partner->source = $source;
			$new_coreg_partner->subcode = $subcode;
			$new_coreg_partner->status = "active"; // defailt setting

			// check if partner is forward_to_leads
			$new_coreg_partner->forward_to_leads = ($coreg_partner_model->is_forward_to_leads($partner))
			? true
			: false;

			// check if is a paid partner
			$new_coreg_partner->is_paid = ($coreg_partner_model->is_paid($partner))
			? true
			: false;

			// For new partner
			if (!empty($is_paid)) {
				$new_coreg_partner->is_paid = true;
			}

			// Get partner status
			if($status = $coreg_partner_model->get_status($partner)) {
				$new_coreg_partner->status = $status;
			}

			$new_coreg_partner->date_created = $now_dt;
			$new_coreg_partner->date_updated = $now_dt;

			// Create new coreg partner record
			if ($new_coreg_partner->create()) {
				Util::log_to_file('partners_feed.log', 'New Coreg Partner', "New Coreg Partner ({$partner}), source ({$source}), and subcode ({$subcode}) created (ID: {$new_coreg_partner->id})");
			} else {
				//echo "Fail to create new coreg partner";
				Util::log_to_file('partners_feed.log', 'New Coreg Partner', "Failed to add new coreg partner ({$partner}), source ({$source}), and subcode ({$subcode})");
			}

		}

		public function set_location($params) {

			$geocode_model = Util::load_model('geocodelocation');
			$geocode_expansion_model = Util::load_model('geocodelocationexpansion');

			$invalid_city = '';
			$valid_postal = FALSE;

			//***** Validate city and postal code

			//-Andy 20130729, TODO: Also check if valid city in expansion table
			// If not in neither then add valid postal and city to expansion table
			// verify if a UK city
			if(!empty($params['city'])) {
				// If city has comma in it will have region. Should remove up to
				// comma
				$comma_pos = strpos($params['city'], ',');
				if($comma_pos !== FALSE) {
					$params['city'] = substr($params['city'],0,$comma_pos);
				}

				if(!$geocode_model->valid_city($params['city']) /*&&
					!$geocode_expansion_model->valid_city($params['city'])*/) {
					// If city not valid remove it
					$invalid_city = $params['city'];
					$params['city'] ='';
				}
			}

			// Extract outward portion of postal code and validate format
			if (!empty($params["zip"])) {

				// Only get outward portion of postal
				$postal_exp = explode(' ', $params["zip"]);
				$postal = $postal_exp[0];

				// Validate postal code format
				if(Util::valid_postal($postal)) {
					// if valid zip then do not use geocode by ip data
					//$use_geo_zip = false;
					$params['zip'] = $postal;
					$valid_postal = TRUE;
				} else {
					// Not valid postal format so remove it
					$params['zip'] = '';
				}

			}

			//***** Find Zip Section

			// Find zip by city if no zip
			if (empty($params['zip']) && !empty($params["city"]) &&
				$postal_code = $geocode_model->get_postal_by_city($params["city"])) {
				$params["zip"] = $postal_code;
			}

			// If zip still not found utilize Geocode Location Expansion data
			if (empty($params["zip"]) && !empty($params["city"]) &&
				$postal_code = $geocode_expansion_model->get_postal_by_city($params["city"])) {
				$params["zip"] = $postal_code;
			}

			//-Andy 20130729, TODO: If passed in city was invalid but postal
			// is valid format, then add location to expansion table here.
			//if(!empty($invalid_city) && $valid_postal === TRUE) {

			//}

			//***** Find City section

			// Find city by zip if no city. Also look in expansion table.
			if (!empty($params['zip']) && empty($params["city"])) {
				$params["city"] = $geocode_model->get_city_by_postal($params["zip"]);
				if(empty($params["city"])) {
					$params["city"] = $geocode_expansion_model->get_city_by_postal($params["zip"]);
				}
			}



			// Use geo lookup by IP only if no city and postal code
			if(empty($params['city']) && empty($params['zip'])) {

				$geocode = $geocode_model->get_loc_by_opt_in_ip($params['ip']);

				if($geocode !== false) {

					if(empty($params['city'])) {
						$params["city"] = $geocode['city'];
					}

					if(empty($params['zip'])) {
						$params["zip"] = $geocode['postal'];
					}
				}

			}

			return $params;

		}

		public function validate_dnm_partner_domain($params, $partner) {

			$blacklisted_domain_model = Util::load_model('blacklisteddomain');
			$coreg_partner_model = Util::load_model('coregpartner');
			$global_removal_model = Util::load_model('globalremoval');
			$bounce_model = Util::load_model('bounce');
			$complaint_model = Util::load_model('complaint');
			$unsubscribe_model = Util::load_model('unsubscribe');

			$email_exp = explode('@', $params['email']);
			$email_domain = array_pop($email_exp);

			$ret = true;

			if($blacklisted_domain_model->is_blacklisted_domain($email_domain)) {
				// Rejection reason: Blacklisted domain
				$err = "Email domain '{$params['email']}' is blacklisted.";
				Util::log_to_file('partners_feed.log', 'Co-reg Rejected', $err);
				$ret = 'is_black';
			}

			// if partner is inactive or pending then reject
			if (!$coreg_partner_model->is_active($partner)) {
				// Rejection reason: Partner inactive
				$err = "Partner '{$partner}' is inactive '{$params['email']}'.";
				Util::log_to_file('partners_feed.log', 'Co-reg Rejected', $err);
				$ret = 'partner_off';
			}

			// Reject if in global removal table
			if ($global_removal_model->has_this_email($params['email'])) {
				// Rejection reason: On global removal
				$err = "Email '{$params['email']}' on global removal list.";
				Util::log_to_file('partners_feed.log', 'Co-reg Rejected', $err);
				$ret = 'is_global';
			}

			// Reject if in complaint table across any site except asoftv
			if ($complaint_model->is_complaint_email($params['email'])) {
				// Rejection reason: On complaint
				$err = "Email '{$params['email']}' on complaint list.";
				Util::log_to_file('partners_feed.log', 'Co-reg Rejected', $err);
				$ret = 'is_complaint';
			}

			// Reject if unsubscribed for specific site
			if ($unsubscribe_model->is_unsubscribe_email($params['email'])) {
				// Rejection reason: On unsubscribe
				$err = "Email '{$params['email']}' on unsubscribe list.";
				Util::log_to_file('partners_feed.log', 'Co-reg Rejected', $err);
				$ret = 'is_unsub';
			}

			// Reject if hard bounce across any site except asoftv
			if ($bounce_model->is_bounced_email($params['email'])) {
				// Rejection reason: On bounce
				$err = "Email '{$params['email']}' on bounce list.";
				Util::log_to_file('partners_feed.log', 'Co-reg Rejected', $err);
				$ret = 'is_bounce';
			}

			// Reject if not top level domain
			if (Util::is_bad_TLD($params['email'])) {
				// Limit the domains for monetization reasons. Do not accept foreign domains because those users unlikely
				// to click on any job links.
				// Rejection reason: Bad top level domain
				$err = "Email '{$params['email']}' has a bad top level domain.";
				Util::log_to_file('partners_feed.log', 'Co-reg Rejected', $err);
				$ret = 'is_btld';
			}

			if($ret===true) {
				$tdUrl = "http://api.restorationmedia.net/mailable.php?e=" . rawurlencode($params['email']) . "&si=5&bc=1&td=1&bv=0";
				$out = Util::curlReq($tdUrl);
				if($out['status']!==200) {
					Util::log_to_file('partners_feed.log', 'TowerData Error',
						"http: {$out['status']} resp: {$out['response']}");
				} else {
					$respDec = json_decode($out['response'], true);
					if(isset($respDec['mailable'])) {
						if(!$respDec['mailable']) {
							$ret = 'is_td_bad';
						}
					}
				}
			}

			return $ret;

			//return TRUE;

		}

		public function get_reject_reasons() {
			$db = Util::get_dbo();
			$sql = "SELECT id, search_key, description FROM reject_reason";
			$result = $db->query($sql);
			$arr = array();
			while($row=$db->fetch_array($result)) {
				$arr[$row['search_key']] = array(
					'id' => $row['id'], 'desc' => $row['description']);
			}
			$result = null;
			return $arr;
		}

	}
