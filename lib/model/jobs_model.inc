<?php
    // vim: ai ts=4 sts=4 et sw=4 tw=79

    /*
     * Created on Oct 17, 2012
     * Written By Andy Nagai
     *
     */

    class JobsModel extends Model {

        function __construct() {

            // All models should have this variable set for default db
            $this->_db = Util::get_dbo();
            $this->setState('geocode_model', Util::load_model('geocodelocation'));

        }

        /**
         * Save search with result data to search_with_results table
         *
         */
        public function save_result_search() {
            $user = $this->getState('user');

            // Defaults
            $partner = 'JUO';
            $email = '';
            $source = '';
            $uid = 0;

            // Get user data
            if(!empty($user)) {

                if(!empty($user->partner)) {
                    $partner = $user->partner;
                }

                $email = $user->email;
                $source = $user->source;
                $uid = $user->id;
            }

            if($partner == 'JUO') {
                $source = 'jobungo.co.uk';
            }

            $page = $this->getState('page')
                ? $this->getState('page')
                : 1;

            // Find and validate city and postal code
            // NOTE 07/14/13 Andy: No longer recording city and postal
            //$loc = $this->parse_location($this->getState('location'));
            $search_engine = $this->getState('search_engine');

            if(empty($search_engine)) {
                $search_engine = 'indeed';
            }

            // Create search_with_results table record
            $search = new SearchWithResults();
            $search->user_id = $uid;
            $search->email = $email;
            $search->query = $this->getState('keyword');
            $search->location = $this->getState('location');
            $search->channel = $this->getState('channel');
            $search->ip = $_SERVER['REMOTE_ADDR'];
            $search->page_number = $page;
            $search->provider_calls = 1;
            $search->search_engine = $search_engine;
            $search->version_id = $this->getState('version_id');
            $search->daysback = $this->getState('days_back');
            $search->radius = $this->getState('radius');
            $search->user_agent = $_SERVER['HTTP_USER_AGENT'];
            $search->total_results = $this->getState('result_total');
            $search->results_on_page = $this->getState('page_total');
            $search->sub_id = $this->getState('sub_id');
            $search->partner = $partner;
            $search->source = $source;
            $search->date_searched = date('Y-m-d H:i:s');
            $search->save();

            //Util::debug($search);

        }

        /**
         * Save search with no result data to no_result_details table
         *
         */
        public function save_no_result_search() {

            $user = $this->getState('user');

            // Defaults
            $partner = 'JUO';
            $email = '';
            $source = '';
            $uid = 0;

            // Get user data
            if(!empty($user)) {

                if($user->partner) {
                    $partner = $user->partner;
                }

                $email = $user->email;
                $source = $user->source;
                $uid = $user->id;
            }

            if($partner == 'JUO') {
                $source = 'jobungo.co.uk';
            }

            // Find and validate city and postal code
            // NOTE 07/14/13 Andy: No longer recording city and postal
            //$loc = $this->parse_location($this->getState('location'));

            // Create no_result_details table record
            $search = new NoResultDetails();
            $search->user_id = $uid;
            $search->email = $email;
            $search->location = $this->getState('location');
            $search->query = $this->getState('keyword');
            $search->channel = $this->getState('channel');
            $search->ip = $_SERVER['REMOTE_ADDR'];
            $search->provider_calls = 1;
            $search->search_engine = 'indeed';
            $search->version_id = $this->getState('version_id');
            $search->daysback = $this->getState('days_back');
            $search->radius = $this->getState('radius');
            $search->user_agent = $_SERVER['HTTP_USER_AGENT'];
            $search->sub_id = $this->getState('sub_id');
            $search->partner = $partner;
            $search->source = $source;
            $search->date_searched = date('Y-m-d H:i:s');
            $search->save();

            //Util::debug($search);

        }

        // For testing regexp
        public function has_zip($location) {

            $location = strtoupper($location);

            $has_postal =
            // looks for A9 9AA, A99 9AA, AA9 9AA, AA99 9AA
            preg_match('/\b([A-Z]{1,2}\d{1,2})\b \b(\d{1}[A-Z]{2})\b/', $location, $match)
            // looks for A9A 9AA, AA9A 9AA
            || preg_match('/\b([A-Z]{1,2}\d{1}[A-Z]?)\b \b(\d{1}[A-Z]{2})\b/', $location, $match)
             // These two look for just the beginning outward part of postal code
            // looks for A9, A99, AA9, AA99
            || preg_match('/\b([A-Z]{1,2}\d{1,2})\b/', $location, $match)
            // looks for A9A, AA9A
            || preg_match('/\b([A-Z]{1,2}\d{1}[A-Z]?)\b/', $location, $match);

            Util::debug($match);

        }

        /**
         * Parse out and validate city and state from location string
         * NOTE 07/14/13 Andy: No longer validating location for search metrics.
         *
         * @param string $location
         * @return array
         */
        public function parse_location($location) {

            $ret['postal'] = '';
            $ret['city'] = '';

            $geocode_model = $this->getState('geocode_model');

            // 1. Convert to lowercase and remove whitespace
            $location = trim(strtolower(urldecode($location)));

            // 2. Replace any punctuation with space
            $location = preg_replace('/[^a-z0-9\-\s]/', " ", $location);

            // 3. Remove any extra space
            $location = Util::make_single_space($location);

            // 4. Check if it has postal code
            // UK postal code formats
            // A9 9AA
            // A99 9AA
            // AA9 9AA
            // AA99 9AA
            // A9A 9AA
            // AA9A 9AA
            //$match = array();

            // NOTE: use simpler Util method to validate postal.
            /*
            $has_postal =
            // looks for A9 9AA, A99 9AA, AA9 9AA, AA99 9AA
            preg_match('/\b([a-z]{1,2}\d{1,2})\b \b(\d{1}[a-z]{2})\b/', $location, $match)
            // looks for A9A 9AA, AA9A 9AA
            || preg_match('/\b([a-z]{1,2}\d{1}[a-z]?)\b \b(\d{1}[a-z]{2})\b/', $location, $match)
             // These two look for just the beginning outward part of postal code
            // looks for A9, A99, AA9, AA99
            || preg_match('/\b([a-z]{1,2}\d{1,2})\b/', $location, $match)
            // looks for A9A, AA9A
            || preg_match('/\b([a-z]{1,2}\d{1}[a-z]?)\b/', $location, $match);
            */

            // Only validate the outward part of postal code if two parts
            $loc_exp = explode(' ', $location);

            if( Util::valid_postal($loc_exp[0]) ) {

                $ret['postal'] = strtoupper($loc_exp[0]);

                // Find valid city name for this postal code
                $ret['city'] = $geocode_model->get_city_by_postal($ret['postal']);

            } else {

                // Validate city name
                if ($geocode_model->is_valid_city($location)) {
                    $ret['city'] = ucwords($location);
                }

                // Get postal code with valid city name
                if($ret['city']) {
                    $ret['postal'] = $geocode_model->get_postal_by_city($ret['city']);
                }

            }

            return $ret;
        }
    }