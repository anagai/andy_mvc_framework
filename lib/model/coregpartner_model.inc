<?php
	// vim: ai ts=4 sts=4 et sw=4 tw=79

	/**
	 * Created on Oct 12, 2012
	 * Written By Sung Hwan Ahn / Andy Nagai
	 *
	 */
	class CoregPartnerModel extends Model {

		function __construct() {

			$this->_db = Util::get_dbo();
			$this->_table_name = 'coreg_partner';
			$this->_table_class_name = str_replace('_', '', $this->_table_name);

		}

		// Checks if there is a partner with partner + source + subcode combination
		public function is_exist($partner, $source, $subcode) {

			$result_array = $this->find_by_sql("SELECT id FROM " . $this->_table_name .
					" WHERE partner='" . $this->_db->escape_value($partner) .
					"' AND source='" . $this->_db->escape_value($source) .
					"' AND subcode='" . $this->_db->escape_value($subcode) . "' LIMIT 1");
			return !empty($result_array) ? true : false;

		}

		public function get_status($partner) {

			$result_array = $this->find_by_sql("SELECT status FROM " . $this->_table_name .
					" WHERE partner='" . $this->_db->escape_value($partner) . "' LIMIT 1");

			if (!empty($result_array)) {
				$result = array_shift($result_array);
				return $result->status;
			} else {
				return false;
			}
		}

		public function send_new_partner_alert($partner) {

			/*
			Util::load_phpmailer();

			$mail = new PHPMailer();

			$mail->IsSMTP();
			//$mail->SMTPDebug = true; // for error checking
			$mail->Host = "173.204.123.41";
			$mail->Port = 25;

			$mail->FromName = "Jobungo UK";
			$mail->From = "mail@jobungo.co.uk";
			$mail->AddAddress("sung@restorationmedia.com", "Sung");
			//$mail->AddAddress("spencer@restorationmedia.com", "Spencer");
			//$mail->AddAddress("raul@restorationmedia.com", "Chance");
			//$mail->AddAddress("stephen@restorationmedia.com", "Stephen");
			//$mail->AddAddress("andy@restorationmedia.com", "Andy");
			//$mail->AddAddress("na@restorationmedia.com", "Na");
			$mail->Sender = STMP_RETURN;

			$mail->Subject = "[Jobungo UK] New Partner Added | " . $partner;
			$mail->IsHTML(true);

			// HTML Version
			$mail->Body = "New partner {$partner} added!";

			// Text Version
			$mail->AltBody = "New partner {$partner} added!";

			$result = $mail->Send();
			return $result;
			*/

			$to_array = array(
				"sung@restorationmedia.com",
				"spencer@restorationmedia.com",
				"raul@restorationmedia.com",
				"stephen@restorationmedia.com",
				"andy@restorationmedia.com",
				"na@restorationmedia.com");
			$from = "mail@jobungo.co.uk";
			$from_name = "Jobungo UK";

			$subject = "[Jobungo UK] New Partner Added | " . $partner;
			$body_text = "New partner {$partner} added!";;
			$body_html = "New partner {$partner} added!";

			$category = "jobungo_uk";

			Util::send_sendgrid_email($to_array, $from, $from_name, $subject, $body_text, $body_html, $category);
		}

		public function is_forward_to_leads($partner) {

			$result_array = $this->find_by_sql("SELECT id FROM " . $this->_table_name .
					" WHERE partner='" . $this->_db->escape_value($partner) .
					"' AND forward_to_leads = 1 LIMIT 1");
			return !empty($result_array) ? true : false;
		}

		public function forward_to_leads($params) {

			$leads_url = "http://67.21.116.28/partnersfeed.php?";

			// These values must stay in this particular sequence
			// Na's parser just takes in values sequentially
			// Na's parser also requires a source, so we're populating it here
			// with the Jobungo address.  -suy, 20111010

			$getForward = array(
					"pid" => $params[ "pid" ],
					"email" => $params[ "email" ],
					"fname" => $params[ "fname" ],
				"lname" => $params[ "lname" ],
				"addr1" => $params[ "address1" ],
				"addr2" => $params[ "address2" ],
				"city" => $params[ "city" ],
				"state" => $params[ "state" ],
				"zip" => $params[ "zip" ],
				"country" => $params[ "country" ],
				"gender" => $params[ "gender" ],
				"dob" => $params[ "birthday" ],
				"source" => $params[ "source" ],
				"ip" => $params[ "ip" ],
					"date" => $params[ "date" ],
			);
					$query_string = http_build_query( $getForward );
					$leads_url .= $query_string;

			// Submit the lead
			Util::curl_request($leads_url, false);

			/*
			$ch = curl_init();
			curl_setopt( $ch, CURLOPT_URL, $leads_url );
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 0 );
			curl_exec( $ch );
			curl_close( $ch );
			*/

		}

		public function is_paid($partner) {

			$result_array = $this->find_by_sql("SELECT id FROM " . $this->_table_name .
					" WHERE partner='" . $this->_db->escape_value($partner) .
					"' AND is_paid = 1 LIMIT 1");

			return !empty($result_array) ? true : false;
		}

		public function is_active($partner) {

			$result_array = $this->find_by_sql("SELECT id FROM " . $this->_table_name .
					" WHERE partner='" . $this->_db->escape_value($partner) .
					"' AND status = 'active' LIMIT 1");
			return !empty($result_array) ? true : false;
		}

		public function check_paid_partner($partner) {

			$url = "http://dashboard.restorationmedia.net/api/" .
				   "check_paid_partner.php" .
				   "?pid=" . urlencode($partner);

			$output = Util::curl_json_decode($url);

			return $output->{"Result"};
		}
	}
