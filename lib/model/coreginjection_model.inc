<?php

    // vim: ai ts=4 sts=4 et sw=4 tw=79

    /*
     * Created on Dec 3, 2012
     * Written By Sung Hwan Ahn / Andy Nagai
     *
     */

    class CoregInjectionModel extends Model {

        function __construct($inject_date) {

            // All models should have this variable set for default db
            $this->_db = Util::get_dbo();
            $this->_table_name = 'injection_' . $inject_date;
            $this->_table_class_name = 'coreginjection';
        }

        public function get_new($inject_date){
            return new CoregInjection($inject_date);
        }
    }
