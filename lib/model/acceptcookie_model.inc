<?php

    // vim: ai ts=4 sts=4 et sw=4 tw=79

    /*
     * Created on May 21, 2014
     * Written By Sung Hwan Ahn
     *
     */

    class AcceptCookieModel extends Model {

        function __construct() {

            // All models should have this variable set for default db
            $this->_db = Util::get_dbo();
            $this->_table_name = 'accept_cookie';
            $this->_table_class_name = str_replace('_', '', $this->_table_name);
        }
    }
