<?php
    // vim: ai ts=4 sts=4 et sw=4 tw=79

    /*
     * Created on Oct 8, 2012
     * Written By Sung Hwan Ahn / Andy Nagai
     *
     */

    class UserModel extends Model {

        function __construct() {

            // All models should have this variable set for default db
            $this->_db = Util::get_dbo();
            $this->_table_name = 'user';
            $this->_table_class_name = str_replace('_', '', $this->_table_name);

            // For remote db connections create a second db connection object
            // e.g. $_shasta_db = $_db->open_connection('shasta')
        }

        public function find_by_email($email='') {
            $result_array = $this->find_by_sql("SELECT * FROM " . $this->_table_name . " WHERE email='" . $this->_db->escape_value($email) . "'");
            return !empty($result_array) ? array_shift($result_array) : false;
        }

        public function find_by_id($uid=0) {
            $uid = (int)$uid;
            $result_array = $this->find_by_sql("SELECT * FROM " . $this->_table_name . " WHERE id=" . $this->_db->escape_value($uid) . " LIMIT 1");
            return !empty($result_array) ? array_shift($result_array) : false;
        }

        public function find_by_email_domain($email_domain) {
        	$result_array = $this->find_by_sql('SELECT * FROM ' . $this->_table_name .
        			' WHERE email_domain="' . $this->_db->escape_value($email_domain) . '"');
        	return !empty($result_array) ? $result_array : false;
        }

        public function update_as_engaged($uid=0) {
            $this->_db->query('UPDATE ' . $this->_table_name .
                " SET has_engaged=1 WHERE id={$uid}");
        }

        public function update_user_loc($user=0, $city='', $postal='') {
            $city =  $this->_db->escape_value($city);
            $postal =  $this->_db->escape_value($postal);
            $sql = "UPDATE " . $this->_table_name .
                " SET city='{$city}', postal_code='{$postal}' WHERE " .
                " id={$user}";
            $this->_db->query($sql);
        }


        // Update no loc coreg user with location
        public function update_noloc_coreg($user, $city='', $postal='') {
            $postal = $this->_db->escape_value($postal);
            $city = $this->_db->escape_value($city);
            $coreg = $user->coreg_id
                ? $user->coreg_id
                : 0;

            $sql = "UPDATE coreg SET city='{$city}', postal_code='{$postal}'
            WHERE id={$coreg} AND reject_reason_id=6
            ";
            $this->_db->query($sql);

            $dateAdded = date('Y-m-d' ,strtotime($user->date_added));

            // Update pivot location recovered counter
            $sql = "UPDATE coregPivotDaily SET NoLocRecovered=NoLocRecovered+1 WHERE Date='{$dateAdded}'";
            $this->_db->query($sql);

            $sql = "UPDATE coregPivotPartner SET NoLocRecovered=NoLocRecovered+1 WHERE Date='{$dateAdded}'
                AND Partner='{$user->partner}' AND Source='{$user->source}'
            ";
            $this->_db->query($sql);

        }

        public function find_by_salted_email($salted_email='') {

            $email_exp = explode(':', $salted_email);

            // If no salt then invalid email
            if(count($email_exp) < 2 || empty($email_exp[1])) {
                return false;
            }

            // Get email value
            $email = $this->_db->escape_value(array_shift($email_exp));

            // Get salt timestamp value and convert to date
            $salt = array_shift($email_exp);
            $user_dt = $this->_db->escape_value(date('Y-m-d H:i:s', $salt));

            $result_array = $this->find_by_sql("SELECT * FROM " . $this->_table_name . " WHERE
                    date_added = '" . $user_dt . "' AND  email='" . $email . "'");

            return !empty($result_array) ? array_shift($result_array) : false;
        }

        public function count_mailable() {

            $sql = "SELECT COUNT(*) FROM " . $this->_table_name . " WHERE is_mailable = 1";
            $result_set = $this->_db->query($sql);
            $row = $this->_db->fetch_array($result_set);
            return array_shift($row);
        }

        public function get_user(){
            return new User();
        }

        public function encryptRegKey() {
            $result = $this->_db->query("SELECT id FROM user where reg_key=''");
            while($row = $this->_db->fetch_array($result)) {
                $encryptU = Util::mcryptEncode($row['id']);
                $userClass = new User();
                $user = $userClass->find($row['id']);
                $user->reg_key = $encryptU;
                $user->update();
            }
        }

        public function addCoregId($user=0, $coreg=0) {
             $this->_db->query("UPDATE user SET coreg_id={$coreg} WHERE id={$user}");
        }
    }