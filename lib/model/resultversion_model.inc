<?php

	// vim: ai ts=4 sts=4 et sw=4 tw=79

	/*
	 * Created on Oct 8, 2012
	 * Written By Sung Hwan Ahn / Andy Nagai
	 *
	 */

	class ResultVersionModel extends Model {

		function __construct() {

			// All models should have this variable set for default db
			$this->_db = Util::get_dbo();
			$this->_table_name = 'result_version';
			$this->_table_class_name = str_replace('_', '', $this->_table_name);

			// For remote db connections create a second db connection object
			// e.g. $_shasta_db = $_db->open_connection('shasta')
		}

		public function find_by_traffic($traffic='organic') {
			$result_array = $this->find_by_sql("SELECT * FROM " . $this->_table_name .
				" WHERE page_type IN ('all', 'odd') AND traffic_type='" . $this->_db->escape_value($traffic) . "'" .
				" AND is_active = 1 AND weight > 0");
			return !empty($result_array) ? $result_array : false;
		}

		public function find_active_by_id($id = 0) {
			$result_array = $this->find_by_sql("SELECT * FROM " . $this->_table_name .
					" WHERE id = " . $id . 	" AND weight > 0 AND is_active = 1");
			return !empty($result_array) ? array_shift($result_array) : false;
		}

		/**
		 * Get version object by traffic and weight
		 *
		 *
		 */
		public function get_version_by_traffic_weight() {

			$browser = new Browser;
			//$_SESSION['uid'] = 'anagai2@yahoo.com';
			//unset($_SESSION['uid']);
			//Util::debug($browser);

			if($browser->is_mobile) {
				$result_versions = $this->find_by_traffic("mobile");
			} elseif(Util::is_session('uid')) {
				$result_versions = $this->find_by_traffic("email");
			} else {
				$result_versions = $this->find_by_traffic("organic");
			}

			if(empty($result_versions)) {
				$version = $this->find_by_id(1);
			} else {
				$version = $this->get_version_by_weight($result_versions);
			}

			//Util::debug($version);

			return !empty($version) ? $version : false;

		}

		/**
		 * Find jrp version to use. Considers if page is standard or alternating
		 *
		 *
		 */
		public function get_jrp_version($page = 0, $version_id = 0) {

			$browser = new Browser;
			//$_SESSION['uid'] = 'anagai2@yahoo.com';
			//unset($_SESSION['uid']);
			//Util::debug($browser);

			if($page == 0) {
				$page = 1;
			}

			// Use specific version if set from email traffic. This will
			// override any v=x passed in after user comes in from email.
			if(Util::is_session('uid_ver')) {
				$version_id = Util::get_session('uid_ver');
			}

			if(!$version_id) {

				if($browser->is_mobile) {
					$traffic_type = "mobile";
				} elseif(Util::is_session('fromemail')) {
					$traffic_type = "email";
				} else {
					$traffic_type = "organic";
				}

				// 07/02/13 Andy: If full_site switch on display email
				// version for users coming from email or organic.
				if(Util::get_session('full_site')) {
					if(Util::is_session('uid')) {
						$traffic_type = "email";
					} else {
						$traffic_type = "organic";
					}
				}

				$result_versions = $this->find_by_traffic($traffic_type);

				//Util::debug($result_versions);

				$page_type = array();

				// Some validations
				if(!empty($result_versions)) {

					// Populate array by page type
					foreach($result_versions as $version_item) {
						$type_key = strtolower($version_item->page_type);

						$page_type[$type_key][] = $version_item;

					}

					// If return both all and odd versions use default
					if(isset($page_type['all']) && isset($page_type['odd'])) {
						$version = $this->find_by_id(1);
					}

				} else {
					// If nothing found use default
					$version = $this->find_by_id(1);
				}

				//Util::debug($page_type);

				// Check if alternating version being used
				if(empty($version) && isset($page_type['odd'])) {

					// Pick an odd version by weight
					$odd_version = $this->get_version_by_weight($page_type['odd']);

					// Get the alternating even version odd version is using
					$even_version = $this->find_active_by_id($odd_version->alt_version_id);

					if(empty($even_version)) {
						// No valid even version found then use default version
						$version = $this->find_by_id(1);
					} else {

						// Valid alternating version found select version
						// based on current page number
						$is_even = $page % 2
							? false
							: true;

						if($is_even) {
							$version = $even_version;
						} else {
							$version = $odd_version;
						}

					}

				}

				// Will get version if all page type
				if(empty($version)) {
					$version = $this->get_version_by_weight($result_versions);
				}
			} else {
				// Get version by v passed in through url
				$version = $this->find_by_id($version_id);
			}

			//Util::debug($version);

			return !empty($version) ? $version : false;

		}

		public function get_jrp_version_by_traffic($traffic) {

			$result_versions = $this->find_by_traffic($traffic);

			if(!empty($result_versions)) {

				$version = $this->get_version_by_weight($result_versions);

				if(empty($version)) {
					$version = $this->find_by_id(1);
				}
			} else {
				$version = $this->find_by_id(1);
			}

			return $version;

		}


		/**
		 * Find version randomly by weight
		 *
		 */
		public function get_version_by_weight($jrp_versions) {

			$total_weight = 0;

			//Util::debug($jrp_versions);
			// get the total weight
			foreach ($jrp_versions as $version) {
				$total_weight += $version->weight;
			}

			// set the random range
			$random_version = mt_rand(1, $total_weight);

			// get a version by weight
			$add_rate = 0;
			foreach ($jrp_versions as $version) {
				if ($random_version <= ($version->weight + $add_rate))
					return $version;
				else
					$add_rate += $version->weight;
			}

			return false;
		}


		/**
		 * Retrieve components to compose a versioned page
		 * on version_test page
		 *
		 * @return string
		 */
		public function get_versionator_files() {

			$version_id = Util::getVar('v');

			// Get version components by url values

			if(!$version_id) {

				$header = Util::getVar('h');
				$css = Util::getVar('c');
				$body = Util::getVar('b');
				$footer = Util::getVar('f');

				// All component files must exists or kickout

				if(!$header || !$css || !$body || !$footer) {
					echo 'Must include either versionator id or all component ids';
					exit;
				}

				$header_path = JRP_HEADER_PATH . 'h' . $header . '.inc';
				if(file_exists($header_path)) {
					$components['header'] = $header_path;
				} else {
					echo 'Header file ' .$header_path . ' does not exist';
					exit;
				}

				$css_path = JRP_CSS_PATH . 'c' . $css . '.css';
				if(file_exists($css_path)) {
					//$_SESSION['component_css'] = $css_path;
					$components['css'] = 'c' . $css . '.css';
				} else {
					echo 'CSS file ' .$css_path . ' does not exist';
					exit;
				}

				$body_path = JRP_BODY_PATH . 'b' . $body . '.inc';
				if(file_exists($body_path)) {
					$components['body'] = $body_path;
				} else {
					echo 'Body file ' .$body_path . ' does not exist';
					exit;
				}

				$footer_path = JRP_FOOTER_PATH . 'f' . $footer . '.inc';
				if(file_exists($footer_path)) {
					$components['footer'] = $footer_path;
				} else {
					echo 'Footer file ' .$footer_path . ' does not exist';
					exit;
				}

				//Util::debug($components['css']);

			} else {

				// get components from versionator table using versionator id

				$version = $this->find_by_id($version_id);

				if(!$version->header_file || !$version->css_file || !$version->body_file || !$version->footer_file) {
					echo 'Not all component files specified in versionator record!';
					exit;
				}

				if(!empty($version)) {

					$header_path = JRP_HEADER_PATH . $version->header_file;
					if(file_exists($header_path)) {
						$components['header'] = $header_path;
					} else {
						echo 'Header file ' .$header_path . ' does not exist';
						exit;
					}

					$css_path = JRP_CSS_PATH . $version->css_file;
					if(file_exists($css_path)) {
						//$_SESSION['component_css'] = $css_path;
						$components['css'] = $version->css_file;
					} else {
						echo 'CSS file ' .$css_path . ' does not exist';
						exit;
					}

					$body_path = JRP_BODY_PATH . $version->body_file;
					if(file_exists($body_path)) {
						$components['body'] = $body_path;
					} else {
						echo 'Body file ' .$body_path . ' does not exist';
						exit;
					}

					$footer_path = JRP_FOOTER_PATH . $version->footer_file;
					if(file_exists($footer_path)) {
						$components['footer'] = $footer_path;
					} else {
						echo 'Footer file ' .$footer_path . ' does not exist';
						exit;
					}

				} else {
					echo 'Versionator record is not found!';
				}

			}

			// Have to return in this format
			$ret['components'] = $components;

			return $ret;


		}

		public function verify_versionator_components($version) {

			$header_path = JRP_HEADER_PATH . $version->header_file;
			if(file_exists($header_path)) {
				$components['header'] = $header_path;
			} else {
				echo 'Header file ' .$header_path . ' does not exist';
				exit;
			}

			$css_path = JRP_CSS_PATH . $version->css_file;
			if(file_exists($css_path)) {
				//$_SESSION['component_css'] = $css_path;
				$components['css'] = $version->css_file;
			} else {
				echo 'CSS file ' .$css_path . ' does not exist';
				exit;
			}

			$body_path = JRP_BODY_PATH . $version->body_file;
			if(file_exists($body_path)) {
				$components['body'] = $body_path;
			} else {
				echo 'Body file ' .$body_path . ' does not exist';
				exit;
			}

			$footer_path = JRP_FOOTER_PATH . $version->footer_file;
			if(file_exists($footer_path)) {
				$components['footer'] = $footer_path;
			} else {
				echo 'Footer file ' .$footer_path . ' does not exist';
				exit;
			}

			return $components;
		}

		public function get_all() {

			$sql = "SELECT * FROM result_version";

			$versions = $this->_db->query($sql);

			return $versions;

		}

	}