<?php

    // vim: ai ts=4 sts=4 et sw=4 tw=79

    /*
     * Created on Dec 3, 2012
     * Written By Sung Hwan Ahn / Andy Nagai
     *
     */

    class CoregModel extends Model {

        function __construct() {

            // All models should have this variable set for default db
            $this->_db = Util::get_dbo();
            $this->_table_name = 'coreg';
            $this->_table_class_name = str_replace('_', '', $this->_table_name);
        }

        public function count_accepted() {

            $sql = "SELECT COUNT(*) FROM " . $this->_table_name . " WHERE accepted = 1";
            $result_set = $this->_db->query($sql);
            $row = $this->_db->fetch_array($result_set);
            return array_shift($row);
        }

        public function count_mailable() {

            $sql = "SELECT COUNT(*) FROM " . $this->_table_name . " WHERE is_mailable = 1";
            $result_set = $this->_db->query($sql);
            $row = $this->_db->fetch_array($result_set);
            return array_shift($row);
        }
    }
