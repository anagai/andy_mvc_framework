<?php

    // vim: ai ts=4 sts=4 et sw=4 tw=79

    /*
     * Created on Dec 13, 2012
     * Written By Sung Hwan Ahn / Andy Nagai
     *
     */

    class InjectionBulkModel extends Model {

        function __construct() {

            // All models should have this variable set for default db
            $this->_db = Util::get_dbo();
            $this->_table_name = 'injection_bulk_2';
            $this->_table_class_name = 'injectionbulk';
        }

        public function find_by_email($email) {

            $result_array = $this->find_by_sql(
                "SELECT * FROM " . $this->_table_name .
                " WHERE email='" . $this->_db->escape_value($email) . "' LIMIT 1");
            return !empty($result_array) ? array_shift($result_array) : false;
        }

        public function find_recent_not_used ($start, $limit) {

            $result_array = $this->find_by_sql(
                "SELECT * FROM " . $this->_table_name .
                " WHERE is_used = 0" .
                " ORDER BY id DESC" .
                " LIMIT {$start} , {$limit}");
            return !empty($result_array) ? $result_array : false;
        }
    }
