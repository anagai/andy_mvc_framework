<?php

    /**
     * Created on Dec 10, 2012
     * Written By Sung Hwan Ahn / Andy Nagai
     *
     */

    // vim: ai ts=4 sts=4 et sw=4 tw=79

    class ResultModel extends Model {

        function __construct() {

            $this->_db = Util::get_dbo();
            $this->_table_name = 'search_with_results';
            $this->_table_class_name = str_replace('_', '', $this->_table_name);
        }

        // Added start and end date for the pagination
        public function get_daily_unique_visitor ($start_date, $end_date) {

            $next_date = strftime("%Y-%m-%d", strtotime($start_date .' +1 day'));

            $sql = "SELECT date(a.date_searched) as date_searched, count(distinct a.email) as total_results FROM " .
                "(SELECT email, date_searched from search_with_results " .
                "UNION SELECT email, date_searched from no_result_details) as a " .
                "WHERE date_searched < '{$next_date}' AND date_searched >= '{$end_date}' " .
                "group by date(a.date_searched) order by a.date_searched DESC";

            $result_array = $this->find_by_sql($sql);

            return !empty($result_array) ? $result_array : false;


        }

        public function count_result_search() {

            $this->_table_name = 'search_with_results';
            $this->_table_class_name = str_replace('_', '', $this->_table_name);
            return $this->count_all();
        }

        public function count_no_result_search() {

            $this->_table_name = 'no_result_details';
            $this->_table_class_name = str_replace('_', '', $this->_table_name);
            return $this->count_all();
        }

        public function count_job_clicks() {

            $this->_table_name = 'job_click';
            $this->_table_class_name = str_replace('_', '', $this->_table_name);
            return $this->count_all();
        }
    }
