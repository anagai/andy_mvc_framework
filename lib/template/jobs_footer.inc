<?php
// vim: ai ts=4 sts=4 et sw=4 tw=79
?>
	     </div>
    </div>
    <footer>
        <div class="widebar">
            <div class="container">
                <div class="sixteen columns row">
                    <div class="four columns alpha">
                        &nbsp;
                    </div>
                    <div class="nine columns">
                        <nav role="footer-nav">
                            <ul>
                                <li><a href="/unsubscribe">Manage Subscriptions</a></li>
                                <li><a href="/employers_contact">Employers</a></li>
                                <li><a href="/privacy">Privacy</a></li>
                                <li><a href="/faq">FAQ</a></li>
                                <li><a href="/about">About</a></li>
                                <li><a href="/contact">Contact</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div style="text-align: right; line-height: 1; margin-top: 12px;" class="three columns omega">
                        <span id='indeed_at'><a href="http://www.indeed.com/" target="_blank">jobs</a> by <a href="http://www.indeed.com/" title="Job Search" target="_blank"><img src="http://www.jobungo.com/images/jobsearch.png" style="border: 0; vertical-align: middle;" alt="Indeed job search"></a></span>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <?php
    if($view != 'recommended_jobs') {
    ?>
    <a class="recommended-jobs-fixed-a" href="/recommended_jobs">
        <div class="recommended-jobs-fixed">
            <div class="recommended-jobs-fixed-count">
                <span>10</span>
            </div>
            <div class="recommended-jobs-fixed-btn">
                <img src="/images/recommended-jobs-fixed.gif">
            </div>
        </div>
    </a>
    <?php
    }
    ?>
    <div id="slidebox">
        <a class="close"></a>
        <h2><a style="display: block;" href="#">There are <span>35,479<br />jobs remaining</span> in your search.</a></h2>
        <p><a class="slidebox-more" href="#">View your next job on page 2 &rarr;</a></p>
    </div>
    <div id="fb-root"></div>

    <div id="myModal" class="modal hide fade in" style="display:none;">
        <div class="modal-body">
            <p type="button" class="close" data-dismiss="modal">×</p>
            <p class="newsletter-signup-subhead">Activate daily job alerts</p>
            <h4>Sign up to receive Customer Service<br />jobs near Los Angeles, CA</h4>
            <input class="remove-bottom results-field" placeholder="Enter your email address" id="regularInput" type="text">
            <button class="full-width results-btn" type="submit">Get alerts!</button>
        </div>
    </div>

    <div id="popupModal" class="modal hide fade in" style="display:none;">
        <div class="modal-body">
            <p type="button" class="close" data-dismiss="modal">×</p>
            <p class="newsletter-signup-subhead">Activate daily job alerts</p>
                <h4>Sign up to receive <?=$html_safe_job?><br />jobs alerts</h4>
                <form action="#" method="post" onsubmit="" />
                <input type="hidden" id="modalPartner" name="partner" value="" />
                <input type="hidden" id="modalSource" name="source" value="" />
                <input type="hidden" id="modalSubcode" name="subcode" value="" />
                <input class="remove-bottom results-field" placeholder="Enter your email address" type="text" value="" name="modalEmail" id="modalEmail" onfocus="if (this.value=='username@example.com') this.value = ''" onblur="if (this.value=='') this.value ='username@example.com'"/>
                <button class="full-width results-btn" type="submit" value="Get Alerts!" onclick="">Get Alerts!</button>
                </form>
                <a class="viewposting" href="#" onclick=""><span class="right">skip &raquo;</span></a>
            </div>
        </div>
    </div>

    <div id="fb-root"></div>
    <script>
/*
   // Load facebook api
    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
 */
    </script>

    <!--
    <div id="fb-modal" class="modal hide fade in" style="display: block;">
        <div class="modal-body">
            <p type="button" class="close" data-dismiss="modal">×</p>
            <div class="fb-like-box" data-href="http://www.jobungo.com" data-width="368" data-show-faces="true" data-stream="false" data-header="true"></div>
        </div>
    </div>
-->
</body>
<script type="text/javascript" src="http://gdc.indeed.com/ads/apiresults.js"></script>
<script type="text/javascript" src="/js/scripts.js"></script>
<script type="text/javascript" src="/js/juk.js"></script>
<? require_once( VIEW_PATH.'metrics.inc' ); ?>
</html>
