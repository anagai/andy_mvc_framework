<?php
// vim: ai ts=4 sts=4 et sw=4 tw=79

	// Prepare job search strings in title and meta keyword
	
	// Build canonical url using canonical pattern set in Appliation->_define_routes()
	// for this view
	$canonical_url = $app->get_canonical_url();

	$result_version_model = Util::load_model('resultversion');

	// Use same method used on jobs view to get version.
	// jobs result css is used site wide depending on traffic type and weight.
	// For alternating versions always uses odd version for site css.
	$url_version_id = Util::getVar('v', '', 'GET', 'int');

	//if($fullsite_ver_id = Util::get_session('fullsite_version_id')) {
		// Use the full site version when user clicks on full site link from mobile version
		// This session variable set in jobs page when user clicks on 'full site' link
	//	$version = $result_version_model->find_by_id($fullsite_ver_id);
	//} else {
		// Use version override or find version normally by traffic and weight
		$version = $result_version_model->get_jrp_version(0, $url_version_id);
	//}

	// The actual page title

	if($page_title = $app->get_page_title()) {
		$page_title .= ' | ';
	} else {
		$page_title = '';
	}

	//Util::debug('version: ' . $version->id);

?>
<!DOCTYPE html>
<html>
<head xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml">
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="description" content="Search for thousands of jobs from around your local area. Use Jobungo's articles and resources to aid in your job search and further your career. Jobungo - Search. Find. Hired." />
<meta name="keywords" content="job search, search for jobs, job website, local jobs, job searching, careers, employment, job search engine, part time employment" />
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Jobungo UK</title>
<link rel="canonical" href="<?= $canonical_url ?>"/>
<link rel="shortcut icon" href="<?= Util::base_url() ?>/favicon.ico">
<link rel="stylesheet" href="/css/versionator_css.php?css=<?= $version->css_file ?>">
<link rel="stylesheet" href="/css/jquery-ui.css">
<script type="text/javascript" src="/js/jquery.min.js"></script>
<!-- ver = <?= $version->id ?>  -->
</head>
<div class="cookie-msg" id="msg_cookies_use">
    By using our services, you agree to our use of cookies. <a href="/cookies">Learn More</a>. <a href="javascript:void(0)" class="accept" id="lnk_cookie_yes">Accept cookies</a>
</div>
<script type="text/javascript">
	$(".close-cta").click(function() {
		$(".cookie-msg").remove();
	});
</script>
<body class="body-home">
<div class="header-banner">
	<div class="container-home">
		<div class="sixteen columns">
			<!--<ul>
				<li><a class="top-nav-resources" href="#">Resources</a></li>
			</ul>
			-->
		</div>
	</div>
</div>
<div class="container-home">
	<header class="sixteen columns">
		<div class="eight columns alpha">
			<h1><a class="logo-home" href="/home">Jobungo</a></h1>
		</div>
		<div class="eight columns omega search-jobs-home">
			<!--<p>Search 36,320 jobs near Los Angeles, CA</p>-->
		</div>
	</header>
</div>
