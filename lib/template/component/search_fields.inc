<?php 

	$out['keyword'] = isset($out['keyword']) 
		? $out['keyword'] 
		: ''; 
	
	$out['location'] = isset($out['location'])
		? $out['location']
		: '';
	
?>
			<script type="text/javascript">
				function dismissTip(id){
					if(document.getElementById(id)!=null){
						document.getElementById(id).style.opacity='0';
						setTimeout(function(){document.getElementById(id).style.display='none';},500);
					}
				}
			</script>
			<div class="seven columns alpha" style="position: relative;">
				<label style="color:#444;font-size: 18px; margin-bottom: 5px;">Keywords <span>Job Title, Skills, or Company</span></label>
				<input id="keyword" type="text" name="q" value="<?= $out['keyword'] ?>" onchange="dismissTip('tipKeywords');" onkeypress="dismissTip('tipKeywords');" tabindex="1">
				<div id="tipKeywords" onclick="dismissTip(this.id);">Enter a keyword to improve your search results</div>
			</div>
			<div class="seven columns" style="position: relative;">
				<label style="color:#444;font-size: 18px; margin-bottom: 5px;">Location <span>City or Postcode</span></label>
				<input id="location" type="text" name="l" value="<?= $out['location'] ?>" onchange="dismissTip('tipLocation');" onkeypress="dismissTip('tipLocation');" tabindex="2">
			</div>
			<div class="two columns omega">
				<label style="font-size: 18px; margin-bottom: 5px;">&nbsp;</label>
				<button class="full-width" type="submit" tabindex="3">Search</button>
			</div>