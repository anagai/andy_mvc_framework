<?php
// vim: ai ts=4 sts=4 et sw=4 tw=79
?>
	<footer class="row">
		<div class="container-home  container-footer row">
			<div class="eleven columns">
				<nav role="footer-nav">
				<ul>
					<li><a href="/unsubscribe">Manage Subscriptions</a></li>
					<li><a href="/employers_contact">Employers</a></li>
					<li><a href="/privacy">Privacy</a></li>
					<li><a href="/about">About</a></li>
					<li><a href="/faq">FAQ</a></li>
					<li><a href="/contact">Contact</a></li>
				</ul>
				</nav>
			</div>
			<div class="footer-slogan five columns omega" id="fb_panel" >
				<div id="fb-root"></div>
				<div class="fb-like" data-href="http://www.facebook.com/jobungo" data-send="false" data-width="300" data-show-faces="true"></div>
			</div>
		</div>
	</footer>
</body>
<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="/js/scripts.js"></script>
<script type="text/javascript" src="/js/juk.js"></script>
<script type="text/javascript" src="/js/jquery.cookie.js"></script>
<?php
$app->include_scripts();
?>
<script type="text/javascript">

	if ($.cookie('juk_cookies_use')) {
		$("#msg_cookies_use").hide();
	}

	$("#lnk_cookie_yes").click(function() {
		$.cookie('juk_cookies_use', 'yes', { path: '/' });
		$("#msg_cookies_use").hide();

		$.post("/ajax/jobs_ajax.php", {
			method: 'accept_cookie'},
		function(response) {
			if(response.status == 'success') {
				// On the cookies page, redirect to job result page
				var url = window.location.href;
				var host = window.location.host;
				if (url.indexOf('http://' + host + '/cookies') != -1) {
					window.location.href = 'http://' + host + '/jobs';
				}
			}
		}, "json"
		).error(function() {});
	});
</script>
<? require_once( VIEW_PATH.'metrics.inc' ); ?>
</html>
