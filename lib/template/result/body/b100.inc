
<div class="cookie-msg-mobile" id="msg_cookies_use">
    By using our services, you agree to our use of cookies. <a href="/cookies">Learn More</a>. <a href="javascript:void(0)" class="accept" id="lnk_cookie_yes">Accept cookies</a>
</div>
<script type="text/javascript">
    $(".close-cta").click(function() {
        $(".cookie-msg").remove();
    });
</script>

<?php

	function job_entry_template($job, $index) {
		if($job['feed']=='adzuna') {
			$adzunaClass = ' adz-job-mobile';
		} else {
			$adzunaClass = '';
		}
	?>

		<article class="mobile-job-entry<?php echo $adzunaClass?>">
			<h5><a target="_blank" class="lnk_job" href="<?=$job["link"]?>" id="lnk_job_<?= $index ?>" onmousedown="<?=$job["onmousedown"]?>"
				data-key="<?=$job['id']?>"
                                data-title="<?=$job['title']?>"
				data-url="<?=$job['link']?>"
                                data-company="<?=$job['company']?>"
                                data-location="<?=$job['city']?>"
                                data-position="<?=$index?>"
                                data-created="<?=$job['ago']?>"
                                data-type="<?=$job['sponsored'] ? 'sponsored' : 'organic'?>"
			><?= $job["title"] ?></a>
			<?php

        	if ($job['ago'] == "1 day ago" || strrpos($job['ago'], "hour")) {
				?>
			<?php
			}

			if(isset($job['featured'])) {
				?><span class="label">FEATURED</span><?php
			}

			$cocty = ucwords($job["company"]);

			if(!empty($cocty) && !empty($job["city"])) {
				$cocty .= ' &mdash; ';
			}

			$cocty .= $job["city"];

			?>
        	</h5>
			<p class="job-entry-meta"><strong><?php echo $cocty ?> <span style="color: #aaa;"><?= $job["ago"]?></span></strong></p>
			<p><?= $job["description"] ?>  <a target="_blank" class="lnk_more_job" href="<?=$job["link"]?>" id="lnk_more_job_<?= $index ?>" onmousedown="<?=$job["onmousedown"]?>">More&raquo;</a></p>
    		<input type="hidden" id="result_key_<?= $index?>" value="<?= $job['id']?>" />
			<input type="hidden" id="result_title_<?= $index?>" value="<?= $job['title']?>" />
			<input type="hidden" id="result_company_<?= $index?>" value="<?= $job['company']?>" />
			<input type="hidden" id="result_location_<?=$index?>" value="<?= $job['city'] ?>" />
			<input type="hidden" id="result_position_<?=$index?>" value="<?= $index?>" />
			<input type="hidden" id="result_age_<?=$index?>" value="<?= $job['ago']?>" />
			<input type="hidden" id="result_sponsored_<?=$index?>" value="<?= $job['sponsored']?>" />
			<input type="hidden" id="result_search_engine_<?=$index?>" value="<?= $job['feed']?>" />
		</article>

	<?php
	}

	$adsPerSlot[1] = 3;
	$adsPerSlot[2] = 3;
	//$adsPerSlot[3] = 3;

	$adCount = 0;
	function ad_template($ad_id, $kw='', $loc='', &$adCount, $pageNum=1, $adsPerSlot) {
		$adCount++;
		$adBlockId = "ad-container-{$adCount}";
		if(empty($pageNum)) {
			$pageNum=1;
		}
		$numOfAds = isset($adsPerSlot[$adCount])
			? $adsPerSlot[$adCount]
			: 1;
	?>
			<article class="mobile-job-entry-ad">
					<div class="sponsored-links">Sponsored Links</div>
					<div id="<?php echo $adBlockId; ?>" class="iframe-ad">
						<script type="text/javascript"><!--
							var pageOptions = {
						        'pubId': 'pub-2202016470033432',
						        'query': '<?php echo $kw . "+jobs+" . $loc ?>',
						        'adPage': <?php echo $pageNum; ?>,
						        'channel': '8378707705',
						        'hl': 'en',
						        'linkTarget': '_blank',
						        'location': false,
						        'plusOnes': false,
						        'sellerRatings': false,
						        'siteLinks': false
						    };
						    var adblock_<?php echo $adCount ?> = {
						        'container': '<?php echo $adBlockId ?>',
						        'number': <?php echo $numOfAds; ?>,
						        'width': '320px',
						        'colorTitleLink': '#1223cc',
						        'colorDomainLink': '#1223cc',
						        'colorBackground': '#fffae7',
						        'lineHeightTitle' : 10,
						        'lineHeightDescription' : 10,
						        'lineHeightDomainLink' : 10,
						        'verticalSpacing' : 2,
						        'colorAdBorder': '#fffae7',
						        'colorAdSeparator' : '#eee',
						        'colorBorder': '#fffae7'
						    };

							//-->
						</script>
					</div>
				</article>
	<?php
	}
?>
			<div class="sixteen columns job-entries">
				<input type="hidden" id="version_id" value="<?php echo $out['version_id'] ?>">
    			<!-- Version: v1_20121024_organic<br /><br />-->

<?php
	if (isset($out["jobs"]) && $out["page_total"] > 0) {

    	$index = 1;

		foreach($out["jobs"] as $job) {

			if($job['id'] == 'ad') {
				ad_template($job['ad_id'], $out['keyword'], $out['location'], $adCount, $out['page'], $adsPerSlot);
			} else {
				job_entry_template($job, $index);
			}
			$index++;
		}

		if (isset($out["pagination"])) {
			$out["pagination"]->display();
		}

		for($x=1;$x<=$adCount;$x++) {
			$doAdds[] = 'adblock_' . $x;
		}

		if(count($doAdds)>0) {
			$addVarStr = implode(',', $doAdds);
		} else {
			$addVarStr = "''";
		}

		?>

		<script type="text/javascript">
			// Initialize google csa ads
			_googCsa('ads', pageOptions, <?php echo $addVarStr ?>);
		</script>

		<?php

	} else {
		echo "No Job Found...<br />";
	}
?>
            <!--
            <div class="pagination">
				<ul>
					<li><a href="#collapse1">Previous Page</a></li>
					<li><a href="#collapse1">Next Page</a></li>
				</ul>
			</div>
			-->

			<div style="margin-top: 80px; position: relative; text-align: center;">
				<script id="mNCC" language="javascript">
				    medianet_width = "260";
				    medianet_height = "200";
				    medianet_crid = "817552167";
				    medianet_versionId = "111299";
				    (function() {
				        var isSSL = 'https:' == document.location.protocol;
				        var mnSrc = (isSSL ? 'https:' : 'http:') + '//contextual.media.net/nmedianet.js?cid=8CU300JG9' + (isSSL ? '&https=1' : '');
				        document.write('<scr' + 'ipt type="text/javascript" id="mNSC" src="' + mnSrc + '"></scr' + 'ipt>');
				    })();
				</script>
			</div>
