<?php

	function job_entry_template($job, $index) {
		if($job['feed']=='adzuna') {
			$adzunaClass = ' adz-job';
		} else {
			$adzunaClass = '';
		}
	?>

				<article class="job-entry<?php echo $adzunaClass?>">
                        <h5><a target="_blank" class="lnk_job" href="<?=$job["link"]?>" id="lnk_job_<?= $index ?>" onmousedown="<?=$job["onmousedown"]?>"
				data-key="<?=$job['id']?>"
				data-title="<?=$job['title']?>"
				data-url="<?=$job['link']?>"
				data-company="<?=$job['company']?>"
				data-location="<?=$job['city']?>"
				data-position="<?=$index?>"
				data-created="<?=$job['ago']?>"
				data-type="<?=$job['sponsored'] ? 'sponsored' : 'organic'?>"
			><?= $job["title"] ?></a>
                        <?php

		            	if ($job['ago'] == "1 day ago" || strrpos($job['ago'], "hour")) {
							?>
						<span class="label">NEW</span>
						<?php
						}

						if(isset($job['featured'])) {
							?><span class="label">FEATURED</span><?php
						}

						$cocty = ucwords($job["company"]);

						if(!empty($cocty) && !empty($job["city"])) {
							$cocty .= ' &mdash; ';
						}

						$cocty .= $job["city"];

						?>
		            	</h5>
						<p class="job-entry-meta"><?php echo $cocty ?> <span><?= $job["ago"]?></span></p>
						<p><?= $job["description"] ?>  <a target="_blank" class="lnk_more_job" href="<?=$job["link"]?>" id="lnk_more_job_<?= $index ?>" onmousedown="<?=$job["onmousedown"]?>">More&raquo;</a></p>

		        		<input type="hidden" id="result_key_<?= $index?>" value="<?= $job['id']?>" />
					<input type="hidden" id="result_title_<?= $index?>" value="<?= $job['title']?>" />
					<input type="hidden" id="result_company_<?= $index?>" value="<?= $job['company']?>" />
					<input type="hidden" id="result_location_<?=$index?>" value="<?= $job['city'] ?>" />
					<input type="hidden" id="result_position_<?=$index?>" value="<?= $index?>" />
					<input type="hidden" id="result_age_<?=$index?>" value="<?= $job['ago']?>" />
					<input type="hidden" id="result_sponsored_<?=$index?>" value="<?= $job['sponsored']?>" />
					<input type="hidden" id="result_search_engine_<?=$index?>" value="<?= $job['feed']?>" />
				</article>

	<?php
	}

	$adsPerSlot[1] = 3;
	$adsPerSlot[2] = 3;
	//$adsPerSlot[3] = 2;

	$adCount = 0;
	function ad_template($ad_id, $kw='', $loc='', &$adCount, $pageNum=1, $adsPerSlot) {
		$adCount++;
		$adBlockId = "ad-container-{$adCount}";
		if(empty($pageNum)) {
			$pageNum=1;
		}
		$numOfAds = isset($adsPerSlot[$adCount])
			? $adsPerSlot[$adCount]
			: 1;
	?>
				<article class="job-entry-ad" style="padding: 0 10px 0 10px;">
					<div class="sponsored-link">Sponsored Links</div>
					<div>
						<div id="<?php echo $adBlockId; ?>" class="iframe-ad">
							<script type="text/javascript"><!--
								var pageOptions = {
							        'pubId': 'pub-2202016470033432',
							        'query': '<?php echo $kw . "+jobs+" . $loc ?>',
							        'adPage': <?php echo $pageNum; ?>,
							        'channel': '8378707705',
							        'hl': 'en',
							        'linkTarget': '_blank',
							        'location': false,
							        'plusOnes': false,
							        'sellerRatings': false,
							        'siteLinks': false
							    };
							    var adblock_<?php echo $adCount ?> = {
							        'container': '<?php echo $adBlockId ?>',
							        'number': <?php echo $numOfAds; ?>,
							        'width': '100%',
							        'colorTitleLink': '#1223cc',
							        'colorDomainLink': '#1223cc',
							        'colorBackground': '#fffae7',
							        'lineHeightTitle' : 10,
							        'lineHeightDescription' : 10,
							        'lineHeightDomainLink' : 10,
							        'verticalSpacing' : 2,
							        'colorAdBorder': '#fffae7',
							        'colorAdSeparator' : '#eeeeee',
							        'colorBorder': '#fffae7'
							    };
								//-->
							</script>
						</div>
					</div>
				</article>
	<?php
	}

?>
			<div class="sixteen columns offset-by-four alpha job-entries">
				<input type="hidden" id="version_id" value="<?php echo $out['version_id'] ?>">
				<br />
				<!--
				<article>
    				<div class="newsletter-signup-top"><a data-toggle="modal" href="#myModal"><span>Click here</span> to signup for email alerts with  jobs  near <?= $out['location'] ?></a></div>
    			</article>
    			-->
    			<!-- Version: v1_20121024_organic<br /><br />-->

<?php
	if (isset($out["jobs"]) && $out["page_total"] > 0) {

    	$index = 1;

		foreach($out["jobs"] as $job) {

			if($job['id'] == 'ad') {
				ad_template($job['ad_id'], $out['keyword'], $out['location'], $adCount, $out['page'], $adsPerSlot);
			} else {
				job_entry_template($job, $index);
			}
			$index++;
		}

		if (isset($out["pagination"])) {
			$out["pagination"]->display();
		}

		for($x=1;$x<=$adCount;$x++) {
			$doAdds[] = 'adblock_' . $x;
		}

		if(count($doAdds)>0) {
			$addVarStr = implode(',', $doAdds);
		} else {
			$addVarStr = "''";
		}
		?>
		<script type="text/javascript">
			// Initialize google csa ads
			_googCsa('ads', pageOptions, <?php echo $addVarStr ?>);
		</script>
		<?php

	} else {
		echo "No Job Found...<br />";
	}
?>

			<div style="margin-top: 18px;">
				<script id="mNCC" language="javascript"> medianet_width='740'; medianet_height= '200'; medianet_crid='222412343'; medianet_hint='<?=$out['keyword']?>'; </script> <script id="mNSC" src="http://contextual.media.net/nmedianet.js?cid=8CU300JG9" language="javascript"></script>
			</div>

            </div>
