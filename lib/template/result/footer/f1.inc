<?php
// vim: ai ts=4 sts=4 et sw=4 tw=79
?>
		</div>
    </div>
    <footer>
        <div class="widebar">
            <div class="container">
                <div class="sixteen columns row">
                    <div class="four columns alpha">
                        &nbsp;
                    </div>
                    <div class="nine columns">
                        <nav role="footer-nav">
                            <ul>
                                <li><a href="/unsubscribe">Manage Subscriptions</a></li>
                                <li><a href="/employers_contact">Employers</a></li>
                                <li><a href="/privacy">Privacy</a></li>
                                <li><a href="/faq">FAQ</a></li>
                                <li><a href="/about">About</a></li>
                                <li><a href="/contact">Contact</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div style="text-align: right; line-height: 1; margin-top: 12px;" class="three columns omega">
                        <span id='indeed_at'><a href="http://www.indeed.co.uk/" target="_blank">jobs</a> by <a href="http://www.indeed.co.uk/" title="Job Search" target="_blank"><img src="http://www.jobungo.co.uk/images/jobsearch.png" style="border: 0; vertical-align: middle;" alt="Indeed job search"></a></span>
                    </div>
                    <div class="four columns alpha">
                        &nbsp;
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <div id="fb-root"></div>
    <!--
    <div id="myModal" class="modal hide fade in" style="display:none;">
        <div class="modal-body">
            <p type="button" class="close" data-dismiss="modal">×</p>
            <p class="newsletter-signup-subhead">Activate daily job alerts</p>
            <h4>Sign up to receive <?= $out['keyword'] ?> jobs<?php if($out['location']) echo '<br />near ' . $out['location'] ?></h4>
            <input class="remove-bottom results-field" placeholder="Enter your email address" id="txt_popup_signup_email" type="text">
            <div class="uk-compliance-joke">
                Jobungo collects your information when you register with us or request our services. We will use this information to provide the services you requested. Please contact us via our contact form if you require any further information. If you agree to Jobungo sending you marketing information, please tick this box: <input type="checkbox" id="chkagree">
            </div>
            <button class="full-width results-btn" type="submit" onclick="Juk.popup_alert_signup()">Get alerts!</button>
        </div>
    </div>
    -->
    <!--
    <div id="popupModal" class="modal hide fade in" style="display:none;">
        <div class="modal-body">
            <p type="button" class="close" data-dismiss="modal">×</p>
            <p class="newsletter-signup-subhead">Activate daily job alerts</p>
                <h4>Sign up to receive <?= $out['location'] ?><br />jobs alerts</h4>
                <form action="#" method="post" onsubmit="" />
                <input type="hidden" id="modalPartner" name="partner" value="" />
                <input type="hidden" id="modalSource" name="source" value="" />
                <input type="hidden" id="modalSubcode" name="subcode" value="" />
                <input class="remove-bottom results-field" placeholder="Enter your email address" type="text" value="" name="modalEmail" id="modalEmail" onfocus="if (this.value=='username@example.com') this.value = ''" onblur="if (this.value=='') this.value ='username@example.com'"/>
                <button class="full-width results-btn" type="submit" value="Get Alerts!" onclick="">Get Alerts!</button>
                </form>
                <a class="viewposting" href="#" onclick=""><span class="right">skip &raquo;</span></a>
          </div>
     </div>
    -->
    </div>

    <script>
/*
   // Load facebook api
    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
 */
    </script>

    <!--
    <div id="fb-modal" class="modal hide fade in" style="display: block;">
        <div class="modal-body">
            <p type="button" class="close" data-dismiss="modal">×</p>
            <div class="fb-like-box" data-href="http://www.jobungo.com" data-width="368" data-show-faces="true" data-stream="false" data-header="true"></div>
        </div>
    </div>
-->
<script type="text/javascript" src="http://gdc.indeed.com/ads/apiresults.js"></script>
<script type="text/javascript" src="/js/scripts.js?time=<?= filemtime(PROJECT_PATH . 'public/js/scripts.js') ?>"></script>
<script type="text/javascript" src="/js/juk.js?time=<?= filemtime(PROJECT_PATH . 'public/js/juk.js') ?>"></script>
<script type="text/javascript" src="/js/jquery.cookie.js"></script>

<script type="text/javascript">

    if ($.cookie('juk_cookies_use')) {
        $("#msg_cookies_use").hide();
    }

    $("#lnk_cookie_yes").click(function() {
        $.cookie('juk_cookies_use', 'yes', { path: '/' });
        $("#msg_cookies_use").hide();

        $.post("/ajax/jobs_ajax.php", {
            method: 'accept_cookie'},
        function(response) {
            if(response.status == 'success') {
                // On the cookies page, redirect to job result page
                var url = window.location.href;
                var host = window.location.host;
                if (url.indexOf('http://' + host + '/cookies') != -1) {
                    window.location.href = 'http://' + host + '/jobs';
                }
            }
        }, "json"
        ).error(function() {});
    });
</script>
<? require_once( VIEW_PATH.'metrics.inc' ); ?>
</body>
</html>
