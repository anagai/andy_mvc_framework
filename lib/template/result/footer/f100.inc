<?php
// vim: ai ts=4 sts=4 et sw=4 tw=79
?>
<br />
            <footer class="mobile-footer">
                <!--<p class="mobile-page-count">Jobs 1 - 10 of 34,201</p>-->
                <p class="mobile-indeed"><a href="#">Jobs by <img src="http://www.jobungo.co.uk/images/jobsearch.png"></a></p>
                <p>Copyright &copy; Jobungo UK <?= date("Y") ?> &mdash; <a href="/jobs?full=1">Full Site</a></p>
                <ul>
                    <li><a href="/privacy">Privacy Policy</a></li>
                    <li><a href="/unsubscribe">Manage Subscriptions</a></li>
                    <li><a href="/contact">Contact Us</a></li>
                </ul>
            </footer>
        </div>
    </div><!-- container -->
<script type="text/javascript" src="http://gdc.indeed.com/ads/apiresults.js"></script>
<script type="text/javascript" src="/js/scripts.js?time=<?= filemtime(PROJECT_PATH . 'public/js/scripts.js') ?>"></script>
<script type="text/javascript" src="/js/mobile.js?time=<?= filemtime(PROJECT_PATH . 'public/js/mobile.js') ?>"></script>
<script type="text/javascript" src="/js/juk.js?time=<?= filemtime(PROJECT_PATH . 'public/js/juk.js') ?>"></script>
<script type="text/javascript" src="/js/jquery.cookie.js"></script>
<script type="text/javascript">

    if ($.cookie('juk_cookies_use')) {
        $("#msg_cookies_use").hide();
    }

    $("#lnk_cookie_yes").click(function() {
        $.cookie('juk_cookies_use', 'yes', { path: '/' });
        $("#msg_cookies_use").hide();

        $.post("/ajax/jobs_ajax.php", {
            method: 'accept_cookie'},
        function(response) {
            if(response.status == 'success') {
                // On the cookies page, redirect to job result page
                var url = window.location.href;
                var host = window.location.host;
                if (url.indexOf('http://' + host + '/cookies') != -1) {
                    window.location.href = 'http://' + host + '/jobs';
                }
            }
        }, "json"
        ).error(function() {});
    });
</script>
<? require_once( VIEW_PATH.'metrics.inc' ); ?>
</body>
</html>
