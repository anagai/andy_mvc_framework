<?php
// vim: ai ts=4 sts=4 et sw=4 tw=79

	$page_title = $app->make_search_title();

	$meta_keyword_string = $app->make_meta_keyword_string();

	// Build canonical url using canonical pattern set in Appliation->_define_routes()
	// for this view
	$canonical_url = $app->get_canonical_url();

?>
<!DOCTYPE html>
<html>
<head xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml">
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="description" content="Search for thousands of jobs from around your local area. Use Jobungo's articles and resources to aid in your job search and further your career. Jobungo - Search. Find. Hired." />
<meta name="keywords" content="job search, search for jobs, job website, local jobs, job searching, careers, employment, job search engine, part time employment<?=$meta_keyword_string?>" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- FB NOT YET IMPLEMENTED -->
<!-- <meta property="og:title" content="Jobungo - Search. Find. Hired."/>
<meta property="og:type" content="website"/>
<meta property="og:url" content="http://www.jobungo.com/"/>
<meta property="og:image" content="http://www.jobungo.com/images/jobungoicon.png"/>
<meta property="og:site_name" content="Jobungo"/>
<meta property="fb:app_id" content="199746173393045"/>-->

<title><?= $page_title?> | Jobungo UK</title>

<link rel="canonical" href="<?= $canonical_url ?>"/>
<link rel="shortcut icon" href="<?= Util::base_url() ?>/favicon.ico">
<link rel="stylesheet" href="/css/versionator_css.php?css=<?= $out['components']['css'] ?>">
<link rel="stylesheet" href="/css/jquery-ui.css">
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript" charset="utf-8">
    (function(G,o,O,g,L,e){G[g]=G[g]||function(){(G[g]['q']=G[g]['q']||[]).push(
    arguments)},G[g]['t']=1*new Date;L=o.createElement(O),e=o.getElementsByTagName(
    O)[0];L.async=1;L.src='//www.google.com/adsense/search/async-ads.js';
    e.parentNode.insertBefore(L,e)})(window,document,'script','_googCsa');
</script>
<!-- ver = <?= $out['version_id'] ?>  -->
</head>
<body>
<div class="cookie-msg" id="msg_cookies_use">
    By using our services, you agree to our use of cookies. <a href="/cookies">Learn More</a>. <a href="javascript:void(0)" class="accept" id="lnk_cookie_yes">Accept cookies</a>
</div>
<script type="text/javascript">
    //uncomment to show current result term in input box always
    /*$(function(){
	$('#keyword').val('<?=$out['keyword']?>');
	$('#location').val('<?=$out['locatoin']?>');
    });*/
    $(".close-cta").click(function() {
        $(".cookie-msg").remove();
    });
</script>
<!--
<div class="rj-widget">
    <div class="close-btn"><img src="/images/close-btn.png"></div>
    <img class="rj-widget-banner" src="/images/rj-banner.png">
    <div class="rj-widget-content">
        // <h2>New recommended jobs <?= $out['display_location'] ?></h2>
        <script type="text/javascript">
            var ind_pub = "1448762077525160";
            var ind_el = "recommended-job-single";
            var ind_pf = 'indeed';
            var ind_q = "";
            // var ind_l = "<?=$out['location']?>";
            var ind_chnl = "jukrjp1";
            var ind_n = 1;
            var ind_d = "http://www.indeed.co.uk";
            var ind_t = 40;
            var ind_c = 30;
        </script>
        <script type="text/javascript" src="http://www.indeed.co.uk/ads/jobroll-widget-v3.js"></script>
        <div id="recommended-job-single"></div>
        <div class="result-button-container">
            <a href="/recommended_jobs?kw=<?=rawurlencode($out['keyword'])?>&l=<?=rawurlencode($out['location'])?>">
            <span class="result-button">See more</span>
            </a>
        </div>
    </div>
</div>
-->
<div class="no-widebar">
    <div class="container">
        <header class="header-results sixteen columns">
            <div class="sidebar-header four columns alpha">
                <h1><a class="logo" href="/">Jobungo</a></h1>
            </div>
            <script type="text/javascript">
                function dismissTip(id){
                    if(document.getElementById(id)!=null){
                        document.getElementById(id).style.opacity='0';
                        setTimeout(function(){document.getElementById(id).style.display='none';},500);
                    }
                }
            </script>
            <form id="jobs_form" name="jobs_form" method="post" action="/jobs">
            <div class="sixteen columns offset-by-four alpha">
                <div class="five columns alpha">
                    <label for="regularInput">Keywords <span class="form-help">Job Title, Skills, or Company</span></label>
                    <input class="remove-bottom results-field" type="text" name="q" id="keyword" value="<?=$out['keyword']?>" onchange="dismissTip('tipKeywords');" onkeypress="dismissTip('tipKeywords');">
                    <?php
                    if(!$out['keyword']) {
                    ?>
                    <div id="tipKeywords" style="top: 90px;" onclick="dismissTip(this.id);">Enter a keyword to improve your search results</div>
                    <?php
                    }
                    ?>
                    <input type="hidden" id="keyword_id">
                </div>
                <div class="five columns">
                    <label for="regularInput">Location <span class="form-help">City or Postcode</span></label>
                    <input class="remove-bottom results-field" type="text" name="l" id="location" value="<?=$out['location']?>" onchange="dismissTip('tipLocation');" onkeypress="dismissTip('tipLocation');">
                    <?php
                    if(!$out['location']) {
                    ?>
                    <div id="tipLocation" style="top: 90px;" onclick="dismissTip(this.id);">Enter a location to improve your search results</div>
                    <?php
                    }
                    ?>
                    <input type="hidden" id="location_id">
                </div>
                <div class="two columns omega">
                    <label>&nbsp;</label>
                    <button class="full-width results-btn" type="submit">Search</button>
                </div>
            </div>
            <input type="hidden" name="task" value="search" />
            </form>
            <input type="hidden" id="hid_days_back" value="<?php echo $out['days_back'] ?>">
            <input type="hidden" id="hid_radius" value="<?php echo $out['radius'] ?>">
            <input type="hidden" id="page_number" value="<?php echo $out['page'] ?>">
        </header>
    </div>
</div>
    <!--
    <div class="widebar">
        <div class="container">
            <div class="sixteen columns">
                <div class="four columns alpha">
                    <p class="job-search-txt">Job Search</p>
                </div>
                <div class="twelve columns omega">
                    <p class="job-search-amount"><?= number_format($out["jobs"]->total) . ' ' . ucwords($out['keyword']) . ' jobs' . $out['display_location'] ?></p>
                </div>
            </div>
        </div>
    </div>
    -->
    <div class="container">
        <div class="sixteen columns row">
           <div class="sidebar four columns alpha">
                <div class="sidebar-unit totalnumber">
                    <h3><?= number_format($out["result_total"]) ?></h3>
                    <p><?= ucwords($out['keyword']) . ' jobs' . $out['display_location'] ?></p>
                </div>

                <!--
                    <?php
                    if(!$out['keyword']) {
                    ?>
                    <div class="sidebar-unit sidebar-note">
                        Tip: Enter a keyword to improve the relevance of your search results
                    </div>
                    <?php
                    }
                    ?>
                -->

                <div id="note">
                    <h3>Receive Email Alerts</h3>
                    <p>With jobs near <span><?= $out['location'] ?></span></p>
                    <!--<form action="/opt-in" method="post">-->
                        <input placeholder="Enter your email address" type="email" required="required" id="txt_signup_email" name="txt_signup_email">
                        <input placeholder="Where you want your next job" type="text" value="<?= $out['location'] ?>" required="required" name="<?= $out['location'] ?>" id="txt_signup_loc">
                        <button type="button" onclick="Juk.alert_signup()">Sign Up</button>
                    <!--</form>-->
                </div>

                <div class="sidebar-unit">
                    <h5>Days Back</h5>
                    <ul>
                        <li><a href="/jobs/d-3/" class="days_back <?php if($out['days_back'] == 3) echo 'active' ?>" rel="3">3 Days</a></li>
                        <li><a href="/jobs/d-7/" class="days_back <?php if($out['days_back'] == 7) echo 'active' ?>" rel="7">7 Days</a></li>
                        <li><a href="/jobs/d-14/" class="days_back <?php if($out['days_back'] == 14) echo 'active' ?>" rel="14">14 Days</a></li>
                        <li><a href="/jobs/d-30/" class="days_back <?php if($out['days_back'] == 30) echo 'active' ?>" rel="30">30 Days</a></li>
                    </ul>
                </div>
                <div class="sidebar-unit">
                    <h5>Search Radius</h5>
                    <ul>
                        <li><a href="/jobs/r-5/" class="radius <?php if($out['radius'] == 5) echo 'active' ?>" rel="5">5 Miles</a></li>
                        <li><a href="/jobs/r-10/" class="radius <?php if($out['radius'] == 10) echo 'active' ?>" rel="10">10 Miles</a></li>
                        <li><a href="/jobs/r-25/" class="radius <?php if($out['radius'] == 25) echo 'active' ?>" rel="25">25 Miles</a></li>
                        <li><a href="/jobs/r-50/" class="radius <?php if($out['radius'] == 50) echo 'active' ?>" rel="50">50 Miles</a></li>
                        <li><a href="/jobs/r-100/" class="radius <?php if($out['radius'] == 100) echo 'active' ?>" rel="100">100 Miles</a></li>
                    </ul>
                </div>
                <div class="sidebar-unit">
                    <h5>Popular Searches</h5>
                    <ul>
                        <?php
                            $pop_items = $out['popular_items'];
                        	foreach($out['popular_items'] as $pop_item) {

							  	// Only display if actual result for this keyword
								if($pop_item['count'] > 0) {

						?>

                        <li><a href="<?= $pop_item['url'] ?>"><?= $pop_item['keyword'] ?> <span class="popular-count"><?= $pop_item['count'] ?> new jobs</span></a></li>

                        <?php
                        		}
							}
                        ?>

                    </ul>
                </div>
                <!--
                <div class="sidebar-unit">
                  <style>#ypaAdWrapper-JobungoYPA iframe
                    {width:100%}
                  </style>
                  <div class="inuvo-ypa-sidebar">
                    <div id='ypaAdWrapper-JobungoYPA'></div>
                  </div>
                </div>
                <script src='https://s.yimg.com/uv/dm/scripts/syndication.js'></script>
                <script type='text/javascript'>
                  window.ypaAds.insertMultiAd({
                  ypaAdConfig : '00000015b',
                  ypaAdTypeTag : '227185',
                  ypaAdTagOptions : {
                  SponsoredSearch : {
                    Mobile : {
                      SrcTag : "litmus_search-ypa-mobile1" },
                    }
                  },
                  ypaPubParams : {
                      query : "<?=$out['keyword']?> jobs"
                  },
                  ypaAdSlotInfo : [
                  { ypaAdSlotId : 'JobungoYPA', ypaAdDivId : 'ypaAdWrapper-JobungoYPA', ypaAdWidth : '100', ypaAdHeight : '400' }
                  ]
                  });
                </script>
                -->
                <!--
                <div class="sidebar-unit" style="position: relative; text-align: center; padding-top: 20px;">
                    <div style="position: absolute; top: 0; left: 20px; font-size: 8px; line-height: 1; color: #aaa;">Sponsored Links</div>
                </div>
                -->
                <!--
                <div class="recommended-jobs-btn-result">
                    <a href="/recommended_jobs?kw=<?=rawurlencode($out['keyword'])?>&l=<?=rawurlencode($out['location'])?>">You have recommended jobs</a>
                </div>
                -->
                <!--
                <div class="sidebar-unit">
                    <h5><a data-toggle="modal" href="#share-results">Share These Results</a></h5>
                    <div class="share-these-results">
                        <form action="emailafriend.php" name="emailafriendform" id="emailafriendform">
                        <input class="share-field" placeholder="Your Email" id="regularInput" type="text">
                        <input class="share-field" placeholder="Friend's Email" id="regularInput" type="text">
                        <textarea class="share-field" placeholder="Message (optional)" id="regularInput" type="text"></textarea>
                        <button class="full-width results-btn" style="padding: 5px 0 2px;" type="submit">Submit</button>
                        </form>
                        <div id="emailafriendholder">
					  		<div id="emailafriendstatus"></div>
						</div>
                    </div>
                </div>
                -->
            </div>
