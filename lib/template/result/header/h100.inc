<?php
// vim: ai ts=4 sts=4 et sw=4 tw=79

	$page_title = $app->make_search_title();

	$meta_keyword_string = $app->make_meta_keyword_string();

	// Build canonical url using canonical pattern set in Appliation->_define_routes()
	// for this view
	$canonical_url = $app->get_canonical_url();


?>
<!DOCTYPE html>
<html>
<head xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml">
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="description" content="Search for thousands of jobs from around your local area. Use Jobungo's articles and resources to aid in your job search and further your career. Jobungo - Search. Find. Hired." />
<meta name="keywords" content="job search, search for jobs, job website, local jobs, job searching, careers, employment, job search engine, part time employment<?=$meta_keyword_string?>" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- FB NOT YET IMPLEMENTED -->
<!-- <meta property="og:title" content="Jobungo - Search. Find. Hired."/>
<meta property="og:type" content="website"/>
<meta property="og:url" content="http://www.jobungo.com/"/>
<meta property="og:image" content="http://www.jobungo.com/images/jobungoicon.png"/>
<meta property="og:site_name" content="Jobungo"/>
<meta property="fb:app_id" content="199746173393045"/>-->

<title><?= $page_title?> | Jobungo UK</title>

<link rel="canonical" href="<?= $canonical_url ?>"/>
<link rel="shortcut icon" href="<?= Util::base_url() ?>/favicon.ico">
<link rel="stylesheet" href="/css/versionator_css.php?css=<?= $out['components']['css'] ?>">
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript" charset="utf-8">
    (function(G,o,O,g,L,e){G[g]=G[g]||function(){(G[g]['q']=G[g]['q']||[]).push(
    arguments)},G[g]['t']=1*new Date;L=o.createElement(O),e=o.getElementsByTagName(
    O)[0];L.async=1;L.src='//www.google.com/adsense/search/async-ads.js';
    e.parentNode.insertBefore(L,e)})(window,document,'script','_googCsa');
</script>
<!-- ver = <?= $out['version_id'] ?>  -->
</head>

<body class="mobile-body">
    <header class="mobile-header">
        <a href="/"><img src="http://www.jobungo.co.uk/images/logo.png" title="Jobungo"></a>
    </header>
    <section class="mobile-search-form cf">
        <form id="jobs_form" name="jobs_form" method="post" action="/jobs">
            <input type="text" name="q" id="keyword" class="mobile-input" value="<?=$out['keyword']?>" placeholder="Job Title, Skills, or Company">
            <input type="text" name="l" id="location" class="mobile-input" value="<?=$out['location']?>" placeholder="City, State, or Zip">
            <button type="submit">Search Jobs</button>
            <input type="hidden" name="task" value="search" />
        </form>
        <input type="hidden" id="hid_days_back" value="<?php echo $out['days_back'] ?>">
        <input type="hidden" id="hid_radius" value="<?php echo $out['radius'] ?>">
        <input type="hidden" id="page_number" value="<?php echo $out['page'] ?>">
    </section>
    <!--
    <div class="mobile-rj-unit">
        <div class="close-btn"><img src="/images/close-btn.png"></div>
        <h3>Recommended Job Alert!</h3>
        <script type="text/javascript">
            var ind_pub = "1448762077525160";
            var ind_el = "recommended-job-single";
            var ind_pf = 'indeed';
            var ind_q = "";
            var ind_l = "<?=$out['location']?>";
            var ind_chnl = "jukrjp1";
            var ind_n = 1;
            var ind_d = "http://www.indeed.co.uk";
            var ind_t = 40;
            var ind_c = 30;
        </script>
        <script type="text/javascript" src="http://www.indeed.co.uk/ads/jobroll-widget-v3.js"></script>
        <div id="recommended-job-single"></div>
        <a class="recommended-job-single-btn" href="/recommended_jobs?kw=<?=rawurlencode($out['keyword'])?>&l=<?=rawurlencode($out['location'])?>">See more recommended jobs</a>
    </div>
    -->
</div>
    </div>
    <div style="margin: 20px auto 0; width: 90%;">
        <div style="width: 100%; margin: 0 auto;">