<?php
// vim: ai ts=4 sts=4 et sw=4 tw=79

	$page_title = $app->make_search_title();

	$meta_keyword_string = $app->make_meta_keyword_string();

	// Build canonical url using canonical pattern set in Appliation->_define_routes()
	// for this view
	$canonical_url = $app->get_canonical_url();

?>
<!DOCTYPE html>
<html>
<head xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml">
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="description" content="Search for thousands of jobs from around your local area. Use Jobungo's articles and resources to aid in your job search and further your career. Jobungo - Search. Find. Hired." />
<meta name="keywords" content="job search, search for jobs, job website, local jobs, job searching, careers, employment, job search engine, part time employment<?=$meta_keyword_string?>" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- FB NOT YET IMPLEMENTED -->
<!-- <meta property="og:title" content="Jobungo - Search. Find. Hired."/>
<meta property="og:type" content="website"/>
<meta property="og:url" content="http://www.jobungo.com/"/>
<meta property="og:image" content="http://www.jobungo.com/images/jobungoicon.png"/>
<meta property="og:site_name" content="Jobungo"/>
<meta property="fb:app_id" content="199746173393045"/>-->

<title><?= $page_title?> | Jobungo UK</title>

<link rel="canonical" href="<?= $canonical_url ?>"/>
<link rel="shortcut icon" href="<?= Util::base_url() ?>/favicon.ico">
<link rel="stylesheet" href="/css/versionator_css.php?css=<?= $out['components']['css'] ?>">
<link rel="stylesheet" href="/css/jquery-ui.css">
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript" charset="utf-8">
    (function(G,o,O,g,L,e){G[g]=G[g]||function(){(G[g]['q']=G[g]['q']||[]).push(
    arguments)},G[g]['t']=1*new Date;L=o.createElement(O),e=o.getElementsByTagName(
    O)[0];L.async=1;L.src='//www.google.com/adsense/search/async-ads.js';
    e.parentNode.insertBefore(L,e)})(window,document,'script','_googCsa');
</script>
<!-- ver = <?= $out['version_id'] ?>  -->
</head>

<body>
<div class="no-widebar">
    <div class="container">
        <header class="header-results sixteen columns">
            <div class="sidebar-header four columns alpha">
                <h1><a class="logo" href="/">Jobungo</a></h1>
            </div>
            <script type="text/javascript">
                function dismissTip(id){
                    if(document.getElementById(id)!=null){
                        document.getElementById(id).style.opacity='0';
                        setTimeout(function(){document.getElementById(id).style.display='none';},500);
                    }
                }
            </script>
            <form id="jobs_form" name="jobs_form" method="post" action="/jobs">
            <div class="sixteen columns offset-by-four alpha">
                <div class="five columns alpha">
                    <label for="regularInput">Keywords <span class="form-help">Job Title, Skills, or Company</span></label>
                    <input class="remove-bottom results-field" type="text" name="q" id="keyword" value="<?=$out['keyword']?>" onchange="dismissTip('tipKeywords');" onkeypress="dismissTip('tipKeywords');">
                    <?php
                    if(!$out['keyword']) {
                    ?>
                    <div id="tipKeywords" style="top: 90px;" onclick="dismissTip(this.id);">Enter a keyword to improve your search results</div>
                    <?php
                    }
                    ?>
                    <input type="hidden" id="keyword_id">
                </div>
                <div class="five columns">
                    <label for="regularInput">Location <span class="form-help">City or Postcode</span></label>
                    <input class="remove-bottom results-field" type="text" name="l" id="location" value="<?=$out['location']?>" onchange="dismissTip('tipLocation');" onkeypress="dismissTip('tipLocation');">
                    <?php
                    if(!$out['location']) {
                    ?>
                    <div id="tipLocation" style="top: 90px;" onclick="dismissTip(this.id);">Enter a location to improve your search results</div>
                    <?php
                    }
                    ?>
                    <input type="hidden" id="location_id">
                </div>
                <div class="two columns omega">
                    <label>&nbsp;</label>
                    <button class="full-width results-btn" type="submit">Search</button>
                </div>
            </div>
            <input type="hidden" name="task" value="search" />
            </form>
            <input type="hidden" id="hid_days_back" value="<?php echo $out['days_back'] ?>">
            <input type="hidden" id="hid_radius" value="<?php echo $out['radius'] ?>">
            <input type="hidden" id="page_number" value="<?php echo $out['page'] ?>">
        </header>
    </div>
</div>
    <!--
    <div class="widebar">
        <div class="container">
            <div class="sixteen columns">
                <div class="four columns alpha">
                    <p class="job-search-txt">Job Search</p>
                </div>
                <div class="twelve columns omega">
                    <p class="job-search-amount"><?= number_format($out["jobs"]->total) . ' ' . ucwords($out['keyword']) . ' jobs' . $out['display_location'] ?></p>
                </div>
            </div>
        </div>
    </div>
    -->
    <div class="container" style="padding-left: 0;">
