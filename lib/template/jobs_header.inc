<?php
// vim: ai ts=4 sts=4 et sw=4 tw=79

	// The seo title in head title tag
	$head_title = $app->make_search_title();

	// The actual page title
	$page_title = $app->get_page_title();

	$meta_keyword_string = $app->make_meta_keyword_string();

	// Build canonical url using canonical pattern set in Appliation->_define_routes()
	// for this view
	$canonical_url = $app->get_canonical_url();

	$view = $app->get_view();

?>
<!DOCTYPE html>
<html>
<head xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml">
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="description" content="Search for thousands of jobs from around your local area. Use Jobungo's articles and resources to aid in your job search and further your career. Jobungo - Search. Find. Hired." />
<meta name="keywords" content="job search, search for jobs, job website, local jobs, job searching, careers, employment, job search engine, part time employment<?=$meta_keyword_string?>" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- NOT NEEDED AT THIS TIME -->
<!-- <meta property="og:title" content="Jobungo UK - Search. Find. Hired."/>
<meta property="og:type" content="website"/>
<meta property="og:url" content="http://www.jobungo.co.uk/"/>
<meta property="og:image" content="http://www.jobungo.co.uk/images/jobungo-logo-small.png"/>
<meta property="og:site_name" content="Jobungo UK"/>
<meta property="fb:app_id" content="NOT CORRECT ID--199746173393045--"/>-->

<title><?= $head_title?> | Jobungo UK</title>

<link rel="canonical" href="<?= $canonical_url ?>"/>
<link rel="shortcut icon" href="<?= Util::base_url() ?>/favicon.ico">
<link rel="stylesheet" href="/css/versionator_css.php?css=<?= $out['components']['css'] ?> ?>">
<link rel="stylesheet" href="/css/jquery-ui.css">
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<script type="text/javascript" src="/js/jquery.min.js"></script>
<!-- ver = <?= $out['version_id'] ?>  -->
</head>

<body>
    <div class="container">
        <header class="header-results sixteen columns">
            <div class="sidebar-header four columns alpha">
                <h1><a class="logo" href="/">Jobungo</a></h1>
            </div>
            <form id="jobs_form" name="jobs_form" method="post" action="/jobs">
            <div class="sixteen columns offset-by-four alpha">
                <div class="five columns alpha">
                    <label for="regularInput">Keywords <span class="form-help">Job Title, Skills, or Company</span></label>
                    <input class="remove-bottom results-field" type="text" name="q" id="keyword" value="<?=$out['keyword']?>">
                    <input type="hidden" id="keyword_id">
                </div>
                <div class="five columns">
                    <label for="regularInput">Location <span class="form-help">City or Postcode</span></label>
                    <input class="remove-bottom results-field" type="text" name="l" id="location" value="<?=$out['location']?>">
                    <input type="hidden" id="location_id">
                </div>
                <div class="two columns omega">
                    <label>&nbsp;</label>
                    <button class="full-width results-btn" type="submit"><img src="/images/search-icon.png"></button>
                </div>
            </div>
            <input type="hidden" name="task" value="search" />
            <input type="hidden" id="page_number" value="<?= $out['page'] ?>">
            </form>
        </header>
    </div>
    <div class="widebar">
        <div class="container">
            <div class="sixteen columns">
                <div class="four columns alpha">
                    <p class="job-search-txt">Job Search</p>
                </div>
                <div class="eight columns omega">
                    <p class="job-search-amount"><?php
                    if($view == 'recommended_jobs') {
                    	echo $page_title;
					} else {
						echo number_format($out["result_total"]) . ' ' . ucwords($out['keyword']) . ' jobs near ' . ucwords($out['location']);
					}
						?></p>
                </div>
                <div class="four columns omega">
					<div id="fb-root"></div>
					 <?php if($view == 'recommended_jobs') { ?>
						<div class="fb-like fb-like-home fb-like-results" data-href="http://www.facebook.com/jobungo" data-width="50" data-layout="button_count" data-show-faces="false" data-stream="false" data-header="false"></div>
					<?php } ?>
            	</div>
        </div>
    </div>
    <div class="container">
        <div class="sixteen columns row">
            <div class="sidebar four columns alpha">
                <?php
                if($view == 'recommended_jobs'){
				?>

				<div class="sidebar-unit">
					<h5 class="days-back"><a style="color: #e28409;" href="/jobs">&larr; Return to search results</a></h5>
				</div>
				<br>
				<?php

				} else {

				?>
                <div class="sidebar-unit">
                    <h5>Days Back</h5>
                    <ul>
                        <li><a href="/jobs/d-3/" class="days_back <?php if($out['days_back'] == 3) echo 'active' ?>" rel="3">3 Days</a></li>
                        <li><a href="/jobs/d-7/" class="days_back <?php if($out['days_back'] == 7) echo 'active' ?>" rel="7">7 Days</a></li>
                        <li><a href="/jobs/d-14/" class="days_back <?php if($out['days_back'] == 14) echo 'active' ?>" rel="14">14 Days</a></li>
                        <li><a href="/jobs/d-30/" class="days_back <?php if($out['days_back'] == 30) echo 'active' ?>" rel="30">30 Days</a></li>
                    </ul>
                </div>
                <div class="sidebar-unit">
                    <h5>Search Radius</h5>
                    <ul>
                        <li><a href="/jobs/r-3/" class="radius <?php if($out['radius'] == 3) echo 'active' ?>" rel="3">3 Miles</a></li>
                        <li><a href="/jobs/r-7/" class="radius <?php if($out['radius'] == 7) echo 'active' ?>" rel="7">7 Miles</a></li>
                        <li><a href="/jobs/r-14/" class="radius <?php if($out['radius'] == 14) echo 'active' ?>" rel="14">14 Miles</a></li>
                        <li><a href="/jobs/r-30/" class="radius <?php if($out['radius'] == 30) echo 'active' ?>" rel="30">30 Miles</a></li>
                    </ul>
                </div>
                <?php

				}

                ?>
                <div class="sidebar-unit">
                    <h5>Popular Searches</h5>
                    <ul>
                        <li><a href="/jobs/q-Customer+Service/l-<?= $out['location'] ?>/">Customer Service <span class="popular-count">12,608 new jobs</span></a></li>
                        <li><a href="/jobs/q-Healthcare/l-<?= $out['location'] ?>/">Healthcare <span class="popular-count">5,204 new jobs</span></a></li>
                        <li><a href="/jobs/q-Driver/l-<?= $out['location'] ?>/">Driver <span class="popular-count">7,308 new jobs</span></a></li>
                        <li><a href="/jobs/q-Data+Entry/l-<?= $out['location'] ?>/">Data Entry <span class="popular-count">6,757 new jobs</span></a></li>
                        <li><a href="/jobs/q-Warehouse/l-<?= $out['location'] ?>/">Warehouse <span class="popular-count">9,757 new jobs</span></a></li>
                        <li><a href="/jobs/q-Sales+Associate/l-<?= $out['location'] ?>/">Sales Associate <span class="popular-count">9,757 new jobs</span></a></li>
                    </ul>
                </div>
                <div class="sidebar-unit">
                    <h5><a data-toggle="modal" href="#share-results">Share These Results</a></h5>
                    <div class="share-these-results">
                        <input class="share-field" placeholder="Your Email" id="regularInput" type="text">
                        <input class="share-field" placeholder="Friend's Email" id="regularInput" type="text">
                        <textarea class="share-field" placeholder="Message (optional)" id="regularInput" type="text"></textarea>
                        <button class="full-width results-btn" style="padding: 5px 0 2px;" type="submit">Submit</button>
                    </div>
                </div>
            </div>