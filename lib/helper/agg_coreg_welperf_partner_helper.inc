<?php

function getWelcomeStats($partners, $campaigns, $dt) {
    // Get welcome campaign stats from ses k2 server
    $conn = Database::get_pdo_conn('ses_rw_prod');

    $nextDt = date('Y-m-d', strtotime($dt . ' +1 day'));
    $data = array();

    // Ensure no lock on table read on k2
    $conn->exec('SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;');
    foreach($campaigns as $camp) {
        echo 'camp: ' . $camp . "\n";

        $useStmt = $conn->prepare("USE ses_{$camp}");

        if(strpos($camp, 'noloc')!==false) {
            $welType = 1;
        } else {
            $welType = 0;
        }
        /*
        if($row['type']=='welcome_email_noloc') {
            $welType = 1;
        } else {
            $welType = 0;
        }
        */

        // db must exist otherwise go to next
        if($useStmt->execute()) {
            //$sql = "SELECT count(1) as cnt FROM click WHERE date_added>=? AND date_added<?";

            // Get click count for all partners
            $sql = "SELECT partner, email FROM click WHERE date_added>=? AND date_added<?
                    AND link_type IN ('result', 'redirect')
            ";
            $dStmt = $conn->prepare($sql);
            $dStmt->execute(array($dt, $nextDt));

            $clickCnt = 0;
            while($dRow=$dStmt->fetch(PDO::FETCH_ASSOC)) {
                $emailKey = dechex(crc32(strtolower($dRow['email'])));

                if(isset($partners[$dRow['partner']])) {
                    $pid = $dRow['partner'];
                } else {
                    $pid = 'unknown';
                }

                // Only count distinct emails per partner
                if(!isset($distEmail[$welType][$pid][$emailKey])) {
                    $data[$welType][$pid]['clik'] += 1;
                    $distEmail[$welType][$pid][$emailKey] = 1;
                    $clickCnt++;
                }

            }
            $dStmt=NULL;
            $dRow=NULL;
            $distEmail=NULL;

            // Get open count for all partners
            $sql = "SELECT partner, email FROM open WHERE date_added>=? AND date_added<?";
            $dStmt = $conn->prepare($sql);
            $dStmt->execute(array($dt, $nextDt));
            //$cnt = $stmt->fetchColumn(0);
            $openCnt = 0;
            while($dRow=$dStmt->fetch(PDO::FETCH_ASSOC)) {
                $emailKey = dechex(crc32(strtolower($dRow['email'])));

                if(isset($partners[$dRow['partner']])) {
                    $pid = $dRow['partner'];
                } else {
                    $pid = 'unknown';
                }

                // Only count distinct emails per partner
                if(!isset($distEmail[$welType][$pid][$emailKey])) {
                    $data[$welType][$pid]['open'] += 1;
                    $distEmail[$welType][$pid][$emailKey] = 1;
                    $openCnt++;
                }

            }
            $dStmt=NULL;
            $dRow=NULL;
            $distEmail=NULL;

            // Get open count for all partners
            $sql = "SELECT partner FROM sent WHERE date_sent>=? AND date_sent<?";
            $dStmt = $conn->prepare($sql);
            $dStmt->execute(array($dt, $nextDt));
            //$cnt = $stmt->fetchColumn(0);
            $sentCnt = 0;
            while($dRow=$dStmt->fetch(PDO::FETCH_ASSOC)) {

                if(isset($partners[$dRow['partner']])) {
                    $data[$welType][$dRow['partner']]['sent'] += 1;
                } else {
                    $data[$welType]['unknown']['sent'] += 1;
                }

                $sentCnt++;
            }
            $dStmt=NULL;
            $dRow=NULL;

            // Get open count for all partners
            $sql = "SELECT partner FROM unsent WHERE date_attempted>=? AND date_attempted<?";
            $dStmt = $conn->prepare($sql);
            $dStmt->execute(array($dt, $nextDt));
            //$cnt = $stmt->fetchColumn(0);
            $unsentCnt = 0;
            while($dRow=$dStmt->fetch(PDO::FETCH_ASSOC)) {

                if(isset($partners[$dRow['partner']])) {
                    $data[$welType][$dRow['partner']]['unsent'] += 1;
                } else {
                    $data[$welType]['unknown']['unsent'] += 1;
                }

                $unsentCnt++;
            }
            $dStmt=NULL;
            $dRow=NULL;

        } else {
            echo "Error: {$camp} db does not exist \n";
            continue;
        }




    }
    //Util::printr($data);
    //echo 'clicks: ' . $clickCnt . " opens: " . $openCnt . " sent: ". $sentCnt . " unsent: " . $unsentCnt . "\n";

    return $data;

}

function getDnmStats($partners, $campaigns, $dt) {
    // Get dnm stats from uk bacon

    $conn = Database::get_pdo_conn('bacon_rw_prod');
    $nextDt = date('Y-m-d', strtotime($dt . ' +1 day'));

    // Ensure no lock on table read on uk bacon
    $conn->exec('SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;');

    // Make welcome campaigns list
    //foreach($campaigns as $camp) {
    //    $campList .= "'" . $camp['camp'] . "',";
    //}

    //$campList = substr($campList,0,-1);

    //echo $campList . "\n";
    foreach($campaigns as $camp) {

        if(strpos($camp, 'noloc')!==false) {
            $welType = 1;
        } else {
            $welType = 0;
        }

        /*
        if($row['type']=='welcome_email_noloc') {
            $welType = 1;
        } else {
            $welType = 0;
        }
        */
        // Get all hard bounces for one day
        $sql = "
            SELECT partner, email_address as email FROM bounce WHERE date_added>=? and date_added<?
            AND type='hard' AND campaign_name=?
            ";

        $dStmt = $conn->prepare($sql);

        if($dStmt->execute(array($dt, $nextDt, $camp))) {
            $bnceCnt = 0;
            while($dRow=$dStmt->fetch(PDO::FETCH_ASSOC)) {
                $emailKey = dechex(crc32(strtolower($dRow['email'])));

                if(isset($partners[$dRow['partner']])) {
                    $pid = $dRow['partner'];
                } else {
                    $pid = 'unknown';
                }

                if(!isset($distEmail[$welType][$pid][$emailKey])) {
                    $data[$welType][$pid]['bnce'] += 1;
                    $distEmail[$welType][$pid][$emailKey] = 1;
                    $bnceCnt++;
                }

            }
        } else {
            $err = $dStmt->errorInfo();
            echo $err[2];
        }

        //echo 'bounces: ' . $bnceCnt . "\n";

        $dStmt=NULL;
        $dRow=NULL;
        $distEmail=NULL;

        // Get all complaints for one day
        $sql = "
            SELECT partner, email_address as email FROM complaint WHERE date_added>=? and date_added<?
            AND campaign_name=?
            ";

        $dStmt = $conn->prepare($sql);

        if($dStmt->execute(array($dt, $nextDt, $camp))) {

            while($dRow=$dStmt->fetch(PDO::FETCH_ASSOC)) {
                $emailKey = dechex(crc32(strtolower($dRow['email'])));

                if(isset($partners[$dRow['partner']])) {
                    $pid = $dRow['partner'];
                } else {
                    $pid = 'unknown';
                }

                if(!isset($distEmail[$welType][$pid][$emailKey])) {
                    $data[$welType][$pid]['cmpt'] += 1;
                    $distEmail[$welType][$pid][$emailKey] = 1;
                }

            }
        } else {
            $err = $dStmt->errorInfo();
            echo $err[2];
        }

        $dStmt=NULL;
        $dRow=NULL;
        $distEmail=NULL;

         // Get all unsubscribed for one day
        $sql = "
            SELECT partner, email_address as email FROM unsubscribe WHERE date_added>=? and date_added<?
            AND campaign_name=?
            ";

        $dStmt = $conn->prepare($sql);

        if($dStmt->execute(array($dt, $nextDt, $camp))) {

            while($dRow=$dStmt->fetch(PDO::FETCH_ASSOC)) {
                $emailKey = dechex(crc32(strtolower($dRow['email'])));

                if(isset($partners[$dRow['partner']])) {
                    $pid = $dRow['partner'];
                } else {
                    $pid = 'unknown';
                }

                if(!isset($distEmail[$welType][$pid][$emailKey])) {
                    $data[$welType][$pid]['unsub'] += 1;
                    $distEmail[$welType][$pid][$emailKey] = 1;
                }
            }
        } else {
            $err = $dStmt->errorInfo();
            echo $err[2];
        }

        $dStmt=NULL;
        $dRow=NULL;
        $distEmail=NULL;
    }

    return $data;

}

function getSiteStats($conn, $partners, $dt) {
    // Get wullo site stats from mckinley

    $nextDt = date('Y-m-d', strtotime($dt . ' +1 day'));

    // Get user stats
    $sql = "SELECT partner, has_engaged FROM user WHERE date_added>=? and date_added<?";

    $dStmt = $conn->prepare($sql);

    if($dStmt->execute(array($dt, $nextDt))) {

        while($dRow=$dStmt->fetch(PDO::FETCH_ASSOC)) {
            if(isset($partners[$dRow['partner']])) {
                $pid = $dRow['partner'];
            } else {
                $pid = 'unknown';
            }

            if($dRow['has_engaged']==1) {
                $data[$pid]['el2'] += 1;
            } else {
                $data[$pid]['el1'] += 1;
            }
        }

    } else {
        $err = $dStmt->errorInfo();
        echo $err[2];
    }

    $dStmt=NULL;
    $dRow=NULL;

    // Get search result stats
    $sql = "SELECT partner FROM search_with_results WHERE date_searched>=? and date_searched<?";

    $dStmt = $conn->prepare($sql);

    if($dStmt->execute(array($dt, $nextDt))) {

        while($dRow=$dStmt->fetch(PDO::FETCH_ASSOC)) {
            if(isset($partners[$dRow['partner']])) {
                $data[$dRow['partner']]['srch'] += 1;
            } else {
                $data['unknown']['srch'] += 1;
            }
        }

    } else {
        $err = $dStmt->errorInfo();
        echo $err[2];
    }

    $dStmt=NULL;
    $dRow=NULL;

    // Get search result stats
    $sql = "SELECT partner FROM job_click WHERE date_clicked>=? and date_clicked<?";

    $dStmt = $conn->prepare($sql);

    if($dStmt->execute(array($dt, $nextDt))) {

        while($dRow=$dStmt->fetch(PDO::FETCH_ASSOC)) {
            if(isset($partners[$dRow['partner']])) {
                $data[$dRow['partner']]['jclik'] += 1;
            } else {
                $data['unknown']['jclik'] += 1;
            }
        }

    } else {
        $err = $dStmt->errorInfo();
        echo $err[2];
    }

    $dStmt=NULL;
    $dRow=NULL;

    //Util::printr($data);

    return $data;

}
