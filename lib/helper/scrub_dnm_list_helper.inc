<?php


    function update_users_with_blacklisted_domain($log_file_name, $last_blacklisted_domain_date) {

        $blacklisted_domain_model = Util::load_model('blacklisteddomain');
        $user_model = Util::load_model('user');

        $total_scrubbed_blacklisted_domain_user = 0;

        $index = 0;

        $saved_last_blacklisted_domain_date = '';

        $latest_blacklisted_domain_list = $blacklisted_domain_model->get_last_added_records($last_blacklisted_domain_date);

        $latest_blacklisted_domain_count = $latest_blacklisted_domain_list->rowCount();

        $dbh = Database::get_pdo_conn(DEFAULT_DB_SETTING);

        print ("Blacklisted Domains to Process: " . $latest_blacklisted_domain_count . "\n");

        while($blacklisted_domain_row = $latest_blacklisted_domain_list->fetch(PDO::FETCH_ASSOC)) {
            //foreach($last_global_removal_list as $last_global_removal) {

            if ($index == 0) $saved_last_blacklisted_domain_date = $blacklisted_domain_row['date_added'];

            $sql = "SELECT id, email FROM user WHERE email_domain=?";
            $uStmt = $dbh->prepare($sql);
            $uStmt->execute(array($blacklisted_domain_row['domain_name']));

            while($user = $uStmt->fetch(PDO::FETCH_ASSOC)) {
                $sql = "UPDATE user SET is_mailable=0 WHERE id=?";
                $updStmt = $dbh->prepare($sql);
                $updStmt->execute(array($user['id']));
                $total_scrubbed_blacklisted_domain_user++;
                Util::log_to_file($log_file_name, "Blacklisted Domain User", $user['email']);
            }
            /*
            if ($user_list = $user_model->find_by_email_domain($blacklisted_domain_row['domain_name'])) {

                foreach($user_list as $user) {

                    if ($user->is_mailable == 1) {
                        $user->is_mailable = 0;
                        $user->update();
                        $total_scrubbed_blacklisted_domain_user++;
                        Util::log_to_file($log_file_name, "Blacklisted Domain User", $user->email);
                    }

                }
            }
            */

            $index++;
        }

        $latest_blacklisted_domain_list = null;

        echo 'Total Blacklisted Domain users scrubbed: ' . $total_scrubbed_blacklisted_domain_user . "\n";

        $ret['bd_latest_date'] = $saved_last_blacklisted_domain_date;
        $ret['bd_count'] = $latest_blacklisted_domain_count;
        $ret['bd_user_scrubbed_count'] = $total_scrubbed_blacklisted_domain_user;
        return $ret;
    }

    function update_users_with_global_removal($log_file_name,$last_global_removal_date) {

        $global_removal_model = Util::load_model('globalremoval');
        $user_model = Util::load_model('user');

        $total_scrubbed_global_removal_user = 0;

        $index = 0;

        $saved_last_global_removal_date = '';

        $last_global_removal_list = $global_removal_model->get_last_added_records($last_global_removal_date);

        $last_global_removal_count = $last_global_removal_list->rowCount();

        print ("Global Removals to Process: " . $last_global_removal_count . "\n");

        $dbh = Database::get_pdo_conn(DEFAULT_DB_SETTING);

        while($global_removal_row = $last_global_removal_list->fetch(PDO::FETCH_ASSOC)) {
            //foreach($last_global_removal_list as $last_global_removal) {

            if ($index == 0) $saved_last_global_removal_date = $global_removal_row['date_updated'];

            $sql = "SELECT id FROM user WHERE email=? AND is_mailable=1";

            $uStmt = $dbh->prepare($sql);
            $uStmt->execute(array($global_removal_row['email']));
            $uId = $uStmt->fetchColumn();

            if(!empty($uId)) {
                $sql = "UPDATE user SET is_mailable=0 WHERE id=?";
                $updStmt = $dbh->prepare($sql);
                $updStmt->execute(array($uId));
                $total_scrubbed_global_removal_user++;
                Util::log_to_file($log_file_name, "Global Removal User", $global_removal_row['email']);
            }
/*
            if ($user = $user_model->find_by_email($global_removal_row['email'])) {

                if ($user->is_mailable == 1) {
                    $user->is_mailable = 0;
                    $user->update();
                    $total_scrubbed_global_removal_user++;
                    Util::log_to_file($log_file_name, "Global Removal User", $user->email);
                }
            }
*/
            $index++;
        }

        $last_global_removal_list = null;
        echo 'Total Global Removal users scrubbed: ' . $total_scrubbed_global_removal_user . "\n";

        $ret['gr_latest_date'] = $saved_last_global_removal_date;
        $ret['gr_count'] = $last_global_removal_count;
        $ret['gr_user_scrubbed_count'] = $total_scrubbed_global_removal_user;
        return $ret;
    }


    function update_users_with_complaint($log_file_name, $last_complaint_date) {

        $complaint_model = Util::load_model('complaint');
        $user_model = Util::load_model('user');

        $total_scrubbed_complaint_users = 0;

        $index = 0;

        $latest_complaint_date = '';

        $latest_complaint_list = $complaint_model->get_last_added_records($last_complaint_date);

        $latest_complaint_count = $latest_complaint_list->rowCount();

        print ("Complaints to Process: " . $latest_complaint_count . "\n");

        $dbh = Database::get_pdo_conn(DEFAULT_DB_SETTING);

        while($complaint_row = $latest_complaint_list->fetch(PDO::FETCH_ASSOC)) {
            //foreach($last_global_removal_list as $last_global_removal) {

            if ($index == 0) $latest_complaint_date = $complaint_row['date_added'];

            $sql = "SELECT id FROM user WHERE email=? AND is_mailable=1";

            $uStmt = $dbh->prepare($sql);
            $uStmt->execute(array($complaint_row['email_address']));
            $uId = $uStmt->fetchColumn();

            if(!empty($uId)) {
                $sql = "UPDATE user SET is_mailable=0 WHERE id=?";
                $updStmt = $dbh->prepare($sql);
                $updStmt->execute(array($uId));
                $total_scrubbed_complaint_users++;
                Util::log_to_file($log_file_name, "Complaint User Scrubbed", $complaint_row['email_address']);
            }

            /*
            if ($user = $user_model->find_by_email($complaint_row['email_address'])) {

                if ($user->is_mailable == 1) {
                    $user->is_mailable = 0;
                    $user->update();
                    $total_scrubbed_complaint_users++;
                    Util::log_to_file($log_file_name, "Complaint User Scrubbed", $user->email);
                }
            }
            */

            $index++;
        }

        $latest_complaint_list = null;

        echo 'Total Complaint Users Scrubbed: ' . $total_scrubbed_complaint_users . "\n";

        $ret['complaint_latest_date'] = $latest_complaint_date;
        $ret['complaint_count'] = $latest_complaint_count;
        $ret['complaint_user_scrubbed_count'] = $total_scrubbed_complaint_users;

        return $ret;
    }

    function update_users_with_bounce($log_file_name, $last_bounce_date) {

        $bounce_model = Util::load_model('bounce');
        $user_model = Util::load_model('user');

        $total_scrubbed_bounce_users = 0;

        $index = 0;

        $latest_bounce_date = '';

        $latest_bounce_list = $bounce_model->get_last_added_records($last_bounce_date);

        $latest_bounce_count = $latest_bounce_list->rowCount();

        print ("Bounces to Process: " . $latest_bounce_count . "\n");

        $dbh = Database::get_pdo_conn(DEFAULT_DB_SETTING);

        while($bounce_row = $latest_bounce_list->fetch(PDO::FETCH_ASSOC)) {
            //foreach($last_global_removal_list as $last_global_removal) {

            if ($index == 0) $latest_bounce_date = $bounce_row['date_added'];

            //Util::printr($complaint_row);
            $sql = "SELECT id FROM user WHERE email=? AND is_mailable=1";

            $uStmt = $dbh->prepare($sql);
            $uStmt->execute(array($bounce_row['email_address']));
            $uId = $uStmt->fetchColumn();

            if(!empty($uId)) {
                $sql = "UPDATE user SET is_mailable=0 WHERE id=?";
                $updStmt = $dbh->prepare($sql);
                $updStmt->execute(array($uId));
                $total_scrubbed_bounce_users++;
                Util::log_to_file($log_file_name, "Bounce Users Scrubbed", $bounce_row['email_address']);
            }

            /*
            if ($user = $user_model->find_by_email($bounce_row['email_address'])) {

                if ($user->is_mailable == 1) {
                    $user->is_mailable = 0;
                    $user->update();
                    $total_scrubbed_bounce_users++;
                    Util::log_to_file($log_file_name, "Bounce Users Scrubbed", $user->email);
                }
            }
            */

            $index++;
        }

        $latest_bounce_list = null;

        echo 'Total Bounce Users Scrubbed: ' . $total_scrubbed_bounce_users . "\n";

        $ret['bounce_latest_date'] = $latest_bounce_date;
        $ret['bounce_count'] = $latest_bounce_count;
        $ret['bounce_user_scrubbed_count'] = $total_scrubbed_bounce_users;

        return $ret;
    }

    function update_users_with_unsubscribe($log_file_name, $last_unsubscribe_date) {

        $unsubscribe_model = Util::load_model('unsubscribe');
        $user_model = Util::load_model('user');

        $total_scrubbed_unsubscribe_users = 0;

        $index = 0;

        $latest_unsubscribe_date = '';

        $latest_unsubscribe_list = $unsubscribe_model->get_last_added_records($last_unsubscribe_date);

        $latest_unsubscribe_count = $latest_unsubscribe_list->rowCount();

        print ("Unsubscribes to Process: " . $latest_unsubscribe_count . "\n");

        $dbh = Database::get_pdo_conn(DEFAULT_DB_SETTING);

        while($unsubscribe_row = $latest_unsubscribe_list->fetch(PDO::FETCH_ASSOC)) {
            //foreach($last_global_removal_list as $last_global_removal) {

            if ($index == 0) $latest_unsubscribe_date = $unsubscribe_row['date_added'];

            //Util::printr($complaint_row);
            $sql = "SELECT id FROM user WHERE email=? AND is_mailable=1";

            $uStmt = $dbh->prepare($sql);
            $uStmt->execute(array($unsubscribe_row['email_address']));
            $uId = $uStmt->fetchColumn();

            if(!empty($uId)) {
                $sql = "UPDATE user SET is_mailable=0 WHERE id=?";
                $updStmt = $dbh->prepare($sql);
                $updStmt->execute(array($uId));
                $total_scrubbed_unsubscribe_users++;
                Util::log_to_file($log_file_name, "Unsubscribe Users Scrubbed", $unsubscribe_row['email_address']);
            }
            /*
            if ($user = $user_model->find_by_email($unsubscribe_row['email_address'])) {

                if ($user->is_mailable == 1) {
                    $user->is_mailable = 0;
                    $user->update();
                    $total_scrubbed_unsubscribe_users++;
                    Util::log_to_file($log_file_name, "Unsubscribe Users Scrubbed", $user->email);
                }
            }
            */

            $index++;
        }

        $latest_unsubscribe_list = null;

        echo 'Total Unsubscribe Users Scrubbed: ' . $total_scrubbed_unsubscribe_users . "\n";

        $ret['unsub_latest_date'] = $latest_unsubscribe_date;
        $ret['unsub_count'] = $latest_unsubscribe_count;
        $ret['unsub_user_scrubbed_count'] = $total_scrubbed_unsubscribe_users;

        return $ret;
    }
?>