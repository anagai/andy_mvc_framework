<?php

function getWelPerfSourceStats($partners, $dt, $logFile) {
    $data = array();
    $dt .= ' 00:00:00';
    $frDt = Util::getUtcDt($dt);
    $toDt = Util::getUtcDt(date("Y-m-d 00:00:00", strtotime($dt . " +1 day")));

    $frStamp = strtotime($frDt . " UTC");
    $toStamp = strtotime($toDt . " UTC");



    //$campList = "'" . implode("','", $campaigns) . "'";

    $url = "http://hydra.restdev.com/v1/site/metrics?method=query";
    $sql = "
        SELECT a.event_sub_id, a.partner_display_id, a.partner_source, a.event_type, count(1) as cnt FROM (
SELECT event_sub_id, partner_display_id, partner_source, event_email, event_type FROM [metrics.email_events]
where time_unix>={$frStamp} and time_unix<{$toStamp} and site='jobungo.co.uk' AND
event_sub_id LIKE 'JUK_WELCOME_%' AND
(
(event_type='click' AND (event_url like '%t=r%' OR event_url like '%t=d%')) OR
event_type='open' OR
(event_type='bounce' and event_bounce_type='hard') OR
event_type='spamreport' OR
event_type='unsubscribe'
) group by event_sub_id, partner_display_id, partner_source, event_email, event_type) as a group by a.event_sub_id, a.partner_display_id, a.partner_source, a.event_type
    ";
    $qry=http_build_query(array("sql"=>$sql));

    $resp = Util::curlReq($url, true, $qry);

    if($resp['status']!==200) {
        Util::log_to_file($logFile, 'Error getting BQ email_events data - getWelPerfSourceStats()', "http: {$resp['status']} resp: {$resp['response']}");
        return false;
    }

    $result = json_decode($resp['response'],true);

    //print_r($result['response']);

    foreach($result['response'] as $row) {

        $src = !empty($row['a_partner_source'])
            ? $row['a_partner_source']
            : '';

        if(isset($partners[$row['a_partner_display_id']])) {
            $pid = $row['a_partner_display_id'];
        } else {
            $pid = 'unknown';
            $src = '';
        }

        $camp = strtolower($row['a_event_sub_id']);

        if(preg_match('/welcome_noloc_\d/', $camp)) {
            //normal no location welcome
            $welType = 1;
        } elseif(preg_match('/np_noloc_\d/', $camp)) {
            //non-premium no location welcome
            $welType = 2;
        } elseif(preg_match('/np_\d/', $camp)) {
            //non-premium normal welcome
            $welType = 3;
        } else {
            //normal welcome
            $welType = 0;
        }

        if($row['a_event_type']=='open') {
            $data[$welType][$pid][$src]['open'] = $row['cnt'];
        } elseif($row['a_event_type']=='click') {
            $data[$welType][$pid][$src]['clik'] = $row['cnt'];
        } elseif($row['a_event_type']=='bounce') {
            $data[$welType][$pid][$src]['bnce'] = $row['cnt'];
        } elseif($row['a_event_type']=='spamreport') {
            $data[$welType][$pid][$src]['cmpt'] = $row['cnt'];
        } elseif($row['a_event_type']=='unsubscribe') {
            $data[$welType][$pid][$src]['unsub'] = $row['cnt'];
        }

    }

    unset($resp);
    unset($result);

     $sql = "
        SELECT camp_name, partner_display_id, partner_source, email_sent, count(1) as cnt FROM
        [metrics.campaign_send] where camp_name like 'JUK_WELCOME_%' and
        time_unix>={$frStamp} and time_unix<{$toStamp} group by camp_name, partner_display_id, partner_source, email_sent
    ";
    $qry=http_build_query(array("sql"=>$sql));

    $resp = Util::curlReq($url, true, $qry);

    if($resp['status']!==200) {
        Util::log_to_file($logFile, 'Error getting BQ campaign_send data - getWelPerfSourceStats()', "http: {$resp['status']} resp: {$resp['response']}");
        return false;
    }

    $result = json_decode($resp['response'],true);

    //print_r($result['response']);

    foreach($result['response'] as $row) {
        $sent = (int)$row['email_sent'];

        $src = !empty($row['partner_source'])
            ? $row['partner_source']
            : '';

        if(isset($partners[$row['partner_display_id']])) {
            $pid = $row['partner_display_id'];
        } else {
            $pid = 'unknown';
            $src = '';
        }

        $camp = strtolower($row['camp_name']);

        if(preg_match('/welcome_noloc_\d/', $camp)) {
            //normal no location welcome
            $welType = 1;
        } elseif(preg_match('/np_noloc_\d/', $camp)) {
            //non-premium no location welcome
            $welType = 2;
        } elseif(preg_match('/np_\d/', $camp)) {
            //non-premium normal welcome
            $welType = 3;
        } else {
            //normal welcome
            $welType = 0;
        }

        if($sent===1) {
            $data[$welType][$pid][$src]['sent'] = $row['cnt'];
        } else {
            $data[$welType][$pid][$src]['unsent'] = $row['cnt'];
        }

    }

    //print_r($data);

    echo "count: " . count($data) . "\n";
    echo "http: {$resp['status']} data: " . count($result['response']) . "\n";
    echo "frDt: {$frDt} toDt: {$toDt} frStamp: {$frStamp} toStamp: {$toStamp}\n";

    return $data;
}

function getWelcomeStats($partners, $campaigns, $dt) {
    // Get welcome campaign stats from ses k2 server
    $conn = Database::get_pdo_conn('ses_rw_prod');

    $nextDt = date('Y-m-d', strtotime($dt . ' +1 day'));
    $data = array();

    // Ensure no lock on table read on k2
    $conn->exec('SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;');
    foreach($campaigns as $camp) {
        echo 'camp: ' . $camp . "\n";

        $useStmt = $conn->prepare("USE ses_{$camp}");

        if(strpos($camp, 'noloc')!==false) {
            $welType = 1;
        } else {
            $welType = 0;
        }

        /*
        if($row['type']=='welcome_email_noloc') {
            $welType = 1;
        } else {
            $welType = 0;
        }
        */

        // db must exist otherwise go to next
        if($useStmt->execute()) {
            //$sql = "SELECT count(1) as cnt FROM click WHERE date_added>=? AND date_added<?";

            // Get click count for all partners
            $sql = "SELECT partner, source, email FROM click WHERE date_added>=? AND date_added<?
                    AND link_type IN ('result', 'redirect')
            ";
            $dStmt = $conn->prepare($sql);
            $dStmt->execute(array($dt, $nextDt));

            $clickCnt = 0;
            while($dRow=$dStmt->fetch(PDO::FETCH_ASSOC)) {
                $emailKey = dechex(crc32(strtolower($dRow['email'])));

                if(isset($partners[$dRow['partner']])) {
                    $pid = $dRow['partner'];
                    $src = $dRow['source'];
                } else {
                    $pid = 'unknown';
                    $src = '';
                }

                // Only count distinct emails per partner
                if(!isset($distEmail[$welType][$pid][$src][$emailKey])) {
                    $data[$welType][$pid][$src]['clik'] += 1;
                    $distEmail[$welType][$pid][$src][$emailKey] = 1;
                    $clickCnt++;
                }

            }
            $dStmt=NULL;
            $dRow=NULL;
            $distEmail=NULL;

            // Get open count for all partners
            $sql = "SELECT partner, source, email FROM open WHERE date_added>=? AND date_added<?";
            $dStmt = $conn->prepare($sql);
            $dStmt->execute(array($dt, $nextDt));
            //$cnt = $stmt->fetchColumn(0);
            $openCnt = 0;
            while($dRow=$dStmt->fetch(PDO::FETCH_ASSOC)) {
                $emailKey = dechex(crc32(strtolower($dRow['email'])));

                if(isset($partners[$dRow['partner']])) {
                    $pid = $dRow['partner'];
                    $src = $dRow['source'];
                } else {
                    $pid = 'unknown';
                    $src = '';
                }

                // Only count distinct emails per partner
                if(!isset($distEmail[$welType][$pid][$src][$emailKey])) {
                    $data[$welType][$pid][$src]['open'] += 1;
                    $distEmail[$welType][$pid][$src][$emailKey] = 1;
                    $openCnt++;
                }

            }
            $dStmt=NULL;
            $dRow=NULL;
            $distEmail=NULL;

            // Get open count for all partners
            $sql = "SELECT partner, source FROM sent WHERE date_sent>=? AND date_sent<?";
            $dStmt = $conn->prepare($sql);
            $dStmt->execute(array($dt, $nextDt));
            //$cnt = $stmt->fetchColumn(0);
            $sentCnt = 0;
            while($dRow=$dStmt->fetch(PDO::FETCH_ASSOC)) {

                if(isset($partners[$dRow['partner']])) {
                    $data[$welType][$dRow['partner']][$dRow['source']]['sent'] += 1;
                } else {
                    $data[$welType]['unknown']['']['sent'] += 1;
                }

                $sentCnt++;
            }
            $dStmt=NULL;
            $dRow=NULL;

            // Get open count for all partners
            $sql = "SELECT partner, source FROM unsent WHERE date_attempted>=? AND date_attempted<?";
            $dStmt = $conn->prepare($sql);
            $dStmt->execute(array($dt, $nextDt));
            //$cnt = $stmt->fetchColumn(0);
            $unsentCnt = 0;
            while($dRow=$dStmt->fetch(PDO::FETCH_ASSOC)) {

                if(isset($partners[$dRow['partner']])) {
                    $data[$welType][$dRow['partner']][$dRow['source']]['unsent'] += 1;
                } else {
                    $data[$welType]['unknown']['']['unsent'] += 1;
                }

                $unsentCnt++;
            }
            $dStmt=NULL;
            $dRow=NULL;

        } else {
            echo "Error: {$camp} db does not exist \n";
            continue;
        }

    }
    //Util::printr($data);
    //echo 'clicks: ' . $clickCnt . " opens: " . $openCnt . " sent: ". $sentCnt . " unsent: " . $unsentCnt . "\n";

    return $data;

}

function getDnmStats($partners, $campaigns, $dt) {
    // Get dnm stats from uk bacon

    $conn = Database::get_pdo_conn('bacon_rw_prod');
    $nextDt = date('Y-m-d', strtotime($dt . ' +1 day'));

    // Ensure no lock on table read on uk bacon
    $conn->exec('SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;');

    // Make welcome campaigns list
    //foreach($campaigns as $camp) {
    //    $campList .= "'" . $camp['camp'] . "',";
    //}

    //$campList = substr($campList,0,-1);

    //echo $campList . "\n";
    foreach($campaigns as $camp) {

        if(strpos($camp, 'noloc')!==false) {
            $welType = 1;
        } else {
            $welType = 0;
        }

        /*
        if($row['type']=='welcome_email_noloc') {
            $welType = 1;
        } else {
            $welType = 0;
        }
        */

        // Get all hard bounces for one day
        $sql = "
            SELECT partner, source, email_address as email FROM bounce WHERE date_added>=? and date_added<?
            AND type='hard' AND campaign_name=?
            ";

        $dStmt = $conn->prepare($sql);

        if($dStmt->execute(array($dt, $nextDt, $camp))) {
            $bnceCnt = 0;
            while($dRow=$dStmt->fetch(PDO::FETCH_ASSOC)) {
                $emailKey = dechex(crc32(strtolower($dRow['email'])));

                if(isset($partners[$dRow['partner']])) {
                    $pid = $dRow['partner'];
                    $src = $dRow['source'];
                } else {
                    $pid = 'unknown';
                    $src = '';
                }

                if(!isset($distEmail[$welType][$pid][$src][$emailKey])) {
                    $data[$welType][$pid][$src]['bnce'] += 1;
                    $distEmail[$welType][$pid][$src][$emailKey] = 1;
                    $bnceCnt++;
                }

            }
        } else {
            $err = $dStmt->errorInfo();
            echo $err[2];
        }

        //echo 'bounces: ' . $bnceCnt . "\n";

        $dStmt=NULL;
        $dRow=NULL;
        $distEmail=NULL;

        // Get all complaints for one day
        $sql = "
            SELECT partner, source, email_address as email FROM complaint WHERE date_added>=? and date_added<?
            AND campaign_name=?
            ";

        $dStmt = $conn->prepare($sql);

        if($dStmt->execute(array($dt, $nextDt, $camp))) {

            while($dRow=$dStmt->fetch(PDO::FETCH_ASSOC)) {
                $emailKey = dechex(crc32(strtolower($dRow['email'])));

                if(isset($partners[$dRow['partner']])) {
                    $pid = $dRow['partner'];
                    $src = $dRow['source'];
                } else {
                    $pid = 'unknown';
                    $src = '';
                }

                if(!isset($distEmail[$welType][$pid][$src][$emailKey])) {
                    $data[$welType][$pid][$src]['cmpt'] += 1;
                    $distEmail[$welType][$pid][$src][$emailKey] = 1;
                }

            }
        } else {
            $err = $dStmt->errorInfo();
            echo $err[2];
        }

        $dStmt=NULL;
        $dRow=NULL;
        $distEmail=NULL;

         // Get all unsubscribed for one day
        $sql = "
            SELECT partner, source, email_address as email FROM unsubscribe WHERE date_added>=? and date_added<?
            AND campaign_name=?
            ";

        $dStmt = $conn->prepare($sql);

        if($dStmt->execute(array($dt, $nextDt, $camp))) {

            while($dRow=$dStmt->fetch(PDO::FETCH_ASSOC)) {
                $emailKey = dechex(crc32(strtolower($dRow['email'])));

                if(isset($partners[$dRow['partner']])) {
                    $pid = $dRow['partner'];
                    $src = $dRow['source'];
                } else {
                    $pid = 'unknown';
                    $src = '';
                }

                if(!isset($distEmail[$welType][$pid][$src][$emailKey])) {
                    $data[$welType][$pid][$src]['unsub'] += 1;
                    $distEmail[$welType][$pid][$src][$emailKey] = 1;
                }
            }
        } else {
            $err = $dStmt->errorInfo();
            echo $err[2];
        }

        $dStmt=NULL;
        $dRow=NULL;
        $distEmail=NULL;
    }

    return $data;

}

function getSiteStats($conn, $partners, $dt) {
    // Get wullo site stats from mckinley

    $nextDt = date('Y-m-d', strtotime($dt . ' +1 day'));

    // Get user stats
    $sql = "SELECT partner, source, has_engaged FROM user WHERE date_added>=? and date_added<?";

    $dStmt = $conn->prepare($sql);

    if($dStmt->execute(array($dt, $nextDt))) {

        while($dRow=$dStmt->fetch(PDO::FETCH_ASSOC)) {
            if(isset($partners[$dRow['partner']])) {
                $pid = $dRow['partner'];
                $src = $dRow['source'];
            } else {
                $pid = 'unknown';
                $src = '';
            }

            if($dRow['has_engaged']==1) {
                $data[$pid][$src]['el2'] += 1;
            } else {
                $data[$pid][$src]['el1'] += 1;
            }
        }

    } else {
        $err = $dStmt->errorInfo();
        echo $err[2];
    }

    $dStmt=NULL;
    $dRow=NULL;

    // Get search result stats
    $sql = "SELECT partner, source FROM search_with_results WHERE date_searched>=? and date_searched<?";

    $dStmt = $conn->prepare($sql);

    if($dStmt->execute(array($dt, $nextDt))) {

        while($dRow=$dStmt->fetch(PDO::FETCH_ASSOC)) {
            if(isset($partners[$dRow['partner']])) {
                $data[$dRow['partner']][$dRow['source']]['srch'] += 1;
            } else {
                $data['unknown']['']['srch'] += 1;
            }
        }

    } else {
        $err = $dStmt->errorInfo();
        echo $err[2];
    }

    $dStmt=NULL;
    $dRow=NULL;

    // Get search result stats
    $sql = "SELECT partner, source FROM job_click WHERE date_clicked>=? and date_clicked<?";

    $dStmt = $conn->prepare($sql);

    if($dStmt->execute(array($dt, $nextDt))) {

        while($dRow=$dStmt->fetch(PDO::FETCH_ASSOC)) {
            if(isset($partners[$dRow['partner']])) {
                $data[$dRow['partner']][$dRow['source']]['jclik'] += 1;
            } else {
                $data['unknown']['']['jclik'] += 1;
            }
        }

    } else {
        $err = $dStmt->errorInfo();
        echo $err[2];
    }

    $dStmt=NULL;
    $dRow=NULL;

    //Util::printr($data);

    return $data;

}
