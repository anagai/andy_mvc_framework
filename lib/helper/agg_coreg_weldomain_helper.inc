<?php

function getDomain($email = '') {

    $email = strtolower($email);

    $domains = array(
        'aim'=>'aol',
        'aol'=>'aol',
        'compuserve'=>'aol',
        'cs'=>'aol',
        'games'=>'aol',
        'love'=>'aol',
        'netscape'=>'aol',
        'wmconnect'=>'aol',
        'wow'=>'aol',
        'ygm'=>'aol',
        'gmail'=>'google',
        'googlemail'=>'google',
        'homail'=>'microsoft',
        'hotamail'=>'microsoft',
        'hotamil'=>'microsoft',
        'hotmail'=>'microsoft',
        'hotmai'=>'microsoft',
        'hotmeil'=>'microsoft',
        'live'=>'microsoft',
        'microsoft'=>'microsoft',
        'microsoftnetwork'=>'microsoft',
        'msnbc'=>'microsoft',
        'msn'=>'microsoft',
        'otmail'=>'microsoft',
        'outlook'=>'microsoft',
        'webtv'=>'microsoft',
        'windowslive'=>'microsoft',
        'ameritech'=>'yahoo',
        'att'=>'yahoo',
        'bellsouth'=>'yahoo',
        'btinternet'=>'yahoo',
        'btopenworld'=>'yahoo',
        'demobroadband'=>'yahoo',
        'flash.nl.rogers'=>'yahoo',
        'geocities'=>'yahoo',
        'kimo'=>'yahoo',
        'maktoob'=>'yahoo',
        'nvbell'=>'yahoo',
        'oddpost'=>'yahoo',
        'ort.nl.rogers'=>'yahoo',
        'ort.rogers'=>'yahoo',
        'pacbell'=>'yahoo',
        'prodigy'=>'yahoo',
        'rocketmail'=>'yahoo',
        'rogers'=>'yahoo',
        'sbcglobal'=>'yahoo',
        'snet'=>'yahoo',
        'swbell'=>'yahoo',
        'uat.nl.rogers'=>'yahoo',
        'uat.rogers'=>'yahoo',
        'verizon'=>'yahoo',
        'wans'=>'yahoo',
        'y7mail'=>'yahoo',
        'yahoomail'=>'yahoo',
        'yahooxtra'=>'yahoo',
        'yahoo'=>'yahoo',
        'ymail'=>'yahoo',
        'comcast'=>'comcast',
    );

    //$email = uri_unescape( $email );

    //$email =~ m/@(.*)/;
    preg_match('/@(.*)/', $email, $domMatch);
    //echo 'email: ' . $email . "\n";
    //echo 'domain: ' . $domMatch[1] . "\n";
    if(!empty($domMatch[1])) {
        preg_match('/(\w+)\.(\w{2,4}\.\w+|\w+)$/', $domMatch[1], $famMatch);
        if(isset($domains[$famMatch[1]])) {
            return $domains[$famMatch[1]];
        } else {
            return 'other';
        }

    } else {
        return 'other';
    }

}

function getWelPerfDomainStats($dt, $logFile) {
    $data = array();
    $dt .= ' 00:00:00';
    $frDt = Util::getUtcDt($dt);
    $toDt = Util::getUtcDt(date("Y-m-d 00:00:00", strtotime($dt . " +1 day")));

    $frStamp = strtotime($frDt . " UTC");
    $toStamp = strtotime($toDt . " UTC");



    //$campList = "'" . implode("','", $campaigns) . "'";

    $url = "http://hydra.restdev.com/v1/site/metrics?method=query";
    $sql = "
        SELECT a.event_sub_id, a.event_domain_family, a.event_type, count(1) as cnt FROM (
SELECT event_sub_id, event_domain_family, event_email, event_type FROM [metrics.email_events]
where time_unix>={$frStamp} and time_unix<{$toStamp} and site='jobungo.co.uk' AND
event_sub_id LIKE 'JUK_WELCOME_%' AND
(
(event_type='click' AND (event_url like '%t=r%' OR event_url like '%t=d%')) OR
event_type='open' OR
(event_type='bounce' and event_bounce_type='hard') OR
event_type='spamreport' OR
event_type='unsubscribe'
) group by event_sub_id, event_domain_family, event_email, event_type) as a group by a.event_sub_id, a.event_domain_family, a.event_type
    ";
    $qry=http_build_query(array("sql"=>$sql));

    $resp = Util::curlReq($url, true, $qry);

    if($resp['status']!==200) {
        Util::log_to_file($logFile, 'Error getting BQ email_events data - getWelPerfDomainStats()', "http: {$resp['status']} resp: {$resp['response']}");
        return false;
    }

    $result = json_decode($resp['response'],true);

    //print_r($result['response']);

    foreach($result['response'] as $row) {

        $camp = strtolower($row['a_event_sub_id']);

        $domain = !empty($row['a_event_domain_family'])
            ? $row['a_event_domain_family']
            : 'other';

        if(preg_match('/welcome_noloc_\d/', $camp)) {
            //normal no location welcome
            $welType = 1;
        } elseif(preg_match('/np_noloc_\d/', $camp)) {
            //non-premium no location welcome
            $welType = 2;
        } elseif(preg_match('/np_\d/', $camp)) {
            //non-premium normal welcome
            $welType = 3;
        } else {
            //normal welcome
            $welType = 0;
        }

        if($row['a_event_type']=='open') {
            $data[$welType][$domain]['open'] = $row['cnt'];
        } elseif($row['a_event_type']=='click') {
            $data[$welType][$domain]['clik'] = $row['cnt'];
        } elseif($row['a_event_type']=='bounce') {
            $data[$welType][$domain]['bnce'] = $row['cnt'];
        } elseif($row['a_event_type']=='spamreport') {
            $data[$welType][$domain]['cmpt'] = $row['cnt'];
        } elseif($row['a_event_type']=='unsubscribe') {
            $data[$welType][$domain]['unsub'] = $row['cnt'];
        }

    }

    unset($resp);
    unset($result);

     $sql = "
        SELECT camp_name, user_domain_family, email_sent, count(1) as cnt FROM
        [metrics.campaign_send] where camp_name like 'JUK_WELCOME_%' and
        time_unix>={$frStamp} and time_unix<{$toStamp} group by camp_name, user_domain_family, email_sent
    ";
    $qry=http_build_query(array("sql"=>$sql));

    $resp = Util::curlReq($url, true, $qry);

    if($resp['status']!==200) {
        Util::log_to_file($logFile, 'Error getting BQ campaign_send data - getWelPerfDomainStats()', "http: {$resp['status']} resp: {$resp['response']}");
        return false;
    }

    $result = json_decode($resp['response'],true);

    //print_r($result['response']);

    foreach($result['response'] as $row) {
        $sent = (int)$row['email_sent'];

        $camp = strtolower($row['camp_name']);

        $domain = !empty($row['user_domain_family'])
            ? $row['user_domain_family']
            : 'other';

        if(preg_match('/welcome_noloc_\d/', $camp)) {
            //normal no location welcome
            $welType = 1;
        } elseif(preg_match('/np_noloc_\d/', $camp)) {
            //non-premium no location welcome
            $welType = 2;
        } elseif(preg_match('/np_\d/', $camp)) {
            //non-premium normal welcome
            $welType = 3;
        } else {
            //normal welcome
            $welType = 0;
        }

        if($sent===1) {
            $data[$welType][$domain]['sent'] = $row['cnt'];
        } else {
            //$data[$welType][$domain]['unsent'] = $row['cnt'];
        }

    }

    //print_r($data);

    echo "count: " . count($data) . "\n";
    echo "http: {$resp['status']} data: " . count($result['response']) . "\n";
    echo "frDt: {$frDt} toDt: {$toDt} frStamp: {$frStamp} toStamp: {$toStamp}\n";

    return $data;
}

function getWelcomeStats($campaigns, $dt) {
    // Get welcome campaign stats from ses k2 server
    $conn = Database::get_pdo_conn('ses_rw_prod');

    $nextDt = date('Y-m-d', strtotime($dt . ' +1 day'));
    $data = array();

    // Ensure no lock on table read on k2
    $conn->exec('SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;');
    foreach($campaigns as $camp) {
        echo 'camp: ' . $camp . "\n";
        $useStmt = $conn->prepare("USE ses_{$camp}");

        if(strpos($camp, 'noloc')!==false) {
            $welType = 1;
        } else {
            $welType = 0;
        }

        /*
        if($row['type']=='welcome_email_noloc') {
            $welType = 1;
        } else {
            $welType = 0;
        }
        */

        // db must exist otherwise go to next
        if($useStmt->execute()) {
            //$sql = "SELECT count(1) as cnt FROM click WHERE date_added>=? AND date_added<?";

            // Get click count for all partners
            $sql = "SELECT email FROM click WHERE date_added>=? AND date_added<?
                    AND link_type IN ('result', 'redirect')
            ";
            $dStmt = $conn->prepare($sql);
            $dStmt->execute(array($dt, $nextDt));

            $clickCnt = 0;
            while($dRow=$dStmt->fetch(PDO::FETCH_ASSOC)) {
                $emailKey = dechex(crc32(strtolower($dRow['email'])));
                $domain = getDomain($dRow['email']);

                // Only count distinct emails per partner
                if(!isset($distEmail[$welType][$domain][$emailKey])) {
                    $data[$welType][$domain]['clik'] += 1;
                    $distEmail[$welType][$domain][$emailKey] = 1;
                    $clickCnt++;
                }

            }
            $dStmt=NULL;
            $dRow=NULL;
            $distEmail=NULL;

            // Get open count for all partners
            $sql = "SELECT email FROM open WHERE date_added>=? AND date_added<?";
            $dStmt = $conn->prepare($sql);
            $dStmt->execute(array($dt, $nextDt));
            //$cnt = $stmt->fetchColumn(0);
            $openCnt = 0;
            while($dRow=$dStmt->fetch(PDO::FETCH_ASSOC)) {
                $emailKey = dechex(crc32(strtolower($dRow['email'])));
                $domain = getDomain($dRow['email']);

                // Only count distinct emails per partner
                if(!isset($distEmail[$welType][$domain][$emailKey])) {
                    $data[$welType][$domain]['open'] += 1;
                    $distEmail[$welType][$domain][$emailKey] = 1;
                    $openCnt++;
                }
            }
            $dStmt=NULL;
            $dRow=NULL;
            $distEmail=NULL;

            // Get open count for all partners
            $sql = "SELECT email FROM sent WHERE date_sent>=? AND date_sent<?";
            $dStmt = $conn->prepare($sql);
            $dStmt->execute(array($dt, $nextDt));
            //$cnt = $stmt->fetchColumn(0);
            $sentCnt = 0;
            while($dRow=$dStmt->fetch(PDO::FETCH_ASSOC)) {
                $domain = getDomain($dRow['email']);
                $data[$welType][$domain]['sent'] += 1;
                $sentCnt++;
            }
            $dStmt=NULL;
            $dRow=NULL;

            /*
            // Get open count for all partners
            $sql = "SELECT email FROM unsent WHERE date_attempted>=? AND date_attempted<?";
            $dStmt = $conn->prepare($sql);
            $dStmt->execute(array($dt, $nextDt));
            //$cnt = $stmt->fetchColumn(0);
            $unsentCnt = 0;
            while($dRow=$dStmt->fetch(PDO::FETCH_ASSOC)) {
                $domain = getDomain($dRow['email']);
                $data[$domain]['unsent'] += 1;
                $unsentCnt++;
            }
            $dStmt=NULL;
            $dRow=NULL;
            */

        } else {
            echo "Error: {$camp} db does not exist \n";
            continue;
        }




    }
    //Util::printr($data);
    //echo 'clicks: ' . $clickCnt . " opens: " . $openCnt . " sent: ". $sentCnt . " unsent: " . $unsentCnt . "\n";

    return $data;

}

function getDnmStats($campaigns, $dt) {
    // Get dnm stats from uk bacon

    $conn = Database::get_pdo_conn('bacon_rw_prod');
    $nextDt = date('Y-m-d', strtotime($dt . ' +1 day'));
    $data = FALSE;

    // Ensure no lock on table read on uk bacon
    $conn->exec('SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;');

    // Make welcome campaigns list
    //foreach($campaigns as $camp) {
    //    $campList .= "'" . $camp['camp'] . "',";
    //}

    //$campList = substr($campList,0,-1);

    //echo $campList . "\n";
    foreach($campaigns as $camp) {

        //echo "proc dnmstats: " . $camp . "\n";

        if(strpos($camp, 'noloc')!==false) {
            $welType = 1;
        } else {
            $welType = 0;
        }

        /*
        if($row['type']=='welcome_email_noloc') {
            $welType = 1;
        } else {
            $welType = 0;
        }
        */

        // Get all hard bounces for one day
        $sql = "
            SELECT email_address as email FROM bounce WHERE date_added >= ? and date_added < ?
            AND type='hard' AND campaign_name=?
            ";

        $dStmt = $conn->prepare($sql);

        if($dStmt->execute(array($dt, $nextDt, $camp))) {
            //echo "bounces: " . $dStmt->rowCount() . "\n";
            while($dRow=$dStmt->fetch(PDO::FETCH_ASSOC)) {
                $emailKey = dechex(crc32(strtolower($dRow['email'])));
                $domain = getDomain($dRow['email']);

                if(!isset($distEmail[$welType][$domain][$emailKey])) {
                    $data[$welType][$domain]['bnce'] += 1;
                    $distEmail[$welType][$domain][$emailKey] = 1;
                }

            }
        } else {
            $err = $dStmt->errorInfo();
            echo $err[2];
        }

        $dStmt=NULL;
        $dRow=NULL;
        $distEmail=NULL;

            // Get all complaints for one day
        $sql = "
            SELECT email_address as email FROM complaint WHERE date_added >= ? and date_added < ?
            AND campaign_name=?
            ";

        $dStmt = $conn->prepare($sql);

        if($dStmt->execute(array($dt, $nextDt, $camp))) {
            //echo "complaints: " . $dStmt->rowCount() . "\n";
            while($dRow=$dStmt->fetch(PDO::FETCH_ASSOC)) {
                $emailKey = dechex(crc32(strtolower($dRow['email'])));
                $domain = getDomain($dRow['email']);

                if(!isset($distEmail[$welType][$domain][$emailKey])) {
                    $data[$welType][$domain]['cmpt'] += 1;
                    $distEmail[$welType][$domain][$emailKey] = 1;
                }

            }

        } else {
            $err = $dStmt->errorInfo();
            echo $err[2];
        }

        $dStmt=NULL;
        $dRow=NULL;
        $distEmail=NULL;

         // Get all unsubscribed for one day
        $sql = "
            SELECT email_address as email FROM unsubscribe WHERE date_added >= ? and date_added < ?
            AND campaign_name=?
            ";

        $dStmt = $conn->prepare($sql);

        if($dStmt->execute(array($dt, $nextDt, $camp))) {
            //echo "unsubscribes: " . $dStmt->rowCount() . "\n";
            while($dRow=$dStmt->fetch(PDO::FETCH_ASSOC)) {
                $emailKey = dechex(crc32(strtolower($dRow['email'])));
                $domain = getDomain($dRow['email']);

                if(!isset($distEmail[$welType][$domain][$emailKey])) {
                    $data[$welType][$domain]['unsub'] += 1;
                    $distEmail[$welType][$domain][$emailKey] = 1;
                }
            }

        } else {
            $err = $dStmt->errorInfo();
            echo $err[2];
        }

        $dStmt=NULL;
        $dRow=NULL;
        $distEmail=NULL;

    }

    return $data;

}
