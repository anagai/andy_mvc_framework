<?php
	// vim: ai ts=4 sts=4 et sw=4 tw=79

	/**
	 * Created on Oct 8, 2012
	 * Written By Andy Nagai
	 *
	 */

	abstract class Controller extends Object {

	    protected $_task;
	    protected $_taskFunc;
	    protected $_output = array();

	    protected abstract function _initState();
	    protected abstract function _display();

	    protected function _initController() {
	    	// Called when controller instantiated
	    	// task should match function name without underscore
            $this->_task = Util::getVar('task');

            $this->_setTaskFunc();

            $this->_initState();

	    }

	    protected function _setTaskFunc() {
	    	// Set the name of task function to execute
	    	$this->_taskFunc = !empty($this->_task)
                ? '_' . $this->_task
                : '_display';
	    }

	    public function runTask() {
	    	// Called from view after controller instantiated
	    	// Execute the task method in child controller
	    	$func = $this->_taskFunc;

            // execute task function
            $this->$func();

            // return output to view
            return $this->_output;
	    }

	    /**
	     * Add object or value to output array
	     *
	     */
	    protected function addOut($name, $val) {

	        if(isset($this->_output[$name])) {
	            $this->_output[$name] = $val;
	        } else {
	            $this->_output = array_merge($this->_output, array($name => $val));
	        }
	    }

	    protected function addMetricsData($metrics){
		foreach( $metrics as $key => $val ){
			$this->addOut($key, $val);
		}
	    }

	}
