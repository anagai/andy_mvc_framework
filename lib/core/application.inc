<?php
// vim: ai ts=4 sts=4 et sw=4 tw=79
class Application extends Object {

    protected $_view;
    protected $_routes = array();
    protected $_view_url;
    protected $_debug_stmt = array();
    protected $_debug_sql_stmt = array();
    protected $_scripts = array();

    public function __construct() {

        $this->_define_routes();

    }

    public function init() {

    	// Set maintenance switch.
    	// True - Site will be down


    	$this->_set_maintenance_switches();

    }

    public function valid_us_ip() {
    	//$geocode = Util::load_model('geocodelocation');

    	//return $geocode->is_us_ip();
    }

    /**
     * Directs users to the correct view, depending on the given page route.
     * Expects the link to be of this format:
     *      http://www.example.com/FOO/other/routing/info/
     *
     * -where FOO is the name of the view to be rendered to the user.
     * To support legacy links and/or SEO targeting or the like, this handler
     * also supports links like these:
     *      http://www.example.com/FOO.html/other/routing/info/
     *      http://www.example.com/FOO.php/other/routing/info/
     *      http://www.example.com/FOO.cf/other/routing/info/
     *
     * -which will all be understood as a request to call FOO.
     *
     * Accepts a parameter ($target) which allows a developer to force a view.
     */
    public function route( $target = FALSE ) {

    	// Modified to use REQUEST_URI instead of the Apache-specific
        // REDIRECT_URL: we don't want to tie ourselves to Apache, since
        // at some point in the future we _may_ use nginx, lighttpd,
        // Varnish, or some other server.
        // -suy, 20121015
        //
        // Ref: http://stackoverflow.com/questions/6483912/

        $redirect_url = "/";
        if( isset( $_SERVER[ "REQUEST_URI" ] ) ) {
            $redirect_url = $_SERVER[ "REQUEST_URI" ];

            // We do this to preserve the behavior we were seeing when
            // we were using REDIRECT_URL (REDIRECT_URL does not include
            // the query string).
            $have_valid_query_string = isset( $_SERVER[ "QUERY_STRING" ] )
                && $_SERVER[ "QUERY_STRING" ];
            if( $have_valid_query_string ) {
                $query_string_index = strpos( $redirect_url,
                    $_SERVER[ "QUERY_STRING" ] );
                $redirect_url = substr( $redirect_url, 0,
                    $query_string_index - 1 );
            }

            // All views will be referred to by lowercase names.
            $redirect_url = strtolower( $redirect_url );
            $redirect_url = trim( $redirect_url );

            $qry_str = $_SERVER[ "QUERY_STRING" ];
        }

        // Go home unless we're told otherwise.
        // TODO: Make this configurable from either the DB or an INI file.
        $view = "home";

        // Pre-initializing to avoid a warning is cheap (and thus worth it).
        // (Just in case we need debugging output and don't have a query.)
        $path_parts = array();

        // REQUEST_URI will always return a single forward-slash if we've
        // hit the home page.  For completeness's sake, we're also
        // checking for a non-empty redirect URL.
        $have_redirect_url = !empty( $redirect_url )
            && $redirect_url != "/";

        if( $have_redirect_url ) {
            // The view will _always_ be the first segment of the URL.
            $path_parts = explode( "/", $redirect_url );

            // Account for corner cases:
            //      http://www.example.com/FOO//additional/slashes/
            //      http://www.example.com/FOO//more//additional////slashes/
            // To handle these cases, we just need to remove empty values.

            // Remove empty value array items
            $path_parts = array_filter( $path_parts );

            // Get the view segment
            $view_segment = array_shift( $path_parts );

            // Separate the view name from any extension
            $view_parts = explode( '.', $view_segment );

            // Get the view name
            $view = array_shift( $view_parts );

            $full_path_parts = $path_parts;

            $invalidRedir = false;

            // Redir if email detected in url segment or query string value.
            // Redirect with clean path to make sure no url with email lands on
            // any page.
            if($view!=='partnersfeed') {
                foreach($full_path_parts as $ndx=>$segment) {
                    $segment = urldecode($segment);
                    // convert space to plus for possible email vals
                    if(preg_match("/@.*\./", $segment)) {
                            $segment = str_replace(' ','+', $segment);
                    }
                    if (filter_var($segment, FILTER_VALIDATE_EMAIL)) {
                        if(preg_match("/^q\-/", $segment)) {
                            $full_path_parts[$ndx] = "q-";
                        } elseif(preg_match("/^l\-/", $segment)) {
                            $full_path_parts[$ndx] = "l-";
                        } else {
                            unset($full_path_parts[$ndx]);
                        }
                        // PII detected in path. redirect with clean path
                        $invalidRedir = true;
                    }
                }

                if(!empty($qry_str)) {
                    parse_str( $qry_str, $qry_parts );
                    foreach($qry_parts as $param=>$paramVal) {
                        $paramVal = urldecode($paramVal);
                        // convert space to plus for possible email vals
                        if(preg_match("/@.*\./", $paramVal)) {
                            $paramVal = str_replace(' ','+', $paramVal);
                        }
                        if(filter_var($paramVal, FILTER_VALIDATE_EMAIL)) {
                            $qry_parts[$param] = '';
                            // PII detected in query string. redirect with clean path
                            $invalidRedir = true;
                        } else {
                            $qry_parts[$param] = $paramVal;
                        }
                    }
                }

                if($invalidRedir===true) {
                    $invalidRedirUrl = "http://{$_SERVER['HTTP_HOST']}/" . implode('/',$full_path_parts);

                    if(count($qry_parts)>0) {
                        $invalidRedirUrl .= '/?' . http_build_query($qry_parts);
                    }

                    header("Location: {$invalidRedirUrl}");
                }

            }
            // E.g.: http://www.example.com/BAR/other/routing/info/
            //      View: "BAR"
            //
            // E.g.: http://www.example.com/FOO.html/other/routing/info/
            //      View: "FOO"
            //
            // E.g.: http://www.example.com/FOO-BAR/other/routing/info/
            //      View: "FOO-BAR"
            //
            // E.g.: http://www.example.com/FOO-BAR.html/other/routing/info/
            //      View: "FOO-BAR"
            //
            // E.g.: http://www.example.com/FOO-BAR.xml.pdf/other/routing/info/
            //      View: "FOO-BAR"



            // This handles all non-jobs and any nested subdir path views.
            if($view != 'jobs' && $view != 'redir') {

            	$view_parts = explode( ".", $redirect_url );

            	// Get the complete view path. There will be no q and l segments.

            	// Get the view name minus extension and remove leading slash
            	$view = substr(array_shift( $view_parts ),1);

            	// Replace url dash with appropriate file system dashes
            	$view = str_replace('/', DS, $view);

            }

            //Util::debug($view);

        }

        // Allow a passed-in target-view to override what we got in the URL.
        if( $target ) {
            $view = $target;
        }

        // Assume the view name is a real view unless proven otherwise.
        $this->_view = $view;

        // Assumption: routes are defined on this Application object
        //      _before_ we call this method!
        $is_defined_route = isset( $this->_routes[ $view ] );
        if( $is_defined_route ) {

            $_route = $this->_routes[ $view ];

            // A custom route will have three parameters.
            // E.g. custom route "jobs-in-tustin-ca" might have fields:
            //      view => "jobs"
            //      query => "q=php&l=irvine,%20ca"
            //      template => "default"
            //
            // For routing, we're primarily concerned with the "view"
            // and "query" fields: we will render the template filename
            // given by the "view" field and pass in the data given
            // by the "query" field.
            $this->_view = $_route[ 'view' ];

            // We want to overwrite any existent parameters with whatever's
            // given in the defined route.
            //
            // Assumption: custom routes will _always_ have a "query" field!
            $query = $_route[ 'query' ];
            if( $query ) {
                parse_str( $query, $query_parts );
                foreach( $query_parts as $key => $value ) {
                    // We overwrite with the value given in the "query"
                    // field, irrespective of whether or not it has a value.
                    //      E.g.: "q=&l=92882" => overwrite q with blank
                    //
                    // Spencer confirmed on 2012-10-16 that it's not a probable
                    // use case that we would use a custom route, but choose
                    // to override the custom route parameters with actual
                    // GET parameters, e.g.:
                    //      http://www.example.com/custom-route/?q=sales
                    // As a result, we do _not_ need to account for possible
                    // overriding GET parameters in the query string.
                    // -suy, 20121016
                    $_GET[ $key ] = $value;
                }
            }
        }



        $db = Util::get_dbo();
        // XXX: Debugging output
        $is_defined_str = $is_defined_route ? "YES" : "NO";
        $debug_message = "
            <ul>
                <li><strong>request uri:</strong> " . $_SERVER[ "REQUEST_URI" ] . "</li>
                <li><strong>redirect url:</strong> " . $redirect_url . "</li>
                <li><strong>query str:</strong> " . $qry_str . "</li>
                <li><strong>orig path parts:</strong> " . print_r($full_path_parts,true) . "</li>
                <li><strong>final path parts:</strong> " . print_r($path_parts,true) . "</li>
                <li><strong>view parts:</strong> " . print_r($view_parts,true) . "</li>
                <li><strong>DB Server:</strong> " . $db->current_server() . "</li>
                <li><strong>DB Name:</strong> " . $db->current_db() . "</li>
                <li><strong>SERVER:</strong> " . print_r($_SERVER, true) . "</li>
                <li><strong>Target:</strong> '$target'</li>
                <li><strong>View:</strong> '$view'</li>
                <li><strong>Custom route?</strong> $is_defined_str</li>
                <li><strong>Path contents:</strong><ul>";
        foreach( $path_parts as $index => $part ) {
            $debug_message .= "<li>'$index' => '$part'</li>\n";
        }
        $this->_app_debug_msg .= "</ul></li>\n";

        // Display all $_GET values
        $debug_message .= "<li><strong>\$_GET contents:</strong><ul>";
        foreach( $_GET as $key => $value ) {
            $debug_message .= "<li>'$key' => '$value'</li>\n";
        }
        $debug_message .= "
                    </ul></li>\n";

        // Display all $_SESSION values
        $debug_message .= "<li><strong>\$_SESSION contents:</strong><ul>";
        foreach( $_SESSION as $key => $value ) {
        	$debug_message .= "<li>'$key' => '$value'</li>\n";
        }
        $debug_message .= "
        			</ul></li>\n
        	</ul>
        	";

        Util::debug( $debug_message );
    }

    /**
     * Renders the page html. If necessary will implement
     * templating parsing and view integration system here.
     */
    public function render() {

    	global $db, $database, $app;

        // When view is not found display 404 error view
        if(!file_exists(VIEW_PATH . $this->_view .".inc")) {
            $this->_view = '404';
        }

        include_once(VIEW_PATH . $this->_view .".inc");

    }

    /**
     * Handle special routes that can be directed to specific view and
     * set of querystring parameters. The query values will be added
     * to $_GET object
     *
     * Each custom route contains three fields:
     *  - view: the view which will be shown to the user
     *  - query: the query string to be passed to the controller
     *  - template: the template (e.g. results page template) which will
     *      be rendered in the view
     *
     * E.g.: We have a view named "jobs" which will render job results.
     * That view will have templates which will be rendered inside of it,
     * e.g. a 3-column template called "201210_3_column" or
     * e.g. an Inuvo-ads template called "201210_inuvo_ads".
     */
    private function _define_routes() {

        $this->_routes["home"] =
            array("view"        => "home",
                "query"             => "",
                "page_title"    => "Home",
                "canonical"        => "l",
                "template"        => "home");

        $this->_routes["jobs"] =
            array("view"        => "jobs",
                "query"             => "",
                   "page_title"    => "Jobs",
                   "canonical"     => "q&l",
                "template"        => "jobs");

        $this->_routes["searching"] =
            array("view"        => "searching",
                "query"             => "",
                "page_title"    => "Who's Searching Now",
                "canonical"     => "",
                "template"        => "home");

        $this->_routes["recommended_jobs"] =
        array("view"        => "recommended_jobs",
                "query"             => "",
                "page_title"    => "Recommended Jobs",
                "canonical"     => "",
                "template"        => "jobs");

        $this->_routes["version_test"] =
        array("view"        => "version_test",
        		"query"             => "",
        		"page_title"    => "Job Result Version Tester",
        		"canonical"     => "",
        		"template"        => "jobs");

        $this->_routes["privacy"] =
        array("view"        => "privacy",
        		"query"             => "",
        		"page_title"    => "Privacy Policy",
        		"canonical"     => "",
        		"template"        => "home");

        $this->_routes["cookies"] =
        array("view"        => "cookies",
                "query"             => "",
                "page_title"    => "Cookie Notice",
                "canonical"     => "",
                "template"        => "home");

        $this->_routes["faq"] =
        array("view"        => "faq",
        		"query"             => "",
        		"page_title"    => "FAQ",
        		"canonical"     => "",
        		"template"        => "home");

        $this->_routes["contact"] =
        array("view"        => "contact",
        		"query"             => "",
        		"page_title"    => "Contact Us",
        		"canonical"     => "",
        		"template"        => "home");

        $this->_routes["unsubscribe"] =
        array("view"        => "unsubscribe",
        		"query"             => "",
        		"page_title"    => "Unsubscribe",
        		"canonical"     => "",
        		"template"        => "home");

        $this->_routes["employers_contact"] =
        array("view"        => "employers_contact",
        		"query"             => "",
        		"page_title"    => "Employer's Contact",
        		"canonical"     => "",
        		"template"        => "home");

        $this->_routes["about"] =
        array("view"        => "about",
        		"query"             => "",
        		"page_title"    => "About Us",
        		"canonical"     => "",
        		"template"        => "home");


    }

    /**
     * Return route object by name
     *
     * @param string $name
     * @return string
     */
    public function get_route($name) {
        return isset($this->_routes[$name]) ? $this->_routes[$name] : NULL;
    }

    /**
     * Get single route parameter
     *
     * @param string $name
     * @param string $param
     * @return string
     */
    public function get_route_param($name, $param) {

        if(!isset($this->_routes[$name]) || empty($param)) {
            return NULL;
        }

        return isset($this->_routes[$name][$param]) ? $this->_routes[$name][$param] : NULL;
    }

    public function get_canonical_url() {

        // Get canonical pattern
        $pattern = $this->get_route_param($this->_view, 'canonical');
        $pattern_exp = explode('&', $pattern);
        $canon_params = array();

        // Build canonical array based on pattern
        foreach($pattern_exp as $param) {
            $val = Util::getVar($param);
            if($val !== NULL) {
                $canon_params[$param] = $val;
            }
        }

        // Build the query string from array with urlencoded values e.g. q=php&l=irvine
        $query = http_build_query($canon_params);

        //Util::debug($query);

        // Make the friendly url e.g. http://jobungo.uk/jobs/q-php/l-irvine/
        $route = Util::build_route(Util::base_url() . '/' . $this->_view . '/?' . $query);



        return $route;

    }

    public function get_view() {
        return $this->_view;
    }

    public function set_view($view) {
    	$this->_view = $view;
    }

    public function get_page_title() {
        return $this->get_route_param($this->_view, 'page_title');
    }

    public function get_page_template() {

        $template = $this->get_route_param($this->_view, 'template');

        if(empty($template)) {
            $template = 'home';
        }

        return $template;

    }

    public function make_search_title() {

        $title = '';

        $keyword = ucwords(Util::get_session('keyword'));
        $location = ucwords(Util::get_session('location'));

        if($keyword) {
            $keyword .= ' ';
        }

        $title = $keyword . 'Jobs, Employment';

        if($location) {
            $title .= ' in ' . $location;
        }

           return $title;

    }

    public function make_meta_keyword_string() {

        $str = '';

        $keyword = Util::get_session('keyword');
        $location = Util::get_session('location');

        if ($keyword) {
            $str = ', ' . $keyword . ' jobs';
        }

        if ($location) {
               $str .= ', ' . $location . ' UK jobs';
        }

        if($location && $keyword) {
            $str .= ', ' . $location . ' UK ' . $keyword . ' jobs';
            $str .= ', ' . $keyword . ' jobs in ' . $location . ' UK';
        }

        return $str;

    }

    public function add_debug_stmt($inp) {

        $this->_debug_stmt[] = $inp;

    }

    public function add_debug_sql_stmt($inp) {

        $this->_debug_sql_stmt[] = $inp;

    }

    /**
     * Prints all debug statements stored in Application object
     *
     */
    public function print_debug_stmt() {

        print '<br><br>----------------------------------------- DEBUG SECTION ------------------------------------------------<br><br>';
        foreach($this->_debug_stmt as $key => $val) {
            print '[file] : ' . $this->_debug_stmt[$key]['file'] . '<br>';
            if($this->_debug_stmt[$key]['class']){
                print '[class] : ' . $this->_debug_stmt[$key]['class'] . '<br>';
            }
            if($this->_debug_stmt[$key]['function'] &&
               $this->_debug_stmt[$key]['function'] != 'include_once' &&
               $this->_debug_stmt[$key]['function'] != 'require_once'){
                print '[function] : ' . $this->_debug_stmt[$key]['function'] . '()<br>';
            }
            print '[line] : ' . $this->_debug_stmt[$key]['line'] . '<br>';
            print '[output] : <br><br>';
            print $this->_debug_stmt[$key]['msg'] .'<br><br>';

        }

    }

    /**
     * Print all sql statements generated from Database object
     *
     */
    public function print_debug_sql_stmt() {
        print '<br><br>------------------------------------------------ SQL -------------------------------------------------------<br><br>';

        foreach($this->_debug_sql_stmt as $key => $val) {
            print $val .'<br><br>';
        }

    }

    public function print_load_stats() {
    	print '<br><br>------------------------------------------------ Load Stats -------------------------------------------------------<br><br>';

    	echo 'Time:'.number_format(microtime(TRUE)-$GLOBALS['Start'],7).' Mem:'.number_format(memory_get_usage()/1024,1).'kb ('.number_format(memory_get_peak_usage()/1024,1).'kb)';

    }

    public function add_script($script) {
    	$this->_scripts[] = $script;
    }

    public function include_scripts() {

    	foreach($this->_scripts as $script) {
    		echo '<script type="text/javascript" src="' . $script ."\"></script>\n";
    	}

    }

    private function _set_maintenance_switches() {

	    // Load maintenance switch file
	    $ini_arr = parse_ini_file(CONFIG_PATH.'maintenance.ini', true);
	    $ini_arr = array_shift($ini_arr);

	    //Util::printr($ini_arr);

	    $is_maintenance = $ini_arr['mtn'] == 'ON'
	    	? TRUE
	    		: FALSE;

	    $is_feed = $ini_arr['feed'] == 'ON'
	    	? TRUE
	    		: FALSE;

	    // Do not allow partners feed when maintenance mode is ON
	    if($is_maintenance == true) {
	    	$is_feed = false;
	    }

	    // Set maintenance switch, true - Site will be down for maintenance
	    define('MAINTENANCE', $is_maintenance);

	    // Set partnersfeed switch. true - partnersfeed allowed
	    define('COREG_FEED', $is_feed);

    }


}
