<?php

    /*
     * Created on Mar 27, 2012
     * Written By Sung Hwan Ahn / Andy Nagai
     */

     // Indeed Job Result API

    class Indeed extends JobAPI {

        protected $publisher_id;
        protected $url = "http://api.indeed.com/ads/apisearch?";

        // $publisher should be either "organic" or "sponsored"
        // $traffic should be among "organic", "email", or "mobile"
        public function __construct($publisher = "sponsored", $traffic = "organic", $channel='') {

            // Andy: Execute parent constructor
            parent::__construct();

            // Read the publisher id from INI file
            $ini_array = parse_ini_file(CONFIG_PATH.'indeed_publisher_id.ini', true);
            $this->publisher_id = $ini_array[$publisher]["publisher_id"];

            // Read the traffic channel from INI file
            $ini_array = parse_ini_file(CONFIG_PATH.'indeed_traffic_channel.ini', true);

            $this->channel = !empty($channel)
                ? $channel
                : $ini_array[$traffic]["channel"];

            $isOrganic = Util::is_session('uid')
                ? 0
                : 1;

            switch($publisher) {
                case 'sponsored':
                    $pub = 's';
                break;
                case 'organic':
                    $pub = 'o';
                break;
                default:
                    $pub = '';
            }

            switch($traffic) {
                case 'email':
                    $traf = 'e';
                break;
                case 'mobile':
                    $traf = 'm';
                break;
                case 'organic';
                    $traf = 'o';
                break;
                default:
                    $traf = '';
            }

            $dbConn = Database::get_pdo_conn(DEFAULT_DB_SETTING);

            $sql = "
               INSERT INTO indeedRequests (Date, PubKey, Channel, TrafficType,
                Organic, Requests) VALUE (NOW(), ?, ?, ?, ?, 1)
                ON DUPLICATE KEY UPDATE Requests = Requests+1;
            ";

            //$stmt = $dbConn->prepare($sql);
            //$stmt->execute(array($pub, $this->channel, $traf, $isOrganic));

        }

        protected function buildParams() {
            // Build parameters for query string

            $remoteip = !empty($_SERVER['HTTP_X_FORWARDED_FOR'])
                ? $_SERVER['HTTP_X_FORWARDED_FOR']
                : $_SERVER['REMOTE_ADDR'];

            $sanitized_params = array(
                "publisher" => $this->publisher_id,
                "v" => 2,
                "format" => "json",
                "sort" => "relevance",
                "highlight" => 1,
                "userip" => $remoteip,
                "useragent" => $_SERVER['HTTP_USER_AGENT'],
                "chnl" => $this->channel,
                "start" => $this->start,
                "limit" => $this->limit,
                "q" => $this->keyword,
                "l" => $this->location,
                "radius" => $this->radius,
                "fromage" => $this->daysback,
                "co" => "gb" // country code
            );

            return $sanitized_params;
        }

        public function job_detail() {

            // Andy NOTE: The $result was originally being passed in from the same object
            // Should not be calling JobApi methods directly
            // Made JobApi methods protected instead of public

            // Make indeed API request
            $output = $this->getJobList();

            $indeed_job_list = json_decode($output['response']);
            $this->total = $indeed_job_list->totalResults;
            $job_list_result = $indeed_job_list->results;
            $jobs_count = sizeof($job_list_result);

            // TO CONFIRM: if we even need to worry about job padding. Indeed
            // doesn't appear to pad.

            // New padding logic
            // Need to calculate with padded jobs indeed adds if over limit
            //if ($this->limit > 0 && $indeed_job_list->totalResults > $this->limit) {
            //    $this->total = ceil($this->total / $this->limit) * $this->limit;
            //}

            /*

            TO CONFIRM: Is this logic needed

            if ($this->limit != 0) {
                if ($indeed_job_list->totalResults < 15) {
                    $this->total = $jobs_count;
                } else {
                    $this->total = ceil($this->total / $this->limit) * $this->limit;
                }
            }
            */

            // Build job list array from result array
            for ($i=0; $i < $jobs_count; $i++) {

                $this->job_list[$i] = $this->_build_job_item($job_list_result[$i]);

            }
        }

        public function job_count() {


            // Make indeed API request
            $output = $this->getJobList();

            $indeed_job_list = json_decode($output['response']);

            // Need to check for network outage which
            // returns nothing.
            if(empty($indeed_job_list)) {
            	$this->total = 0;
            } else {
            	$this->total = $indeed_job_list->totalResults;
            }

            // TO CONFIRM: if we even need to worry about job padding. Indeed
            // doesn't appear to pad.

            // New padding logic
            // Need to calculate with padded jobs indeed adds if over limit
            //if ($this->limit > 0 && $indeed_job_list->totalResults > $this->limit) {
            //    $this->total = ceil($this->total / $this->limit) * $this->limit;
            //}

            /*

            CONFIRM: Is this logic needed

            if ($this->limit != 0) {
                if ($indeed_job_list->totalResults < 15) {
                    $this->total = $jobs_count;
                } else {
                    $this->total = ceil($this->total / $this->limit) * $this->limit;
                }
            }
            */
        }

        /**
         *
         * @param string $job_key
         * @return multitype
         */
        public function get_job_by_key($job_key) {

            $url = "http://api.indeed.com/ads/apigetjobs" .
                   "?publisher=" . $this->publisher_id .
                   "&v=2" .
                   "&format=json" .
                   "&jobkeys=" . $job_key;

            $resp = Util::curl_request($url);
            $data = json_decode($resp);
            $rst = $data->results;

            //Util::debug($data);

            if(isset($resp) && isset($data->results) && count($data->results) > 0) {
                $job = array_shift($rst);
                // returns job item array
                return $this->_build_job_item($job);
            } else {
                return false;
            }

            //return !empty($jobs) ? array_shift($jobs) : false;
        }

        /**
         * Return custom array for job result item
         *
         * @param string $item
         * @return array
         * */
        private function _build_job_item($item) {

            $arr = array();

            $arr["id"] = $item->jobkey;
            $arr["sponsored"] = $item->sponsored;
            $arr["title"] = $item->jobtitle;
            $arr["company"] = $item->company;
            $arr["city"] = $item->city;
            //$arr["country"] = $each_job->country;
            $arr["ago"] = $item->formattedRelativeTime;
            $arr["description"] = $item->snippet;
            $arr["link"] = $item->url;
            $arr["onmousedown"] = $item->onmousedown;
            $arr['feed'] = 'indeed';

            return $arr;

        }
    }

?>