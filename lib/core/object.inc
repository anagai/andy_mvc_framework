<?php
	// vim: ai ts=4 sts=4 et sw=4 tw=79

	/**
	 * Created on Oct 8, 2012
	 * Written by Andy Nagai
	 *
	 */

	class Object {

		protected $_state = array();
		protected $_error;
		static private $_inst = array();

	    // when this method called ensure returns same single
	    // instance of class. usage: $obj = SomeObj::getInstance()
	    static public function getInstance() {
	        // name of the instantiated class
	        $called = get_called_class();
	        if (!isset(self::$_inst[$called])) {
	            // use late static binding to instantiate invoked class
	            self::$_inst[$called] = new Static();
	        }
	        return self::$_inst[$called];
	    }

		/**
		 * Add to state array
		 *
		 */
		public function setState($name, $val) {

			//if(isset($this->_state[$name])) {
			if(is_object($val) || is_array($val) || is_resource($val)) {
				$this->_state[$name] =& $val;
			}	else {
				$this->_state[$name] = $val;
			}
			//} else {
		//		$this->_state = array_merge($this->_state, array($name => $val));
		//	}
		}

		/**
		 * Get from state array
		 *
		 */
		public function getState($name) {

			return isset($this->_state[$name]) ? $this->_state[$name] : NULL;
		}

		/**
		 * Get the error message
		 */
		public function getError() {
			return $this->_error;
		}

		/**
		 * Set error message for later retrieval
		 */
		public function setError($msg) {
			$this->_error = $msg;
		}

	}