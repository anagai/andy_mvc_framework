<?php

    /*
     * Created on Mar 27, 2012
     * Written By Sung Hwan Ahn / Andy Nagai
     */

     class JobAPI {

        protected $keyword;
        protected $location;

        protected $radius;
        protected $daysback;
        protected $start;  // indeed
        protected $page;   // juju
        protected $limit; // results per page

        protected $order = "relevance";
        protected $channel; // indeed_traffic_channel.ini

        protected $publisher_id;
        protected $url;

        protected $total;
        protected $job_list = array();

        public function __construct() {

            $this->radius = 25;
            $this->daysback = 7;
            $this->start = 0;
            $this->page = 1;
            $this->limit = 15;
        }

        public function __set($name, $value) {

            $this->$name = $value;
        }

        public function __get($name) {

            return $this->$name;
        }

        protected function getJobList($inpUrl='') {

            // Construct query string
            if(!empty($inpUrl)) {
                $url = $inpUrl;
            } else {
                $query_string = http_build_query( $this->buildParams() );
                $url = $this->url . $query_string;
            }

            $resp = Util::curlReq($url);

            // Get results from Indeed
            // Note: CURLOPT_XXX are _constants_
            /*
            $curl_options = array(
                CURLOPT_URL => $url,
                CURLOPT_HEADER => 0,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_TIMEOUT => 5,
                CURLOPT_ENCODING => 'gzip,deflate',
            );
            $ch = curl_init();
            curl_setopt_array( $ch, $curl_options );
            $output = curl_exec( $ch );
            $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            */
            // Only log for debugging
            if( FALSE ) {
                $forhttp = !empty($_SERVER['HTTP_X_FORWARDED_FOR'])
                    ? $_SERVER['HTTP_X_FORWARDED_FOR']
                    : '';
                Util::log_to_file('job_api_calls.log','Indeed request',
                    "http code: {$resp['status']} user ip: {$_SERVER['REMOTE_ADDR']} " .
                    "forwarded ip: {$forhttp} url: {$url}");
            }
            return $resp;
        }

        protected function getLocation($job_list, $states_arr) {

            $location = "International";

            // City, State
            if ($job_list["city"] != "" && $job_list["state"] != "") {
                $location = $job_list["city"] . ", " . $job_list["state"];
            }

            // State (full text)
            if ($job_list["city"] == "" && $job_list["state"] != "") {
                $location = getValueFromArrayKey($states_arr, $job_list["state"]);
            }

            // Country
            if ($job_list["city"] == "" && $job_list["state"] == "" && $job_list["country"] != "") {
                if ($job_list["country"] == "US") {
                    $location = "United States";
                } else {
                    $location = "International";
                }
            }

            return $location;
        }
    }
?>
