<?php
    // vim: ai ts=4 sts=4 et sw=4 tw=79

    /**
     * Created on Oct 8, 2012
     * Written By Sung Hwan Ahn / Andy Nagai
     *
     */

    class Model extends Object {

        protected $_db;
        protected $_table_name;
        protected $_table_class_name;

        /**
         * Search a table and return array of table objects
         *
         * @param str $sql
         * @param resource $remote_db
         * @return array
         */
        public function find_by_sql($sql = '', $remote_db = '') {

            if(!empty($remote_db)) {
                $db = $remote_db;
            } else {
                $db = $this->_db;
            }

            $result_set = $db->query($sql);
            $object_array = array();

            // Use php method to generate user object
            while ($row = $result_set->fetch_object($this->_table_class_name)) {
                $object_array[] = $row;
            }

            return $object_array;
        }

        public function find_all($type="") {

            if ($type == "active") {
                return $this->find_by_sql("SELECT * FROM " . $this->_table_name . " WHERE status = 'active'");
            } else if ($type == 1) {
                return $this->find_by_sql("SELECT * FROM " . $this->_table_name . " WHERE is_active = 1");
            } else {
                return $this->find_by_sql("SELECT * FROM " . $this->_table_name);
            }
        }

        public function find_all_limit($start, $limit) {

            return $this->find_by_sql("SELECT * FROM " . $this->_table_name . " LIMIT {$start} , {$limit}");
        }

        public function find_by_id($id=0) {

            $result_array = $this->find_by_sql("SELECT * FROM " . $this->_table_name . " WHERE id=" . $this->_db->escape_value($id) . " LIMIT 1");
            return !empty($result_array) ? array_shift($result_array) : false;
        }

        public function result_exists($sql) {

            if(!empty($remote_db)) {
                $db = $remote_db;
            } else {
                $db = $this->_db;
            }

            $result_set = $db->query($sql);

            //Util::debug($result_set);
            // Any sql errors are caught by database->confirm_query() when running
            // sql through $db->query()
            // If no result then num_rows returns 0
            return $result_set->num_rows > 0 ? true : false;

        }

        public function count_all() {

            $sql = "SELECT COUNT(*) FROM " . $this->_table_name;
            $result_set = $this->_db->query($sql);
            $row = $this->_db->fetch_array($result_set);
            return array_shift($row);
        }
    }