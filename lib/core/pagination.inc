<?php

	/*
	 * Created on Oct 17, 2012
	 * Written By Sung Hwan Ahn
	 * Observed By Andy Nagai
	 *
	 */

	// This is a helper class to make paginating records easy.

	class Pagination {

		public $current_page;
		public $per_page;
		public $total_count;
		public $page_list;
		public $traffic_type;
		
		public function __construct($page=1, $per_page=20, $total_count=0, $page_list=10, $traffic_type = '') {

			$this->current_page = (int) $page;
			$this->per_page = (int) $per_page;
			$this->total_count = (int) $total_count;
			$this->page_list = (int) $page_list;
			$this->traffic_type = strtolower($traffic_type);
		}

		public function offset() {

			// Assuming 20 items per page:
			// page 1 has an offset of 0 [1-20]  = (1-1) * 20
			// page 2 has an offset of 20 [21-40] = (2-1) * 20
			// in other words, page 2 starts with item 21
			return ($this->current_page - 1) * $this->per_page;
		}

		public function total_pages() {
			return ceil($this->total_count / $this->per_page);
		}

		public function page_begin_item() {
			return $this->offset() + 1;
		}

		public function page_end_item() {

			$page_end_item = $this->offset() + $this->per_page;

			if ($page_end_item > $this->total_count) {
				$page_end_item = $this->total_count;
			}

			return $page_end_item;
		}

		public function begin_page_number() {
			return (ceil($this->current_page / $this->page_list) - 1) * $this->page_list + 1;
		}

		public function end_page_number() {

			$end_page_number = $this->begin_page_number() + $this->page_list - 1;

			if ($end_page_number > $this->total_pages()) {
				$end_page_number = $this->total_pages();
			}

			return $end_page_number;
		}

		public function previous_page($n=1) {
			return $this->current_page - $n;
		}

		public function next_page($n=1) {
			return $this->current_page + $n;
		}

		public function has_previous_page($n=1) {
			return $this->previous_page($n) >= 1 ? true : false;
		}

		public function has_next_page($n=1) {
			return $this->next_page($n) <= $this->total_pages() ? true : false;
		}

		public function go_page($page) {

			$curURL = Util::cur_page_url();

			// No Previous Page Block
			if ($page == 0) {
				$curURL = "javascript:void(0);";
			}
			// No Next Page Block
			else if ($page > $this->total_pages()) {
				$curURL = "javascript:void(0);";
			}
			// Has No Page Parameter but has other parameter
			else if (strpos($curURL, "?") && !strpos($curURL, "?pg=")) {
				$curURL .= "?pg=" . $page;
			}
			// Has Page Parameter
			else {
				$curURL = substr($curURL, 0, strpos($curURL, "?pg="));
				$curURL .= "?pg=" . $page;
			}

			return $curURL;
		}

		public function display() {
			?>
            
			 
			<div class="pagination pagination-centered remove-bottom">
			    <!--<p><strong><?=number_format($this->page_begin_item())?> - <?=number_format($this->page_end_item())?> of <?=number_format($this->total_count)?></strong></p>-->	
				<ul>
					<?PHP
					if ($this->has_previous_page()) {
						echo "<li><a href='{$this->go_page($this->previous_page())}'>Prev</a></li>";
					}
					
					if($this->traffic_type != 'mobile') {
					
						if ($this->current_page > ($this->page_list-3)) {
							echo "<li><a href='{$this->go_page(1)}'>1</a></li>";
							echo "<li><a href='{$this->go_page(2)}'>2</a></li>";
							echo "<li><a href='javascript:void(0);'>...</a></li>";
							echo "<li><a href='{$this->go_page($this->previous_page(3))}'>{$this->previous_page(3)}</a></li>";
							echo "<li><a href='{$this->go_page($this->previous_page(2))}'>{$this->previous_page(2)}</a></li>";
							echo "<li><a href='{$this->go_page($this->previous_page(1))}'>{$this->previous_page(1)}</a></li>";
							echo "<li class='active'><a href='javascript:void(0);'>{$this->current_page}</a></li>";
							if ($this->has_next_page(1))
								echo "<li><a href='{$this->go_page($this->next_page(1))}'>{$this->next_page(1)}</a></li>";
							if ($this->has_next_page(2))
								echo "<li><a href='{$this->go_page($this->next_page(2))}'>{$this->next_page(2)}</a></li>";
							if ($this->has_next_page(3))
								echo "<li><a href='{$this->go_page($this->next_page(3))}'>{$this->next_page(3)}</a></li>";
						} else {
							for ($i=$this->begin_page_number(); $i<=$this->end_page_number(); $i++) {
								if ($i == $this->current_page) {
									echo "<li class='active'><a href='javascript:void(0);'>{$i}</a></li>";
								} else {
									echo "<li><a href='" . $this->go_page($i) . "'>{$i}</a></li>";
								}
							}
						}
					}

					if ($this->has_next_page()) {
						echo "<li><a href='{$this->go_page($this->next_page())}'>Next</a></li>";
					}
					?>
				</ul>
			</div>
			
            <?php
		}
	}
?>