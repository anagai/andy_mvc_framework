<?php

    /*
     * Created on Dec 10, 2012
     * Written By Sung Hwan Ahn
     *
     */

    // This class uses to connect RM Dashbaord

    class Access {

        protected $key_file = "dashboard_key.ini";

        public $da_hash;
        public $da_ip;
        public $office_ip;

        function __construct() {

            $ini_array = parse_ini_file(CONFIG_PATH . $this->key_file);

            $dashboard_key = $ini_array["key"];
            $dashboard_salt = $ini_array["salt"];
            $this->da_ip = $ini_array["ip"];
            $this->office_ip = $ini_array["office_ip"];

            $this->da_hash = sha1($dashboard_key . $dashboard_salt . $this->da_ip);
        }
    }
?>