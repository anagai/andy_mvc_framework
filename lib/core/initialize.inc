<?php
// vim: ai ts=4 sts=4 et sw=4 tw=79

// **********************************************
//                          Global Constants
// **********************************************

$GLOBALS['Start']=microtime(TRUE);

// Debug switch. True - display echo statements
define('DEBUG', false);

define( "SITE_DOMAIN", "www.jobungo.co.uk" );

// Offical Launch Date
defined('DATE_LAUNCH') ? null : define('DATE_LAUNCH', "2012-12-07 00:00:00");

// Prod server by default. to run local just edit db_settings.ini
define('PROD_SERVER', true);


// publicly-accessible location.
define( "SITE_ROOT", "http://" . SITE_DOMAIN . "/" );

// DIRECTORY_SEPARATOR is a PHP pre-defined constant
// (\ for windows, / for Unix)
define('DS', DIRECTORY_SEPARATOR);

// If you move this hierarchy to a new location, you should only have to change
// this variable, assuming all the directories remain in the same place.
define( "PROJECT_PATH", __DIR__ . DS . ".." . DS . ".." . DS );

// ---------------------------------------------------------------------------
// Generally, the directory hierarchy in this section should remain the same
// ---------------------------------------------------------------------------

// Configuration files, e.g. database settings, go here.
define( "CONFIG_PATH", PROJECT_PATH . "etc" . DS );

// Path to class libraries
define( "CLASS_PATH", PROJECT_PATH . "lib" . DS );
define( "CORE_PATH", CLASS_PATH . "core" . DS );
define( "CONTROLLER_PATH", CLASS_PATH . "controller" . DS );
define( "MODEL_PATH", CLASS_PATH . "model" . DS );
define( "VIEW_PATH", CLASS_PATH . "view" . DS );
define( "TABLE_PATH", CLASS_PATH . "table" . DS );
define( "TEMPLATE_PATH", CLASS_PATH . "template" . DS );
define( "JRP_VERSION_PATH", TEMPLATE_PATH . "result" . DS );
define( "VENDOR_PATH", CLASS_PATH . "vendor" . DS );
define( "HELPER_PATH", CLASS_PATH . "helper" . DS );
define( "CSV_PATH", PROJECT_PATH . "var" . DS . "csv" . DS );

// Path to job result page versionator
define( "JRP_PATH", TEMPLATE_PATH . "result" . DS );
define( "JRP_HEADER_PATH", JRP_PATH . "header" . DS );
define( "JRP_CSS_PATH", JRP_PATH . "css" . DS );
define( "JRP_BODY_PATH", JRP_PATH . "body" . DS );
define( "JRP_FOOTER_PATH", JRP_PATH . "footer" . DS );

// SQLite databases and the like go here.
define( "DATA_PATH", PROJECT_PATH . "var" . DS );

// Path to log directory
define( "LOG_PATH", PROJECT_PATH . "log" . DS );

// Default database setting for site. From /etc/db_settings.ini
// Erebus is production server. If script running from wilson then
// will use local connection since main db is on same machine.
// Need to use gethostname() when calling script from cli.
if(PROD_SERVER === true) {
    define('DEFAULT_DB_SETTING', 'jobungo_uk_rw_prod');
} else {
    define('DEFAULT_DB_SETTING', 'jobungo_uk_rw_dev');
}

// Site ID for JUK from Bacon server Sites table
define('SITE_ID', 5);

// Default geocode location city
define('DEFAULT_CITY', 'London');

// Default geocode location postcode
define('DEFAULT_POSTCODE', 'E11');


//****** SSH Server connections
// K2 server. Used to upload campaign csv files
define('K2_HOST', "164.40.141.52");
define('K2_USER', "andy");
define('K2_PASS', "r3stm3d1a");
define('K2_DIR', "/srv/ses_v2/");

define('ESAM_HOST', "176.58.120.39");
define('ESAM_USER', "sftp");
define('ESAM_PASS', "r3stm3d1a");

define('BLANC_HOST', "anagai-ses25.blanc.restdev.com");
define('BLANC_USER', "sftp");
define('BLANC_PASS', "r3stm3d1a");

define('SNOWDON_HOST', "snowdon.restdev.com");
define('SNOWDON_USER', "sftp");
define('SNOWDON_PASS', "_TVaDCY+?tTa5rxBQWtA+=4cauAD%8%7");

define('DIABLO_HOST', "173.1.196.164");
define('DIABLO_USER', "sung");
define('DIABLO_PASS', "r3stm3d1a");

//define('ESAM_DIR', "/srv/ses_v2/");

        // **********************************************
        //                          Include core and vendor libraries
        // **********************************************

// Autoloading classes will be done like this, at least until the PHP devs
// get their heads out of their collective asses and fix the behavior
// of spl_autoload_register().
// Ref: https://bugs.php.net/bug.php?id=49625
// anagai 10/9/12: autoload the table class files only so do not have to include a table
// file here for each new table
function __autoload( $class ) {
    require_once( TABLE_PATH . strtolower($class) . "_table.inc" );
}

//******* Load Core classes
// Load Object class. All classes inherit from Object class
require_once( CORE_PATH . "object.inc" );
// Load Database class
require_once( CORE_PATH . "database.inc" );
// Load Controller class. Each controller object inherits from Controller class.
require_once( CORE_PATH . "controller.inc" );
// Load Model class. Each model object inherits from Model class.
require_once( CORE_PATH . "model.inc" );
// Load Table class. Each table object inherits from Table class.
require_once( CORE_PATH . "table.inc" );
// Load Pagination class.
require_once( CORE_PATH . "pagination.inc" );
// Global utility functions. Called statically.
require_once( CORE_PATH . "util.inc" );
// JobApi class
require_once( CORE_PATH . "jobapi.inc" );
// Indeed class
require_once( CORE_PATH . "indeed.inc" );
// Browser detection class
require_once( CORE_PATH . "browser.inc" );
// Load Application class
require_once( CORE_PATH . "application.inc" );
// Adzuna class
require_once( CORE_PATH . "adzuna.inc" );
// JS Obfuscation Lib
require_once( CORE_PATH . "jsobf/JsObf.php" );


// Each page inherits the Page class

// Load max-mind geo ip classes
require_once(VENDOR_PATH."maxmind".DS."geoipcity.inc");
require_once(VENDOR_PATH."maxmind".DS."geoipregionvars.php");

// Do not start session of executed from command line
if(php_sapi_name() != "cli") {
    if(session_id() == '' ) {
        Util::my_session_start();
    }
}

// Load all database settings from db_settings.ini
Util::load_db_settings();

// **********************************************
//                          Global variables and initialization
// **********************************************

$app = new Application();
// Initialize default global database objects
$database = new Database(); // localhost
$db =& $database;
// Used to store remote database objects
$remote_db = array();
$pdo_db_connections = array();
$view = null;

// ******* Setup environment settings

// Setup any special initialization that is needed
$app->init();

// Load Balancer IP Address Patch
if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $_SERVER['REMOTE_ADDR'] = $_SERVER['HTTP_X_FORWARDED_FOR'];
}

// The timezone identifier (default is UTC)
// date_default_timezone_set('Europe/London');
date_default_timezone_set('Europe/London');
?>
