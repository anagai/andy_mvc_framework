<?php

    class Adzuna extends JobAPI {

        protected $publisher_id;
        protected $_jobUrl = "http://api.adzuna.com:80/v1/api/jobs/gb/search/1?app_id=be5957c8&app_key=ddcde3052a053d4377f55f9de3dbae75";

        // $publisher should be either "organic" or "sponsored"
        // $traffic should be among "organic", "email", or "mobile"
        public function __construct() {

            // Andy: Execute parent constructor
            parent::__construct();

        }

        public function job_detail() {

            //results_per_page=5&what=sales&where=birmingham&distance=25&max_days_old=3&sort_direction=down&sort_by=relevance

            $params = "&results_per_page={$this->limit}" .
                "&what=" . rawurlencode($this->keyword) .
                "&where=" . rawurlencode($this->location) .
                "&distance={$this->radius}" .
                "&max_days_old={$this->daysback}" .
                "&sort_by=relevance&sort_direction=down";

            $url = $this->_jobUrl.$params;

            $url .= "&exclude_sources=1084:3648:3649:3650:17272:17273:17493:17494:17495:" .
                "17496:17497:17498:17499:17500:17501:17502:17503:17504:17505:17506:17507:" .
                "17508:17509:17510:17511:17512:17513:17514:17515:17516:17517:17518:17519:17520:" .
                "17521:17522:17523:17524:17525:17526:17527:17528:17529:17530:17531:17532:17533:" .
                "17534:17535:17536:17537:17538:17539:17540:17541:17542:17543:17544:17545:17546:17547:" .
                "17548:17549:17550:17551:17552:17553:17554:17555:17556:21731:20918:20919:20920:20921";

            $output = $this->getJobList($url);

            $resultDec = json_decode($output['response'], true);

            $this->total = $resultDec['count'];

            $job_list_result = $resultDec['results'];

            if(preg_match("/^[4|5]\d+/", trim($output['status']), $match)) {
                switch($match[0]) {
                    case 400:
                    $msg = "Incorrect parameters";
                    break;
                    case 410:
                    $msg = "Authorization failed";
                    break;
                    case 404:
                    $msg = "Resource could not be found";
                    break;
                    case 500:
                    $msg = "Internal server error";
                    break;
                }
                Util::log_to_file('adzuna.log',"CURL ERROR","http: {$output['status']} msg: {$msg} url: {$url}");
            } else {
                if($this->total>0) {
                    // Build job list array from result array
                    foreach ($job_list_result as $key=>$job) {
                        $this->job_list[$key] = $this->_build_job_item($job);
                    }
                    //Util::log_to_file('adzuna.log',"request","http: {$output['status']} url: {$url} count: {$this->total}");
                } else {
                    Util::log_to_file('adzuna.log',"NO RESULT","kw: {$this->keyword} " .
                        "loc: {$this->location} radius: {$this->radius} daysback: {$this->daysback} url: {$url}");
                }
            }

            //return $this->job_list;
        }

       /**
         * Return custom array for job result item
         *
         * @param string $item
         * @return array
         * */
        private function _build_job_item($item) {

            $arr = array();

            $arr["id"] = $item['id'];
            $arr["sponsored"] = '';
            $arr["title"] = strip_tags($item['title']);
            $arr["company"] = $item['company']['display_name'];

            $arr["city"] = $item['location']['area'][count($item['location']['area'])-1];

            //$arr["country"] = $each_job->country;
            $arr["ago"] = Util::timeElaspedString($item['created']);
            $desc = Util::truncateWordsByLength(strip_tags($item['description']));
            $arr["description"] = $desc;
            $arr["link"] = $item['redirect_url'];
            $arr["onmousedown"] = '';
            $arr['feed'] = 'adzuna';

            return $arr;

        }
    }

?>