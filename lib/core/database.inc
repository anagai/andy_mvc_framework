<?php
	// vim: ai ts=4 sts=4 et sw=4 tw=79

	/*
	 * Created on Oct 8, 2012
	 * Written By Sung Hwan Ahn / Andy Nagai
	 *
	 */

 	class Database extends Object {

 		private $_connection;
 		private $_magic_quotes_active;
 		private $_real_escape_string_exists;

		private $_current_db;
		private $_current_server;

		private $_last_query;


 		function __construct($db_server = DB_SERVER, $db_name = DB_NAME) {

 			$this->open_connection($db_server, $db_name);
 			$this->_magic_quotes_active = get_magic_quotes_gpc();
 			$this->_real_escape_string_exists = function_exists("mysql_real_escape_string");
 		}

 		public function open_connection($db_server, $db_name) {

 			$db_server = strtolower($db_server);

 			if($db_server== 'localhost' || $db_server=='127.0.0.1') $db_server = DEFAULT_DB_SETTING;

 			$db_settings = Util::get_db_settings($db_server);

 			$db_host = $db_settings['host'];
 			$db_port = $db_settings['port'];
 			$db_user = $db_settings['username'];
 			$db_pass = $db_settings['password'];
 			$db_name  = isset($db_settings['database']) ? $db_settings['database'] : $db_name;
			//echo 'host: ' . $db_host . ' user: ' . $db_user . ' pass: ' . $db_pass . ' name: ' . $db_name . ' port: ' . $db_port . "\n";

			if(isset($db_settings['client-key'])) {
                $_db=mysqli_init();
                $_db->ssl_set($db_settings['client-key'],$db_settings['client-cert'], NULL, NULL, NULL);
                $_db->real_connect($db_host, $db_user, $db_pass, $db_name,$db_port,NULL,MYSQLI_CLIENT_SSL);
                $this->_connection = $_db;
            } else {

				$this->_connection = mysqli_connect($db_host, $db_user, $db_pass, $db_name, $db_port);
				if (!$this->_connection) {
					die ("Database connection failed: " . $this->_connection->error);
				}

			}

			$this->_current_db = $db_name;
			$this->_current_server = $db_server;
		}

		public function get_pdo_conn($db_server = '', $db_name = '') {
			$db_server = strtolower($db_server);

			//if($db_server== 'localhost' || $db_server=='127.0.0.1') $db_server = DEFAULT_DB_SETTING;

			$db_settings = Util::get_db_settings($db_server);

			$db_host = $db_settings['host'];
			$db_port = $db_settings['port'];
			$db_user = $db_settings['username'];
			$db_pass = $db_settings['password'];
			$db_name  = !empty($db_name)
				? $db_name
				: isset($db_settings['database'])
					? $db_settings['database']
					: '';
			//echo 'host: ' . $db_host . 'user: ' . $db_user . 'pass: ' . $db_pass . 'name: ' . $db_name . ' port: ' . $db_port . "\n";
			try
			{

				$client_cert = NULL;

		        // If there is a ssl cert then use it to connect
				if(isset($db_settings['client-cert'])) {
					$client_cert = array(PDO::MYSQL_ATTR_SSL_KEY=>$db_settings['client-key'],
		            PDO::MYSQL_ATTR_SSL_CERT=>$db_settings['client-cert']);
		        }

				$dbh = new PDO('mysql:host='. $db_host .';dbname=' . $db_name . ';port=' . $db_port, $db_user, $db_pass, $client_cert);

				$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);

				return $dbh;
			}
			catch(PDOException $exception)
			{
				Util::log_to_file("database.log", "PDO connection Failed", $exception->getMessage());
				return false;
			}

		}

		public function close_connection() {

			if (isset($this->_connection)) {
				mysqli_close($this->_connection);
				unset($this->_connection);
			}
		}

		public function current_db() {

			return 	$this->_current_db;
		}

		public function current_server() {

			return 	$this->_current_server;
		}

		public function query($sql) {
			$this->_last_query = $sql;
			$result = mysqli_query($this->_connection, $sql);
			Util::debug_sql($sql);
			$this->confirm_query($result);
			return $result;
		}

		public function prepare($sql) {
			$sth = $this->_connection->prepare($sql) or die("Database error: " . $this->connection->error);
			return $sth;
		}

		public function escape_value($value) {

			if ($this->_real_escape_string_exists) {
				// undo any magic quote effects so mysql_real_escape_string can do the work
				if ($this->_magic_quotes_active) {
					$value = stripslashes($value);
				}
				$value = mysqli_real_escape_string($this->_connection, $value);
			} else {
				// if magic quotes aren't already on then add slashes manually
				if (!$this->_magic_quotes_active) {
					$value = addslashes($value);
				}
				// if magic quotes are active, then the slashes already exist
			}
			return $value;
		}

		// "database-neutral" methods
		public function fetch_array($result_set) {
			return mysqli_fetch_assoc($result_set);
		}

		public function num_rows($result_set) {
			return mysqli_num_rows($result_set);
		}

		public function insert_id() {
			// get the last id inserted over the current db connection
			return mysqli_insert_id($this->_connection);
		}

		public function affected_rows() {
			return mysqli_affected_rows($this->_connection);
		}

		private function confirm_query($result) {

			if (!$result) {
				$output = "Database query failed: " . $this->_connection->error . "<hr />";
				$output .= "Last SQL query: " . $this->_last_query;
				Util::log_to_file("database.log", "Query Failed", $output );
				die("Sorry, something's amiss; we'll be looking into this shortly!");
			}
		}

		public function select_db($db_name = '') {

			$ret = false;

			if(!empty($db_name)) {
				$ret = mysqli_select_db($this->_connection, $db_name);
			}

			return $ret;
		}

		public function set_remote_database($server = '', $dbName = '') {
			global $remote_db;

			if(!empty($server) && !empty($dbName)) {
				if(isset($remote_db[$server])) {
					// If connections exists but different db then change
					if ($remote_db[$server]->current_db() != $dbName) {
						$remote_db[$server]->select_db($dbName);
					}
				} else {
					// Create new database object and add to remote_db array
					$remote_db[$server] = new Database($server, $dbName);
				}
			}

		}

		public function add_pdo_db_conn($server = '', $dbName = '') {
			global $pdo_db_connections;
			global $database;

			//if(!empty($server) && !empty($dbName)) {
				if(!isset($pdo_db_connections[$server])) {
					// Create new database object and add to remote_db array
					$pdo_db_connections[$server] = $database->get_pdo_conn($server, $dbName);
				}

			//}

		}

		public function pdo_execute(&$sth, $params) {

			if(!empty($params)) {
				$sth->execute($params);
			} else {
				$sth->execute();
			}

			if($sth->errorCode() > 0) {
				die("Sorry, something's amiss; we'll be looking into this shortly!");
				Util::log_to_file("database.log", "Query Failed", $sth->errorInfo());
				return false;
			} else {
				return $sth;
			}
		}

		// ********** Generic query helper methods

		/**
		 * This always returns value in first column and first record found
		 * The sql should only return single row. Will add limit 1 if not in sql.
		 *
		 * @param string $sql
		 * @return Ambigous <string, unknown>
		 */
		public function getValue($sql) {

			$val = '';

			//if(!preg_match('/\blimit 1\b/i', $sql, $match)) {
			//	$sql .= ' LIMIT 1';
			//}

			$rst = $this->query($sql);

			if($row = mysqli_fetch_row($rst)) {
				return $row[0];
			} else {
				return false;
			}

		}

		/**
		 * Return single row of data as an object
		 * Will add limit 1 if is not set in sql
		 *
		 */
		public function getRow($sql) {

			$val = '';

			if(!preg_match('/\blimit 1\b/i', $sql, $match)) {
				$sql .= ' LIMIT 1';
			}

			$rst = $this->query($sql);

			if($row = mysqli_fetch_object($rst)) {
				return $row;
			} else {
				return false;
			}

		}



	}

?>
