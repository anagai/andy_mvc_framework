<?php
	// vim: ai ts=4 sts=4 et sw=4 tw=79

	/**
	 * Created on Oct 9, 2012
	 * Written By Sung Hwan Ahn / Andy Nagai
	 *
	 * CRUD methods for Table objects
	 *
	 */

	class Table extends Object {

		protected $_table_name;
		protected $_db_fields;
		protected $_db;
		protected $_id_field;
		protected $_id_data_type;
		private static $_instance;

		/**
		 * Return singleton instance of table class.
		 * Used when using finder methods.
		 *
		 *
		 */
		public static function instance() {
			$cls = get_called_class();
			if(!isset(self::$_instance)) {
				self::$_instance = new $cls;
			}

			return self::$_instance;

			//	return new $cls;
		}

		//********* CRUD methods

		public function save() {

			// A new record won't have an id yet.
			return isset($this->id) ? $this->update() : $this->create();
		}

		public function create() {

			// Don't forget your SQL syntax and good habits:
			// - INSERT INTO table (key, key) VALUES ('value', 'value')
			// - single-quotes around all values
			// - escape all values to prevent SQL injection

			$attributes = $this->sanitized_attributes();

			$sql = "INSERT INTO " . $this->_table_name . " (";
			$sql .= join(", ", array_keys($attributes));
			$sql .= ") VALUES ('";
			$sql .= join("', '", array_values($attributes));
			$sql .= "')";

			if ($this->_db->query($sql)) {
				$this->id = $this->_db->insert_id();
				return true;
			} else {
				return false;
			}
		}

		public function update() {

			// Don't forget your SQL syntax and good habits:
			// - UPDATE table SET key='value', key='value' WHERE condition
			// - single-quotes around all values
			// - escape all values to prevent SQL injection

			$attributes = $this->sanitized_attributes();
			$attribute_pairs = array();
			foreach ($attributes as $key => $value) {
				$attribute_pairs[] = "{$key}='{$value}'";
			}

			// Get the id field to look up
			if (isset($this->_id_field) && property_exists($this, $this->_id_field)) {
				$id_field = $this->_id_field;
			} else {
				$id_field = 'id';
			}

			// Clean up the id value passed in
			$id_val = $this->_db->escape_value($this->$id_field);

			// Surround value with single quotes if id is string data type
			if($this->_id_data_type == 'str') {
				$id_val = "'" . $id_val . "'";
			}

			$sql = "UPDATE " . $this->_table_name . " SET ";
					$sql .= join(", ", $attribute_pairs);
					$sql .= " WHERE $id_field = " . $id_val;

			$this->_db->query($sql);

			return ($this->_db->affected_rows() == 1) ? true : false;
		}

		public function delete() {

			// Don't forget your SQL syntax and good habits:
			// - DELETE FROM table WHERE condition LIMIT 1
			// - escape all values to prevent SQL injection
			// - use LIMIT 1

			// Get the id field to look up
			if (isset($this->_id_field) && property_exists($this, $this->_id_field)) {
				$id_field = $this->_id_field;
			} else {
				$id_field = 'id';
			}

			// Clean up the id value passed in
			$id_val = $this->_db->escape_value($this->$id_field);

			// Surround value with single quotes if id is string data type
			if($this->_id_data_type == 'str') {
				$id_val = "'" . $id_val . "'";
			}

			$sql = "DELETE FROM " . $this->_table_name .
				" WHERE $id_field = " . $id_val .
				" LIMIT 1";

			$this->_db->query($sql);
			return ($this->_db->affected_rows() == 1) ? true : false;
		}

		//*********** Finder methods

		public function exists($id) {

			// Get the id field to look up
			if (isset($this->_id_field) && property_exists($this, $this->_id_field)) {
				$id_field = $this->_id_field;
			} else {
				$id_field = 'id';
			}

			// Clean up the id value passed in
			$id_val = $this->_db->escape_value($id);

			// Surround value with single quotes if id is string data type
			if($this->_id_data_type == 'str') {
				$id_val = "'" . $id_val . "'";
			}

			$sql = "SELECT $id_field FROM " . $this->_table_name .
				" WHERE $id_field = $id_val LIMIT 1";

			$result = $this->_db->query($sql);

			return $result->num_rows > 0 ? true : false;
		}

		public function find($id) {

			// Get the id field to look up
			if (isset($this->_id_field) && property_exists($this, $this->_id_field)) {
				$id_field = $this->_id_field;
			} else {
				$id_field = 'id';
			}

			// Clean up the id value passed in
			$id_val = $this->_db->escape_value($id);

			// Surround value with single quotes if id is string data type
			if($this->_id_data_type == 'str') {
				$id_val = "'" . $id_val . "'";
			}

			$sql = "SELECT * FROM " . $this->_table_name .
			" WHERE $id_field = $id_val LIMIT 1";

			$result = $this->_db->query($sql);

			if($result->num_rows > 0) {
				$table_class_name = str_replace('_', '', $this->_table_name);
				// This will return a table class with members from result record.
				$table_object = $result->fetch_object($table_class_name);
				return $table_object;

			} else {
				// Did not find record
				return false;
			}

		}

		// This will return all table rows as an result object
		public function find_all_resource() {
			$sql = "SELECT * FROM $this->_table_name";
			$result = $this->_db->query($sql);
			if($result->num_rows > 0) {
				return $result;
			} else {
				// There are no records
				return FALSE;
			}
		}

		//********* Helper methods

		public function set_id_field($field = '', $data_type = '') {
			$this->_id_field = $field;
			$this->_id_data_type = $data_type;
		}

		private function attributes() {

			// Return array of key/value pairs with the actual values
			// set for this object
			$attributes = array();

			// Get the public variables for this class. Need to be called outside of
			// context of current object to get only public variables.
			if(!isset($this->_db_fields)) {
				// calls get_class_vars() outside context of current scope
				$this->_db_fields = call_user_func('get_class_vars',get_class($this));
			}

			// CONCERN: If there is no matching field in table sql will break.
			// The fields defined in Table classes must match whats in the table
			foreach ($this->_db_fields as $field=>$val) {
				//if (property_exists($this, $field)) {
					$attributes[$field] = $this->$field;
				//}
			}

			return $attributes;
		}

		protected function sanitized_attributes() {

			$clean_attributes = array();

			// sanitize the values before submitting
			// NOTE: does not alter the actual value of each attribute
		    $attributes = $this->attributes();
			foreach ($attributes as $key => $value) {
				$clean_attributes[$key] = $this->_db->escape_value($value);
			}

			return $clean_attributes;
		}

	}
?>
