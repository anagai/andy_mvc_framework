<?php

    /*
     * Created on Jul 10, 2012
     * Written By Andy Nagai
     *
     */

    /**
    * Detect browser type and version
    * Usage:
    *
    * $Browser = new Browser;
    * echo "$Browser->name - $Browser->version - $Browser->agent";
    */

    class Browser {

        private $props    = array("version" => "0.0.0",
                                  "name" => "unknown",
                                  "agent" => "unknown",
                                  "device" => "unknown",
                                  "is_mobile" => false) ;

        public function __Construct() {

            $browsers = array("firefox", "msie", "opera", "chrome", "safari",
                              "mozilla", "seamonkey",    "konqueror", "netscape",
                              "gecko", "navigator", "mosaic", "lynx", "amaya",
                              "omniweb", "avant", "camino", "flock", "aol", "obigo");

            $this->agent = strtolower($_SERVER['HTTP_USER_AGENT']);
            foreach($browsers as $browser) {
                if (preg_match("#($browser)[/ ]?([0-9.]*)#", $this->agent, $match))    {
                    $this->name = $match[1] ;
                    $this->version = $match[2] ;
                    break ;
                }
            }

            $mobile_devices = "iphone|ipad|ipod|android|mobile|blackberry|mini|windows\sce|windows\sphone|iemobile|palm|symbian|psp|kindle|colornook|smartphone|pantech";
            $desktop_devices = "windows|mac|linux|CrOS";

            if (preg_match("#($mobile_devices)#", $this->agent, $match))    {
                $this->device = $match[0];
                $this->is_mobile = true;
            } elseif (preg_match("#($desktop_devices)#", $this->agent, $match))    {
                $this->device = $match[0];
                $this->is_mobile = false;
            }

        }

        public function __Get($name) {

            if (!array_key_exists($name, $this->props))    {
                die ("No such property or function $name") ;
            }
            return $this->props[$name] ;
        }
    }
?>