<?php
	// vim: ai ts=4 sts=4 et sw=4 tw=79

	/**
	 * Created on Oct 12, 2012
	 * Written By Andy Nagai
	 *
	 */

 	class GeocodeLocationExpansion extends Table {

		public $id;
		public $country;
		public $region;
		public $city;
		public $postal_code;

		public function __construct() {

			$this->_table_name = 'geocode_location_expansion';
			$this->_db = Util::get_dbo();

		}

 	}