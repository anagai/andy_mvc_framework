<?php
	// vim: ai ts=4 sts=4 et sw=4 tw=79
	
	/**
	 * Created on Oct 8, 2012
	 * Written By Sung Hwan Ahn / Andy Nagai
	 *
	 */

 	class CoregPartner extends Table {
 		
 		public $id;
 		public $partner;
 		public $source;
 		public $subcode;
 		public $status;
 		public $forward_to_leads;
 		public $is_paid;
 		public $date_created;
 		public $date_updated;
 		
		public function __construct() {
			$this->_table_name = 'coreg_partner';
					
			$this->_db = Util::get_dbo();
		}
		
 	}