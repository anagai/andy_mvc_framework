<?php
	// vim: ai ts=4 sts=4 et sw=4 tw=79
	
	/**
	 * Created on Oct 8, 2012
	 * Written By Sung Hwan Ahn / Andy Nagai
	 *
	 */

 	class Corrections extends Table {
 			
		public $id;
		public $bad;
		public $good;
		
		public function __construct() {
			$this->_table_name = 'corrections';
					
			$this->_db = Util::get_dbo();
		}
		
 	}