<?php
	// vim: ai ts=4 sts=4 et sw=4 tw=79
	
	/**
	 * Created on Dec 1, 2013
	 * Written By Andy Nagai
	 *
	 */
 	
 	class BlacklistedDomain extends Table {
 		 			
		public $id;
		public $site_id;
		public $site_name;
		public $domain_name;
		public $is_active;
		public $date_added;
		public $date_updated;		
		
		public function __construct() {

			$this->_table_name = 'blacklisted_domain';
			$this->_db = Util::get_bacon_db();
		}
	
	}
?>
