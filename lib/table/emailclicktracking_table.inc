<?php
	// vim: ai ts=4 sts=4 et sw=4 tw=79

	/**
	 * Created on Dec 3, 2012
	 * Written By Andy Nagai
	 *
	 */

 	class EmailClickTracking extends Table {

		public $id;
		public $email_version_id;
		public $result_version_id;
		public $user_id;
		public $partner;
		public $source;
		public $action;
		public $date_created;

		public function __construct() {
			$this->_table_name = 'email_click_tracking';

			$this->_db = Util::get_dbo();
		}

 	}