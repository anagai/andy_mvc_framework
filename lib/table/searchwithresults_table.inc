<?php
	// vim: ai ts=4 sts=4 et sw=4 tw=79

	/**
	 * Created on Oct 17, 2012
	 * Written By Andy Nagai
	 *
	 */

 	class SearchWithResults extends Table {

		public $id;
		public $user_id;
		public $email;
		public $query;
		public $location;
		public $channel;
		public $ip;
		public $page_number;
		public $provider_calls;
		public $search_engine;
		public $version_id;
		public $daysback;
		public $radius;
		public $user_agent;
		public $total_results;
		public $results_on_page;
		public $sub_id;
		public $partner;
		public $source;
		public $date_searched;

		public function __construct() {
			$this->_table_name = 'search_with_results';

			$this->_db = Util::get_dbo();
		}

 	}