<?php
    // vim: ai ts=4 sts=4 et sw=4 tw=79

    /**
     * Created on July 5, 2013
     * Written By Sung Hwan Ahn / Andy Nagai
     *
     */

    class SearchUniqueIp extends Table {

        public $id;
        public $search_date;
        public $version_id;
        public $page_number;
        public $ip;

        public function __construct() {
            $this->_table_name = 'search_unique_ip';

            $this->_db = Util::get_dbo();
        }

    }