<?php
	// vim: ai ts=4 sts=4 et sw=4 tw=79
	
	/**
	 * Created on Oct 8, 2012
	 * Written By Sung Hwan Ahn / Andy Nagai
	 *
	 */

 	class GeocodeLocation extends Table {
 			
		public $id;
		public $country;
		public $region;
		public $city;
		public $postal_code;
		public $latitude;
		public $longitude;
		public $metro_code;
		public $area_code;
		
		public function __construct() {
			$this->_table_name = 'geocode_location';
					
			$this->_db = Util::get_dbo();
		}
		
 	}