<?php
	// vim: ai ts=4 sts=4 et sw=4 tw=79

	/**
	 * Created on Oct 8, 2012
	 * Written By Sung Hwan Ahn / Andy Nagai
	 *
	 */

 	class NoResultDetails extends Table {

		public $id;
		public $user_id;
		public $email;
		public $query;
		public $location;
		public $channel;
		public $ip;
		public $page_number;
		public $provider_calls;
		public $search_engine;
		public $version_id;
		public $daysback;
		public $radius;
		public $user_agent;
		public $sub_id;
		public $partner;
		public $source;
		public $date_searched;

		public function __construct() {
			$this->_table_name = 'no_result_details';

			$this->_db = Util::get_dbo();
		}

 	}