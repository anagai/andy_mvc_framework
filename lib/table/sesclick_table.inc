<?php
    // vim: ai ts=4 sts=4 et sw=4 tw=79

    /**
     * Created on Dec 12, 2012
     * Written By Sung Hwan Ahn / Andy Nagai
     *
     */

     class SESClick extends Table {

        public $id;
        public $user_id;
        public $email;
        public $domain;
        public $partner;
        public $source;
        public $link_type;
        public $date_added;

        public function __construct() {

            $this->_table_name = 'click';
        }
     }