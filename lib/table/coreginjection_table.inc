<?php
    // vim: ai ts=4 sts=4 et sw=4 tw=79

    /**
     * Created on Dec 6, 2012
     * Written By Sung Hwan Ahn / Andy Nagai
     *
     */

     class CoregInjection extends Table {

        public $id;
        public $email;
        public $first_name;
        public $last_name;
        public $city;
        public $postal_code;
        public $partner;
        public $source;
        public $opt_in_ip;
        public $date_opt_in;
        public $accepted;
        public $rejection_reason;
        public $is_mailable;

        public function __construct() {
            //$this->_table_name = 'injection_' . $inject_date;
            $this->_db = Util::get_dbo();
        }
     }
