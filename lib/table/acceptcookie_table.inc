<?php
	// vim: ai ts=4 sts=4 et sw=4 tw=79

	/**
	 * Created on May 21, 2014
	 * Written By Sung Hwan Ahn
	 *
	 */

	class AcceptCookie extends Table {

		public $id;
		public $email;
		public $ip;
		public $device;
		public $browser;
		public $browser_version;
		public $user_agent;
		public $is_mobile;
		public $date_accepted;

		public function __construct() {
			$this->_table_name = 'accept_cookie';
			$this->_db = Util::get_dbo();
		}
	}
