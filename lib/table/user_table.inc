<?php
	// vim: ai ts=4 sts=4 et sw=4 tw=79

	/**
	 * Created on Oct 8, 2012
	 * Written By Sung Hwan Ahn / Andy Nagai
	 *
	 */

 	class User extends Table {

		public $id;
		public $email;
		public $email_domain;
		public $first_name;
		public $last_name;
		public $city;
		public $postal_code;
		public $partner;
		public $source;
		public $reg_key;
		public $subcode;
		public $sub_id;
		public $opt_in_ip;
		public $date_opt_in;
		public $is_mailable;
		public $has_engaged;
		public $coreg_id;
		public $nowrm;
		public $date_added;
		public $date_coreg_sent;
		public $date_last_mailed;

		public function __construct() {
			$this->_table_name = 'user';

			$this->_db = Util::get_dbo();
		}

		public function full_name() {

			if (isset($this->first_name) && isset($this->last_name)) {
				return $this->first_name . " " . $this->last_name;
			} else {
				return "";
			}
		}
 	}