<?php
    // vim: ai ts=4 sts=4 et sw=4 tw=79

    /**
     * Created on Dec 11, 2012
     * Written By Sung Hwan Ahn / Andy Nagai
     *
     */

     class Campaign extends Table {

        public $id;
        public $database_name;
        public $name;
        public $description;
        public $site_name;
        public $send_profile;
        public $number_actual_recipients;
        public $number_sent;
        public $number_unsent;
        public $number_bounced;
        public $number_delivered;
        public $number_opened;
        public $number_gross_clicked;
        public $number_unique_clicked;
        public $number_unsubscribed;
        public $number_complained;
        public $date_send_started;
        public $date_send_ended;

        public function __construct() {

            $this->_table_name = 'campaign';
            $this->_db = Util::get_ses_db();
        }
    }
?>
