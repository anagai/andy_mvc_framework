<?php
	// vim: ai ts=4 sts=4 et sw=4 tw=79
	
	/**
	 * Created on Nov 1, 2012
	 * Written By Sung Hwan Ahn / Andy Nagai
	 *
	 */
 	
 	class GlobalRemoval extends Table {
 		 			
		public $id;
		public $email;
		public $date_created;	
		
		public function __construct() {

			$this->_table_name = 'global_removal';
			$this->_db = Util::get_bacon_db();
		}
				
	}
?>
