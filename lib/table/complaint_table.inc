<?php
	// vim: ai ts=4 sts=4 et sw=4 tw=79
	
	/**
	 * Created on Nov 7, 2012
	 * Written By Andy Nagai
	 *
	 */
 	
 	class Complaint extends Table {
 		 			
		public $id;
		public $site_id;
		public $campaign_name;
		public $email_address;
		public $domain;
		public $partner;
		public $source;
		public $subcode;
		public $original_message_id;
		public $details;
		public $date_received;
		public $date_added;		
		
		public function __construct() {
			
			$this->_table_name = 'complaint';
			$this->_db = Util::get_bacon_db();
		}
	
	}
?>
