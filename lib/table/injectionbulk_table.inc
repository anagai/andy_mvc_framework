<?php
    // vim: ai ts=4 sts=4 et sw=4 tw=79

    /**
     * Created on Dec 13, 2012
     * Written By Sung Hwan Ahn / Andy Nagai
     *
     */

     class InjectionBulk extends Table {

        public $id;
        public $email;
        public $first_name;
        public $last_name;
        public $source;
        public $opt_in_ip;
        public $date_opt_in;
        public $city;
        public $region;
        public $postal_code;
        public $is_used;

        public function __construct() {
            $this->_table_name = 'injection_bulk_2';
            $this->_id_field = 'id';
            $this->_id_data_type = 'int';
            $this->_db = Util::get_dbo();
        }
     }
