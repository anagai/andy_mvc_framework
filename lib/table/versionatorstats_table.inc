<?php
    // vim: ai ts=4 sts=4 et sw=4 tw=79

    /**
     * Created on July 5, 2013
     * Written By Sung Hwan Ahn / Andy Nagai
     *
     */

    class VersionatorStats extends Table {

        public $id;
        public $search_date;
        public $version_id;
        public $page_number;
        public $unique_visitors;
        public $page_views;
        public $job_results;
        public $organic_clicks;
        public $sponsored_clicks;

        public function __construct() {
            $this->_table_name = 'versionator_stats';

            $this->_db = Util::get_dbo();
        }

    }