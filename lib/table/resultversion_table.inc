<?php
	// vim: ai ts=4 sts=4 et sw=4 tw=79
	
	/**
	 * Created on Oct 8, 2012
	 * Written By Sung Hwan Ahn / Andy Nagai
	 *
	 */

 	class ResultVersion extends Table {
 			
		public $id;
		public $alt_version_id;
		public $name;
		public $description;
		public $weight;
		public $header_file;
		public $css_file;
		public $body_file;
		public $footer_file;
		public $traffic_type;
		public $page_type;
		public $items_per_page;
		public $organic_per_page;
		public $sponsored_per_page;
		public $ad_per_page;
		public $result_pattern;
		public $date_created;
		public $date_updated;
		public $date_expire;
		public $modal_enabled;
		public $is_active;
				
		public function __construct() {
			$this->_table_name = 'result_version';
							
			$this->_db = Util::get_dbo();
		}
		
 	}