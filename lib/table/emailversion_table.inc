<?php
	// vim: ai ts=4 sts=4 et sw=4 tw=79
	
	/**
	 * Created on Oct 8, 2012
	 * Written By Sung Hwan Ahn / Andy Nagai
	 *
	 */

 	class EmailVersion extends Table {
 			
		public $id;
		public $type;
		public $name;
		public $campaign_name;
		public $description;
		public $weight;
		public $file_name;
		public $subject;
		public $date_created;
		public $date_updated;
		public $date_expire;
		public $is_active;
 		
		public function __construct() {
			$this->_table_name = 'email_version';
					
			$this->_db = Util::get_dbo();
		}
		
 	}