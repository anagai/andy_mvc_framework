<?php
    // vim: ai ts=4 sts=4 et sw=4 tw=79

    /**
     * Created on July 15, 2013
     * Written By Sung Hwan Ahn / Andy Nagai
     *
     */

    class PartnerSourceStats extends Table {

        public $search_date;
        public $partner;
        public $source;
        public $page_views;
        public $unique_views;
        public $job_clicks;
        public $unique_clicks;

        public function __construct() {
            $this->_table_name = 'partner_source_stats';

            $this->_db = Util::get_dbo();
        }

    }