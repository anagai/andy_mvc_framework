<?php
	// vim: ai ts=4 sts=4 et sw=4 tw=79

	/**
	 * Created on Oct 8, 2012
	 * Written By Sung Hwan Ahn / Andy Nagai
	 *
	 */

 	class JobClick extends Table {

		public $id;
		public $user_id;
		public $email;
		public $query;
		public $location;
		public $radius;
		public $daysback;
		public $page_number;
		public $ip;
		public $device;
		public $browser;
		public $browser_version;
		public $is_mobile;
		public $version_id;
		public $engine;
		public $job_key;
		public $job_title;
		public $job_company;
		public $job_location;
		public $position;
		public $age;
		public $user_agent;
		public $sub_id;
		public $partner;
		public $source;
		public $sponsored;
		public $date_clicked;

		public function __construct() {
			$this->_table_name = 'job_click';

			$this->_db = Util::get_dbo();
		}

 	}