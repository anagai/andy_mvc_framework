<?php
	// vim: ai ts=4 sts=4 et sw=4 tw=79

	/**
	 * Created on Oct 8, 2012
	 * Written By Sung Hwan Ahn / Andy Nagai
	 *
	 */

 	class Coreg extends Table {

 		public $id;
 		public $email;
 		public $email_domain;
 		public $first_name;
 		public $last_name;
 		public $city;
 		public $postal_code;
 		public $partner;
 		public $source;
 		public $subcode;
 		public $sub_id;
 		public $opt_in_ip;
 		public $date_opt_in;
 		public $accepted;
 		public $reject_reason_id;
 		public $rejection_reason;
 		public $is_mailable;
 		public $date_created;

		public function __construct() {
			$this->_table_name = 'coreg';

			$this->_db = Util::get_dbo();
		}

 	}