<?php
	// vim: ai ts=4 sts=4 et sw=4 tw=79

	/**
	 * Created on Oct 8, 2012
	 * Written By Sung Hwan Ahn / Andy Nagai
	 *
	 */

 	class Clickers90 extends Table {

		public $id;
		public $user_id;
		public $email;
		public $first_name;
		public $city;
		public $postal_code;
		public $keyword;
		public $is_keyword_valid;
		public $partner;
		public $source;
		public $date_opt_in;
		public $opt_in_ip;
		public $date_updated;
		public $datetime_updated;
		public $date_added;

		public function __construct() {
			$this->_table_name = 'clickers90';
			$this->_db = Util::get_dbo();
		}

 	}