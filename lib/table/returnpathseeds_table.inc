<?php
    // vim: ai ts=4 sts=4 et sw=4 tw=79

    /**
     * Created on July 1, 2013
     * Written By Sung Hwan Ahn / Andy Nagai
     *
     */

    class ReturnPathSeeds extends Table {

        public $id;
        public $first_name;
        public $last_name;
        public $email;
        public $city;
        public $postal_code;
        public $keyword_1;

        public function __construct() {
            $this->_table_name = 'return_path_seeds';

            $this->_db = Util::get_dbo();
        }

    }