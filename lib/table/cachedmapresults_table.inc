<?php
	// vim: ai ts=4 sts=4 et sw=4 tw=79
	
	/**
	 * Created on Nov 6, 2012
	 * Written By Andy Nagai
	 *
	 */
 	
 	class CachedMapResults extends Table {
 		 			
		public $id;
		public $job;
		public $city;
		public $state;
		public $zip;
		public $longitude;
		public $latitude;
		public $location;
		public $extra;
				
		public function __construct() {
			$this->_table_name = 'cached_map_results';
				
			$this->_db = Util::get_dbo();
		}
		
	}
?>
