<?php
    // vim: ai ts=4 sts=4 et sw=4 tw=79

    /**
     * Created on Dec 12, 2012
     * Written By Sung Hwan Ahn / Andy Nagai
     *
     */

     class SESUser extends Table {

        public $id;
        public $first_name;
        public $last_name;
        public $email;
        public $city;
        public $state;
        public $zip;
        public $partner;
        public $source;
        public $keyword_1;
        public $keyword_2;
        public $keyword_3;
        public $keyword_4;
        public $keyword_5;

        public function __construct() {

            $this->_table_name = 'user';
        }
     }